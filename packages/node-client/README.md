[![NPM](https://nodei.co/npm/jsonql-node-client.png?compact=true)](https://npmjs.org/package/jsonql-node-client)

# jsonql node client

This is the server to server client using node.js, all options identical to the browser client @jsonql/client

**BREAKING CHANGE IN 1.3.0**

*We have add a `namespaced` option to the configuration, the default is `false`
It means, all your resolver no longer need to be `client.query` or `client.mutation`.
For using with socket client, it will still be under `client.socket` that is due to the way how its
constructed, and add to the client.

If you pass `namespaced: true` then it will act exactly the same like before. Bear in mind, this option
will require you to have every single resolver to have a unique name, otherwise the name will get overwritten
by one another. Use it with caution.*

## Installation

```sh
$ npm i jsonql-node-client
```

You don't usually use this directly, this is part of the sub module for other jsonql modules.

---

Main website: [jsonql](http://jsonql.org)

UNLICENSED

[NEWBRAN LTD](http://newbran.ch) & [TO1SOURCE](http://to1source.cn)
