// the main interface generator
const { validateAsync } = require('jsonql-params-validator')
const { finalCatch } = require('jsonql-errors')
// display, injectToFn, chainFns, 
const { getDebug, isContract } = require('./utils')
const debug = getDebug('generator')

/**
 * @param {object} jsonqlInstance the init instance of jsonql client
 * @param {object} contract the static contract
 * @return {object} contract may be from server
 */
function getContract(jsonqlInstance, contract = {}) {
  if (isContract(contract)) {
    return Promise.resolve(contract)
  }
  return jsonqlInstance.getContract()
}

/**
 * generate authorisation specific methods
 * @param {string} name of method
 * @param {object} opts configuration
 * @param {object} contract to match
 * @return {function} for use
 */
function authMethodGenerator(jsonqlInstance, name, opts, contract) {
  return (...args) => {
    const params = contract.auth[name].params;
    const values = params.map((p, i) => args[i]);
    const header = args[params.length] || {};
    return validateAsync(args, params)
      .then(() => jsonqlInstance
          .query
          .apply(jsonqlInstance, [name, values, header])
      )
      .catch(finalCatch)
  }
}

/**
 * Actually generate the client
 * @TODO make it getter of the obj instead of just a prop
 * @param {object} jsonqlInstance
 * @param {object} config options
 * @param {object} contract
 * @return {object} constructed functions call
 */
function generator(jsonqlInstance, config, contract) {
  const { namespaced } = config
  let queryObj = {}
  let mutationObj = {}
  let obj = {}
  // process the query first
  for (let queryFn in contract.query) {
    // to keep it clean we use a param to id the auth method
    // const fn = (_contract.query[queryFn].auth === true) ? 'auth' : queryFn;
    // generate the query method
    queryObj[queryFn] = (...args) => {
      const params = contract.query[queryFn].params
      const _args = params.map((param, i) => args[i])
      debug('query', queryFn, _args)
      // the +1 parameter is the extra headers we want to pass
      const header = args[params.length] || {};
      // hook the validateAsync into the chain
      return validateAsync(_args, params)
        .then(() => jsonqlInstance
            .query
            .apply(jsonqlInstance, [queryFn, _args, header])
        )
        .catch(finalCatch)
    }
  }
  // process the mutation, the reason the mutation has a fixed number of parameters
  // there is only the payload, and conditions parameters
  // plus a header at the end
  for (let mutationFn in contract.mutation) {
    mutationObj[mutationFn] = (payload, conditions) => {
      const params = contract.mutation[mutationFn].params;
      const header = {};
      return validateAsync([payload, conditions], params)
        .then(() => jsonqlInstance
            .mutation
            .apply(jsonqlInstance, [mutationFn, payload, conditions, header])
        )
        .catch(finalCatch)
    }
  }
  // there is only one call `issuer` we want
  if (config.enableAuth && contract.auth) {
    const { loginHandlerName, logoutHandlerName } = config;
    // login
    if (contract.auth[loginHandlerName]) {
      // changing to the name the config specify
      obj[loginHandlerName] = (...args) => {
        const loginHandler = authMethodGenerator(jsonqlInstance, loginHandlerName, config, contract)
        return Reflect.apply(loginHandler, null, args)
          .then(jsonqlInstance.loginCallback.bind(jsonqlInstance))
      }
    }
    // logout
    if (contract.auth[logoutHandlerName]) {
      obj[logoutHandlerName] = (...args) => {
        // the logout will call after the http call completed
        const logoutHandler = authMethodGenerator(jsonqlInstance, logoutHandlerName, config, contract)
        return Reflect.apply(logoutHandler, null, args)
          .then(jsonqlInstance.logoutCallback.bind(jsonqlInstance))
      }
    } else {
      // @TODO need to better figure out how to apply the user supplied method here
      obj[logoutHandlerName] = () => {
        jsonqlInstance.logoutCallback()
      }
    }
    // we have to make this into a function if I want to
    // need to create a getter method for it otherwise
    // it's straight through pass to the instance property
    obj.userdata = () => jsonqlInstance.userdata
  }
  if (namespaced === true) {
    obj.query = queryObj
    obj.mutation = mutationObj
  } else {
    obj = Object.assign(obj, queryObj, mutationObj)
  }

  if (namespaced === true) {
    // create an alias to the helloWorld make it easier for testing
    obj.helloWorld = obj.query.helloWorld
  }
  // the eventEmitter getter - it should not be inside the auth!
  obj.eventEmitter = () => jsonqlInstance.eventEmitter
  // store this once again and export it
  obj.getJsonqlInstance = () => jsonqlInstance
  // output
  return obj
}
// export
module.exports = {
  getContract,
  generator
}
