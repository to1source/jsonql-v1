/**
 * Base class to inherit from
 * @TODO we should move all the main back here
 * because the main interface should be a generator based on the contract
 */
const { join } = require('path')
const fsx = require('fs-extra')
const merge = require('lodash.merge')
const { decodeToken } = require('jsonql-jwt')
// using node-cache from v1.1.0
// it becomes a class since 1.1.7
const JsonqlCacheClass = require('./jsonql-cache-class')
const { getRemoteContract } = require('./get-remote-contract')
const { getDebug } = require('../utils')
const debug = getDebug('jsonql-base-cls')

class JsonqlClientClass extends JsonqlCacheClass {

  constructor(config) {
    super(config)
    this.opts = config
    // @TODO we need to figure out an alternative way to store the session data
    // this.__store__ = {}; // @TODO remove this later
    this.__url__ = [this.opts.hostname, this.opts.jsonqlPath].join('/')
  }

  /**
   * we decode the token using the jwt-decode method when useJwt === true
   * @return {mixed} false if the useJwt options is not on
   */
  get userdata() {
    if (this.opts.useJwt) {
      let token = this.__getAuthToken()
      return token ? decodeToken(token) : false
    }
    return false
  }

  /**
   * computed property
   * @return {object} for the accepted header
   */
  get baseHeader() {
    const { contentType } = this.opts
    return {
      'Accept': contentType,
      'Content-Type': contentType
    }
  }

  /**
   * a phony intercepter for now until we replace it with flyio
   * @param {object} payload send to server
   * @param {mixed} body from server with the data key
   */
  __interceptor(payload, body) {
    const result = typeof body === 'string' ? JSON.parse(body) : body
    // const resolverName = Object.keys(payload)[0]
    
    return result
  }

  /**
   * @return {object} cache burster
   * @private
   */
  __cacheBurst() {
    return { _cb: Date.now() }
  }

  /**
   * @TODO need to change to use with our own new request method
   * @param {object} header to set
   * @return {object} combined header
   * @private
   */
  __createHeaders(header = {}) {
    const authHeader = this.__getAuthHeader()
    let _header = merge({}, header, authHeader)
    debug('sending header', _header)

    return _header
  }

  /**
   * Check if the contract is expired or not
   * @TODO this is not enable until we know if we need it
   * @param {object} contract what it said
   * @return {boolean} false on OK
   */
  __isContractExpired(contract) {
    return false
    /*
    if (contract) {
      const { expired } = contract;
      if (!expired) {
        return false;
      }
      let now = Date.now()
      if (now >= expired) {
        return true;
      }
    }
    return false;
    */
  }

  /**
   * Return the stored contract if any
   * @TODO need an option to compare the time and check with the server
   * @return {object} return a promise to resolve it
   */
  __readContract() {
    const { contract } = this.opts;
    // first check if we have a contract already in memory
    if (contract && typeof contract === 'object' && (contract.query || contract.mutation)) {
      debug('return contract from opts')
      this.setter('contract', contract)
      if (!this.__isContractExpired(contract)) {
        return Promise.resolve(contract)
      }
    }
    // new in v1.1.7 read from cache
    let cacheContract = this.getter('contract')
    if (cacheContract) {
      if (this.__isContractExpired(cacheContract)) {
        this.setter('contract', false) // just clear it out
      } else {
        debug(`get contract from cache`, cacheContract)
        return Promise.resolve(cacheContract)
      }
    }
    // check if there is a contract store locally
    const file = join(this.opts.contractDir, this.opts.contractFileName)
    if (fsx.existsSync(file)) {
      let fc = fsx.readJsonSync(file)
      debug('return contract from file: ', file)
      if (!this.__isContractExpired(fc)) {
        this.setter('contract', fc) // cache it
        return Promise.resolve(fc)
      }
    }
    // now grab it from remote
    let authHeader = {}
    if (this.opts.contractKey !== false) {
      authHeader = {[this.opts.contractKeyName]: this.opts.contractKey}
    }
    debug(`contract out: ${this.opts.contractDir}`)
    // use the remote method to get it
    return getRemoteContract(Object.assign({
      remote: this.__url__,
      out: this.opts.contractDir
    }, { authHeader }), contract => {
      this.setter('contract', contract)
    })
  }
}

// export
module.exports = JsonqlClientClass
