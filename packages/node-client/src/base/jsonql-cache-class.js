// using node-cache for storage
// @2019-08-25 we use the hostname as the base key then store the
// data associate with this particular object

// @TODO we should take this class out and allow to use a third party cacher method

const NodeCache = require('node-cache')
const merge = require('lodash.merge')

const { getConfigValue } = require('jsonql-utils')
const { getDebug } = require('../utils')

const debug = getDebug('jsonql-cache-class')

class JsonqlCacheClass {
  constructor(config) {
    this.nc = new NodeCache()
    this.ncBaseKey = config.hostname;
    debug('ncBaseKey', this.ncBaseKey)
    // should init the base cache object
    this.baseCacheObj = {}; // @TODO should we use a Set instead?
  }

  /**
   * base cache object getter
   * @return {object} the base cache object
   */
  get baseCacheObj() {
    const obj = this.nc.get(this.ncBaseKey)
    // @BUG this is always undefined?
    debug('get baseCacheObj', obj)
    return obj;
  }

  /**
   * base cache object setter
   * @param {object} values it should be an merge object
   * @return {void}
   */
  set baseCacheObj(values) {
    debug('set baseCacheObj', values)
    this.nc.set(this.ncBaseKey, values)
  }

  /**
   * the value getter
   * @param {*} key any qualify object
   * @return {*} false when not found
   */
  getter(key) {
    return getConfigValue(key, this.baseCacheObj) || false;
  }

  /**
   * store the value, id by key
   * @param {*} key any qualify key
   * @param {*} value any storable value
   * @return {void}
   */
  setter(key, value) {
    debug('setter', key, value)
    let obj = this.baseCacheObj;
    this.baseCacheObj = merge(obj, {[key]: value})
  }
}

module.exports = JsonqlCacheClass
