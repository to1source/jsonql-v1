// we extend the nbEventService before we pass it over
const NBEventService = require('nb-event-service')
const { getDebug } = require('../utils')
const debug = getDebug('jsonql-evt-srv')

class JsonqlNodeClientEventService extends NBEventService {

  constructor() {
    super({ logger: debug })
  }
  // overwrite
  get name() {
    return 'jsonql-node-client-event-service'
  }
}

module.exports = { JsonqlNodeClientEventService }
