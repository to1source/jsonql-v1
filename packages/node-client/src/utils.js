const { inspect } = require('util')
const debug = require('debug')

const {
  isObjectHasKey,
  resultHandler,
  injectToFn,
  chainFns,
  isContract
} = require('jsonql-utils')

const MODULE_NAME = 'jsonql-node-client'

/**
 * Helper to display more of the debug info
 * @param {*} data what to show
 * @param {boolean} full or not
 * @return {*} formatted data to display
 */
const display = (data, full = false) => (
  full ? inspect(data, false, null, true) : (data ? data.toString() : false)
)

/**
 * @param {string} name of this debug instance
 * @return {function} debug handler 
 */
const getDebug = name => debug(MODULE_NAME).extend(name)

// export
module.exports = {
  display,
  isObjectHasKey,
  resultHandler,
  injectToFn,
  chainFns,
  isContract,
  getDebug
}
