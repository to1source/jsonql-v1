const contractApi = require('jsonql-contract')
const fsx = require('fs-extra')
const { join } = require('path')

module.exports = (clean = false) => {
  const outDir = join(__dirname, 'contract', 'tmp' , 'server')
  const contractFile = join(outDir, 'contract.json')
  if (fsx.existsSync(contractFile)) {
    return Promise.resolve(fsx.readJsonSync(contractFile))
  }
  /*
  just keep the damn thing
  if (clean === true) {
    return fsx.removeSync(join(outDir, 'contract.json'))
  }
  */
  return contractApi({
    inDir: join(__dirname, 'resolvers'),
    outDir
  }).then(() => fsx.readJsonSync(join(outDir, 'contract.json')))
}
