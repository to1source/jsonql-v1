const debug = require('debug')('jsonql-node-client:test:customValidator')

/**
 * result
 * @param {*} userdata from last processor
 * @return {*} userdata
 */
module.exports = function customValidator(userdata) {
  debug('customValidator called', userdata)
  return userdata;
}
