const debug = require('debug')('jsonql-node-client:test:login')
const { loginToken, token } = require('../../options')

/**
 * The login function
 * @param {string} credential to check
 * @return {boolean|string} string on ok
 */
module.exports = function login(credential) {
  debug(`I got [${credential}] compare to ${loginToken}`)
  // @NOTE we expect the return result is an object!
  return credential === loginToken ? token : false;
}
