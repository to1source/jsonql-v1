const debug = require('debug')('jsonql-node-client:test:issuer')
const { token } = require('../../options')

/**
 * This is secondary valdiator provide by the developer
 * @param {string} userToken
 * @return {object} IT MUST RETURN AN OBJECT!!!
 */
module.exports = function validator(userToken) {
  return token === userToken ? {userId: 1} : 0;
}
