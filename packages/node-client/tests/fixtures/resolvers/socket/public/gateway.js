// new public socket interface

/**
 * tell the gateway what you want and wait for the broadcast
 * @param {string} msg a message
 * @return {string} the first will just say, hang on
 */
module.exports = function gateway(msg) {


  return `hang on ... got your message ${msg}`;
}
