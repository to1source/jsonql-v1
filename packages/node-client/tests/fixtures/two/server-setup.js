const server = require('server-io-core')
const jsonqlKoa = require('../jsonql-koa')
const { join } = require('path')
const dummy = join(__dirname, 'dummy')

module.exports = (port, resolverDir, contractDir) => {
  const name = `server${port}`
  const { app, stop } = server({
    webroot: dummy,
    open: false,
    debugger: false,
    reload: false,
    socket: false,
    port: port,
    middlewares: [
      jsonqlKoa({
        name,
        resolverDir,
        contractDir
      })
    ]
  })
  return {
    name,
    [name]: {app, stop}
  }
}
