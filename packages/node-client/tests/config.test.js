// just testing the config
const test = require('ava')
const { join } = require('path')
const { checkConfiguration, checkSocketClientType } = require('../src')
const { 
  JS_WS_NAME, 
  SOCKET_TYPE_CLIENT_ALIAS,
  SOCKET_TYPE_KEY 
} = require('jsonql-constants')
const debug = require('debug')('jsonql-node-client:test:config')

let contractDir = join(__dirname, 'fixtures', 'contract', 'tmp')
let hostname = 'http://localhost:8899'
let contractKey = 'what-wrong-with-you-mf'


test('It should not throw Error', async t => {

  const opts = await checkConfiguration({
    hostname,
    contractDir,
    contractKey,
    optionNotInList: 'whatever'
  })

  t.is(opts.contractKey, contractKey)
  t.falsy(opts.optionNotInList)

})

test(`It should able to take the server type or using the alias`, t => {

  const config1 = {[SOCKET_TYPE_KEY]: JS_WS_NAME}
  const result1 = checkSocketClientType(config1)
  
  t.deepEqual(result1, config1)

  const config2 = {[SOCKET_TYPE_CLIENT_ALIAS]: JS_WS_NAME}
  const result2 = checkSocketClientType(config2)

  t.deepEqual(result2, config1)

  const config3 = {serverType: null}

  t.throws(() => {
    const result3 = checkSocketClientType(config3)
    debug(result3)
  }, null, `Expect to throw an error because the type was incorrect`)


  const config4 = {whatever: 'whatever', 'appDir': 'app', 'contractDir': 'some/path/to/contract'}

  const result4 = checkSocketClientType(config4)

  t.is(result4[SOCKET_TYPE_KEY], null)

})
