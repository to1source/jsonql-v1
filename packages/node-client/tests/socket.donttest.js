// testing the socket client
const test = require('ava')
const debug = require('debug')('jsonql-node-client:test:socket')
const serverWithSocket = require('./fixtures/server-with-socket')
const nodeClient = require('../index')

const { loginToken, token } = require('./fixtures/options')


test.before(async t => {
  const { stop, config, port } = await serverWithSocket()
  const opts = Object.assign({
    hostname: `http://localhost:${port}`,
    serverType: 'ws'
  }, config)
  t.context.stop = stop;
  t.context.client = await nodeClient(opts)
})

test.after(t => {
  t.context.stop()
})

test(`It should able to connect the server via http`, async t => {
  const result = await t.context.client.helloWorld()
  t.is('Hello world!', result)
})

test.cb(`It should able to connect to the public socket interface`, t => {
  t.plan(1)
  const socket = t.context.client.socket
  // debug(t.context.client.socket)
  socket.gateway('yo')
  socket.gateway.onResult = function(result) {
    debug(result)
    t.truthy(result)
    t.end()
  }
})

test.cb(`It should able to connect to the private interface after login`, t => {
  t.plan(4)
  const client = t.context.client
  const socket = client.socket
  // first try to connect to the private interface and should not be able to connect
  socket.secretChat('calling')
    .catch(err => {
      debug(err)
      t.pass()
      // now try to call the auth
      client.login(loginToken)
        .then(result => {
          debug('login result', result)
        })
    })
  // this event will get fire first
  socket.secretChat.onError = function(err) {
    debug(err)
    t.pass()
  }

  socket.onLogin = function(result) {
    t.pass()
    // try again
    socket.secretChat('try again')
    // @TODO need to check why the promise is not resolved
    socket.secretChat.onResult = function(result) {
      t.is(result, 'got it')
      t.end()
    }
  }


})
