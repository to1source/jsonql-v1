// all the require methods
const { join, extname } = require('path')
const fsx = require('fs-extra')
const _ = require('lodash')
const {
  DEFAULT_CONTRACT_FILE_NAME,
  PUBLIC_CONTRACT_FILE_NAME,
  RESOLVER_TYPES,
  AUTH_TYPE
} = require('jsonql-constants')
const publicDir = process.env.PUBLIC_DIR || join(__dirname, 'public')
const assets = ['/jsonql-web-console.css']
const resolverTypes = [AUTH_TYPE].concat(RESOLVER_TYPES)
const debug = require('debug')('jsonql-web-console:utils')

/**
 * Just to read the contract json
 * search for the contract file
 * should we check if there is a public-contract first
 * if it's not found then we check if there is a contract.json?
 * also in the case of just start up, it might happen a fight situation
 * @param {object} config configuration
 */
const getContractJson = config => {
  const searchDir = [
    join(config.contractDir, PUBLIC_CONTRACT_FILE_NAME),
    join(config.contractDir, DEFAULT_CONTRACT_FILE_NAME)
  ]
  return _(searchDir)
    .chain()
    .filter(dir => fsx.existsSync(dir))
    .thru(dirs => {
      debug('dirs', dirs)
      if (dirs.length) {
        return fsx.readJsonSync(dirs[0])
      }
      return false;
    })
    .value()
}

/**
 * check if the path matches our assets
 * @param {object} ctx koa context
 * @return {boolean|string} false when its not
 */
const isWebConsoleAssets = ctx => {
  return _(assets)
    .chain()
    .filter(path => ctx.path === path)
    .thru(path => {
      if (path.length) {
        return path[0]
      }
      return false;
    })
    .value()
}

/**
 * Get document (string) byte length for use in header
 * @param {string} doc to calculate
 * @return {number} length
 */
const getDocLen = doc => Buffer.byteLength(doc, 'utf8')

/**
 * Pre-render the block data and add to the json for final output
 * @param {object} json the contract data
 * @return {object} the transformed version
 */
const transformTplData = json => {
  let outJson = json;
  if (outJson.timestamp) {
    debug('timestamp', outJson.timestamp)
    outJson.timestamp = timeStamp2String(parseInt(outJson.timestamp))
  }

  const tpl = fsx.readFileSync(join(publicDir, 'templates', 'block.tpl.html'))
  return resolverTypes.map(resolverType => {
    let typeName = resolverType + 'Block';
    let data = json[resolverType]
    debug(resolverType, data)
    if (data && !_.isEqual(data, {})) {
      return {
        [typeName]: _.template(tpl)({
          resolverType,
          resolverData: data
        })
      }
    }
    return {[typeName]: false, [resolverType]: false}
  })
  .reduce(_.merge, outJson)
}

/**
 * @param {string} content output string from file
 * @param {object} json contract
 */
const renderTpl = (content, json) => {
  return _.template(content)(transformTplData(json))
}

/**
 * Render out the content
 * @param {object} ctx koa context
 * @param {string} file to serve up
 * @return {void} nothing should stop here
 */
const render = (ctx, file, json = false) => {
  let content = fsx.readFileSync(join(publicDir, file))
  if (json !== false) {
    content = renderTpl(content, json)
  }
  let ext = extname(file)
  if (ext === '.css') {
    ctx.type = 'text/css';
  } else if (ext === '.js') {
    ctx.type = 'application/javascript';
  } else {
    ctx.type = 'text/html';
  }

  ctx.size = getDocLen(content)
  // should we try to get the contentType?
  // ctx.type = opts.contentType;
  ctx.status = 200;
  ctx.body = content;
}

// from https://blog.csdn.net/xxm524/article/details/47373089
function timeStamp2String(time) {
  var datetime = new Date();
      datetime.setTime(time);
  var year = datetime.getFullYear();
  var month = datetime.getMonth() + 1;
  var date = datetime.getDate();
  var hour = datetime.getHours();
  var minute = datetime.getMinutes();
  var second = datetime.getSeconds();
  var mseconds = datetime.getMilliseconds();
  return year + "-" + month + "-" + date+" "+hour+":"+minute+":"+second+"."+mseconds;
}


module.exports = {
  getContractJson,
  isWebConsoleAssets,
  getDocLen,
  transformTplData,
  renderTpl,
  render
}
