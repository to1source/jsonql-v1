// The main interface to the middleware
const {
  getContractJson,
  isWebConsoleAssets,
  getDocLen,
  render
} = require('./utils')
const debug = require('debug')('jsonql-web-console:middleware')
let contractJson = null;
/**
 * @param {object} config configuration
 * @param {function} isJsonqlConsoleUrl to check if it's the top calling methods
 */
module.exports = function jsonqlWebConsole(config, isJsonqlConsoleUrl) {
  return async function(ctx, next) {
    if (isJsonqlConsoleUrl(ctx, config)) {
      let error = false;
      if (!contractJson) {
        // we should try to check if ctx contain the contract at this point as well
        if (ctx.state && ctx.state.jsonql) {
          const { contract } = ctx.state.jsonql;
          debug('ctx.state.jsonql.contract', contract)
          if (contract) {
            contractJson = contract;
          } else {
            error = true; // just throw it
          }
        }

        if (!contractJson) {
          contractJson = getContractJson(config)
          debug('contractJson', contractJson)
          error = !!contractJson
        }
      }

      if (error !== false) {
        // render the error page
        return render(ctx, 'error.html')
      }

      return render(ctx, 'index.html', contractJson)
    } else {
      const asset = isWebConsoleAssets(ctx, config)
      if (asset !== false) {
        return render(ctx, asset)
      }
    }
    await next()
  }
}
