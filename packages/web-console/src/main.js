import Vue from 'vue'
import App from './App.vue'
// import AsyncComputed from 'vue-async-computed'
// import store from './store'
// Vue.use(AsyncComputed)
Vue.config.productionTip = false

import Jsonql from './plugin/jsonql'

Vue.use(Jsonql)

new Vue({
  dummy: {msg: async () => {
    return await new Promise(resolver => {
      setTimeout(() => {
        resolver('This is the client')
      },300)
    })
  }},
  // store,
  render: h => h(App),
}).$mount('#app')
