
// import contract from './public-contract.json'
import 'core-js'
import jsonqlClient from './static.js'

console.log(jsonqlClient)
/*
export default jsonqlClient({
  // hostname: 'http://localhost:8081',
  keepContract: false,
  showContractDesc: true
})
*/

let Vue;
let ctn = 0
function jsonqlInit() {
  const options = this.$options;
  console.info('inside the jsoonqlInit', ++ctn, options)
  /*
  if (options.dummy) {
    console.info(options.dummy.msg)
  } else if (options.parent.dummy) {
    console.info('parent', options.parent.dummy.msg)
  }
  */
}

export default {
  install: function(_Vue) {
    if (Vue && _Vue === Vue) {
      console.info(`Vue.mixin already called`)
      return
    }
    Vue = _Vue
    Vue.mixin({ beforeCreate: jsonqlInit })
  }
}
