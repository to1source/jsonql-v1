// look at the vuex source code and it just apply the $store to Vue using the mixin ==> beforeCreate: initStore
// so we should able to use the same way
// Now the thing is - we will have two version
// 1. A plain jsonql for Vue using the mixin method --> vue-jsonql
// 2. Another plugin(s) for Vuex that becomes a module --> vuex-jsonql-module
// Another additonal is we should check out how to supply async data to the template using the vue-async-computed?
