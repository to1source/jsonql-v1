# jsonql-web-console

This is not for standalone use, please do not install it directly.

This is for [jsonql-koa](https://npmjs.com/package/jsonql-koa)

When you pass the `enableWebConsole: true` to the start up option,
and open (assuming you are using the default options) `http://localhost:8000/jsonql`
then you will see a pretty documentation using the `contract.json` file

---

ISC

Joel Chu (c) 2019
