// run this in browser and check if the render actually work
const { join } = require('path')
const serverIoCore = require('server-io-core')
const baseDir = join(__dirname, '..', '..')
const jsonqlWebConsole = require('../../koa')
const debug = require('debug')('jsonql-web-console:test:browser')

serverIoCore({
  webroot: [
    join(baseDir, 'public')
  ],
  middlewares: [
    jsonqlWebConsole({
      contractDir: join(__dirname, 'contract'),
      jsonqlPath: '/jsonql'
    }, function(ctx, config) {

      return ctx.method === 'GET' && ctx.path === config.jsonqlPath
    })
  ]
})
