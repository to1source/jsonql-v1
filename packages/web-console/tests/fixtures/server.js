const serverIo = require('server-io-core')
const { jsonqlKoa } = require('jsonql-koa')
const { join } = require('path')
const baseDir = join(__dirname, '..', '..')

serverIo({
    port: 8082,
    open: false,
    reload: false,
    debugger: false,
    webroot: [
      join(baseDir, 'node_modules'),
      join(baseDir, 'public')
    ],
    middlewares: [
      jsonqlKoa({
        jsType: 'es',
        resolverDir: join(__dirname, 'resolvers'),
        contractDir: join(__dirname, 'contract'),
        keysDir: join(__dirname, 'keys')
      })
    ]
})
