
/**
 * @param {number} id a phony id
 * @return {object} an object with whatever
 *
 */
export default function defaultCall(id) {

  return {id: id}
}
