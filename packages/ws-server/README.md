# jsonql-ws-server

> Setup WebSocket server for the jsonql to run on the same host, automatic generate public / private channel using contract

This is not mean to use as standalone server (although you can)

This is included as optional dependencies as one of the following module:

- [@jsonql/koa](https://npmjs.com/package/@jsonql/koa) complete Node server side setup with Koa
- @jsonql/express (in development)

Please check our main documentation site at [jsonql.org](https://jsonql.js.org)
for further information

## Note

The module can work as a standalone WebSocket server.

```js
const http = require('http')
const fsx = require('fs-extra')
const { jsonqlWsServer } = require('jsonql-ws-server')
const server = http.createServer()
// when using this as standalone, you need to include the contract from somewhere
const config = {
  contract: fsx.readJsonSync('path/to/public.json')
}
// init
jsonqlWsServer(config, server)
  .then(() => {
    // start up the server
    server.listen(PORT)
  })
```

But this is not recommended.

---

[NEWBRAN LTD UK](https://newbran.ch)

[TO1SOURCE CHINA](https://to1source.cn)
