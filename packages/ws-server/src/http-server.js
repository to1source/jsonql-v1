// this is for standalone mode then we just create a server 
const http = require('http')

/**
 * Create a http server for standalone server 
 * @param {number} port port number to listen to
 * @return {object} http server instance
 */
function createHttpServer(port = false) {
  const server = http.createServer(function(req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' })
    res.write('request successfully proxied!' + '\n' + JSON.stringify(req.headers, true, 2))
    res.end()
  })
  
  if (port && !isNaN(parseInt(port))) {
    server.listen(port)
  }

  return server 
}

module.exports = { createHttpServer }