// this is ws specific so we put them here
const url = require('url')
const colors = require('colors/safe')
// const { groupByNamespace } = require('jsonql-utils')
const { WebSocket, getDebug } = require('./modules')
/**
 * Create a rainbow debug
 */
function debug(str, ...args) {
  let db = getDebug('internal')
  Reflect.apply(db, null, [colors.rainbow(str+'')].concat(args))
}

/**
 * @param {object} req http.server request object
 * @return {string} the strip slash of pathname
 */
const getPath = req => {
  const { pathname } = url.parse(req.url)
  // debug('pathname', pathname, pathname.substring(0, 1), pathname.substring(1, pathname.length));
  return pathname.substring(0, 1) === '/' ? pathname.substring(1, pathname.length) : pathname
}

/**
 * @param {array} nsps with name as key
 * @param {string} name of path name to compare
 * @return {object} ws
 */
const getNspByPath = (nsps, name) => {
  for (let p in nsps) {
    if (name === p) {
      return nsps[name]
    }
  }

  return false
}

/**
 * what the name said
 * @param {object} wss the websocket
 * @param {string} data things to broadcast
 * @param {boolean} me send to myself or not
 */
function broadcast(wss, data, me = false) {
  wss.clients.forEach(function each(client) {
    // @TODO add the me bit
    if (client.readyState === WebSocket.OPEN) {
      // @TODO need to package the data first
      client.send(data)
    }
  })
}

/**
 * get ip via the request object
 * @param {object} req request object
 * @return {string} or null?
 */
function getReqIp(req) {
  return req.connection.remoteAddress
}

/**
 * Get the ip via the x-forward-for header via nginx
 * @param {object} req request object
 * @return {string} or null?
 */
function getIpFromNginx(req) {
  return req.headers['x-forwarded-for'].split(/\s*,\s*/)[0]
}

module.exports = {
  getPath,
  getNspByPath,
  debug,
  broadcast,
  getReqIp,
  getIpFromNginx
}
