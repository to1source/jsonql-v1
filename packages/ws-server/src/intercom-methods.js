// this will be part of the init client sequence
// as soon as we create a ws client
// we listen to the on.connect event 
// then we send a init-ping event back to the server
// and server issue a csrf token back to use 
// we use this token to create a new client and destroy the old one
const  {
  INTERCOM_RESOLVER_NAME, 
  SOCKET_PING_EVENT_NAME,
  HEADERS_KEY,
  DATA_KEY,
  CSRF_HEADER_KEY
} = require( 'jsonql-constants')
const  { 
  createQueryStr, 
  extractWsPayload,
  timestamp,
  toJson 
} = require( 'jsonql-utils')
const {
  JsonqlError
} = require( 'jsonql-errors')
const { debug } = require('./utils')

const CSRF_HEADER_NOT_EXIST_ERR = 'CSRF header is not in the received payload'

/**
 * call the server to get a csrf token 
 * @return {string} formatted payload to send to the server 
 */
function createInitPing() {
  const ts = timestamp()
  return createQueryStr(INTERCOM_RESOLVER_NAME, [SOCKET_PING_EVENT_NAME, ts])
}

/**
 * Take the raw on.message result back then decoded it 
 * @param {*} payload the raw result from server?
 * @return {object} the csrf payload
 */
function extractPingResult(payload) {
  const json = toJson(payload)
  const result = extractWsPayload(json)
  if (result[DATA_KEY] && result[DATA_KEY][CSRF_HEADER_KEY]) {
    return {
      [HEADERS_KEY]: result[DATA_KEY]
    }
  }
  throw new JsonqlError('extractPingResult', CSRF_HEADER_NOT_EXIST_ERR)
}

/**
 * Create a generic intercom method
 * @param {string} type the event type 
 * @param {array} args if any 
 * @return {string} formatted payload to send
 */
function createIntercomPayload(type, ...args) {
  const ts = timestamp()
  let payload = [type].concat(args)
  payload.push(ts)
  return createQueryStr(INTERCOM_RESOLVER_NAME, payload)
}

/**
 * We need to intercept some of the intercom methods such as logout / disconnect 
 * and handle the framework part here before we pass it to the generic part 
 * @TODO need to figure out where we could insert this method 
 * 
 */
function interceptIntercomCall(opts, ws, namespace, payload, userdata) {
  // what we need is from the payload 
  const json = toJson(payload)
  const result = extractWsPayload(json)
  // we might not need to do anything here, but let see first
  // check the documentation about the left over connection 
  debug('interceptIntercomCall', result)

  /*
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();
 
    ws.isAlive = false;
    ws.ping(noop);
  })
  */
}


module.exports = { 
  createInitPing, 
  extractPingResult, 
  createIntercomPayload,
  interceptIntercomCall
}