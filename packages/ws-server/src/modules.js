// keep all the import from ws-server-core in one place
// for easy switching to debug
const WebSocket = require('ws')
const { 
  wsServerDefaultOptions 
} = require('./core/options')
const {
  checkSocketServerType,

  SOCKET_STATE_KEY,

  jsonqlWsServerCore,
  jsonqlWsServerCoreAction,

  wsServerConstProps,
  getNamespace,
  
  getDebug,
  getRainbowDebug,

  getUserdata,
  isUserdata,
  prepareUserdata,

  getSocketHandler,

  prepareConnectConfig
} = require('jsonql-ws-server-core') 
// require('../../ws-server-core')

  
const { 
  jwtDecode, 
  getWsAuthToken
} = require('@jsonql/security')

module.exports = {
  WebSocket,
  // ours
  checkSocketServerType,
  jsonqlWsServerCore,
  jsonqlWsServerCoreAction,

  wsServerDefaultOptions,
  wsServerConstProps,
  getNamespace,

  getDebug,
  getRainbowDebug,

  getUserdata,
  isUserdata,
  prepareUserdata,

  SOCKET_STATE_KEY,

  getSocketHandler,

  jwtDecode, 
  getWsAuthToken,

  prepareConnectConfig
}
