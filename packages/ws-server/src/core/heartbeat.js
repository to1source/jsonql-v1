// clean up disconnected client
const { debug } = require('../utils')
const { HEART_BEAT_INTERVAL } = require('./options')
/**
 * placeholder 
 */
function noop() {}
 
/**
 * setup the ws state 
 */
function heartbeat() {
  debug('pong heartbeat')
  this.isAlive = true
}

/** 
 * we can this inside the create-ws-server
 * wss.on('connection',
 * @param {object} ws the connected ws client
 * @return {void}
 */
function pong(ws) {
  ws.isAlive = true
  ws.on('pong', heartbeat)
}

/**
 * This method should be call globally
 * @param {object} wss the ws server instance
 * @return {void}
 */
function runCheckBrokenConnection(wss, config) {

  const interval = setInterval(function ping() {
    wss.clients.forEach(function each(ws) {
      if (ws.isAlive === false) {
        return ws.terminate()
      }
      ws.isAlive = false
      ws.ping(noop)
    })
  }, config[HEART_BEAT_INTERVAL])
 
  wss.on('close', function close() {
    clearInterval(interval)
  })
}

module.exports = {
  noop,
  heartbeat,
  pong,
  runCheckBrokenConnection
}