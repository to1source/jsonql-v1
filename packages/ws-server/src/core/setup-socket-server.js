// ws setup all the listener to handle the incoming calls
const { parseJson } = require('jsonql-utils')
const {
  getRainbowDebug,
  getUserdata
} = require('../modules')
const { socketHandler } = require('./socket-handler')
const { createWsServer } = require('./create-ws-server')
const { pong } = require('./heartbeat')

const rdebug = getRainbowDebug('ws-setup')

/**
 * This is the core of the communicaton engine
 * and this method get pass back to the ws-server-core to finish the setup
 * @param {object} opts configuration
 * @param {object} server the http server instance
 * @return {object} nspObj itself with addtional properties
 */
function setupSocketServer(opts, server) {
  const nsps = createWsServer(opts, server)
  // nsps[namespace] contains the namespace --> WebSocket instance
  for (let namespace in nsps) {

    nsps[namespace].on('connection', (ws, req) => {
      // setup the init isAlive state 
      pong(ws) 
      // the rest of the implementation
      let deliverFn = msg => ws.send(msg)
      let userdata = getUserdata(req)
      rdebug(namespace, ' --> connected')
      // @TODO need to redo here the nspGroup can be empty???
      ws.on('message', data => {
        let payload = parseJson(data)
        rdebug(`${namespace} --> on.message -->`, payload)
        // we now wrap everything in one generic method
        socketHandler(opts, ws, deliverFn, req, namespace, payload, userdata)
      })
    })
  }

  return nsps
}


module.exports = { setupSocketServer }
