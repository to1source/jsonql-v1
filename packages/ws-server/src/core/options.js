// since this is very specific to the WebSocket verify method 
// we add this option to the default 
// and we should do the same for those that is only applicable to socket.io 
const { wsServerDefaultOptions } = require('jsonql-ws-server-core')
const { createConfig } = require('jsonql-params-validator')
const { 
  TOKEN_DELIVER_LOCATION_PROP_KEY, 
  TOKEN_IN_URL,
  TOKEN_IN_HEADER,
  STRING_TYPE,
  NUMBER_TYPE,
  ENUM_KEY
} = require('jsonql-constants')

const HEART_BEAT_INTERVAL = 'heartbeat'

const AVAILABLE_PLACES = [
  TOKEN_IN_URL,
  TOKEN_IN_HEADER
]

const configCheckMap = {
  [TOKEN_DELIVER_LOCATION_PROP_KEY]: createConfig(TOKEN_IN_URL, [STRING_TYPE], {
    [ENUM_KEY]: AVAILABLE_PLACES
  }),
  [HEART_BEAT_INTERVAL]: createConfig(30000, [NUMBER_TYPE])
}

const DEFAULT_PORT = 9981

module.exports = {
  HEART_BEAT_INTERVAL,
  DEFAULT_PORT,
  configCheckMap,
  wsServerDefaultOptions: Object.assign(wsServerDefaultOptions, configCheckMap)
}