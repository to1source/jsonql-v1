// web socket server based on ws
// const { getNamespace } = require('jsonql-utils')
const { createVerifyClient } = require('./security')
const { getPath, getNspByPath } = require('../utils')
const { getDebug, WebSocket } = require('../modules')
const { runCheckBrokenConnection } = require('./heartbeat')

const debug = getDebug('ws-setup')
const colors = require('colors/safe')

/**
 * Get the WS namespace instance to handle the call
 * @param {object} config configuration
 * @return {array} of nsps
 */
const generateNsp = config => {
  const { namespaces } = config.nspInfo 
  return namespaces
    .map(name => {
      const verifyClient = createVerifyClient(name, config)
      const opt = Object.assign({}, config.serverInitOption, { noServer: true, verifyClient })
      
      return {
        [name]: new WebSocket.Server(opt)
      }
    })
    .reduce((last, next) => Object.assign(last, next), {})
}


/**
 * This method get pass back to the ws-server-core to finish the server setup
 * @param {object} config options
 * @param {object} server http.createServer instance
 * @return {object} ws server instance with namespace as key
 */
function createWsServer(config, server) {
  const nsps = generateNsp(config)
  // we will always call it via a namespace
  server.on('upgrade', function upgrade(req, socket, head) {
    const pathname = getPath(req)

    debug('Hear the upgrade event', colors.yellow(pathname))

    let wss = false
    if ((wss = getNspByPath(nsps, pathname)) !== false) {
      wss.handleUpgrade(req, socket, head, function done(ws) {
        debug('found a wss to handle the call')
        wss.emit('connection', ws, req)
        // monitor the broken connection
        runCheckBrokenConnection(wss, config)
      })
    }
    /* @1.4.11 we dont' destroy the connection any more
    because during init the client there might not be a call
    here to get handle */
    else {
      debug(colors.red('Unhandled socket'))
      // socket.destroy()
    }
  })
  // add one more listener here to check out what's wrong with the server
  server.on('error', function(err) {
    debug(`ws server.on error`, err)
  })

  return nsps
}


// export
module.exports = { createWsServer }
