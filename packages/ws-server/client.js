// we also provide a node client here
const WebSocket = require('ws')
const { JsonqlError } = require('jsonql-errors')
const { 
  createInitPing, 
  extractPingResult 
} = require('./src/intercom-methods')
const debug = require('debug')('jsonql-ws-server:client')
const { prepareConnectConfig } = require('./src/modules')


/**
 * Create a client with auth token
 * @param {string} url start with ws:// @TODO check this?
 * @param {string} [token = false] the jwt token
 * @param {object} [opts = {}] pass init options to it
 * @return {object} ws instance
 */
function basicClient(uri, config = {}, token = false) {
  // let uri = token ? `${url}?${TOKEN_PARAM_NAME}=${token}` : url
  const { url, opts } = prepareConnectConfig(uri, config, token)
  debug('----> basicClient ---->', url, opts)
  
  return new WebSocket(url, opts)
}

/**
 * Wrap the first init-ping in one place just resolve the message
 * @param {*} ws 
 * @return {promise} 
 */
function initConnect(ws) {
  
  return new Promise((resolver, rejecter) => {
    ws.onopen = function() {
      ws.send(createInitPing())
    }
  
    ws.onmessage = function(payload) {
      try {
        ws.terminate()
        resolver(extractPingResult(payload.data))
      } catch(e) {
        rejecter(new JsonqlError('fullClient.onmessage', e))
      }
    } 
  
    ws.onerror = function(err) {
      rejecter(err)
    }
  })
}

/**
 * Full client with the CSRF token features, and return as promise
 * @param {*} url 
 * @param {*} token 
 * @param {*} opts 
 * @return {promise} resolve the verify connected client
 */
function fullClient(url, opts = {}, token = false) {
  
  return new Promise((resolver, rejecter) => {
    const bc = basicClient(url, opts, token)

    return initConnect(bc)
      .then(header => {

        const config = Object.assign({}, opts, header)
        
        debug(`second return config ---->`, config)
        // this is more elegant then the @jsonql/ws version 
        // because all it does is connect --> get code --> terminate --> re-establish connection with new config
        resolver(basicClient(url, config, token))

      })
      .catch(err => {
        rejecter(err)
      })
  })
}

// breaking change export as name 
module.exports = { 
  basicClient, 
  fullClient 
}