// Very basic test for testing the base code with a enableAuth:true server 
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const colors = require('colors/safe')
// this will get the umd version of the client module
const { JSONQL_PATH } = require('jsonql-constants')
// const { decodeToken } = require('jsonql-jwt')

const { basicClient } = require('../client')
const { extractWsPayload } = require('jsonql-ws-server-core')
const serverSetup = require('./fixtures/server')
const createToken = require('./fixtures/token')
const createPayload = require('./fixtures/create-payload')

const debug = require('debug')('jsonql-ws-server:test:basic')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))

const payload = {name: 'Joel', Location: 'Zhuhai'}
const payload1 = {name: 'Davide', location: 'London'}
const port = 3001
const baseUrl = `ws://localhost:${port}/${JSONQL_PATH}/`
// const { rainbow } = colors


test.before(async t => {
  const { app, io } = await serverSetup({
    contract,
    contractDir,
    privateMethodDir: 'private',
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })

  t.context.token = createToken(payload)
  t.context.token1 = createToken(payload1)

  t.context.io = io
  t.context.app = app

  t.context.app.listen(port)

  // @1.3.0 there is no different between the two only need the token param now
  t.context.client_public = basicClient(baseUrl + 'public')
  // t.context.client_private = basicClient(baseUrl + 'private', {}, t.context.token)
})

test.after(t => {
  t.context.app.close()
})

test.cb('It should able to connect to public namespace without a token', t => {
  // connect to the private channel
  t.plan(2)

  let client = t.context.client_public

  t.truthy(t.context.io[ [JSONQL_PATH, 'public'].join('/') ])

  client.on('open', () => {
    const payload = createPayload('availableToEveryone')
    debug('open payload', payload)
    client.send(payload)
  })

  client.on('message', data => {
    debug('received raw data', data)

    let json = extractWsPayload(data)
    debug('reply', json)
    t.true(json.data.indexOf('You get a public message') > -1)
    // client.close()
    t.end()
  })

})
