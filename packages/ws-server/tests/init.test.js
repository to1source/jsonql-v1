// testing the init connection
const test = require('ava')
// const WebSocket = require('ws')
const { join } = require('path')
const fsx = require('fs-extra')

const { basicClient } = require('../client')

const wsServer = require('./fixtures/server')
const { 
  JSONQL_PATH, 
  INTERCOM_RESOLVER_NAME, 
  SOCKET_PING_EVENT_NAME
} = require('jsonql-constants')
const debug = require('debug')('jsonql-ws-server:test:ws')

const contractDir = join(__dirname, 'fixtures', 'contract')
const contractFile = join(contractDir, 'contract.json')
const contract = fsx.readJsonSync(contractFile)

const { extractWsPayload } = require('jsonql-ws-server-core')
const createPayload = require('./fixtures/create-payload')
const port = 3004
// const msg = 'Something'
// let ctn = 0;

test.before(async t => {
  const { app, io } = await wsServer({
    contractDir,
    contract
  })
  t.context.io = io
  t.context.server = app
  t.context.server.listen(port)
  t.context.client = basicClient(`ws://localhost:${port}/${JSONQL_PATH}`)
})

test.after(t => {
  t.context.server.close()
})

test.cb(`Testing the init ping call`, t => {
  t.plan(1)
  let client = t.context.client

  client.on('open', () => {
    const ts = Date.now()
    client.send(
      createPayload(INTERCOM_RESOLVER_NAME, SOCKET_PING_EVENT_NAME, ts)
    )
  })

  client.on('message', data => {
    const json = extractWsPayload(data)
    debug('json', json)
    t.pass()
    
    // t.end()
    // reinit the client with an options
    const newClient = basicClient(`ws://localhost:${port}/${JSONQL_PATH}`, {
      headers: json.data
    })

    newClient.on('open', () => {
      t.end()
    })
  })

})
