// this one will provide all the configuration options including the contract
const { join } = require('path')
const fsx = require('fs-extra')

const debug = require('debug')('jsonql-ws-server:test:full-setup')
const resolverDir = join(__dirname, 'resolvers')

const { jsonqlWsServer } = require('../../index')
const http = require('http')

// start
const server = http.createServer(function(req, res) {
  res.writeHead(200, { 'Content-Type': 'text/plain' })
  res.write('request successfully proxied!' + '\n' + JSON.stringify(req.headers, true, 2))
  res.end()
})

module.exports = function(_config = {}) {
  const config = {
    resolverDir,
    serverType: 'socket.io'
  }
  const opts = Object.assign(config, _config)
  return new Promise(resolver => {
    jsonqlWsServer(opts, server)
      .then(io => {
        debug('socket setup completed')
        resolver({
          app: server,
          io
        })
      })
  })
}
