const { join } = require('path')

const dir = process.env.DIR 
let args = [__dirname, 'contract']
if (dir) {
  args.push(dir)
}

module.exports = {
  enableAuth: true,
  resolverDir: join(__dirname, 'resolvers'),
  contractDir: Reflect.apply(join, null, args)
};
