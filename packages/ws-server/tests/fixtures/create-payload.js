const createPayload = (name, ...args) => {
  return JSON.stringify({
    [name]: { args: args }
  })
}

module.exports = createPayload
