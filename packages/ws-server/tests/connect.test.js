// this will be testing the new auth client using the header options 
const test = require('ava')

const { join } = require('path')
const fsx = require('fs-extra')
const colors = require('colors/safe')
// this will get the umd version of the client module
const { 
  JSONQL_PATH, 
  TOKEN_IN_HEADER,
  TOKEN_DELIVER_LOCATION_PROP_KEY
} = require('jsonql-constants')

const { basicClient, fullClient } = require('../client')
const { extractWsPayload } = require('jsonql-ws-server-core')
const serverSetup = require('./fixtures/server')
const createToken = require('./fixtures/token')
const createPayload = require('./fixtures/create-payload')

const debug = require('debug')('jsonql-ws-server:test:jwt-auth')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))

const payload = {name: 'Joel', Location: 'Zhuhai'}
// const payload1 = {name: 'Davide', location: 'London'}
const port = 3002
const baseUrl = `ws://localhost:${port}/${JSONQL_PATH}/`
// const { rainbow } = colors
// const EventEmitter = require('@to1source/event')

test.before(async t => {

  t.context.tokenConfig = {
    [TOKEN_DELIVER_LOCATION_PROP_KEY]: TOKEN_IN_HEADER
  }

  const { app } = await serverSetup({
    contract,
    contractDir,
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys'),
    [TOKEN_DELIVER_LOCATION_PROP_KEY]: TOKEN_IN_HEADER
  })

  app.listen(port)
  t.context.app = app 
  t.context.token = createToken(payload)
})

test.after(t => {
  t.context.app.close()
})

test.cb(`Should able to use the new header option to create a auth client`, t => {

  t.plan(1)
  
  const client = basicClient(baseUrl + 'private', t.context.tokenConfig, t.context.token)

  client.on('open', function() {
    client.send( createPayload('secretChatroom', 'what', 'ever') )
  })

  client.on('message', function(payload) {
    let json = extractWsPayload(payload)
    debug(colors.blue.bgYellow('on.message from private'), json)

    t.truthy(json)
    t.end()
  })
})

test.cb(`Testing the fullClient with CSRF feature`, t => {

  t.plan(2)

  fullClient(baseUrl + 'private', t.context.tokenConfig, t.context.token)
    .then(client => {

      client.onopen = function() {
        debug(`did we get this event????`)
        t.pass()
        client.send( createPayload('secretChatroom', 'what', 'ever') )
      }
      // when we call using the onmessage method instead of the on.callback 
      // we get a raw socket object with `data` property 
      client.onmessage = function(payload) {
        // debug(colors.yellow.bgBlue(`onmessage raw payload`), payload)
        let json = extractWsPayload(payload.data)
        debug(colors.blue.bgYellow('on.message from private'), json)
        t.truthy(json)
        t.end()
      }
    })
    .catch(err => {
      debug(colors.red(`ERROR in fulleClient`), err)
      t.end()
    })
})

test.cb(`Test connect to public namespace with the token and try to get back the userdata`, t => {

  t.plan(3)

  let ctn = 0

  fullClient(baseUrl + 'public', t.context.tokenConfig, t.context.token)
    .then(client => {
      client.onopen = function() {
        ++ctn 
        t.pass()
        client.send( createPayload('availableToEveryone') )
      }
      client.onmessage = function(payload) {
        ++ctn 
        const { data } = payload 

        debug(colors.yellow.bgBlue('the result raw data'), data)
        
        let json = extractWsPayload(data)
        t.truthy(json)

        if (ctn >= 3) {
          t.end()
        }
      }
    })
})