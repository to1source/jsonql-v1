// this will return the sync interface instead of the switching
// because that will cause a lot of confusion about the api
// new module interface for @jsonql/client
// this will be use with the @jsonql/ws, @jsonql/socketio
import { jsonqlSync, getEventEmitter } from './src'
import { isContract } from './src/utils'
import { JsonqlError } from 'jsonql-errors'

/**
 * When pass a static contract then it return a static interface
 * otherwise it will become the async interface
 * @param {object} Fly the http engine
 * @param {object} config configuration
 * @return {object} jsonqlClient
 */
export function jsonqlStaticClient(Fly, config) {
  if (config.contract && isContract(config.contract)) {
    const ee = getEventEmitter(config.debugOn)
    return jsonqlSync(ee, Fly, config)
  }
  throw new JsonqlError('jsonqlStaticClient', `Expect to pass the contract via configuration!`)
}
