// new module interface for @jsonql/client
// this will be use with the @jsonql/ws, @jsonql/socketio
// 1.5.8 we move the implmentation out and they should be in the @jsonql/client
import { jsonqlApiGenerator } from './src/core/jsonql-api-generator'
import { jsonqlCbGenerator } from './src/core/jsonql-cb-generator'
import { JsonqlBaseEngine } from './src/base'
import { getEventEmitter } from './src'
import { checkOptionsAsync } from './src/options'
import { getContractFromConfig } from './src/utils'
// merge from opt.js not point of having two
// export the options for the pre-check to use
import { preConfigCheck } from 'jsonql-utils/module'
import { checkConfig } from 'jsonql-params-validator'

import { appProps, constProps } from './src/options/base-options'

/**
 * This will combine the socket client options and merge this one
 * then do a pre-check on both at the same time
 * @param {object} [extraProps = {}]
 * @param {object} [extraConstProps = {}]
 * @return {function} to process the developer options
 */
function getPreConfigCheck(extraProps = {}, extraConstProps = {}) {
  // we only want a shallow copy instead of deep merge
  const aProps = Object.assign({}, appProps, extraProps)
  const cProps = Object.assign({}, constProps, extraConstProps)
  return preConfigCheck(aProps, cProps, checkConfig)
}
// rename for export
const jsonqlHttpClientAppProps = appProps
const jsonqlHttpClientConstProps = constProps
// just duplicate it and export to avoid the crash out
// and take down those name later on
const JsonqlBase = JsonqlBaseEngine

// export
export {
  jsonqlApiGenerator,
  jsonqlCbGenerator,
  jsonqlHttpClientAppProps,
  jsonqlHttpClientConstProps,
  JsonqlBase,
  JsonqlBaseEngine, // this will affect a lot of the deps later
  getEventEmitter,
  checkOptionsAsync,
  getContractFromConfig,
  getPreConfigCheck
}
