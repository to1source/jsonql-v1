// main export interface
import { jsonqlAsync, getEventEmitter } from './src'

/**
 * When pass a static contract then it return a static interface
 * otherwise it will become the async interface
 * @param {object} fly the http engine - already init object not the class!
 * @param {object} config configuration
 * @return {object} jsonqlClient
 */
export default function jsonqlClient(fly, config) {
  const ee = getEventEmitter(config.debugOn)
  // @TODO the callback style will be the default interface from now on
  return jsonqlAsync(ee, fly, config)
}
