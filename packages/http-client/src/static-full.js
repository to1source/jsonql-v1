// This is the static version that build with the Fly for Browser
import Fly from 'flyio/dist/npm/fly'
import { jsonqlStaticClient } from '../static'

// this is the slim client without Fly
export default function jsonqlStaticClientFull(config = {}) {
  return jsonqlStaticClient(new Fly(), config)
}
