// base HttpClass
import merge from 'lodash-es/merge'
import {
  createQuery,
  createMutation,
  getNameFromPayload
} from 'jsonql-utils/src/params-api'
import { cacheBurst } from 'jsonql-utils/src/urls'
import { resultHandler } from 'jsonql-utils/src/results'
import { isString } from 'jsonql-params-validator'
import {
  // JsonqlValidationError,
  JsonqlServerError,
  // JsonqlError,
  clientErrorsHandler
} from 'jsonql-errors'
import {
  API_REQUEST_METHODS,
  DEFAULT_HEADER,
  JSONP_CALLBACK_NAME,
  SHOW_CONTRACT_DESC_PARAM
} from 'jsonql-constants'
import { LOG_ERROR_SWITCH } from '../utils'

// extract the one we need
const [ POST, PUT ] = API_REQUEST_METHODS

import StoreClass from './store-cls'

export default class HttpClass extends StoreClass {
  /**
   * The opts has been check at the init stage
   * @param {object} opts configuration options
   */
  constructor(opts) {
    super(opts)
    // @1.2.1 for adding query to the call on the fly
    this.extraHeader = {}
    this.extraParams = {}
    // this.log('start up opts', opts);
  }

  // set headers for that one call
  set headers(header) {
    this.extraHeader = header
  }

  /**
   * Create the reusage request method
   * @param {object} payload jsonql payload
   * @param {object} options extra options add the request
   * @param {object} headers extra headers add to the call
   * @return {object} the fly request instance
   */
  request(payload, options = {}, headers = {}) {
    this.headers = headers;
    let params = merge({}, cacheBurst(), this.extraParams)
    // @TODO need to add a jsonp url and payload
    if (this.opts.enableJsonp) {
      let resolverName = getNameFromPayload(payload)
      params = merge({}, params, {[JSONP_CALLBACK_NAME]: resolverName})
      payload = payload[resolverName]
    }
    // double up the url param and see what happen @TODO remove later
    const reqParams = merge({}, { method: POST, params }, options)
    this.log('request params', reqParams, this.jsonqlEndpoint)

    return this.httpEngine.request(this.jsonqlEndpoint, payload, reqParams)
  }

  /**
   * This will replace the create baseRequest method
   * @return {null} nothing to return
   */
  reqInterceptor() {
    this.httpEngine.interceptors.request.use(
      req => {
        const headers = this.getHeaders()
        this.log('request interceptor call', headers)

        for (let key in headers) {
          req.headers[key] = headers[key]
        }
        return req
      }
    )
  }

  // @TODO
  processJsonp(result) {
    return resultHandler(result)
  }

  /**
   * This will be replacement of the first then call
   * @return {null} nothing to return
   */
  resInterceptor() {
    const self = this
    const jsonp = self.opts.enableJsonp
    this.httpEngine.interceptors.response.use(
      res => {
        this.log('response interceptor call', res)
        self.cleanUp()
        // now more processing here
        // there is a problem if we throw the result.error here
        // the original data is lost, so we need to do what we did before
        // deal with that error in the first then instead
        const result = isString(res.data) ? JSON.parse(res.data) : res.data
        if (jsonp) {
          return self.processJsonp(result)
        }
        return resultHandler(result)
      },
      // this get call when it's not 200
      err => {
        self.cleanUp()
        this.log(LOG_ERROR_SWITCH, err)
        throw new JsonqlServerError('Server side error', err)
      }
    )
  }

  /**
   * Get the headers inject into the call
   * @return {object} headers
   */
  getHeaders() {
    if (this.opts.enableAuth) {
      return merge({}, DEFAULT_HEADER, this.getAuthHeader(), this.extraHeader)
    }
    return merge({}, DEFAULT_HEADER, this.extraHeader)
  }

  /**
   * Post http call operation to clean up things we need
   */
  cleanUp() {
    this.extraHeader = {}
    this.extraParams = {}
  }

  /**
   * GET for contract only
   * @return {promise} resolve the contract
   */
  getRemoteContract() {
    if (this.opts.showContractDesc) {
      this.extraParams = merge({}, this.extraParams, SHOW_CONTRACT_DESC_PARAM)
    }
    return this.request({}, {method: 'GET'}, this.contractHeader)
      .then(clientErrorsHandler)
      .then(result => {
        this.log('get contract result', result)
        // when refresh the window the result is different!
        // @TODO need to check the Koa side about why is that
        // also it should set a flag if we want the description or not
        if (result.cache && result.contract) {
          return result.contract;
        }
        // just the normal result
        return result
      })
      .catch(err => {
        this.log(LOG_ERROR_SWITCH, 'getRemoteContract err', err)
        throw new JsonqlServerError('getRemoteContract', err)
      })
  }

 /**
  * POST to server - query
  * @param {object} name of the resolver
  * @param {array} args arguments
  * @return {object} promise resolve to the resolver return
  */
 query(name, args = []) {
   return this.request(createQuery(name, args))
    .then(clientErrorsHandler)
 }

 /**
  * PUT to server - mutation
  * @param {string} name of resolver
  * @param {object} payload what it said
  * @param {object} conditions what it said
  * @return {object} promise resolve to the resolver return
  */
 mutation(name, payload = {}, conditions = {}) {
   return this.request(createMutation(name, payload, conditions), {method: PUT})
    .then(clientErrorsHandler)
 }

}
