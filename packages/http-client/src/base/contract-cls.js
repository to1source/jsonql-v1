// all the contract related methods will be here
import { JsonqlValidationError } from 'jsonql-errors'
// import { timestamp } from 'jsonql-utils/src/timestamp'
import { isContract } from 'jsonql-utils/src/contract'
import { CLS_CONTRACT_NAME } from '../utils'
// import { localStore } from '../stores'
import HttpClass from './http-cls'

// export
export default class ContractClass extends HttpClass {

  constructor(opts) {
    super(opts)
  }

  /**
   * return the contract public api
   * @return {object} contract
   */
  getContract() {
    const contract = this.readContract()
    this.log('getContract first call', contract)
    return contract ? Promise.resolve(contract)
                    : this.getRemoteContract().then(this.storeContract.bind(this))
  }

  /**
   * We are changing the way how to auth to get the contract.json
   * Instead of in the url, we will be putting that key value in the header
   * @return {object} header
   */
  get contractHeader() {
    let base = {};
    if (this.opts.contractKey !== false) {
      base[this.opts.contractKeyName] = this.opts.contractKey;
    }
    return base;
  }

  /**
   * Save the contract to local store
   * @param {object} contract to save
   * @return {object|boolean} false when its not a contract or contract on OK
   */
  storeContract(contract) {
    // first need to check if the contract is a contract
    if (!isContract(contract)) {
      throw new JsonqlValidationError(`Contract is malformed!`)
    }
    this.lset = {[CLS_CONTRACT_NAME]: contract}
    // return it
    this.log('storeContract return result', contract)
    return contract;
  }

  /**
   * return the contract from options or localStore
   * @return {object|boolean} false on not found
   */
  readContract() {
    let contract = isContract(this.opts.contract)
    if (contract !== false) {
      return contract;
    }
    let data = this.lget
    if (data) {
      return data[CLS_CONTRACT_NAME]
    }
    return false;
  }
}
