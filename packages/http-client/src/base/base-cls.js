// this the core of the internal storage management
// import { CREDENTIAL_STORAGE_KEY } from 'jsonql-constants'
// import { isObject, isArray } from 'jsonql-params-validator'
// import { JsonqlValidationError } from 'jsonql-errors'
// import { timestamp } from 'jsonql-utils/src/timestamp'
// import { inArray } from 'jsonql-utils/src/generic'
import { LOG_ERROR_SWITCH } from '../utils'
import AuthCls from './auth-cls'

// This class will only focus on the storage system
export default class JsonqlBaseEngine extends AuthCls {
  // change the order of the interface in 1.4.10 to match up the top level
  constructor(httpEngine, opts) {
    super(opts)
    // change at 1.4.10 pass it directly without init it
    this.httpEngine = httpEngine // fly.js
    // this two methods defined in http-cls, and execute the create interceptors
    this.reqInterceptor()
    this.resInterceptor()
  }

  /**
   * construct the end point
   * @return {string} the end point to call
   */
  get jsonqlEndpoint() {
    const baseUrl = this.opts.hostname || ''
    return [baseUrl, this.opts.jsonqlPath].join('/')
  }

  /**
   * simple log control by the debugOn option
   * @param {array<*>} args
   * @return {void}
   */
  log(...args) {
    if (this.opts.debugOn === true) {
      const fns = ['info', 'error']
      const idx = (args[0] === LOG_ERROR_SWITCH) ? 1 : 0
      args.splice(0, idx)
      // add an id to the beginning
      Reflect.apply(console[fns[idx]], console, ['[JSONQL_LOG]'].concat(args))
    }
    /* make it a function and pass to it?
    else if (typeof this.opts.debugOn === 'function') {
      Reflect.apply(this.opts.debugOn, null, [args])
    } */
  }

}
