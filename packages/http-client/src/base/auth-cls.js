// this is the new auth class that integrate with the jsonql-jwt
// all the auth related methods will be here
import { decodeToken } from 'jsonql-jwt/src/client'
import { isNumber } from 'jsonql-params-validator'
import { JsonqlValidationError } from 'jsonql-errors'
import { CREDENTIAL_STORAGE_KEY, BEARER } from 'jsonql-constants'
import { CLS_PROFILE_IDX, ZERO_IDX, USERDATA_TABLE } from '../utils'
import ContractClass from './contract-cls'
// export
export default class AuthClass extends ContractClass {

  constructor(opts) {
    super(opts)
    if (opts.enableAuth) {
      this.setDecoder = decodeToken;
    }
    // cache
    this.__userdata__ = null;
  }

  /**
   * for overwrite
   * @param {string} token stored token
   * @return {string} token
   */
  decoder(token) {
    return token;
  }

  /**
   * set the profile index
   * @param {number} idx
   */
  set profileIndex(idx) {
    const key = CLS_PROFILE_IDX
    if (isNumber(idx)) {
      this[key] = idx;
      if (this.opts.persistToken) {
        this.lset = {[key]: idx}
      }
      return;
    }
    throw new JsonqlValidationError('profileIndex', `Expect idx to be number but got ${typeof idx}`)
  }

  /**
   * get the profile index
   * @return {number} idx
   */
  get profileIndex() {
    const key = CLS_PROFILE_IDX
    if (this.opts.persistToken) {
      const data = this.lget;
      if (data[key]) {
        return data[key]
      }
    }
    return this[key] ? this[key] : ZERO_IDX
  }

  /**
   * Return the token from session store
   * @param {number} [idx=false] profile index
   * @return {string} token
   */
  rawAuthToken(idx = false) {
    if (idx !== false) {
      this.profileIndex = idx;
    }
    // this should return from the base
    return this.jsonqlToken; // see base-cls
  }

  /**
   * Setter to add a decoder when retrieve user token
   * @param {function} d a decoder
   */
  set setDecoder(d) {
    if (typeof d === 'function') {
      this.decoder = d;
    }
  }

  /**
   * getter to return the session or local store set method
   * @param {*} data to save
   * @return {object} set method
   */
  set saveProfile(data) {
    if (this.opts.persistToken) {
      // this.log('--- saveProfile lset ---', data)
      this.lset = data
    } else {
      // this.log('--- saveProfile sset ---', data)
      this.sset = data
    }
  }

  /**
   * getter to return the session or local store get method
   * @return {object} get method
   */
  get readProfile() {
    return this.opts.persistToken ? this.lget : this.sget
  }

  // these were in the base class before but it should be here
  /**
   * save token
   * @param {string} token to store
   * @return {string|boolean} false on failed
   */
  set jsonqlToken(token) {
    const data = this.readProfile
    const key = CREDENTIAL_STORAGE_KEY
    // @TODO also have to make sure the token is not already existed!
    let tokens = (data && data[key]) ? data[key] : []
    tokens.push(token)
    this.saveProfile = {[key]: tokens}
    // store the userdata
    this.__userdata__ = this.decoder(token)
    this.jsonqlUserdata = this.__userdata__
  }

  /**
   * Jsonql token getter
   * 1.5.1 each token associate with the same profileIndex
   * @return {string|boolean} false when failed
   */
  get jsonqlToken() {
    const data = this.readProfile
    const key = CREDENTIAL_STORAGE_KEY
    if (data && data[key]) {
      this.log('-- jsonqlToken --', data[key], this.profileIndex, data[key][this.profileIndex])
      return data[key][this.profileIndex]
    }
    return false
  }

  /**
   * this one will use the sessionStore
   * basically we hook this onto the token store and decode it to store here
   * we only store one decoded user data at a time, but the token can be multiple
   */
  set jsonqlUserdata(userdata) {
    this.sset = {[USERDATA_TABLE]: userdata}
  }

  /**
   * this one store in the session store
   * get login userdata decoded jwt
   * 1.5.1 each userdata associate with the same profileIndex
   * @return {object|null}
   */
  get jsonqlUserdata() {
    const data = this.sget
    return data ? data[USERDATA_TABLE] : false
  }

  /**
   * Construct the auth header
   * @return {object} header
   */
  getAuthHeader() {
    const token = this.jsonqlToken // only call the getter to get the default one
    return token ? {[this.opts.AUTH_HEADER]: `${BEARER} ${token}`} : {};
  }

  /**
   * return all the stored token and decode it
   * @param {number} [idx=false] profile index
   * @return {array|boolean|string} false not found or array
   */
  getProfiles(idx = false) {
    const self = this; // just in case the scope problem
    const data = self.readProfile
    const key = CREDENTIAL_STORAGE_KEY
    if (data && data[key]) {
      if (idx !== false && isNumber(idx)) {
        return data[key][idx] || false
      }
      return data[key].map(self.decoder.bind(self))
    }
    return false
  }

  /**
   * call after the login
   * @param {string} token return from server
   * @return {object} decoded token to userdata object
   */
  postLoginAction(token) {
    this.jsonqlToken = token
    
    return { token, userdata: this.__userdata__ }
  }

  /**
   * call after the logout @TODO
   */
  postLogoutAction(...args) {
    console.info(`postLogoutAction`, args)
  }
}
