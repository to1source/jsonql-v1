// this will be the sync version
import { jsonqlApiGenerator } from './core/jsonql-api-generator'
import { JsonqlBaseEngine } from './base'
import { checkOptions } from './options'

/**
 * when the client contains a valid contract
 * @param {object} ee EventEmitter
 * @param {object} fly the fly client
 * @param {object} [config={}] configuration
 * @return {object} the client
 */
export function jsonqlSync(ee, fly, config = {}) {
  const { contract } = config

  const opts = checkOptions(config)

  const jsonqlBaseCls = new JsonqlBaseEngine(fly, opts)

  return jsonqlApiGenerator(jsonqlBaseCls, opts, contract, ee)
}
