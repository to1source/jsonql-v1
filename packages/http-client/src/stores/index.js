// export store interface
// @TODO need to figure out how to make this as a outside dependencies instead of built into it
import localStoreEngine from './local-store'
import sessionStoreEngine from './session-store'

// export back the raw version for development purposes
export const localStore = localStoreEngine
export const sessionStore = sessionStoreEngine
