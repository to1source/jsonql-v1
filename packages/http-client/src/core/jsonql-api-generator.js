// Generate the resolver for developer to use

// @TODO when enableAuth we need to add one extra check
// before the resolver call make it to the core
// which is checking the login state, if the developer
// is calling a private method without logging in
// then we should throw the JsonqlForbiddenError at this point
// instead of making a round trip to the server
/*
import { LOGOUT_NAME, ISSUER_NAME, KEY_WORD } from 'jsonql-constants'
import { validateAsync } from 'jsonql-params-validator'
import {
  JsonqlValidationError,
  JsonqlError,
  clientErrorsHandler,
  finalCatch
} from 'jsonql-errors'
*/
import { methodsGenerator, addPropsToClient } from './methods-generator'

/**
 * @param {object} jsonqlInstance jsonql class instance
 * @param {object} config options
 * @param {object} contract the contract
 * @param {object} ee eventEmitter
 * @return {object} constructed functions call
 */
export const jsonqlApiGenerator = (jsonqlInstance, config, contract, ee) => {
  // V1.3.0 - now everything wrap inside this method
  let client = methodsGenerator(jsonqlInstance, ee, config, contract)

  client = addPropsToClient(client, jsonqlInstance, ee, config, contract)
  // output
  return client
}
