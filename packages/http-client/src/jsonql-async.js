// this is new for the flyio and normalize the name from now on
import { jsonqlApiGenerator } from './core/jsonql-api-generator'
import { JsonqlBaseEngine } from './base'
import { checkOptionsAsync } from './options'
import { getContractFromConfig } from './utils'

/**
@TODO in the 1.6.x

The default client without passing the contract as static option should be
a callback style interface, the reason is the cb call accept any name
(internally it just turn into an event name and pre-register it) and on the
developer side, they don't need to care when the contract finish loading,
because we could reverse trigger the calls in queue. 

**/

/**
 * Main interface for jsonql fetch api
 * @param {object} ee EventEmitter
 * @param {object} fly this is really pain in the backside ... long story
 * @param {object} [config={}] configuration options
 * @return {object} jsonql client
 */
export function jsonqlAsync(ee, fly, config = {}) {
  return checkOptionsAsync(config)
    .then(opts => (
      {
        baseClient: new JsonqlBaseEngine(fly, opts),
        opts: opts
      }
    ))
    .then( ({baseClient, opts}) => (
      getContractFromConfig(baseClient, opts.contract)
        .then(contract => jsonqlApiGenerator(baseClient, opts, contract, ee))
      )
    )
}
