// export interface
import { appProps, constProps } from './base-options'
import { checkConfig } from 'jsonql-params-validator'
import { postConfigCheck } from 'jsonql-utils/src/pre-config-check'
import { objHasProp } from 'jsonql-utils/src/obj-define-props'
import { CHECKED_KEY } from 'jsonql-constants'
/**
 * 1.5.0 overload the orginal functions to pass over the check
 */
function checkOptionsAsync(config) {
  const fn = postConfigCheck(appProps, constProps, checkConfig)
  const { contract } = config;
  return fn(config)
    .then(result => {
      result.contract = contract
      return result
    })
}

/**
 * sync version without needing the promise
 */
function checkOptions(config) {
  return objHasProp(config, CHECKED_KEY) ? Object.assign(config, constProps)
                                         : checkConfig(config, appProps, constProps)
}

export {
  checkOptionsAsync,
  checkOptions
}
