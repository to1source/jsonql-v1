// all the client configuration options here
import {
  JSONQL_PATH,
  CONTENT_TYPE,
  BEARER,
  CLIENT_STORAGE_KEY,
  CLIENT_AUTH_KEY,
  CONTRACT_KEY_NAME,
  AUTH_HEADER,
  ISSUER_NAME,
  LOGOUT_NAME,
  BOOLEAN_TYPE,
  STRING_TYPE,
  NUMBER_TYPE,
  ARRAY_TYPE,
  DEFAULT_HEADER
} from 'jsonql-constants'
import { createConfig } from 'jsonql-params-validator'
export const constProps = {
  contract: false,
  MUTATION_ARGS: ['name', 'payload', 'conditions'], // this seems wrong?
  CONTENT_TYPE,
  BEARER,
  AUTH_HEADER
}

// grab the localhost name and put into the hostname as default
const getHostName = () => {
  try {
    return [window.location.protocol, window.location.host].join('//')
  } catch(e) {
    return '/'
  }
}

export const appProps = {
  // The hostname to call
  hostname: createConfig(getHostName(), [STRING_TYPE]),
  // The path on the server NOT RECOMMENDED to change!
  jsonqlPath: createConfig(JSONQL_PATH, [STRING_TYPE]),
  // the name of the auth handler, if you want to change it but it must change in pair on both server and client side
  loginHandlerName: createConfig(ISSUER_NAME, [STRING_TYPE]),
  logoutHandlerName: createConfig(LOGOUT_NAME, [STRING_TYPE]),
  // @TODO add to koa v1.3.0 - this might remove in the future
  enableJsonp: createConfig(false, [BOOLEAN_TYPE]),
  enableAuth: createConfig(false, [BOOLEAN_TYPE]),
  // enable useJwt by default @TODO replace with something else and remove them later
  useJwt: createConfig(true, [BOOLEAN_TYPE]),
  // when true then store infinity or pass a time in seconds then we check against
  // the token date of creation
  persistToken: createConfig(false, [BOOLEAN_TYPE, NUMBER_TYPE]),
  // the header
  // v1.2.0 we are using this option during the dev
  // so it won't save anything to the localstorage and fetch a new contract
  // whenever the browser reload
  useLocalstorage: createConfig(true, [BOOLEAN_TYPE]), // should we store the contract into localStorage
  storageKey: createConfig(CLIENT_STORAGE_KEY, [STRING_TYPE]),// the key to use when store into localStorage
  authKey: createConfig(CLIENT_AUTH_KEY, [STRING_TYPE]),// the key to use when store into the sessionStorage
  // -1 always fetch contract,
  // 0 never expired,
  // > 0 then compare the timestamp with the current one to see if we need to get contract again
  contractExpired: createConfig(0, [NUMBER_TYPE]),
  // useful during development
  keepContract: createConfig(true, [BOOLEAN_TYPE]),
  exposeContract: createConfig(false, [BOOLEAN_TYPE]),
  exposeStore: createConfig(false, [BOOLEAN_TYPE]), // whether to allow developer to access the store fn
  // @1.2.1 new option for the contract-console to fetch the contract with description
  showContractDesc: createConfig(false, [BOOLEAN_TYPE]),
  // if the server side is lock by the key you need this
  contractKey: createConfig(false, [BOOLEAN_TYPE]),
  // same as above they go in pairs
  contractKeyName: createConfig(CONTRACT_KEY_NAME, [STRING_TYPE]),
  enableTimeout: createConfig(false, [BOOLEAN_TYPE]), // @TODO
  timeout: createConfig(5000, [NUMBER_TYPE]), // 5 seconds
  returnInstance: createConfig(false, [BOOLEAN_TYPE]),
  allowReturnRawToken: createConfig(false, [BOOLEAN_TYPE]),
  debugOn: createConfig(false, [BOOLEAN_TYPE]),
  ///////////////////////////////
  // options added in 1.6.0    //
  ///////////////////////////////
  // we will flatten all the resolver into the client level if this is false
  namespaced: createConfig(false, [BOOLEAN_TYPE]),
  // use the session store to cache the result temporary, or set a expired time (in ms) @TODO in 1.7.0
  cacheResult: createConfig(false, [BOOLEAN_TYPE, NUMBER_TYPE]),
  cacheExcludedList: createConfig([], [ARRAY_TYPE])
}
