// as of 1.5.15 this no longer build
// it's a source files that require to build on the your client side
// this is the new Event base with callback style interface
// the export will be different and purposely design for framework that
// is very hard to use Promise such as Vue
import { jsonqlCbGenerator } from './src/core/jsonql-cb-generator'
import { JsonqlBaseEngine } from './src/base'
import { checkOptions } from './src/options'
import { getContractFromConfig } from './src/utils'
import { getEventEmitter } from './src'

/**
 * this is the slim client without Fly, you pick the version of Fly to use
 * This is a breaking change because it swap the input positions
 * @param {object} fly fly.js
 * @param {object} config configuration
 * @return {object} the jsonql client instance
 */
export function jsonqlCallbackClient(fly, config) {
  const { contract } = config
  const opts = checkOptions(config)
  const jsonqlBaseCls = new JsonqlBaseEngine(fly, opts)
  const contractPromise = getContractFromConfig(jsonqlBaseCls, contract)
  const ee = getEventEmitter(opts.debugOn)
  // finally
  let methods = jsonqlCbGenerator(jsonqlBaseCls, opts, contractPromise, ee)
  methods.eventEmitter = ee
  // 1.5.21 port the logger for other part to use
  methods.getLogger = (name) => (...args) => Reflect.apply(jsonqlInstance.log, jsonqlInstance, [name, ...args])
  return methods
}
