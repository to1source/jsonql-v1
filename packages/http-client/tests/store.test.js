// testing the store functions
const test = require('ava')
const { hashCode } = require('nb-event-service/src/hash-code')
const debug = require('debug')('jsonql-client:test:store')

test(`It should able to use the hashCode function from nb-event-service`, t => {

  const code = hashCode('http://localhost:3456')

  debug(code, typeof code)

  t.truthy(code)

})
