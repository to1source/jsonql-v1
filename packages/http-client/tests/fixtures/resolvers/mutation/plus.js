const debug = require('debug')('jsonql-client:mutation:plus');

/**
 * @param {object} payload the payload object
 * @param {string} payload.a required key a
 * @param {string} payload.b required key b
 * @param {object} [conditions={}] optional
 * @return {string} concat string of a + b;
 */
module.exports = function(payload, conditions = {}) {
  debug('mutation.plus called', payload);
  const result = payload.a + payload.b;
  debug('result is', result);
  return result;
}
