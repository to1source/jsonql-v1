const { join } = require('path')
const options = require('./options.json')
const favicon = require('koa-favicon')
const serverIoCore = require('server-io-core')
const jsonqlKoa = require('@jsonql/koa')
const port = options.server.port;
const debug = require('debug')('jsonql-client:test:server')
const env = process.env.NODE_ENV;
// why the fuck?
jsonqlMiddleware = jsonqlKoa.default

const dummy = () => {
  return async function(ctx, next) {
    // debug(ctx.path)
    if (ctx.path === '/test') {
      ctx.status = 200;
      ctx.body = JSON.stringify({'message': 'Hello world'})
      return;
    }
    await next()
  }
}

module.exports = function(config = {}) {
  const { stop } = serverIoCore({
    webroot: [
      __dirname,
      join(__dirname, '..', '..', 'node_modules'),
      join(__dirname, '..', '..', 'dist')
    ],
    // open: true,
    // debugger: true,
    port: port,
    // reload: true, //env !== 'test', @BUG it keep reload not sure what is the problem at the moment
    middlewares: [
      favicon(join(__dirname, 'favicon.ico')),
      // dummy(),
      jsonqlMiddleware(Object.assign({
        // enableAuth: true,
        resolverDir: join(__dirname, 'resolvers'),
        contractDir: join(__dirname, 'contracts', 'tmp')
      }, config))
    ]
  });
  return stop;
}
