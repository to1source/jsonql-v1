
const runQunitSetup = require('./run-qunit-setup')
const config = {
  "port": 10081,
  "webroot": [
    "./tests/qunit/webroot",
    "./tests/qunit/tests",
    "./dist",
    "./node_modules"
  ],
  "open": true,
  "reload": true,
  "testFilePattern": "*-test.js",
  "baseDir": "tests"
}

runQunitSetup(config)
