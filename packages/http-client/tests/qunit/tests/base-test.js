// The basic test copy from main.test.js

QUnit.test('jsonqlClient should able to connect to server', function(assert) {

  var done1 = assert.async()
  var done2 = assert.async()
  var done3 = assert.async()
  var done4 = assert.async()
  var done5 = assert.async()

  jsonqlClient({
      hostname: 'http://localhost:10081',
      showContractDesc: true,
      keepContract: false,
      debugOn: true,
      enableAuth: true
    })
    .then(function(client) {
      console.log('inited client', client, client.contract)
      console.info('eventEmitter', client.eventEmitter.name)

      // 1.4.2 use the shorthand version
      client.helloWorld().then(function(result) {
        assert.equal('Hello world!', result, "Hello world test done")
        done1()
      })
      // test the query with wrong param
      client.getSomething(1).catch(err => {
        assert.equal(err.className, 'JsonqlValidationError', 'Expect validation error')
        done2()
      })
      // call the mutation before login
      client.plus({a: '1', b: '1'})
        .catch(err => {
          console.error(`--- NOT LOGIN ERROR ---`, err)
          // @NOTE this is a cheat because the finalCatch from jsonql-errors not always able to catch the correct error
          assert.equal(err.className, 'JsonqlForbiddenError', 'Not allow to call private method before login')
          done3()

          // test the login
          client.login('joel')
            .then(userdata => {
              // console.info('login result', userdata)
              assert.equal(userdata.name, 'joel', 'Expect the login stored userdata name to be the same as we supplied')
              done4()
              // test the mutation AGAIN
              client.plus({a: 'a', b: 'b'})
                .then(result => {
                  assert.equal(result, 'ab', 'We should now able to call private mutation method after login')
                  done5()
                })
                .catch(err => {
                  console.log('still not able to call private method?', err)
                })
              // @TODO test the logout method
            })
            // @TODO test the swith profile
        })
    })
})
