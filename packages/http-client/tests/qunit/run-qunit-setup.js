// this will grab the list of test files
// inject into the html and run the server
const glob = require('glob')
const { join, resolve } = require('path')
const serverIoCore = require('server-io-core')
const jsonqlKoaDir = join(__dirname, '..', 'fixtures')
const { jsonqlKoa } = require('jsonql-koa')
const fsx = require('fs-extra')

const publicJson = fsx.readJsonSync(join(jsonqlKoaDir, 'contracts', 'public-contract.json'))

/**
 * @param {object} config configuration
 * @return {object} promise resolve the config for server-io-core
 */
const getConfig = (config) => {
  const baseDir = join(config.baseDir, 'qunit', 'tests')
  // console.info('baseDir', baseDir, config.testFilePattern)
  return new Promise((resolver, rejecter) => {
    glob(join(baseDir, config.testFilePattern), function(err, files) {
      // console.info('files found', files)
      if (err || !files.length) {
        console.error('glob error', 'FAILED TO FETCH ANY TEST FILES!')
        return rejecter(err)
      }
      // now start the server
      let opts = {
        port: config.port,
        webroot: config.webroot,
        open: config.open,
        reload: config.reload,
        inject: {
          insertBefore: false,
          target: {
            body: files.map( file => file.replace(baseDir, '') )
          },
          replace: [
            {
              target: '<!--REPLACE WITH JSON-->',
              str: '<script>\n' +
              'var publicJson = ' +
              JSON.stringify(publicJson) +
              '\n</script>\n'
            }
          ]
        },
        middlewares: [
          jsonqlKoa({
            enableAuth: true,
            useJwt: true,
            keysDir: join(jsonqlKoaDir, 'keys'),
            resolverDir: join(jsonqlKoaDir, 'resolvers'),
            contractDir: join(jsonqlKoaDir, 'contracts')
          })
        ]
      }
      resolver(opts)
    })
  })
}
// export it
module.exports = async function runQunit(userConfig) {
  return getConfig(userConfig)
    .then(serverIoCore)
    .catch(err => {
      console.error('runQunit error', err)
    })
}
