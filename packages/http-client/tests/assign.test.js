// just testing the DIY assign
const test = require('ava')

const list = [
  ['query', 'helloWorld'],
  ['query', 'getSomething'],
  ['mutation', 'saveSomething']
]

test('It should able to merge this array together', t => {
  let obj = list.map(l => (
    { [l[0]]: [ l[1] ] }
  ))
  .reduce((first, next) => {
    return Object.assign(first, next)
  }, {})

  console.info(obj)

  t.pass()

})
