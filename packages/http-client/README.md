# jsonql-client

**We have made some major changes starting from the 1.6.0, the old README is completely obsoleted**

Please wait for the brand new README, and we will update the our main website [jsonql.org](https://jsonql.js.org) ASAP

---

Joel (2020-02-29)
