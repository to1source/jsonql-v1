/**
 * Rollup config
 */
import { join } from 'path';
import buble from 'rollup-plugin-buble';

import { terser } from "rollup-plugin-terser";
import replace from 'rollup-plugin-replace';
import commonjs from 'rollup-plugin-commonjs';

import json from 'rollup-plugin-json';

import nodeResolve from 'rollup-plugin-node-resolve';
import nodeGlobals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';
import size from 'rollup-plugin-bundle-size';
// support async functions
import async from 'rollup-plugin-async';
// get the version info
import { version } from './package.json';

const env = process.env.NODE_ENV;
const target = process.env.TARGET;

let plugins = [
  json({
    preferConst: true
  }),
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    mainFields: ['module', 'main', 'browser']
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__PLACEHOLDER__': `version: ${version} module: ${env==='prod' ? 'cjs' : 'umd'}`
  }),
  terser(),
  size()
]
let file;
let inFile = join(__dirname, 'index.js')
switch (target) {
  case 'cjs':
    file = join(__dirname, 'dist', 'jsonql-errors.cjs.js')
    break;
  case 'umd':
    file = join(__dirname, 'dist', 'jsonql-errors.umd.js')
    break;
  default:

    inFile = join(__dirname, 'src', 'general.js')
    file = join(__dirname, 'general.js')

}

console.log('Output file is %s', file)

let config = {
  input: inFile,
  output: {
    name: 'jsonqlErrors',
    file: file,
    format: env === 'prod' ? 'cjs' : 'umd',
    sourcemap: true,
    globals: {
      'promise-polyfill': 'Promise',
      'debug': 'debug'
    }
  },
  external: [
    'debug',
    'Promise',
    'promise-polyfill'
  ],
  plugins: plugins
};

export default config;
