/**
 * Rollup config for building the node.js version for jsonql-utils
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'
import { terser } from "rollup-plugin-terser"
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'
import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
// import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
import async from 'rollup-plugin-async'

import pkg from './package.json'

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    preferBuiltins: true
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  // builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__VERSION__': pkg.version
  })
]

let globals = {}
let external = [
  'debug',
  'fs',
  'path',
  'tty',
  'util',
  'os'
]

let sourceFile = 'index.js'
let distFile = 'main.js'
let type = 'cjs'

switch (env) {
  case 'BROWSER':
    sourceFile = 'module.js'
    distFile = 'browser.js'
    type = 'umd'
    plugins.push(terser())
  break;
  case 'CLIENT':
    sourceFile = 'module.js'
    distFile = join('..', '@jsonql', 'client', 'src', 'jsonql-utils.js')
    type = 'es'
  break;
  default:
    plugins.push(terser())
}
plugins.push(size())
let config = {
  input: join(__dirname, sourceFile),
  output: {
    name: 'jsonqlUtils',
    file: join(__dirname, distFile),
    format: type,
    sourcemap: env !== 'CLIENT',
    globals
  },
  plugins,
  external
}

export default config
