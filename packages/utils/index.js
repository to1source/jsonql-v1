// this is a new one that only include the browser usable modules
// and the for node only modules
// then the main will include this one instead, then when run the build script
// we won't get those missing node modules blah blah problems
import {
  // chain-fns
  chainFns,
  chainPromises,
  // contract
  extractArgsFromPayload,
  extractParamsFromContract,
  extractSocketPart,
  isContract, // alias
  // generic
  inArray,
  toArray,
  isObjectHasKey,
  dasherize,
  createEvt,
  timestamp,
  urlParams,
  cacheBurst,
  cacheBurstUrl,
  getConfigValue,
  isNotEmpty,
  toJson,
  parseJson,
  isFunc,
  nil,
  assign,
  freeze,
  // params-api
  toPayload,
  formatPayload,
  createQuery,
  createQueryStr,
  createMutation,
  createMutationStr,
  getQueryFromArgs,
  getQueryFromPayload,
  getMutationFromArgs,
  getMutationFromPayload,
  
  // objectDefineProp
  injectToFn,
  objDefineProps,
  objHasProp,
  // results
  getCallMethod,
  packResult,
  packError,
  resultHandler,
  isJsonqlErrorObj,

  VERSION,

  groupByNamespace,
  getNamespaceInOrder,
  getNamespace,
  getNspInfoByConfig,
  getResolverFromPayload,
  // 1.0.6
  isRegExp,
  getRegex,
  // 1.1.0
  createWsReply,
  createReplyMsg,
  createAcknowledgeMsg,
  isWsReply,
  extractWsPayload,
  createSendPayload
} from './module'
import { buff } from './src/jwt'
// node import
import { replaceErrors, printError } from './src/node-error'
import { findFromContract } from './src/node-find-from-contract'
import {
  isJsonqlPath,
  isJsonqlRequest,
  isJsonqlConsoleUrl
} from './src/jsonql-handlers'
import {
  getDocLen,
  headerParser,
  isHeaderPresent,
  getPathToFn
} from './src/node-middleware'

// export
export {
  // node export
  buff,
  findFromContract,
  // errors
  replaceErrors,
  printError,
  // koa
  isJsonqlPath,
  isJsonqlRequest,
  isJsonqlConsoleUrl,
  // middleware
  getDocLen,
  headerParser,
  isHeaderPresent,
  getCallMethod,
  getPathToFn,
  packResult,
  packError,
  resultHandler,
  isJsonqlErrorObj,
  // chain-fns
  chainFns,
  chainPromises,
  // contract
  extractArgsFromPayload,
  extractParamsFromContract,
  extractSocketPart,
  isContract, // alias
  // generic
  inArray,
  toArray,
  isObjectHasKey,
  dasherize,
  createEvt,
  timestamp,
  urlParams,
  cacheBurst,
  cacheBurstUrl,
  getConfigValue,
  isNotEmpty,
  toJson,
  parseJson,
  isFunc,
  nil,
  assign,
  freeze,
  // params-api
  toPayload,
  formatPayload,
  createQuery,
  createQueryStr,
  createMutation,
  createMutationStr,
  getQueryFromArgs,
  getQueryFromPayload,
  getMutationFromArgs,
  getMutationFromPayload,
  
  // objectDefineProp
  injectToFn,
  objDefineProps,
  objHasProp,

  VERSION,

  groupByNamespace,
  getNamespaceInOrder,
  getNamespace,
  getNspInfoByConfig,
  getResolverFromPayload,
  
  isRegExp,
  getRegex,

  createWsReply,
  createReplyMsg,
  createAcknowledgeMsg,
  isWsReply,
  extractWsPayload,
  createSendPayload
}
