// check options
const { jsonqlKoaOptions } = require('./options')
const { extendConfigCheck } = require('jsonql-koa')
// @TODO there will be the socket.io version or primus in the future
// therefore the merge options should be conditional
const { JS_WS_NAME, JS_WS_SOCKET_IO_NAME, JS_PRIMUS_NAME } = require('jsonql-constants')
const { inArray } = require('jsonql-utils')
// how to make this an optional module? 
const { wsServerDefaultOptions, wsServerConstProps } = require('jsonql-ws-server')
const extendOptions = {
  nil: {
    appProps: {},
    constProps: {}
  },
  [JS_WS_NAME]: {
    appProps: wsServerDefaultOptions,
    constProps: wsServerConstProps
  },
  [JS_WS_SOCKET_IO_NAME]: {
    appProps: {},
    constProps: {}
  },
  [JS_PRIMUS_NAME]: {
    appProps: {},
    constProps: {}
  }
}

const availableServers = [
  JS_WS_NAME 
  // JS_WS_SOCKET_IO_NAME
  // JS_PRIMUS_NAME add this in the future
]

/**
 * Just check the server type before we run the configuration
 * @param {object} config options 
 * @return {object} the serverType + options or empty objects when there is no serverType field  
 */
function getServerType(config) {
  const { serverType } = config
  if (serverType) {
    if (inArray(availableServers, serverType)) {
      return extendOptions[serverType]
    }
  }
  return extendOptions.nil 
}

/**
 * the default api to check and init the config
 * @param {object} config user supply configuration
 * @return {object} configuration
 */
function checkOptions(config) {
  // @TODO here is a catch 22, if we take the serverType out, then we double check it again ... 
  const { appProps, constProps } = getServerType(config)

  // first create a combine options
  return extendConfigCheck(
    Object.assign({}, jsonqlKoaOptions, appProps),
    constProps,
    config
  )
}

module.exports = {
  checkOptions,
  getServerType
}
