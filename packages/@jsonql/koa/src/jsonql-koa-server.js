// main
const debug = require('debug')('jsonql-koa:jsonql-koa-server')
const { checkOptions } = require('./options')
const { initServer } = require('./init-server')
const { version } = require('../package.json')

/**
 * We need to create this as a class to protect the property
 * @param {object} config options
 * @return {object} with several method to control the server
 */
class JsonqlKoaServer {

  constructor(config = {}, middlewares = []) {
    // keep a copy of the original config - do we need this anymore?
    // const originalConfig = Object.assign({}, config)
    this.opts = checkOptions(config)
    debug('JsonqlKoaServer.opts', this.opts)
    const { server, app, ws } = initServer(this.opts, middlewares)
    this.server = server
    this.app = app
    this.ws = ws
    this.started = false
    if (this.opts.autoStart && this.opts.port) {
      this.start()
    }
  }

  // start the server
  start(port = null) {
    if (!this.started) {
      console.info(`[jsonql Koa server] version: ${version} start on ${port || this.opts.port}`)
      this.server.listen(port || this.opts.port)
      this.started = true
    }
  }

  // stop the server
  stop() {
    this.server.close()
  }
}

// default export
module.exports = JsonqlKoaServer
