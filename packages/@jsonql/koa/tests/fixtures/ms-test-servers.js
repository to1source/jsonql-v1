// server for the ms test only
const jsonqlKoaServer = require('../../index')
const { join } = require('path')
const {
  resolverDir,
  contractBaseDir,
  keysDir,
  AUTH_DIR,
  BASIC_DIR,
  SOCKET_DIR,
  MS_A_DIR,
  MS_B_DIR,
  MS_C_DIR,
  PORT_A,
  PORT_B,
  PORT_C
} = require('./options')

module.exports = function createMsTestServer(name, extraClientConfig = false) {
  let opts = { serverType: 'ws' }
  if (name === 'A' || name === 'A1') { // use all the default options here
    let cbd = join(contractBaseDir, MS_A_DIR)
    opts.resolverDir = resolverDir
    opts.contractDir = cbd
    opts.port = name === 'A1' ? PORT_A + 5 : PORT_A
    opts.name = 'serverA'
    opts.clientConfig = [{
      hostname: `http://localhost:${ name === 'A1' ? PORT_B + 5 : PORT_B }`,
      name: 'client0',
      serverType: 'ws',
      contractDir: join(cbd, 'client0')
    }]
    if (extraClientConfig) {
      opts.clientConfig.push(extraClientConfig)
    }
  } else if (name === 'C') {
    opts.resolverDir = join(__dirname, 'resolvers-ext')
    opts.contractDir = join(contractBaseDir, MS_C_DIR)
    opts.port = PORT_C
    opts.name = 'serverC'
  } else {
    opts.resolverDir = join(__dirname, 'resolvers-ext')
    opts.contractDir = join(contractBaseDir, MS_B_DIR)
    opts.port = name === 'B1' ? PORT_B + 5 : PORT_B
    opts.name = 'serverB'
  }
  return jsonqlKoaServer(opts)
}
