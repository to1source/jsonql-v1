// create server for testing
const jsonqlKoaServer = require('../../index')
const { join } = require('path')
const {
  resolverDir,
  contractBaseDir,
  keysDir,
  AUTH_DIR,
  BASIC_DIR,
  SOCKET_DIR
} = require('./options')

module.exports = function testServer(config = {}) {
  let { enableAuth, serverType } = config;
  let contractDir;
  switch (true) {
    case enableAuth && !serverType:
      contractDir = join(contractBaseDir, AUTH_DIR)
      break;
    case enableAuth && serverType:
      contractDir = join(contractBaseDir, SOCKET_DIR)
      break;
    default:
      contractDir = join(contractBaseDir, BASIC_DIR)
  }

  const baseConfig = {
    resolverDir,
    contractDir,
    keysDir
  }
  return jsonqlKoaServer(Object.assign(baseConfig, config))
}
