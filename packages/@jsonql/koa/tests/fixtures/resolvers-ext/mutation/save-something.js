/**
 * this is for a remote C server to call
 * @param {object} payload
 * @param {number} condition
 * @return {object} result
 */
module.exports = function saveSomething(payload, condition) {
  // just pack it up and return it
  return {
    payload,
    ctn: condition,
    timestamp: [Date.now()]
  }
}
