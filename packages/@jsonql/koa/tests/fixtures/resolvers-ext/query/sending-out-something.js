/**
 * This is publicly available server whenever connect just send out a random message
 * @return {string} a random message
 */
module.exports = function sendingOutSomething() {

  return `sending out message on ${Date.now()}`
}
