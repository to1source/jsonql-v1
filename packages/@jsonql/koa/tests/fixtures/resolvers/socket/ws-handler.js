// will test this one with the send property

/**
 * method just for testing the ws
 * @param {string} msg message
 * @param {number} timestamp timestamp
 * @return {string} msg + time lapsed
 */
module.exports = function wsHandler(msg, timestamp) {

  wsHandler.send = 'I am sending a message back from ws';

  return new Promise(resolver => {
    setTimeout(() => {
      resolver(msg + ' - ' +(Date.now() - timestamp))
    }, 1000)
  })
}
