const debug = require('debug')('jsonql-koa:resolvers:auth:login')
/**
 * login method
 * @param {string} name give it a name
 * @return {object} a userdata object with timestamp
 */
module.exports = function login(name) {
  debug(`Login got call with ${name}`)
  return { name, timestamp: Date.now() }
}
