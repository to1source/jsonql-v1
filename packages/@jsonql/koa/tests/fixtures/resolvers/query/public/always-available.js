/**
 * This is a public method that is always available
 * @return {string} a message
 */
module.exports = function alwaysAvailale() {
  return 'Hello there'
};
