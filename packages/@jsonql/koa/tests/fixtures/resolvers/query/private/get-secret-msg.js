// this resolver will not be include if the privateMethodDir is not set

/**
 * a hidden private method
 * @return {string} a secret message
 */
module.exports = function getSecretMsg() {
  return 'Let me tell ya a secret ...'
}
