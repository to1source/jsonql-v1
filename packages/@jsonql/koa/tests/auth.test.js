const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const debug = require('debug')('jsonql-koa:test:auth')
const jsonqlNodeClient = require('jsonql-node-client')
const { HELLO } = require('jsonql-constants')
const jsonqlKoaServer = require('./fixtures/test-server')
const { contractBaseDir, AUTH_DIR } = require('./fixtures/options')
const contractDir = join(contractBaseDir, AUTH_DIR)
const port = 8082

test.before(t => {
  const jsonqlKoaInt = jsonqlKoaServer({
    port: port,
    autoStart: true,
    enableAuth: true
  })

  t.context.stop = () => jsonqlKoaInt.stop()
})

test.after( t => {
  t.context.stop()
  fsx.removeSync(contractDir)
  fsx.removeSync(join(__dirname, 'fixtures', 'keys'))
})

test.beforeEach(async t => {
  t.context.client = await jsonqlNodeClient({
    hostname: `http://localhost:${port}`,
    enableAuth: true,
    contractDir
  })
})

test.serial(`It should able to setup a auth server`, async t => {
  const client = t.context.client
  const result = await client.helloWorld()
  t.is(result, HELLO)
})

test.serial(`It should not able to connect to a private method`, async t => {
  const client = t.context.client
  const error = await t.throwsAsync(client.getSecretMsg())
  // debug(error)
	t.true(error.message.indexOf('JsonqlForbiddenError') > -1)
})

test.serial.cb(`It should able to login`, t => {
  t.plan(2)
  const client = t.context.client
  const name = 'Joel'
  client.login(name)
    .then(result => {
      debug('login result', result)
      const userdata = client.userdata()
      t.is(userdata.name, name)

      client.getSecretMsg()
        .then(result => {
          t.is(result, 'Let me tell ya a secret ...')
          t.end()
        })

    })
    .catch(err => {
      debug('login error', err)
    })
})
