// this is an experiemental generic module for vuex
import { jsonqlStaticClient } from '../static'

/**
 * @param {string} str to upper case first word
 * @return {string} uppercase first word string
 */
const ucword = str => (
  str.substr(0, 1).toUpperCase() + str.substr(1, str.length - 1)
)

/**
 * create actions on the fly
 * @param {object} jsonqlClient static version
 * @param {object} config the configuration object
 * @return {array} actions, names
 */
function getActions(jsonqlClient, config) {
  const { prefix, contract, namespaced } = config // could be undefined but we want false boolean
  const availableTypes = ['query', 'mutation', 'auth']
  let actions = {}
  let names = {}
  for (let name in contract) {
    if (availableTypes.indexOf(name) > -1) {
      for (let resolverName in contract[name]) {
        let actionName = prefix === true ? name + ucword(resolverName) : resolverName
        // keep the name for export
        names[actionName] = null
        let clientInstance = namespaced === true ? jsonqlClient[name] : jsonqlClient
        // define actions
        actions[actionName] = (ctx, ...args) => {
          return Reflect.apply(clientInstance[resolverName], jsonqlClient, args)
            .then(result => {
              ctx.commit('addToResult', {name: actionName, result})
              return result
            })
            .catch(error => {
              ctx.commit('addToError', {name: actionName, error})
              throw new Error(error)
            })
        }
      }
    }
  }
  return [ actions, names ]
}

/**
 * We export a function to generate the relevant code
 * @param {object} Fly the fly object for http request
 * @param {object} config configuration with the contract
 * @return {object} the Vuex store object
 */
function getJsonqlVuexModule(Fly, config) {
  const jsonqlClient = jsonqlStaticClient(Fly, config)
  const [ actions, names ] = getActions(jsonqlClient, config)
  // just create the structure on the fly
  return {
    namespaced: true,
    state: {
      result: names,
      error: names
    },
    mutations: {
      addToResult(state, { name, result }) {
        state.result[name] = result
      },
      addToError(state, { name, error}) {
        state.error[name] = error
      }
    },
    // because jsonql are all async call,
    // every resolver will be warp in as action
    actions: actions,
    getters: {
      getJsonqlResult: (state) => (name) => {
        return state.result[name]
      }
    }
  }
}

export { getJsonqlVuexModule, getActions }
