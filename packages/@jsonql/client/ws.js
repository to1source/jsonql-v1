// this is the jsonql client with ws
// use like import jsonqlClient from '@jsonql/client/ws'
import { getJsonqlClient, initSocketClient } from './src'
import jsonqlWsClient from '@jsonql/ws'
import {
  jsonqlWsDefaultConstProps,
  jsonqlWsDefaultOptions
} from 'jsonql-ws-client-core'

/**
 * The top API to get the jsonql client with WebSocket client
 * @param {object} fly the fly instance NOT the Fly class itself
 * @param {object} [config={}] developer supply options
 * @return {object} the jsonql browser client with ws socket
 * @public
 */
export default function createJsonqlHttpWsClient(fly, config = {}) {
  // get the client generator function
  const clientGenFn = getJsonqlClient(fly, jsonqlWsDefaultOptions, {})
  // now run it with the config input
  return clientGenFn(config)
    .then(({client, contract, opts}) => {
      // console.info('contract', contract)
      // console.info('opts', opts)
      // return client
      const fn = initSocketClient(jsonqlWsClient)
      return fn(client, contract, opts)
    })
}
