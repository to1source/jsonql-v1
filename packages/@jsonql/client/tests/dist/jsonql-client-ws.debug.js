(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = global || self, global.jsonqlClientWs = factory());
}(this, (function () { 'use strict';

  // the core stuff to id if it's calling with jsonql
  var DATA_KEY = 'data';
  var ERROR_KEY = 'error';

  var JSONQL_PATH = 'jsonql';
  // according to the json query spec
  var CONTENT_TYPE = 'application/vnd.api+json';
  var CHARSET = 'charset=utf-8';
  var DEFAULT_HEADER = {
    'Accept': CONTENT_TYPE,
    'Content-Type': [ CONTENT_TYPE, CHARSET ].join(';')
  };

  // export const INDEX = 'index'; use INDEX_KEY instead
  var DEFAULT_TYPE = 'any';

  // @TODO remove this is not in use
  // export const CLIENT_CONFIG_FILE = '.clients.json';
  // export const CONTRACT_CONFIG_FILE = 'jsonql-contract-config.js';
  // type of resolvers
  var QUERY_NAME = 'query';
  var MUTATION_NAME = 'mutation';
  var SOCKET_NAME = 'socket';
  // for calling the mutation
  var PAYLOAD_PARAM_NAME = 'payload'; // @TODO shortern them
  var CONDITION_PARAM_NAME = 'condition';
  var QUERY_ARG_NAME = 'args';
  var TIMESTAMP_PARAM_NAME = 'TS';
  // new jsonp
  var JSONP_CALLBACK_NAME = 'jsonqlJsonpCallback';

  // methods allow
  var API_REQUEST_METHODS = ['POST', 'PUT'];
  // for  contract-cli
  var KEY_WORD = 'continue';

  var TYPE_KEY = 'type';
  var OPTIONAL_KEY = 'optional';
  var ENUM_KEY = 'enumv';  // need to change this because enum is a reserved word
  var ARGS_KEY = 'args';
  var CHECKER_KEY = 'checker';
  var ALIAS_KEY = 'alias';
  var CHECKED_KEY = '__checked__';
  var LOGIN_NAME = 'login';
  var ISSUER_NAME = LOGIN_NAME; // legacy issue need to replace them later
  var LOGOUT_NAME = 'logout';

  var AUTH_HEADER = 'Authorization';
  var BEARER = 'Bearer';

  // for client use @TODO need to clean this up some of them are not in use
  var CREDENTIAL_STORAGE_KEY = 'jsonqlcredential';
  var CLIENT_STORAGE_KEY = 'jsonqlstore';
  var CLIENT_AUTH_KEY = 'jsonqlauthkey';
  // contract key
  var CONTRACT_KEY_NAME = 'X-JSONQL-CV-KEY';
  var SHOW_CONTRACT_DESC_PARAM = {desc: 'y'};

  var OR_SEPERATOR = '|';

  var STRING_TYPE = 'string';
  var BOOLEAN_TYPE = 'boolean';
  var ARRAY_TYPE = 'array';
  var OBJECT_TYPE = 'object';

  var NUMBER_TYPE = 'number';
  var ARRAY_TYPE_LFT = 'array.<';
  var ARRAY_TYPE_RGT = '>';

  var NO_ERROR_MSG = 'No message';
  var NO_STATUS_CODE = -1;
  var LOGIN_EVENT_NAME = '__login__';
  var LOGOUT_EVENT_NAME = '__logout__';

  // for ws servers
  var WS_REPLY_TYPE = '__reply__';
  var WS_EVT_NAME = '__event__';
  var WS_DATA_NAME = '__data__';
  var EMIT_REPLY_TYPE = 'emit';
  var ACKNOWLEDGE_REPLY_TYPE = 'acknowledge';
  var ERROR_TYPE = 'error';

  var NSP_SET = 'nspSet';
  var PUBLIC_NAMESPACE = 'publicNamespace';

  var JS_WS_SOCKET_IO_NAME = 'socket.io';
  var JS_WS_NAME = 'ws';

  // for ws client
  var ON_MESSAGE_PROP_NAME = 'onMessage';
  var ON_RESULT_PROP_NAME = 'onResult';
  var ON_ERROR_PROP_NAME = 'onError';
  var ON_READY_PROP_NAME = 'onReady';
  var ON_LOGIN_PROP_NAME = 'onLogin'; // new @1.8.6

  var SEND_MSG_PROP_NAME = 'send';
  // breaking change from 1.8.8 the above name was the name below
  // the names are using in the new resolver.on getter interface
  var MESSAGE_PROP_NAME = 'message';
  var RESULT_PROP_NAME = 'result';
  var ERROR_PROP_NAME = 'error';
  var READY_PROP_NAME = 'ready';
  var LOGIN_PROP_NAME = 'login'; // new @1.8.6
  var NOT_LOGIN_ERR_MSG = 'NOT LOGIN';
  var HSA_ALGO = 'HS256';
  var TOKEN_PARAM_NAME = 'token';
  var IO_ROUNDTRIP_LOGIN = 'roundtip';
  var IO_HANDSHAKE_LOGIN = 'handshake';

  /**
   * This is a custom error to throw when server throw a 406
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var Jsonql406Error = /*@__PURE__*/(function (Error) {
    function Jsonql406Error() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];
      // We can't access the static name from an instance
      // but we can do it like this
      this.className = Jsonql406Error.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, Jsonql406Error);
      }
    }

    if ( Error ) Jsonql406Error.__proto__ = Error;
    Jsonql406Error.prototype = Object.create( Error && Error.prototype );
    Jsonql406Error.prototype.constructor = Jsonql406Error;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return 406;
    };

    staticAccessors.name.get = function () {
      return 'Jsonql406Error';
    };

    Object.defineProperties( Jsonql406Error, staticAccessors );

    return Jsonql406Error;
  }(Error));

  /**
   * This is a custom error to throw when server throw a 500
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var Jsonql500Error = /*@__PURE__*/(function (Error) {
    function Jsonql500Error() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = Jsonql500Error.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, Jsonql500Error);
      }
    }

    if ( Error ) Jsonql500Error.__proto__ = Error;
    Jsonql500Error.prototype = Object.create( Error && Error.prototype );
    Jsonql500Error.prototype.constructor = Jsonql500Error;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return 500;
    };

    staticAccessors.name.get = function () {
      return 'Jsonql500Error';
    };

    Object.defineProperties( Jsonql500Error, staticAccessors );

    return Jsonql500Error;
  }(Error));

  /**
   * this is the 403 Forbidden error
   * that means this user is not login
   * use the 401 for try to login and failed
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlForbiddenError = /*@__PURE__*/(function (Error) {
    function JsonqlForbiddenError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlForbiddenError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlForbiddenError);
      }
    }

    if ( Error ) JsonqlForbiddenError.__proto__ = Error;
    JsonqlForbiddenError.prototype = Object.create( Error && Error.prototype );
    JsonqlForbiddenError.prototype.constructor = JsonqlForbiddenError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return 403;
    };

    staticAccessors.name.get = function () {
      return 'JsonqlForbiddenError';
    };

    Object.defineProperties( JsonqlForbiddenError, staticAccessors );

    return JsonqlForbiddenError;
  }(Error));

  /**
   * This is a custom error to throw when pass credential but fail
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlAuthorisationError = /*@__PURE__*/(function (Error) {
    function JsonqlAuthorisationError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlAuthorisationError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlAuthorisationError);
      }
    }

    if ( Error ) JsonqlAuthorisationError.__proto__ = Error;
    JsonqlAuthorisationError.prototype = Object.create( Error && Error.prototype );
    JsonqlAuthorisationError.prototype.constructor = JsonqlAuthorisationError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return 401;
    };

    staticAccessors.name.get = function () {
      return 'JsonqlAuthorisationError';
    };

    Object.defineProperties( JsonqlAuthorisationError, staticAccessors );

    return JsonqlAuthorisationError;
  }(Error));

  /**
   * This is a custom error when not supply the credential and try to get contract
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlContractAuthError = /*@__PURE__*/(function (Error) {
    function JsonqlContractAuthError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlContractAuthError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlContractAuthError);
      }
    }

    if ( Error ) JsonqlContractAuthError.__proto__ = Error;
    JsonqlContractAuthError.prototype = Object.create( Error && Error.prototype );
    JsonqlContractAuthError.prototype.constructor = JsonqlContractAuthError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return 401;
    };

    staticAccessors.name.get = function () {
      return 'JsonqlContractAuthError';
    };

    Object.defineProperties( JsonqlContractAuthError, staticAccessors );

    return JsonqlContractAuthError;
  }(Error));

  /**
   * This is a custom error to throw when the resolver throw error and capture inside the middleware
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlResolverAppError = /*@__PURE__*/(function (Error) {
    function JsonqlResolverAppError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlResolverAppError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlResolverAppError);
      }
    }

    if ( Error ) JsonqlResolverAppError.__proto__ = Error;
    JsonqlResolverAppError.prototype = Object.create( Error && Error.prototype );
    JsonqlResolverAppError.prototype.constructor = JsonqlResolverAppError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return 500;
    };

    staticAccessors.name.get = function () {
      return 'JsonqlResolverAppError';
    };

    Object.defineProperties( JsonqlResolverAppError, staticAccessors );

    return JsonqlResolverAppError;
  }(Error));

  /**
   * This is a custom error to throw when could not find the resolver
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlResolverNotFoundError = /*@__PURE__*/(function (Error) {
    function JsonqlResolverNotFoundError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlResolverNotFoundError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlResolverNotFoundError);
      }
    }

    if ( Error ) JsonqlResolverNotFoundError.__proto__ = Error;
    JsonqlResolverNotFoundError.prototype = Object.create( Error && Error.prototype );
    JsonqlResolverNotFoundError.prototype.constructor = JsonqlResolverNotFoundError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return 404;
    };

    staticAccessors.name.get = function () {
      return 'JsonqlResolverNotFoundError';
    };

    Object.defineProperties( JsonqlResolverNotFoundError, staticAccessors );

    return JsonqlResolverNotFoundError;
  }(Error));

  // this get throw from within the checkOptions when run through the enum failed
  var JsonqlEnumError = /*@__PURE__*/(function (Error) {
    function JsonqlEnumError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlEnumError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlEnumError);
      }
    }

    if ( Error ) JsonqlEnumError.__proto__ = Error;
    JsonqlEnumError.prototype = Object.create( Error && Error.prototype );
    JsonqlEnumError.prototype.constructor = JsonqlEnumError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlEnumError';
    };

    Object.defineProperties( JsonqlEnumError, staticAccessors );

    return JsonqlEnumError;
  }(Error));

  // this will throw from inside the checkOptions
  var JsonqlTypeError = /*@__PURE__*/(function (Error) {
    function JsonqlTypeError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlTypeError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlTypeError);
      }
    }

    if ( Error ) JsonqlTypeError.__proto__ = Error;
    JsonqlTypeError.prototype = Object.create( Error && Error.prototype );
    JsonqlTypeError.prototype.constructor = JsonqlTypeError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlTypeError';
    };

    Object.defineProperties( JsonqlTypeError, staticAccessors );

    return JsonqlTypeError;
  }(Error));

  // allow supply a custom checker function
  // if that failed then we throw this error
  var JsonqlCheckerError = /*@__PURE__*/(function (Error) {
    function JsonqlCheckerError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlCheckerError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlCheckerError);
      }
    }

    if ( Error ) JsonqlCheckerError.__proto__ = Error;
    JsonqlCheckerError.prototype = Object.create( Error && Error.prototype );
    JsonqlCheckerError.prototype.constructor = JsonqlCheckerError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlCheckerError';
    };

    Object.defineProperties( JsonqlCheckerError, staticAccessors );

    return JsonqlCheckerError;
  }(Error));

  // custom validation error class
  // when validaton failed
  var JsonqlValidationError = /*@__PURE__*/(function (Error) {
    function JsonqlValidationError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlValidationError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlValidationError);
      }
    }

    if ( Error ) JsonqlValidationError.__proto__ = Error;
    JsonqlValidationError.prototype = Object.create( Error && Error.prototype );
    JsonqlValidationError.prototype.constructor = JsonqlValidationError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlValidationError';
    };

    Object.defineProperties( JsonqlValidationError, staticAccessors );

    return JsonqlValidationError;
  }(Error));

  /**
   * This is a custom error to throw whenever a error happen inside the jsonql
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlError = /*@__PURE__*/(function (Error) {
    function JsonqlError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlError);
        // this.detail = this.stack;
      }
    }

    if ( Error ) JsonqlError.__proto__ = Error;
    JsonqlError.prototype = Object.create( Error && Error.prototype );
    JsonqlError.prototype.constructor = JsonqlError;

    var staticAccessors = { name: { configurable: true },statusCode: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlError';
    };

    staticAccessors.statusCode.get = function () {
      return NO_STATUS_CODE;
    };

    Object.defineProperties( JsonqlError, staticAccessors );

    return JsonqlError;
  }(Error));

  // this is from an example from Koa team to use for internal middleware ctx.throw
  // but after the test the res.body part is unable to extract the required data
  // I keep this one here for future reference

  var JsonqlServerError = /*@__PURE__*/(function (Error) {
    function JsonqlServerError(statusCode, message) {
      Error.call(this, message);
      this.statusCode = statusCode;
      this.className = JsonqlServerError.name;
    }

    if ( Error ) JsonqlServerError.__proto__ = Error;
    JsonqlServerError.prototype = Object.create( Error && Error.prototype );
    JsonqlServerError.prototype.constructor = JsonqlServerError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlServerError';
    };

    Object.defineProperties( JsonqlServerError, staticAccessors );

    return JsonqlServerError;
  }(Error));

  // server side

  var errors = /*#__PURE__*/Object.freeze({
    __proto__: null,
    Jsonql406Error: Jsonql406Error,
    Jsonql500Error: Jsonql500Error,
    JsonqlForbiddenError: JsonqlForbiddenError,
    JsonqlAuthorisationError: JsonqlAuthorisationError,
    JsonqlContractAuthError: JsonqlContractAuthError,
    JsonqlResolverAppError: JsonqlResolverAppError,
    JsonqlResolverNotFoundError: JsonqlResolverNotFoundError,
    JsonqlEnumError: JsonqlEnumError,
    JsonqlTypeError: JsonqlTypeError,
    JsonqlCheckerError: JsonqlCheckerError,
    JsonqlValidationError: JsonqlValidationError,
    JsonqlError: JsonqlError,
    JsonqlServerError: JsonqlServerError
  });

  // this will add directly to the then call in each http call
  var JsonqlError$1 = JsonqlError;

  /**
   * We can not just check something like result.data what if the result if false?
   * @param {object} obj the result object
   * @param {string} key we want to check if its exist or not
   * @return {boolean} true on found
   */
  var isObjectHasKey = function (obj, key) {
    var keys = Object.keys(obj);
    return !!keys.filter(function (k) { return key === k; }).length;
  };

  /**
   * It will ONLY have our own jsonql specific implement check
   * @param {object} result the server return result
   * @return {object} this will just throw error
   */
  function clientErrorsHandler(result) {
    if (isObjectHasKey(result, 'error')) {
      var error = result.error;
      var className = error.className;
      var name = error.name;
      var errorName = className || name;
      // just throw the whole thing back
      var msg = error.message || NO_ERROR_MSG;
      var detail = error.detail || error;
      if (errorName && errors[errorName]) {
        throw new errors[className](msg, detail)
      }
      throw new JsonqlError$1(msg, detail)
    }
    // pass through to the next
    return result;
  }

  /**
   * this will put into generator call at the very end and catch
   * the error throw from inside then throw again
   * this is necessary because we split calls inside and the throw
   * will not reach the actual client unless we do it this way
   * @param {object} e Error
   * @return {void} just throw
   */
  function finalCatch(e) {
    // this is a hack to get around the validateAsync not actually throw error
    // instead it just rejected it with the array of failed parameters
    if (Array.isArray(e)) {
      // if we want the message then I will have to create yet another function
      // to wrap this function to provide the name prop
      throw new JsonqlValidationError('', e)
    }
    var msg = e.message || NO_ERROR_MSG;
    var detail = e.detail || e;
    // @BUG the instance of not always work for some reason!
    // need to figure out a better way to find out the type of the error
    switch (true) {
      case e instanceof Jsonql406Error:
        throw new Jsonql406Error(msg, detail)
      case e instanceof Jsonql500Error:
        throw new Jsonql500Error(msg, detail)
      case e instanceof JsonqlForbiddenError:
        throw new JsonqlForbiddenError(msg, detail)
      case e instanceof JsonqlAuthorisationError:
        throw new JsonqlAuthorisationError(msg, detail)
      case e instanceof JsonqlContractAuthError:
        throw new JsonqlContractAuthError(msg, detail)
      case e instanceof JsonqlResolverAppError:
        throw new JsonqlResolverAppError(msg, detail)
      case e instanceof JsonqlResolverNotFoundError:
        throw new JsonqlResolverNotFoundError(msg, detail)
      case e instanceof JsonqlEnumError:
        throw new JsonqlEnumError(msg, detail)
      case e instanceof JsonqlTypeError:
        throw new JsonqlTypeError(msg, detail)
      case e instanceof JsonqlCheckerError:
        throw new JsonqlCheckerError(msg, detail)
      case e instanceof JsonqlValidationError:
        throw new JsonqlValidationError(msg, detail)
      case e instanceof JsonqlServerError:
        throw new JsonqlServerError(msg, detail)
      default:
        throw new JsonqlError(msg, detail)
    }
  }

  /**
   * Checks if `value` is classified as an `Array` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an array, else `false`.
   * @example
   *
   * _.isArray([1, 2, 3]);
   * // => true
   *
   * _.isArray(document.body.children);
   * // => false
   *
   * _.isArray('abc');
   * // => false
   *
   * _.isArray(_.noop);
   * // => false
   */
  var isArray = Array.isArray;

  var global$1 = (typeof global !== "undefined" ? global :
              typeof self !== "undefined" ? self :
              typeof window !== "undefined" ? window : {});

  /** Detect free variable `global` from Node.js. */
  var freeGlobal = typeof global$1 == 'object' && global$1 && global$1.Object === Object && global$1;

  /** Detect free variable `self`. */
  var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

  /** Used as a reference to the global object. */
  var root = freeGlobal || freeSelf || Function('return this')();

  /** Built-in value references. */
  var Symbol = root.Symbol;

  /** Used for built-in method references. */
  var objectProto = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty = objectProto.hasOwnProperty;

  /**
   * Used to resolve the
   * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
   * of values.
   */
  var nativeObjectToString = objectProto.toString;

  /** Built-in value references. */
  var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

  /**
   * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
   *
   * @private
   * @param {*} value The value to query.
   * @returns {string} Returns the raw `toStringTag`.
   */
  function getRawTag(value) {
    var isOwn = hasOwnProperty.call(value, symToStringTag),
        tag = value[symToStringTag];

    try {
      value[symToStringTag] = undefined;
      var unmasked = true;
    } catch (e) {}

    var result = nativeObjectToString.call(value);
    if (unmasked) {
      if (isOwn) {
        value[symToStringTag] = tag;
      } else {
        delete value[symToStringTag];
      }
    }
    return result;
  }

  /** Used for built-in method references. */
  var objectProto$1 = Object.prototype;

  /**
   * Used to resolve the
   * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
   * of values.
   */
  var nativeObjectToString$1 = objectProto$1.toString;

  /**
   * Converts `value` to a string using `Object.prototype.toString`.
   *
   * @private
   * @param {*} value The value to convert.
   * @returns {string} Returns the converted string.
   */
  function objectToString(value) {
    return nativeObjectToString$1.call(value);
  }

  /** `Object#toString` result references. */
  var nullTag = '[object Null]',
      undefinedTag = '[object Undefined]';

  /** Built-in value references. */
  var symToStringTag$1 = Symbol ? Symbol.toStringTag : undefined;

  /**
   * The base implementation of `getTag` without fallbacks for buggy environments.
   *
   * @private
   * @param {*} value The value to query.
   * @returns {string} Returns the `toStringTag`.
   */
  function baseGetTag(value) {
    if (value == null) {
      return value === undefined ? undefinedTag : nullTag;
    }
    return (symToStringTag$1 && symToStringTag$1 in Object(value))
      ? getRawTag(value)
      : objectToString(value);
  }

  /**
   * Creates a unary function that invokes `func` with its argument transformed.
   *
   * @private
   * @param {Function} func The function to wrap.
   * @param {Function} transform The argument transform.
   * @returns {Function} Returns the new function.
   */
  function overArg(func, transform) {
    return function(arg) {
      return func(transform(arg));
    };
  }

  /** Built-in value references. */
  var getPrototype = overArg(Object.getPrototypeOf, Object);

  /**
   * Checks if `value` is object-like. A value is object-like if it's not `null`
   * and has a `typeof` result of "object".
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
   * @example
   *
   * _.isObjectLike({});
   * // => true
   *
   * _.isObjectLike([1, 2, 3]);
   * // => true
   *
   * _.isObjectLike(_.noop);
   * // => false
   *
   * _.isObjectLike(null);
   * // => false
   */
  function isObjectLike(value) {
    return value != null && typeof value == 'object';
  }

  /** `Object#toString` result references. */
  var objectTag = '[object Object]';

  /** Used for built-in method references. */
  var funcProto = Function.prototype,
      objectProto$2 = Object.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString = funcProto.toString;

  /** Used to check objects for own properties. */
  var hasOwnProperty$1 = objectProto$2.hasOwnProperty;

  /** Used to infer the `Object` constructor. */
  var objectCtorString = funcToString.call(Object);

  /**
   * Checks if `value` is a plain object, that is, an object created by the
   * `Object` constructor or one with a `[[Prototype]]` of `null`.
   *
   * @static
   * @memberOf _
   * @since 0.8.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
   * @example
   *
   * function Foo() {
   *   this.a = 1;
   * }
   *
   * _.isPlainObject(new Foo);
   * // => false
   *
   * _.isPlainObject([1, 2, 3]);
   * // => false
   *
   * _.isPlainObject({ 'x': 0, 'y': 0 });
   * // => true
   *
   * _.isPlainObject(Object.create(null));
   * // => true
   */
  function isPlainObject(value) {
    if (!isObjectLike(value) || baseGetTag(value) != objectTag) {
      return false;
    }
    var proto = getPrototype(value);
    if (proto === null) {
      return true;
    }
    var Ctor = hasOwnProperty$1.call(proto, 'constructor') && proto.constructor;
    return typeof Ctor == 'function' && Ctor instanceof Ctor &&
      funcToString.call(Ctor) == objectCtorString;
  }

  /**
   * A specialized version of `_.map` for arrays without support for iteratee
   * shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns the new mapped array.
   */
  function arrayMap(array, iteratee) {
    var index = -1,
        length = array == null ? 0 : array.length,
        result = Array(length);

    while (++index < length) {
      result[index] = iteratee(array[index], index, array);
    }
    return result;
  }

  /** `Object#toString` result references. */
  var symbolTag = '[object Symbol]';

  /**
   * Checks if `value` is classified as a `Symbol` primitive or object.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
   * @example
   *
   * _.isSymbol(Symbol.iterator);
   * // => true
   *
   * _.isSymbol('abc');
   * // => false
   */
  function isSymbol(value) {
    return typeof value == 'symbol' ||
      (isObjectLike(value) && baseGetTag(value) == symbolTag);
  }

  /** Used as references for various `Number` constants. */
  var INFINITY = 1 / 0;

  /** Used to convert symbols to primitives and strings. */
  var symbolProto = Symbol ? Symbol.prototype : undefined,
      symbolToString = symbolProto ? symbolProto.toString : undefined;

  /**
   * The base implementation of `_.toString` which doesn't convert nullish
   * values to empty strings.
   *
   * @private
   * @param {*} value The value to process.
   * @returns {string} Returns the string.
   */
  function baseToString(value) {
    // Exit early for strings to avoid a performance hit in some environments.
    if (typeof value == 'string') {
      return value;
    }
    if (isArray(value)) {
      // Recursively convert values (susceptible to call stack limits).
      return arrayMap(value, baseToString) + '';
    }
    if (isSymbol(value)) {
      return symbolToString ? symbolToString.call(value) : '';
    }
    var result = (value + '');
    return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
  }

  /**
   * The base implementation of `_.slice` without an iteratee call guard.
   *
   * @private
   * @param {Array} array The array to slice.
   * @param {number} [start=0] The start position.
   * @param {number} [end=array.length] The end position.
   * @returns {Array} Returns the slice of `array`.
   */
  function baseSlice(array, start, end) {
    var index = -1,
        length = array.length;

    if (start < 0) {
      start = -start > length ? 0 : (length + start);
    }
    end = end > length ? length : end;
    if (end < 0) {
      end += length;
    }
    length = start > end ? 0 : ((end - start) >>> 0);
    start >>>= 0;

    var result = Array(length);
    while (++index < length) {
      result[index] = array[index + start];
    }
    return result;
  }

  /**
   * Casts `array` to a slice if it's needed.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {number} start The start position.
   * @param {number} [end=array.length] The end position.
   * @returns {Array} Returns the cast slice.
   */
  function castSlice(array, start, end) {
    var length = array.length;
    end = end === undefined ? length : end;
    return (!start && end >= length) ? array : baseSlice(array, start, end);
  }

  /**
   * The base implementation of `_.findIndex` and `_.findLastIndex` without
   * support for iteratee shorthands.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {Function} predicate The function invoked per iteration.
   * @param {number} fromIndex The index to search from.
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseFindIndex(array, predicate, fromIndex, fromRight) {
    var length = array.length,
        index = fromIndex + (fromRight ? 1 : -1);

    while ((fromRight ? index-- : ++index < length)) {
      if (predicate(array[index], index, array)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.isNaN` without support for number objects.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
   */
  function baseIsNaN(value) {
    return value !== value;
  }

  /**
   * A specialized version of `_.indexOf` which performs strict equality
   * comparisons of values, i.e. `===`.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function strictIndexOf(array, value, fromIndex) {
    var index = fromIndex - 1,
        length = array.length;

    while (++index < length) {
      if (array[index] === value) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseIndexOf(array, value, fromIndex) {
    return value === value
      ? strictIndexOf(array, value, fromIndex)
      : baseFindIndex(array, baseIsNaN, fromIndex);
  }

  /**
   * Used by `_.trim` and `_.trimEnd` to get the index of the last string symbol
   * that is not found in the character symbols.
   *
   * @private
   * @param {Array} strSymbols The string symbols to inspect.
   * @param {Array} chrSymbols The character symbols to find.
   * @returns {number} Returns the index of the last unmatched string symbol.
   */
  function charsEndIndex(strSymbols, chrSymbols) {
    var index = strSymbols.length;

    while (index-- && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
    return index;
  }

  /**
   * Used by `_.trim` and `_.trimStart` to get the index of the first string symbol
   * that is not found in the character symbols.
   *
   * @private
   * @param {Array} strSymbols The string symbols to inspect.
   * @param {Array} chrSymbols The character symbols to find.
   * @returns {number} Returns the index of the first unmatched string symbol.
   */
  function charsStartIndex(strSymbols, chrSymbols) {
    var index = -1,
        length = strSymbols.length;

    while (++index < length && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
    return index;
  }

  /**
   * Converts an ASCII `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function asciiToArray(string) {
    return string.split('');
  }

  /** Used to compose unicode character classes. */
  var rsAstralRange = '\\ud800-\\udfff',
      rsComboMarksRange = '\\u0300-\\u036f',
      reComboHalfMarksRange = '\\ufe20-\\ufe2f',
      rsComboSymbolsRange = '\\u20d0-\\u20ff',
      rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
      rsVarRange = '\\ufe0e\\ufe0f';

  /** Used to compose unicode capture groups. */
  var rsZWJ = '\\u200d';

  /** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
  var reHasUnicode = RegExp('[' + rsZWJ + rsAstralRange  + rsComboRange + rsVarRange + ']');

  /**
   * Checks if `string` contains Unicode symbols.
   *
   * @private
   * @param {string} string The string to inspect.
   * @returns {boolean} Returns `true` if a symbol is found, else `false`.
   */
  function hasUnicode(string) {
    return reHasUnicode.test(string);
  }

  /** Used to compose unicode character classes. */
  var rsAstralRange$1 = '\\ud800-\\udfff',
      rsComboMarksRange$1 = '\\u0300-\\u036f',
      reComboHalfMarksRange$1 = '\\ufe20-\\ufe2f',
      rsComboSymbolsRange$1 = '\\u20d0-\\u20ff',
      rsComboRange$1 = rsComboMarksRange$1 + reComboHalfMarksRange$1 + rsComboSymbolsRange$1,
      rsVarRange$1 = '\\ufe0e\\ufe0f';

  /** Used to compose unicode capture groups. */
  var rsAstral = '[' + rsAstralRange$1 + ']',
      rsCombo = '[' + rsComboRange$1 + ']',
      rsFitz = '\\ud83c[\\udffb-\\udfff]',
      rsModifier = '(?:' + rsCombo + '|' + rsFitz + ')',
      rsNonAstral = '[^' + rsAstralRange$1 + ']',
      rsRegional = '(?:\\ud83c[\\udde6-\\uddff]){2}',
      rsSurrPair = '[\\ud800-\\udbff][\\udc00-\\udfff]',
      rsZWJ$1 = '\\u200d';

  /** Used to compose unicode regexes. */
  var reOptMod = rsModifier + '?',
      rsOptVar = '[' + rsVarRange$1 + ']?',
      rsOptJoin = '(?:' + rsZWJ$1 + '(?:' + [rsNonAstral, rsRegional, rsSurrPair].join('|') + ')' + rsOptVar + reOptMod + ')*',
      rsSeq = rsOptVar + reOptMod + rsOptJoin,
      rsSymbol = '(?:' + [rsNonAstral + rsCombo + '?', rsCombo, rsRegional, rsSurrPair, rsAstral].join('|') + ')';

  /** Used to match [string symbols](https://mathiasbynens.be/notes/javascript-unicode). */
  var reUnicode = RegExp(rsFitz + '(?=' + rsFitz + ')|' + rsSymbol + rsSeq, 'g');

  /**
   * Converts a Unicode `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function unicodeToArray(string) {
    return string.match(reUnicode) || [];
  }

  /**
   * Converts `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function stringToArray(string) {
    return hasUnicode(string)
      ? unicodeToArray(string)
      : asciiToArray(string);
  }

  /**
   * Converts `value` to a string. An empty string is returned for `null`
   * and `undefined` values. The sign of `-0` is preserved.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to convert.
   * @returns {string} Returns the converted string.
   * @example
   *
   * _.toString(null);
   * // => ''
   *
   * _.toString(-0);
   * // => '-0'
   *
   * _.toString([1, 2, 3]);
   * // => '1,2,3'
   */
  function toString(value) {
    return value == null ? '' : baseToString(value);
  }

  /** Used to match leading and trailing whitespace. */
  var reTrim = /^\s+|\s+$/g;

  /**
   * Removes leading and trailing whitespace or specified characters from `string`.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category String
   * @param {string} [string=''] The string to trim.
   * @param {string} [chars=whitespace] The characters to trim.
   * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
   * @returns {string} Returns the trimmed string.
   * @example
   *
   * _.trim('  abc  ');
   * // => 'abc'
   *
   * _.trim('-_-abc-_-', '_-');
   * // => 'abc'
   *
   * _.map(['  foo  ', '  bar  '], _.trim);
   * // => ['foo', 'bar']
   */
  function trim(string, chars, guard) {
    string = toString(string);
    if (string && (guard || chars === undefined)) {
      return string.replace(reTrim, '');
    }
    if (!string || !(chars = baseToString(chars))) {
      return string;
    }
    var strSymbols = stringToArray(string),
        chrSymbols = stringToArray(chars),
        start = charsStartIndex(strSymbols, chrSymbols),
        end = charsEndIndex(strSymbols, chrSymbols) + 1;

    return castSlice(strSymbols, start, end).join('');
  }

  // bunch of generic helpers

  /**
   * DIY in Array
   * @param {array} arr to check from
   * @param {*} value to check against
   * @return {boolean} true on found
   */
  var inArray = function (arr, value) { return !!arr.filter(function (a) { return a === value; }).length; };

  /**
   * @param {object} obj for search
   * @param {string} key target
   * @return {boolean} true on success
   */
  var isObjectHasKey$1 = function(obj, key) {
    try {
      var keys = Object.keys(obj);
      return inArray(keys, key)
    } catch(e) {
      // @BUG when the obj is not an OBJECT we got some weird output
      return false;
      /*
      console.info('obj', obj)
      console.error(e)
      throw new Error(e)
      */
    }
  };

  /**
   * Removes all key-value entries from the list cache.
   *
   * @private
   * @name clear
   * @memberOf ListCache
   */
  function listCacheClear() {
    this.__data__ = [];
    this.size = 0;
  }

  /**
   * Performs a
   * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
   * comparison between two values to determine if they are equivalent.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to compare.
   * @param {*} other The other value to compare.
   * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
   * @example
   *
   * var object = { 'a': 1 };
   * var other = { 'a': 1 };
   *
   * _.eq(object, object);
   * // => true
   *
   * _.eq(object, other);
   * // => false
   *
   * _.eq('a', 'a');
   * // => true
   *
   * _.eq('a', Object('a'));
   * // => false
   *
   * _.eq(NaN, NaN);
   * // => true
   */
  function eq(value, other) {
    return value === other || (value !== value && other !== other);
  }

  /**
   * Gets the index at which the `key` is found in `array` of key-value pairs.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} key The key to search for.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function assocIndexOf(array, key) {
    var length = array.length;
    while (length--) {
      if (eq(array[length][0], key)) {
        return length;
      }
    }
    return -1;
  }

  /** Used for built-in method references. */
  var arrayProto = Array.prototype;

  /** Built-in value references. */
  var splice = arrayProto.splice;

  /**
   * Removes `key` and its value from the list cache.
   *
   * @private
   * @name delete
   * @memberOf ListCache
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function listCacheDelete(key) {
    var data = this.__data__,
        index = assocIndexOf(data, key);

    if (index < 0) {
      return false;
    }
    var lastIndex = data.length - 1;
    if (index == lastIndex) {
      data.pop();
    } else {
      splice.call(data, index, 1);
    }
    --this.size;
    return true;
  }

  /**
   * Gets the list cache value for `key`.
   *
   * @private
   * @name get
   * @memberOf ListCache
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function listCacheGet(key) {
    var data = this.__data__,
        index = assocIndexOf(data, key);

    return index < 0 ? undefined : data[index][1];
  }

  /**
   * Checks if a list cache value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf ListCache
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function listCacheHas(key) {
    return assocIndexOf(this.__data__, key) > -1;
  }

  /**
   * Sets the list cache `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf ListCache
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the list cache instance.
   */
  function listCacheSet(key, value) {
    var data = this.__data__,
        index = assocIndexOf(data, key);

    if (index < 0) {
      ++this.size;
      data.push([key, value]);
    } else {
      data[index][1] = value;
    }
    return this;
  }

  /**
   * Creates an list cache object.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function ListCache(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `ListCache`.
  ListCache.prototype.clear = listCacheClear;
  ListCache.prototype['delete'] = listCacheDelete;
  ListCache.prototype.get = listCacheGet;
  ListCache.prototype.has = listCacheHas;
  ListCache.prototype.set = listCacheSet;

  /**
   * Removes all key-value entries from the stack.
   *
   * @private
   * @name clear
   * @memberOf Stack
   */
  function stackClear() {
    this.__data__ = new ListCache;
    this.size = 0;
  }

  /**
   * Removes `key` and its value from the stack.
   *
   * @private
   * @name delete
   * @memberOf Stack
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function stackDelete(key) {
    var data = this.__data__,
        result = data['delete'](key);

    this.size = data.size;
    return result;
  }

  /**
   * Gets the stack value for `key`.
   *
   * @private
   * @name get
   * @memberOf Stack
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function stackGet(key) {
    return this.__data__.get(key);
  }

  /**
   * Checks if a stack value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf Stack
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function stackHas(key) {
    return this.__data__.has(key);
  }

  /**
   * Checks if `value` is the
   * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
   * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an object, else `false`.
   * @example
   *
   * _.isObject({});
   * // => true
   *
   * _.isObject([1, 2, 3]);
   * // => true
   *
   * _.isObject(_.noop);
   * // => true
   *
   * _.isObject(null);
   * // => false
   */
  function isObject(value) {
    var type = typeof value;
    return value != null && (type == 'object' || type == 'function');
  }

  /** `Object#toString` result references. */
  var asyncTag = '[object AsyncFunction]',
      funcTag = '[object Function]',
      genTag = '[object GeneratorFunction]',
      proxyTag = '[object Proxy]';

  /**
   * Checks if `value` is classified as a `Function` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a function, else `false`.
   * @example
   *
   * _.isFunction(_);
   * // => true
   *
   * _.isFunction(/abc/);
   * // => false
   */
  function isFunction(value) {
    if (!isObject(value)) {
      return false;
    }
    // The use of `Object#toString` avoids issues with the `typeof` operator
    // in Safari 9 which returns 'object' for typed arrays and other constructors.
    var tag = baseGetTag(value);
    return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
  }

  /** Used to detect overreaching core-js shims. */
  var coreJsData = root['__core-js_shared__'];

  /** Used to detect methods masquerading as native. */
  var maskSrcKey = (function() {
    var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
    return uid ? ('Symbol(src)_1.' + uid) : '';
  }());

  /**
   * Checks if `func` has its source masked.
   *
   * @private
   * @param {Function} func The function to check.
   * @returns {boolean} Returns `true` if `func` is masked, else `false`.
   */
  function isMasked(func) {
    return !!maskSrcKey && (maskSrcKey in func);
  }

  /** Used for built-in method references. */
  var funcProto$1 = Function.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString$1 = funcProto$1.toString;

  /**
   * Converts `func` to its source code.
   *
   * @private
   * @param {Function} func The function to convert.
   * @returns {string} Returns the source code.
   */
  function toSource(func) {
    if (func != null) {
      try {
        return funcToString$1.call(func);
      } catch (e) {}
      try {
        return (func + '');
      } catch (e) {}
    }
    return '';
  }

  /**
   * Used to match `RegExp`
   * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
   */
  var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

  /** Used to detect host constructors (Safari). */
  var reIsHostCtor = /^\[object .+?Constructor\]$/;

  /** Used for built-in method references. */
  var funcProto$2 = Function.prototype,
      objectProto$3 = Object.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString$2 = funcProto$2.toString;

  /** Used to check objects for own properties. */
  var hasOwnProperty$2 = objectProto$3.hasOwnProperty;

  /** Used to detect if a method is native. */
  var reIsNative = RegExp('^' +
    funcToString$2.call(hasOwnProperty$2).replace(reRegExpChar, '\\$&')
    .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
  );

  /**
   * The base implementation of `_.isNative` without bad shim checks.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a native function,
   *  else `false`.
   */
  function baseIsNative(value) {
    if (!isObject(value) || isMasked(value)) {
      return false;
    }
    var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
    return pattern.test(toSource(value));
  }

  /**
   * Gets the value at `key` of `object`.
   *
   * @private
   * @param {Object} [object] The object to query.
   * @param {string} key The key of the property to get.
   * @returns {*} Returns the property value.
   */
  function getValue(object, key) {
    return object == null ? undefined : object[key];
  }

  /**
   * Gets the native function at `key` of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {string} key The key of the method to get.
   * @returns {*} Returns the function if it's native, else `undefined`.
   */
  function getNative(object, key) {
    var value = getValue(object, key);
    return baseIsNative(value) ? value : undefined;
  }

  /* Built-in method references that are verified to be native. */
  var Map$1 = getNative(root, 'Map');

  /* Built-in method references that are verified to be native. */
  var nativeCreate = getNative(Object, 'create');

  /**
   * Removes all key-value entries from the hash.
   *
   * @private
   * @name clear
   * @memberOf Hash
   */
  function hashClear() {
    this.__data__ = nativeCreate ? nativeCreate(null) : {};
    this.size = 0;
  }

  /**
   * Removes `key` and its value from the hash.
   *
   * @private
   * @name delete
   * @memberOf Hash
   * @param {Object} hash The hash to modify.
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function hashDelete(key) {
    var result = this.has(key) && delete this.__data__[key];
    this.size -= result ? 1 : 0;
    return result;
  }

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED = '__lodash_hash_undefined__';

  /** Used for built-in method references. */
  var objectProto$4 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$3 = objectProto$4.hasOwnProperty;

  /**
   * Gets the hash value for `key`.
   *
   * @private
   * @name get
   * @memberOf Hash
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function hashGet(key) {
    var data = this.__data__;
    if (nativeCreate) {
      var result = data[key];
      return result === HASH_UNDEFINED ? undefined : result;
    }
    return hasOwnProperty$3.call(data, key) ? data[key] : undefined;
  }

  /** Used for built-in method references. */
  var objectProto$5 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$4 = objectProto$5.hasOwnProperty;

  /**
   * Checks if a hash value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf Hash
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function hashHas(key) {
    var data = this.__data__;
    return nativeCreate ? (data[key] !== undefined) : hasOwnProperty$4.call(data, key);
  }

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED$1 = '__lodash_hash_undefined__';

  /**
   * Sets the hash `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf Hash
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the hash instance.
   */
  function hashSet(key, value) {
    var data = this.__data__;
    this.size += this.has(key) ? 0 : 1;
    data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED$1 : value;
    return this;
  }

  /**
   * Creates a hash object.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function Hash(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `Hash`.
  Hash.prototype.clear = hashClear;
  Hash.prototype['delete'] = hashDelete;
  Hash.prototype.get = hashGet;
  Hash.prototype.has = hashHas;
  Hash.prototype.set = hashSet;

  /**
   * Removes all key-value entries from the map.
   *
   * @private
   * @name clear
   * @memberOf MapCache
   */
  function mapCacheClear() {
    this.size = 0;
    this.__data__ = {
      'hash': new Hash,
      'map': new (Map$1 || ListCache),
      'string': new Hash
    };
  }

  /**
   * Checks if `value` is suitable for use as unique object key.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
   */
  function isKeyable(value) {
    var type = typeof value;
    return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
      ? (value !== '__proto__')
      : (value === null);
  }

  /**
   * Gets the data for `map`.
   *
   * @private
   * @param {Object} map The map to query.
   * @param {string} key The reference key.
   * @returns {*} Returns the map data.
   */
  function getMapData(map, key) {
    var data = map.__data__;
    return isKeyable(key)
      ? data[typeof key == 'string' ? 'string' : 'hash']
      : data.map;
  }

  /**
   * Removes `key` and its value from the map.
   *
   * @private
   * @name delete
   * @memberOf MapCache
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function mapCacheDelete(key) {
    var result = getMapData(this, key)['delete'](key);
    this.size -= result ? 1 : 0;
    return result;
  }

  /**
   * Gets the map value for `key`.
   *
   * @private
   * @name get
   * @memberOf MapCache
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function mapCacheGet(key) {
    return getMapData(this, key).get(key);
  }

  /**
   * Checks if a map value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf MapCache
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function mapCacheHas(key) {
    return getMapData(this, key).has(key);
  }

  /**
   * Sets the map `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf MapCache
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the map cache instance.
   */
  function mapCacheSet(key, value) {
    var data = getMapData(this, key),
        size = data.size;

    data.set(key, value);
    this.size += data.size == size ? 0 : 1;
    return this;
  }

  /**
   * Creates a map cache object to store key-value pairs.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function MapCache(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `MapCache`.
  MapCache.prototype.clear = mapCacheClear;
  MapCache.prototype['delete'] = mapCacheDelete;
  MapCache.prototype.get = mapCacheGet;
  MapCache.prototype.has = mapCacheHas;
  MapCache.prototype.set = mapCacheSet;

  /** Used as the size to enable large array optimizations. */
  var LARGE_ARRAY_SIZE = 200;

  /**
   * Sets the stack `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf Stack
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the stack cache instance.
   */
  function stackSet(key, value) {
    var data = this.__data__;
    if (data instanceof ListCache) {
      var pairs = data.__data__;
      if (!Map$1 || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
        pairs.push([key, value]);
        this.size = ++data.size;
        return this;
      }
      data = this.__data__ = new MapCache(pairs);
    }
    data.set(key, value);
    this.size = data.size;
    return this;
  }

  /**
   * Creates a stack cache object to store key-value pairs.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function Stack(entries) {
    var data = this.__data__ = new ListCache(entries);
    this.size = data.size;
  }

  // Add methods to `Stack`.
  Stack.prototype.clear = stackClear;
  Stack.prototype['delete'] = stackDelete;
  Stack.prototype.get = stackGet;
  Stack.prototype.has = stackHas;
  Stack.prototype.set = stackSet;

  var defineProperty = (function() {
    try {
      var func = getNative(Object, 'defineProperty');
      func({}, '', {});
      return func;
    } catch (e) {}
  }());

  /**
   * The base implementation of `assignValue` and `assignMergeValue` without
   * value checks.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function baseAssignValue(object, key, value) {
    if (key == '__proto__' && defineProperty) {
      defineProperty(object, key, {
        'configurable': true,
        'enumerable': true,
        'value': value,
        'writable': true
      });
    } else {
      object[key] = value;
    }
  }

  /**
   * This function is like `assignValue` except that it doesn't assign
   * `undefined` values.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function assignMergeValue(object, key, value) {
    if ((value !== undefined && !eq(object[key], value)) ||
        (value === undefined && !(key in object))) {
      baseAssignValue(object, key, value);
    }
  }

  /**
   * Creates a base function for methods like `_.forIn` and `_.forOwn`.
   *
   * @private
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {Function} Returns the new base function.
   */
  function createBaseFor(fromRight) {
    return function(object, iteratee, keysFunc) {
      var index = -1,
          iterable = Object(object),
          props = keysFunc(object),
          length = props.length;

      while (length--) {
        var key = props[fromRight ? length : ++index];
        if (iteratee(iterable[key], key, iterable) === false) {
          break;
        }
      }
      return object;
    };
  }

  /**
   * The base implementation of `baseForOwn` which iterates over `object`
   * properties returned by `keysFunc` and invokes `iteratee` for each property.
   * Iteratee functions may exit iteration early by explicitly returning `false`.
   *
   * @private
   * @param {Object} object The object to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @param {Function} keysFunc The function to get the keys of `object`.
   * @returns {Object} Returns `object`.
   */
  var baseFor = createBaseFor();

  /** Detect free variable `exports`. */
  var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports = freeModule && freeModule.exports === freeExports;

  /** Built-in value references. */
  var Buffer = moduleExports ? root.Buffer : undefined,
      allocUnsafe = Buffer ? Buffer.allocUnsafe : undefined;

  /**
   * Creates a clone of  `buffer`.
   *
   * @private
   * @param {Buffer} buffer The buffer to clone.
   * @param {boolean} [isDeep] Specify a deep clone.
   * @returns {Buffer} Returns the cloned buffer.
   */
  function cloneBuffer(buffer, isDeep) {
    if (isDeep) {
      return buffer.slice();
    }
    var length = buffer.length,
        result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length);

    buffer.copy(result);
    return result;
  }

  /** Built-in value references. */
  var Uint8Array$1 = root.Uint8Array;

  /**
   * Creates a clone of `arrayBuffer`.
   *
   * @private
   * @param {ArrayBuffer} arrayBuffer The array buffer to clone.
   * @returns {ArrayBuffer} Returns the cloned array buffer.
   */
  function cloneArrayBuffer(arrayBuffer) {
    var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
    new Uint8Array$1(result).set(new Uint8Array$1(arrayBuffer));
    return result;
  }

  /**
   * Creates a clone of `typedArray`.
   *
   * @private
   * @param {Object} typedArray The typed array to clone.
   * @param {boolean} [isDeep] Specify a deep clone.
   * @returns {Object} Returns the cloned typed array.
   */
  function cloneTypedArray(typedArray, isDeep) {
    var buffer = isDeep ? cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
    return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
  }

  /**
   * Copies the values of `source` to `array`.
   *
   * @private
   * @param {Array} source The array to copy values from.
   * @param {Array} [array=[]] The array to copy values to.
   * @returns {Array} Returns `array`.
   */
  function copyArray(source, array) {
    var index = -1,
        length = source.length;

    array || (array = Array(length));
    while (++index < length) {
      array[index] = source[index];
    }
    return array;
  }

  /** Built-in value references. */
  var objectCreate = Object.create;

  /**
   * The base implementation of `_.create` without support for assigning
   * properties to the created object.
   *
   * @private
   * @param {Object} proto The object to inherit from.
   * @returns {Object} Returns the new object.
   */
  var baseCreate = (function() {
    function object() {}
    return function(proto) {
      if (!isObject(proto)) {
        return {};
      }
      if (objectCreate) {
        return objectCreate(proto);
      }
      object.prototype = proto;
      var result = new object;
      object.prototype = undefined;
      return result;
    };
  }());

  /** Used for built-in method references. */
  var objectProto$6 = Object.prototype;

  /**
   * Checks if `value` is likely a prototype object.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
   */
  function isPrototype(value) {
    var Ctor = value && value.constructor,
        proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto$6;

    return value === proto;
  }

  /**
   * Initializes an object clone.
   *
   * @private
   * @param {Object} object The object to clone.
   * @returns {Object} Returns the initialized clone.
   */
  function initCloneObject(object) {
    return (typeof object.constructor == 'function' && !isPrototype(object))
      ? baseCreate(getPrototype(object))
      : {};
  }

  /** `Object#toString` result references. */
  var argsTag = '[object Arguments]';

  /**
   * The base implementation of `_.isArguments`.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an `arguments` object,
   */
  function baseIsArguments(value) {
    return isObjectLike(value) && baseGetTag(value) == argsTag;
  }

  /** Used for built-in method references. */
  var objectProto$7 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$5 = objectProto$7.hasOwnProperty;

  /** Built-in value references. */
  var propertyIsEnumerable = objectProto$7.propertyIsEnumerable;

  /**
   * Checks if `value` is likely an `arguments` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an `arguments` object,
   *  else `false`.
   * @example
   *
   * _.isArguments(function() { return arguments; }());
   * // => true
   *
   * _.isArguments([1, 2, 3]);
   * // => false
   */
  var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
    return isObjectLike(value) && hasOwnProperty$5.call(value, 'callee') &&
      !propertyIsEnumerable.call(value, 'callee');
  };

  /** Used as references for various `Number` constants. */
  var MAX_SAFE_INTEGER = 9007199254740991;

  /**
   * Checks if `value` is a valid array-like length.
   *
   * **Note:** This method is loosely based on
   * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
   * @example
   *
   * _.isLength(3);
   * // => true
   *
   * _.isLength(Number.MIN_VALUE);
   * // => false
   *
   * _.isLength(Infinity);
   * // => false
   *
   * _.isLength('3');
   * // => false
   */
  function isLength(value) {
    return typeof value == 'number' &&
      value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
  }

  /**
   * Checks if `value` is array-like. A value is considered array-like if it's
   * not a function and has a `value.length` that's an integer greater than or
   * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
   * @example
   *
   * _.isArrayLike([1, 2, 3]);
   * // => true
   *
   * _.isArrayLike(document.body.children);
   * // => true
   *
   * _.isArrayLike('abc');
   * // => true
   *
   * _.isArrayLike(_.noop);
   * // => false
   */
  function isArrayLike(value) {
    return value != null && isLength(value.length) && !isFunction(value);
  }

  /**
   * This method is like `_.isArrayLike` except that it also checks if `value`
   * is an object.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an array-like object,
   *  else `false`.
   * @example
   *
   * _.isArrayLikeObject([1, 2, 3]);
   * // => true
   *
   * _.isArrayLikeObject(document.body.children);
   * // => true
   *
   * _.isArrayLikeObject('abc');
   * // => false
   *
   * _.isArrayLikeObject(_.noop);
   * // => false
   */
  function isArrayLikeObject(value) {
    return isObjectLike(value) && isArrayLike(value);
  }

  /**
   * This method returns `false`.
   *
   * @static
   * @memberOf _
   * @since 4.13.0
   * @category Util
   * @returns {boolean} Returns `false`.
   * @example
   *
   * _.times(2, _.stubFalse);
   * // => [false, false]
   */
  function stubFalse() {
    return false;
  }

  /** Detect free variable `exports`. */
  var freeExports$1 = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule$1 = freeExports$1 && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports$1 = freeModule$1 && freeModule$1.exports === freeExports$1;

  /** Built-in value references. */
  var Buffer$1 = moduleExports$1 ? root.Buffer : undefined;

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeIsBuffer = Buffer$1 ? Buffer$1.isBuffer : undefined;

  /**
   * Checks if `value` is a buffer.
   *
   * @static
   * @memberOf _
   * @since 4.3.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
   * @example
   *
   * _.isBuffer(new Buffer(2));
   * // => true
   *
   * _.isBuffer(new Uint8Array(2));
   * // => false
   */
  var isBuffer = nativeIsBuffer || stubFalse;

  /** `Object#toString` result references. */
  var argsTag$1 = '[object Arguments]',
      arrayTag = '[object Array]',
      boolTag = '[object Boolean]',
      dateTag = '[object Date]',
      errorTag = '[object Error]',
      funcTag$1 = '[object Function]',
      mapTag = '[object Map]',
      numberTag = '[object Number]',
      objectTag$1 = '[object Object]',
      regexpTag = '[object RegExp]',
      setTag = '[object Set]',
      stringTag = '[object String]',
      weakMapTag = '[object WeakMap]';

  var arrayBufferTag = '[object ArrayBuffer]',
      dataViewTag = '[object DataView]',
      float32Tag = '[object Float32Array]',
      float64Tag = '[object Float64Array]',
      int8Tag = '[object Int8Array]',
      int16Tag = '[object Int16Array]',
      int32Tag = '[object Int32Array]',
      uint8Tag = '[object Uint8Array]',
      uint8ClampedTag = '[object Uint8ClampedArray]',
      uint16Tag = '[object Uint16Array]',
      uint32Tag = '[object Uint32Array]';

  /** Used to identify `toStringTag` values of typed arrays. */
  var typedArrayTags = {};
  typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
  typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
  typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
  typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
  typedArrayTags[uint32Tag] = true;
  typedArrayTags[argsTag$1] = typedArrayTags[arrayTag] =
  typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
  typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
  typedArrayTags[errorTag] = typedArrayTags[funcTag$1] =
  typedArrayTags[mapTag] = typedArrayTags[numberTag] =
  typedArrayTags[objectTag$1] = typedArrayTags[regexpTag] =
  typedArrayTags[setTag] = typedArrayTags[stringTag] =
  typedArrayTags[weakMapTag] = false;

  /**
   * The base implementation of `_.isTypedArray` without Node.js optimizations.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
   */
  function baseIsTypedArray(value) {
    return isObjectLike(value) &&
      isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
  }

  /**
   * The base implementation of `_.unary` without support for storing metadata.
   *
   * @private
   * @param {Function} func The function to cap arguments for.
   * @returns {Function} Returns the new capped function.
   */
  function baseUnary(func) {
    return function(value) {
      return func(value);
    };
  }

  /** Detect free variable `exports`. */
  var freeExports$2 = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule$2 = freeExports$2 && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports$2 = freeModule$2 && freeModule$2.exports === freeExports$2;

  /** Detect free variable `process` from Node.js. */
  var freeProcess = moduleExports$2 && freeGlobal.process;

  /** Used to access faster Node.js helpers. */
  var nodeUtil = (function() {
    try {
      // Use `util.types` for Node.js 10+.
      var types = freeModule$2 && freeModule$2.require && freeModule$2.require('util').types;

      if (types) {
        return types;
      }

      // Legacy `process.binding('util')` for Node.js < 10.
      return freeProcess && freeProcess.binding && freeProcess.binding('util');
    } catch (e) {}
  }());

  /* Node.js helper references. */
  var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

  /**
   * Checks if `value` is classified as a typed array.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
   * @example
   *
   * _.isTypedArray(new Uint8Array);
   * // => true
   *
   * _.isTypedArray([]);
   * // => false
   */
  var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

  /**
   * Gets the value at `key`, unless `key` is "__proto__" or "constructor".
   *
   * @private
   * @param {Object} object The object to query.
   * @param {string} key The key of the property to get.
   * @returns {*} Returns the property value.
   */
  function safeGet(object, key) {
    if (key === 'constructor' && typeof object[key] === 'function') {
      return;
    }

    if (key == '__proto__') {
      return;
    }

    return object[key];
  }

  /** Used for built-in method references. */
  var objectProto$8 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$6 = objectProto$8.hasOwnProperty;

  /**
   * Assigns `value` to `key` of `object` if the existing value is not equivalent
   * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
   * for equality comparisons.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function assignValue(object, key, value) {
    var objValue = object[key];
    if (!(hasOwnProperty$6.call(object, key) && eq(objValue, value)) ||
        (value === undefined && !(key in object))) {
      baseAssignValue(object, key, value);
    }
  }

  /**
   * Copies properties of `source` to `object`.
   *
   * @private
   * @param {Object} source The object to copy properties from.
   * @param {Array} props The property identifiers to copy.
   * @param {Object} [object={}] The object to copy properties to.
   * @param {Function} [customizer] The function to customize copied values.
   * @returns {Object} Returns `object`.
   */
  function copyObject(source, props, object, customizer) {
    var isNew = !object;
    object || (object = {});

    var index = -1,
        length = props.length;

    while (++index < length) {
      var key = props[index];

      var newValue = customizer
        ? customizer(object[key], source[key], key, object, source)
        : undefined;

      if (newValue === undefined) {
        newValue = source[key];
      }
      if (isNew) {
        baseAssignValue(object, key, newValue);
      } else {
        assignValue(object, key, newValue);
      }
    }
    return object;
  }

  /**
   * The base implementation of `_.times` without support for iteratee shorthands
   * or max array length checks.
   *
   * @private
   * @param {number} n The number of times to invoke `iteratee`.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns the array of results.
   */
  function baseTimes(n, iteratee) {
    var index = -1,
        result = Array(n);

    while (++index < n) {
      result[index] = iteratee(index);
    }
    return result;
  }

  /** Used as references for various `Number` constants. */
  var MAX_SAFE_INTEGER$1 = 9007199254740991;

  /** Used to detect unsigned integer values. */
  var reIsUint = /^(?:0|[1-9]\d*)$/;

  /**
   * Checks if `value` is a valid array-like index.
   *
   * @private
   * @param {*} value The value to check.
   * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
   * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
   */
  function isIndex(value, length) {
    var type = typeof value;
    length = length == null ? MAX_SAFE_INTEGER$1 : length;

    return !!length &&
      (type == 'number' ||
        (type != 'symbol' && reIsUint.test(value))) &&
          (value > -1 && value % 1 == 0 && value < length);
  }

  /** Used for built-in method references. */
  var objectProto$9 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$7 = objectProto$9.hasOwnProperty;

  /**
   * Creates an array of the enumerable property names of the array-like `value`.
   *
   * @private
   * @param {*} value The value to query.
   * @param {boolean} inherited Specify returning inherited property names.
   * @returns {Array} Returns the array of property names.
   */
  function arrayLikeKeys(value, inherited) {
    var isArr = isArray(value),
        isArg = !isArr && isArguments(value),
        isBuff = !isArr && !isArg && isBuffer(value),
        isType = !isArr && !isArg && !isBuff && isTypedArray(value),
        skipIndexes = isArr || isArg || isBuff || isType,
        result = skipIndexes ? baseTimes(value.length, String) : [],
        length = result.length;

    for (var key in value) {
      if ((inherited || hasOwnProperty$7.call(value, key)) &&
          !(skipIndexes && (
             // Safari 9 has enumerable `arguments.length` in strict mode.
             key == 'length' ||
             // Node.js 0.10 has enumerable non-index properties on buffers.
             (isBuff && (key == 'offset' || key == 'parent')) ||
             // PhantomJS 2 has enumerable non-index properties on typed arrays.
             (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
             // Skip index properties.
             isIndex(key, length)
          ))) {
        result.push(key);
      }
    }
    return result;
  }

  /**
   * This function is like
   * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
   * except that it includes inherited enumerable properties.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   */
  function nativeKeysIn(object) {
    var result = [];
    if (object != null) {
      for (var key in Object(object)) {
        result.push(key);
      }
    }
    return result;
  }

  /** Used for built-in method references. */
  var objectProto$a = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$8 = objectProto$a.hasOwnProperty;

  /**
   * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   */
  function baseKeysIn(object) {
    if (!isObject(object)) {
      return nativeKeysIn(object);
    }
    var isProto = isPrototype(object),
        result = [];

    for (var key in object) {
      if (!(key == 'constructor' && (isProto || !hasOwnProperty$8.call(object, key)))) {
        result.push(key);
      }
    }
    return result;
  }

  /**
   * Creates an array of the own and inherited enumerable property names of `object`.
   *
   * **Note:** Non-object values are coerced to objects.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Object
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   * @example
   *
   * function Foo() {
   *   this.a = 1;
   *   this.b = 2;
   * }
   *
   * Foo.prototype.c = 3;
   *
   * _.keysIn(new Foo);
   * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
   */
  function keysIn(object) {
    return isArrayLike(object) ? arrayLikeKeys(object, true) : baseKeysIn(object);
  }

  /**
   * Converts `value` to a plain object flattening inherited enumerable string
   * keyed properties of `value` to own properties of the plain object.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Lang
   * @param {*} value The value to convert.
   * @returns {Object} Returns the converted plain object.
   * @example
   *
   * function Foo() {
   *   this.b = 2;
   * }
   *
   * Foo.prototype.c = 3;
   *
   * _.assign({ 'a': 1 }, new Foo);
   * // => { 'a': 1, 'b': 2 }
   *
   * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
   * // => { 'a': 1, 'b': 2, 'c': 3 }
   */
  function toPlainObject(value) {
    return copyObject(value, keysIn(value));
  }

  /**
   * A specialized version of `baseMerge` for arrays and objects which performs
   * deep merges and tracks traversed objects enabling objects with circular
   * references to be merged.
   *
   * @private
   * @param {Object} object The destination object.
   * @param {Object} source The source object.
   * @param {string} key The key of the value to merge.
   * @param {number} srcIndex The index of `source`.
   * @param {Function} mergeFunc The function to merge values.
   * @param {Function} [customizer] The function to customize assigned values.
   * @param {Object} [stack] Tracks traversed source values and their merged
   *  counterparts.
   */
  function baseMergeDeep(object, source, key, srcIndex, mergeFunc, customizer, stack) {
    var objValue = safeGet(object, key),
        srcValue = safeGet(source, key),
        stacked = stack.get(srcValue);

    if (stacked) {
      assignMergeValue(object, key, stacked);
      return;
    }
    var newValue = customizer
      ? customizer(objValue, srcValue, (key + ''), object, source, stack)
      : undefined;

    var isCommon = newValue === undefined;

    if (isCommon) {
      var isArr = isArray(srcValue),
          isBuff = !isArr && isBuffer(srcValue),
          isTyped = !isArr && !isBuff && isTypedArray(srcValue);

      newValue = srcValue;
      if (isArr || isBuff || isTyped) {
        if (isArray(objValue)) {
          newValue = objValue;
        }
        else if (isArrayLikeObject(objValue)) {
          newValue = copyArray(objValue);
        }
        else if (isBuff) {
          isCommon = false;
          newValue = cloneBuffer(srcValue, true);
        }
        else if (isTyped) {
          isCommon = false;
          newValue = cloneTypedArray(srcValue, true);
        }
        else {
          newValue = [];
        }
      }
      else if (isPlainObject(srcValue) || isArguments(srcValue)) {
        newValue = objValue;
        if (isArguments(objValue)) {
          newValue = toPlainObject(objValue);
        }
        else if (!isObject(objValue) || isFunction(objValue)) {
          newValue = initCloneObject(srcValue);
        }
      }
      else {
        isCommon = false;
      }
    }
    if (isCommon) {
      // Recursively merge objects and arrays (susceptible to call stack limits).
      stack.set(srcValue, newValue);
      mergeFunc(newValue, srcValue, srcIndex, customizer, stack);
      stack['delete'](srcValue);
    }
    assignMergeValue(object, key, newValue);
  }

  /**
   * The base implementation of `_.merge` without support for multiple sources.
   *
   * @private
   * @param {Object} object The destination object.
   * @param {Object} source The source object.
   * @param {number} srcIndex The index of `source`.
   * @param {Function} [customizer] The function to customize merged values.
   * @param {Object} [stack] Tracks traversed source values and their merged
   *  counterparts.
   */
  function baseMerge(object, source, srcIndex, customizer, stack) {
    if (object === source) {
      return;
    }
    baseFor(source, function(srcValue, key) {
      stack || (stack = new Stack);
      if (isObject(srcValue)) {
        baseMergeDeep(object, source, key, srcIndex, baseMerge, customizer, stack);
      }
      else {
        var newValue = customizer
          ? customizer(safeGet(object, key), srcValue, (key + ''), object, source, stack)
          : undefined;

        if (newValue === undefined) {
          newValue = srcValue;
        }
        assignMergeValue(object, key, newValue);
      }
    }, keysIn);
  }

  /**
   * This method returns the first argument it receives.
   *
   * @static
   * @since 0.1.0
   * @memberOf _
   * @category Util
   * @param {*} value Any value.
   * @returns {*} Returns `value`.
   * @example
   *
   * var object = { 'a': 1 };
   *
   * console.log(_.identity(object) === object);
   * // => true
   */
  function identity(value) {
    return value;
  }

  /**
   * A faster alternative to `Function#apply`, this function invokes `func`
   * with the `this` binding of `thisArg` and the arguments of `args`.
   *
   * @private
   * @param {Function} func The function to invoke.
   * @param {*} thisArg The `this` binding of `func`.
   * @param {Array} args The arguments to invoke `func` with.
   * @returns {*} Returns the result of `func`.
   */
  function apply(func, thisArg, args) {
    switch (args.length) {
      case 0: return func.call(thisArg);
      case 1: return func.call(thisArg, args[0]);
      case 2: return func.call(thisArg, args[0], args[1]);
      case 3: return func.call(thisArg, args[0], args[1], args[2]);
    }
    return func.apply(thisArg, args);
  }

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeMax = Math.max;

  /**
   * A specialized version of `baseRest` which transforms the rest array.
   *
   * @private
   * @param {Function} func The function to apply a rest parameter to.
   * @param {number} [start=func.length-1] The start position of the rest parameter.
   * @param {Function} transform The rest array transform.
   * @returns {Function} Returns the new function.
   */
  function overRest(func, start, transform) {
    start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
    return function() {
      var args = arguments,
          index = -1,
          length = nativeMax(args.length - start, 0),
          array = Array(length);

      while (++index < length) {
        array[index] = args[start + index];
      }
      index = -1;
      var otherArgs = Array(start + 1);
      while (++index < start) {
        otherArgs[index] = args[index];
      }
      otherArgs[start] = transform(array);
      return apply(func, this, otherArgs);
    };
  }

  /**
   * Creates a function that returns `value`.
   *
   * @static
   * @memberOf _
   * @since 2.4.0
   * @category Util
   * @param {*} value The value to return from the new function.
   * @returns {Function} Returns the new constant function.
   * @example
   *
   * var objects = _.times(2, _.constant({ 'a': 1 }));
   *
   * console.log(objects);
   * // => [{ 'a': 1 }, { 'a': 1 }]
   *
   * console.log(objects[0] === objects[1]);
   * // => true
   */
  function constant(value) {
    return function() {
      return value;
    };
  }

  /**
   * The base implementation of `setToString` without support for hot loop shorting.
   *
   * @private
   * @param {Function} func The function to modify.
   * @param {Function} string The `toString` result.
   * @returns {Function} Returns `func`.
   */
  var baseSetToString = !defineProperty ? identity : function(func, string) {
    return defineProperty(func, 'toString', {
      'configurable': true,
      'enumerable': false,
      'value': constant(string),
      'writable': true
    });
  };

  /** Used to detect hot functions by number of calls within a span of milliseconds. */
  var HOT_COUNT = 800,
      HOT_SPAN = 16;

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeNow = Date.now;

  /**
   * Creates a function that'll short out and invoke `identity` instead
   * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
   * milliseconds.
   *
   * @private
   * @param {Function} func The function to restrict.
   * @returns {Function} Returns the new shortable function.
   */
  function shortOut(func) {
    var count = 0,
        lastCalled = 0;

    return function() {
      var stamp = nativeNow(),
          remaining = HOT_SPAN - (stamp - lastCalled);

      lastCalled = stamp;
      if (remaining > 0) {
        if (++count >= HOT_COUNT) {
          return arguments[0];
        }
      } else {
        count = 0;
      }
      return func.apply(undefined, arguments);
    };
  }

  /**
   * Sets the `toString` method of `func` to return `string`.
   *
   * @private
   * @param {Function} func The function to modify.
   * @param {Function} string The `toString` result.
   * @returns {Function} Returns `func`.
   */
  var setToString = shortOut(baseSetToString);

  /**
   * The base implementation of `_.rest` which doesn't validate or coerce arguments.
   *
   * @private
   * @param {Function} func The function to apply a rest parameter to.
   * @param {number} [start=func.length-1] The start position of the rest parameter.
   * @returns {Function} Returns the new function.
   */
  function baseRest(func, start) {
    return setToString(overRest(func, start, identity), func + '');
  }

  /**
   * Checks if the given arguments are from an iteratee call.
   *
   * @private
   * @param {*} value The potential iteratee value argument.
   * @param {*} index The potential iteratee index or key argument.
   * @param {*} object The potential iteratee object argument.
   * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
   *  else `false`.
   */
  function isIterateeCall(value, index, object) {
    if (!isObject(object)) {
      return false;
    }
    var type = typeof index;
    if (type == 'number'
          ? (isArrayLike(object) && isIndex(index, object.length))
          : (type == 'string' && index in object)
        ) {
      return eq(object[index], value);
    }
    return false;
  }

  /**
   * Creates a function like `_.assign`.
   *
   * @private
   * @param {Function} assigner The function to assign values.
   * @returns {Function} Returns the new assigner function.
   */
  function createAssigner(assigner) {
    return baseRest(function(object, sources) {
      var index = -1,
          length = sources.length,
          customizer = length > 1 ? sources[length - 1] : undefined,
          guard = length > 2 ? sources[2] : undefined;

      customizer = (assigner.length > 3 && typeof customizer == 'function')
        ? (length--, customizer)
        : undefined;

      if (guard && isIterateeCall(sources[0], sources[1], guard)) {
        customizer = length < 3 ? undefined : customizer;
        length = 1;
      }
      object = Object(object);
      while (++index < length) {
        var source = sources[index];
        if (source) {
          assigner(object, source, index, customizer);
        }
      }
      return object;
    });
  }

  /**
   * This method is like `_.assign` except that it recursively merges own and
   * inherited enumerable string keyed properties of source objects into the
   * destination object. Source properties that resolve to `undefined` are
   * skipped if a destination value exists. Array and plain object properties
   * are merged recursively. Other objects and value types are overridden by
   * assignment. Source objects are applied from left to right. Subsequent
   * sources overwrite property assignments of previous sources.
   *
   * **Note:** This method mutates `object`.
   *
   * @static
   * @memberOf _
   * @since 0.5.0
   * @category Object
   * @param {Object} object The destination object.
   * @param {...Object} [sources] The source objects.
   * @returns {Object} Returns `object`.
   * @example
   *
   * var object = {
   *   'a': [{ 'b': 2 }, { 'd': 4 }]
   * };
   *
   * var other = {
   *   'a': [{ 'c': 3 }, { 'e': 5 }]
   * };
   *
   * _.merge(object, other);
   * // => { 'a': [{ 'b': 2, 'c': 3 }, { 'd': 4, 'e': 5 }] }
   */
  var merge = createAssigner(function(object, source, srcIndex) {
    baseMerge(object, source, srcIndex);
  });

  /**
   * check if the object has name property
   * @param {object} obj the object to check
   * @param {string} name the prop name
   * @return {*} the value or undefined
   */
  function objHasProp(obj, name) {
    var prop = Object.getOwnPropertyDescriptor(obj, name);
    return prop !== undefined && prop.value ? prop.value : prop;
  }

  /** `Object#toString` result references. */
  var stringTag$1 = '[object String]';

  /**
   * Checks if `value` is classified as a `String` primitive or object.
   *
   * @static
   * @since 0.1.0
   * @memberOf _
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a string, else `false`.
   * @example
   *
   * _.isString('abc');
   * // => true
   *
   * _.isString(1);
   * // => false
   */
  function isString(value) {
    return typeof value == 'string' ||
      (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag$1);
  }

  var lookup = [];
  var revLookup = [];
  var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array;
  var inited = false;
  function init () {
    inited = true;
    var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    for (var i = 0, len = code.length; i < len; ++i) {
      lookup[i] = code[i];
      revLookup[code.charCodeAt(i)] = i;
    }

    revLookup['-'.charCodeAt(0)] = 62;
    revLookup['_'.charCodeAt(0)] = 63;
  }

  function toByteArray (b64) {
    if (!inited) {
      init();
    }
    var i, j, l, tmp, placeHolders, arr;
    var len = b64.length;

    if (len % 4 > 0) {
      throw new Error('Invalid string. Length must be a multiple of 4')
    }

    // the number of equal signs (place holders)
    // if there are two placeholders, than the two characters before it
    // represent one byte
    // if there is only one, then the three characters before it represent 2 bytes
    // this is just a cheap hack to not do indexOf twice
    placeHolders = b64[len - 2] === '=' ? 2 : b64[len - 1] === '=' ? 1 : 0;

    // base64 is 4/3 + up to two characters of the original data
    arr = new Arr(len * 3 / 4 - placeHolders);

    // if there are placeholders, only get up to the last complete 4 chars
    l = placeHolders > 0 ? len - 4 : len;

    var L = 0;

    for (i = 0, j = 0; i < l; i += 4, j += 3) {
      tmp = (revLookup[b64.charCodeAt(i)] << 18) | (revLookup[b64.charCodeAt(i + 1)] << 12) | (revLookup[b64.charCodeAt(i + 2)] << 6) | revLookup[b64.charCodeAt(i + 3)];
      arr[L++] = (tmp >> 16) & 0xFF;
      arr[L++] = (tmp >> 8) & 0xFF;
      arr[L++] = tmp & 0xFF;
    }

    if (placeHolders === 2) {
      tmp = (revLookup[b64.charCodeAt(i)] << 2) | (revLookup[b64.charCodeAt(i + 1)] >> 4);
      arr[L++] = tmp & 0xFF;
    } else if (placeHolders === 1) {
      tmp = (revLookup[b64.charCodeAt(i)] << 10) | (revLookup[b64.charCodeAt(i + 1)] << 4) | (revLookup[b64.charCodeAt(i + 2)] >> 2);
      arr[L++] = (tmp >> 8) & 0xFF;
      arr[L++] = tmp & 0xFF;
    }

    return arr
  }

  function tripletToBase64 (num) {
    return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F]
  }

  function encodeChunk (uint8, start, end) {
    var tmp;
    var output = [];
    for (var i = start; i < end; i += 3) {
      tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2]);
      output.push(tripletToBase64(tmp));
    }
    return output.join('')
  }

  function fromByteArray (uint8) {
    if (!inited) {
      init();
    }
    var tmp;
    var len = uint8.length;
    var extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes
    var output = '';
    var parts = [];
    var maxChunkLength = 16383; // must be multiple of 3

    // go through the array every three bytes, we'll deal with trailing stuff later
    for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
      parts.push(encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)));
    }

    // pad the end with zeros, but make sure to not forget the extra bytes
    if (extraBytes === 1) {
      tmp = uint8[len - 1];
      output += lookup[tmp >> 2];
      output += lookup[(tmp << 4) & 0x3F];
      output += '==';
    } else if (extraBytes === 2) {
      tmp = (uint8[len - 2] << 8) + (uint8[len - 1]);
      output += lookup[tmp >> 10];
      output += lookup[(tmp >> 4) & 0x3F];
      output += lookup[(tmp << 2) & 0x3F];
      output += '=';
    }

    parts.push(output);

    return parts.join('')
  }

  function read (buffer, offset, isLE, mLen, nBytes) {
    var e, m;
    var eLen = nBytes * 8 - mLen - 1;
    var eMax = (1 << eLen) - 1;
    var eBias = eMax >> 1;
    var nBits = -7;
    var i = isLE ? (nBytes - 1) : 0;
    var d = isLE ? -1 : 1;
    var s = buffer[offset + i];

    i += d;

    e = s & ((1 << (-nBits)) - 1);
    s >>= (-nBits);
    nBits += eLen;
    for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

    m = e & ((1 << (-nBits)) - 1);
    e >>= (-nBits);
    nBits += mLen;
    for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

    if (e === 0) {
      e = 1 - eBias;
    } else if (e === eMax) {
      return m ? NaN : ((s ? -1 : 1) * Infinity)
    } else {
      m = m + Math.pow(2, mLen);
      e = e - eBias;
    }
    return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
  }

  function write (buffer, value, offset, isLE, mLen, nBytes) {
    var e, m, c;
    var eLen = nBytes * 8 - mLen - 1;
    var eMax = (1 << eLen) - 1;
    var eBias = eMax >> 1;
    var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0);
    var i = isLE ? 0 : (nBytes - 1);
    var d = isLE ? 1 : -1;
    var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0;

    value = Math.abs(value);

    if (isNaN(value) || value === Infinity) {
      m = isNaN(value) ? 1 : 0;
      e = eMax;
    } else {
      e = Math.floor(Math.log(value) / Math.LN2);
      if (value * (c = Math.pow(2, -e)) < 1) {
        e--;
        c *= 2;
      }
      if (e + eBias >= 1) {
        value += rt / c;
      } else {
        value += rt * Math.pow(2, 1 - eBias);
      }
      if (value * c >= 2) {
        e++;
        c /= 2;
      }

      if (e + eBias >= eMax) {
        m = 0;
        e = eMax;
      } else if (e + eBias >= 1) {
        m = (value * c - 1) * Math.pow(2, mLen);
        e = e + eBias;
      } else {
        m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
        e = 0;
      }
    }

    for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

    e = (e << mLen) | m;
    eLen += mLen;
    for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

    buffer[offset + i - d] |= s * 128;
  }

  var toString$1 = {}.toString;

  var isArray$1 = Array.isArray || function (arr) {
    return toString$1.call(arr) == '[object Array]';
  };

  var INSPECT_MAX_BYTES = 50;

  /**
   * If `Buffer.TYPED_ARRAY_SUPPORT`:
   *   === true    Use Uint8Array implementation (fastest)
   *   === false   Use Object implementation (most compatible, even IE6)
   *
   * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
   * Opera 11.6+, iOS 4.2+.
   *
   * Due to various browser bugs, sometimes the Object implementation will be used even
   * when the browser supports typed arrays.
   *
   * Note:
   *
   *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
   *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
   *
   *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
   *
   *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
   *     incorrect length in some situations.

   * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
   * get the Object implementation, which is slower but behaves correctly.
   */
  Buffer$2.TYPED_ARRAY_SUPPORT = global$1.TYPED_ARRAY_SUPPORT !== undefined
    ? global$1.TYPED_ARRAY_SUPPORT
    : true;

  function kMaxLength () {
    return Buffer$2.TYPED_ARRAY_SUPPORT
      ? 0x7fffffff
      : 0x3fffffff
  }

  function createBuffer (that, length) {
    if (kMaxLength() < length) {
      throw new RangeError('Invalid typed array length')
    }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      // Return an augmented `Uint8Array` instance, for best performance
      that = new Uint8Array(length);
      that.__proto__ = Buffer$2.prototype;
    } else {
      // Fallback: Return an object instance of the Buffer class
      if (that === null) {
        that = new Buffer$2(length);
      }
      that.length = length;
    }

    return that
  }

  /**
   * The Buffer constructor returns instances of `Uint8Array` that have their
   * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
   * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
   * and the `Uint8Array` methods. Square bracket notation works as expected -- it
   * returns a single octet.
   *
   * The `Uint8Array` prototype remains unmodified.
   */

  function Buffer$2 (arg, encodingOrOffset, length) {
    if (!Buffer$2.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer$2)) {
      return new Buffer$2(arg, encodingOrOffset, length)
    }

    // Common case.
    if (typeof arg === 'number') {
      if (typeof encodingOrOffset === 'string') {
        throw new Error(
          'If encoding is specified then the first argument must be a string'
        )
      }
      return allocUnsafe$1(this, arg)
    }
    return from(this, arg, encodingOrOffset, length)
  }

  Buffer$2.poolSize = 8192; // not used by this implementation

  // TODO: Legacy, not needed anymore. Remove in next major version.
  Buffer$2._augment = function (arr) {
    arr.__proto__ = Buffer$2.prototype;
    return arr
  };

  function from (that, value, encodingOrOffset, length) {
    if (typeof value === 'number') {
      throw new TypeError('"value" argument must not be a number')
    }

    if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
      return fromArrayBuffer(that, value, encodingOrOffset, length)
    }

    if (typeof value === 'string') {
      return fromString(that, value, encodingOrOffset)
    }

    return fromObject(that, value)
  }

  /**
   * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
   * if value is a number.
   * Buffer.from(str[, encoding])
   * Buffer.from(array)
   * Buffer.from(buffer)
   * Buffer.from(arrayBuffer[, byteOffset[, length]])
   **/
  Buffer$2.from = function (value, encodingOrOffset, length) {
    return from(null, value, encodingOrOffset, length)
  };

  if (Buffer$2.TYPED_ARRAY_SUPPORT) {
    Buffer$2.prototype.__proto__ = Uint8Array.prototype;
    Buffer$2.__proto__ = Uint8Array;
  }

  function assertSize (size) {
    if (typeof size !== 'number') {
      throw new TypeError('"size" argument must be a number')
    } else if (size < 0) {
      throw new RangeError('"size" argument must not be negative')
    }
  }

  function alloc (that, size, fill, encoding) {
    assertSize(size);
    if (size <= 0) {
      return createBuffer(that, size)
    }
    if (fill !== undefined) {
      // Only pay attention to encoding if it's a string. This
      // prevents accidentally sending in a number that would
      // be interpretted as a start offset.
      return typeof encoding === 'string'
        ? createBuffer(that, size).fill(fill, encoding)
        : createBuffer(that, size).fill(fill)
    }
    return createBuffer(that, size)
  }

  /**
   * Creates a new filled Buffer instance.
   * alloc(size[, fill[, encoding]])
   **/
  Buffer$2.alloc = function (size, fill, encoding) {
    return alloc(null, size, fill, encoding)
  };

  function allocUnsafe$1 (that, size) {
    assertSize(size);
    that = createBuffer(that, size < 0 ? 0 : checked(size) | 0);
    if (!Buffer$2.TYPED_ARRAY_SUPPORT) {
      for (var i = 0; i < size; ++i) {
        that[i] = 0;
      }
    }
    return that
  }

  /**
   * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
   * */
  Buffer$2.allocUnsafe = function (size) {
    return allocUnsafe$1(null, size)
  };
  /**
   * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
   */
  Buffer$2.allocUnsafeSlow = function (size) {
    return allocUnsafe$1(null, size)
  };

  function fromString (that, string, encoding) {
    if (typeof encoding !== 'string' || encoding === '') {
      encoding = 'utf8';
    }

    if (!Buffer$2.isEncoding(encoding)) {
      throw new TypeError('"encoding" must be a valid string encoding')
    }

    var length = byteLength(string, encoding) | 0;
    that = createBuffer(that, length);

    var actual = that.write(string, encoding);

    if (actual !== length) {
      // Writing a hex string, for example, that contains invalid characters will
      // cause everything after the first invalid character to be ignored. (e.g.
      // 'abxxcd' will be treated as 'ab')
      that = that.slice(0, actual);
    }

    return that
  }

  function fromArrayLike (that, array) {
    var length = array.length < 0 ? 0 : checked(array.length) | 0;
    that = createBuffer(that, length);
    for (var i = 0; i < length; i += 1) {
      that[i] = array[i] & 255;
    }
    return that
  }

  function fromArrayBuffer (that, array, byteOffset, length) {
    array.byteLength; // this throws if `array` is not a valid ArrayBuffer

    if (byteOffset < 0 || array.byteLength < byteOffset) {
      throw new RangeError('\'offset\' is out of bounds')
    }

    if (array.byteLength < byteOffset + (length || 0)) {
      throw new RangeError('\'length\' is out of bounds')
    }

    if (byteOffset === undefined && length === undefined) {
      array = new Uint8Array(array);
    } else if (length === undefined) {
      array = new Uint8Array(array, byteOffset);
    } else {
      array = new Uint8Array(array, byteOffset, length);
    }

    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      // Return an augmented `Uint8Array` instance, for best performance
      that = array;
      that.__proto__ = Buffer$2.prototype;
    } else {
      // Fallback: Return an object instance of the Buffer class
      that = fromArrayLike(that, array);
    }
    return that
  }

  function fromObject (that, obj) {
    if (internalIsBuffer(obj)) {
      var len = checked(obj.length) | 0;
      that = createBuffer(that, len);

      if (that.length === 0) {
        return that
      }

      obj.copy(that, 0, 0, len);
      return that
    }

    if (obj) {
      if ((typeof ArrayBuffer !== 'undefined' &&
          obj.buffer instanceof ArrayBuffer) || 'length' in obj) {
        if (typeof obj.length !== 'number' || isnan(obj.length)) {
          return createBuffer(that, 0)
        }
        return fromArrayLike(that, obj)
      }

      if (obj.type === 'Buffer' && isArray$1(obj.data)) {
        return fromArrayLike(that, obj.data)
      }
    }

    throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.')
  }

  function checked (length) {
    // Note: cannot use `length < kMaxLength()` here because that fails when
    // length is NaN (which is otherwise coerced to zero.)
    if (length >= kMaxLength()) {
      throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                           'size: 0x' + kMaxLength().toString(16) + ' bytes')
    }
    return length | 0
  }
  Buffer$2.isBuffer = isBuffer$1;
  function internalIsBuffer (b) {
    return !!(b != null && b._isBuffer)
  }

  Buffer$2.compare = function compare (a, b) {
    if (!internalIsBuffer(a) || !internalIsBuffer(b)) {
      throw new TypeError('Arguments must be Buffers')
    }

    if (a === b) { return 0 }

    var x = a.length;
    var y = b.length;

    for (var i = 0, len = Math.min(x, y); i < len; ++i) {
      if (a[i] !== b[i]) {
        x = a[i];
        y = b[i];
        break
      }
    }

    if (x < y) { return -1 }
    if (y < x) { return 1 }
    return 0
  };

  Buffer$2.isEncoding = function isEncoding (encoding) {
    switch (String(encoding).toLowerCase()) {
      case 'hex':
      case 'utf8':
      case 'utf-8':
      case 'ascii':
      case 'latin1':
      case 'binary':
      case 'base64':
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return true
      default:
        return false
    }
  };

  Buffer$2.concat = function concat (list, length) {
    if (!isArray$1(list)) {
      throw new TypeError('"list" argument must be an Array of Buffers')
    }

    if (list.length === 0) {
      return Buffer$2.alloc(0)
    }

    var i;
    if (length === undefined) {
      length = 0;
      for (i = 0; i < list.length; ++i) {
        length += list[i].length;
      }
    }

    var buffer = Buffer$2.allocUnsafe(length);
    var pos = 0;
    for (i = 0; i < list.length; ++i) {
      var buf = list[i];
      if (!internalIsBuffer(buf)) {
        throw new TypeError('"list" argument must be an Array of Buffers')
      }
      buf.copy(buffer, pos);
      pos += buf.length;
    }
    return buffer
  };

  function byteLength (string, encoding) {
    if (internalIsBuffer(string)) {
      return string.length
    }
    if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' &&
        (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
      return string.byteLength
    }
    if (typeof string !== 'string') {
      string = '' + string;
    }

    var len = string.length;
    if (len === 0) { return 0 }

    // Use a for loop to avoid recursion
    var loweredCase = false;
    for (;;) {
      switch (encoding) {
        case 'ascii':
        case 'latin1':
        case 'binary':
          return len
        case 'utf8':
        case 'utf-8':
        case undefined:
          return utf8ToBytes(string).length
        case 'ucs2':
        case 'ucs-2':
        case 'utf16le':
        case 'utf-16le':
          return len * 2
        case 'hex':
          return len >>> 1
        case 'base64':
          return base64ToBytes(string).length
        default:
          if (loweredCase) { return utf8ToBytes(string).length } // assume utf8
          encoding = ('' + encoding).toLowerCase();
          loweredCase = true;
      }
    }
  }
  Buffer$2.byteLength = byteLength;

  function slowToString (encoding, start, end) {
    var loweredCase = false;

    // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
    // property of a typed array.

    // This behaves neither like String nor Uint8Array in that we set start/end
    // to their upper/lower bounds if the value passed is out of range.
    // undefined is handled specially as per ECMA-262 6th Edition,
    // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
    if (start === undefined || start < 0) {
      start = 0;
    }
    // Return early if start > this.length. Done here to prevent potential uint32
    // coercion fail below.
    if (start > this.length) {
      return ''
    }

    if (end === undefined || end > this.length) {
      end = this.length;
    }

    if (end <= 0) {
      return ''
    }

    // Force coersion to uint32. This will also coerce falsey/NaN values to 0.
    end >>>= 0;
    start >>>= 0;

    if (end <= start) {
      return ''
    }

    if (!encoding) { encoding = 'utf8'; }

    while (true) {
      switch (encoding) {
        case 'hex':
          return hexSlice(this, start, end)

        case 'utf8':
        case 'utf-8':
          return utf8Slice(this, start, end)

        case 'ascii':
          return asciiSlice(this, start, end)

        case 'latin1':
        case 'binary':
          return latin1Slice(this, start, end)

        case 'base64':
          return base64Slice(this, start, end)

        case 'ucs2':
        case 'ucs-2':
        case 'utf16le':
        case 'utf-16le':
          return utf16leSlice(this, start, end)

        default:
          if (loweredCase) { throw new TypeError('Unknown encoding: ' + encoding) }
          encoding = (encoding + '').toLowerCase();
          loweredCase = true;
      }
    }
  }

  // The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
  // Buffer instances.
  Buffer$2.prototype._isBuffer = true;

  function swap (b, n, m) {
    var i = b[n];
    b[n] = b[m];
    b[m] = i;
  }

  Buffer$2.prototype.swap16 = function swap16 () {
    var len = this.length;
    if (len % 2 !== 0) {
      throw new RangeError('Buffer size must be a multiple of 16-bits')
    }
    for (var i = 0; i < len; i += 2) {
      swap(this, i, i + 1);
    }
    return this
  };

  Buffer$2.prototype.swap32 = function swap32 () {
    var len = this.length;
    if (len % 4 !== 0) {
      throw new RangeError('Buffer size must be a multiple of 32-bits')
    }
    for (var i = 0; i < len; i += 4) {
      swap(this, i, i + 3);
      swap(this, i + 1, i + 2);
    }
    return this
  };

  Buffer$2.prototype.swap64 = function swap64 () {
    var len = this.length;
    if (len % 8 !== 0) {
      throw new RangeError('Buffer size must be a multiple of 64-bits')
    }
    for (var i = 0; i < len; i += 8) {
      swap(this, i, i + 7);
      swap(this, i + 1, i + 6);
      swap(this, i + 2, i + 5);
      swap(this, i + 3, i + 4);
    }
    return this
  };

  Buffer$2.prototype.toString = function toString () {
    var length = this.length | 0;
    if (length === 0) { return '' }
    if (arguments.length === 0) { return utf8Slice(this, 0, length) }
    return slowToString.apply(this, arguments)
  };

  Buffer$2.prototype.equals = function equals (b) {
    if (!internalIsBuffer(b)) { throw new TypeError('Argument must be a Buffer') }
    if (this === b) { return true }
    return Buffer$2.compare(this, b) === 0
  };

  Buffer$2.prototype.inspect = function inspect () {
    var str = '';
    var max = INSPECT_MAX_BYTES;
    if (this.length > 0) {
      str = this.toString('hex', 0, max).match(/.{2}/g).join(' ');
      if (this.length > max) { str += ' ... '; }
    }
    return '<Buffer ' + str + '>'
  };

  Buffer$2.prototype.compare = function compare (target, start, end, thisStart, thisEnd) {
    if (!internalIsBuffer(target)) {
      throw new TypeError('Argument must be a Buffer')
    }

    if (start === undefined) {
      start = 0;
    }
    if (end === undefined) {
      end = target ? target.length : 0;
    }
    if (thisStart === undefined) {
      thisStart = 0;
    }
    if (thisEnd === undefined) {
      thisEnd = this.length;
    }

    if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
      throw new RangeError('out of range index')
    }

    if (thisStart >= thisEnd && start >= end) {
      return 0
    }
    if (thisStart >= thisEnd) {
      return -1
    }
    if (start >= end) {
      return 1
    }

    start >>>= 0;
    end >>>= 0;
    thisStart >>>= 0;
    thisEnd >>>= 0;

    if (this === target) { return 0 }

    var x = thisEnd - thisStart;
    var y = end - start;
    var len = Math.min(x, y);

    var thisCopy = this.slice(thisStart, thisEnd);
    var targetCopy = target.slice(start, end);

    for (var i = 0; i < len; ++i) {
      if (thisCopy[i] !== targetCopy[i]) {
        x = thisCopy[i];
        y = targetCopy[i];
        break
      }
    }

    if (x < y) { return -1 }
    if (y < x) { return 1 }
    return 0
  };

  // Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
  // OR the last index of `val` in `buffer` at offset <= `byteOffset`.
  //
  // Arguments:
  // - buffer - a Buffer to search
  // - val - a string, Buffer, or number
  // - byteOffset - an index into `buffer`; will be clamped to an int32
  // - encoding - an optional encoding, relevant is val is a string
  // - dir - true for indexOf, false for lastIndexOf
  function bidirectionalIndexOf (buffer, val, byteOffset, encoding, dir) {
    // Empty buffer means no match
    if (buffer.length === 0) { return -1 }

    // Normalize byteOffset
    if (typeof byteOffset === 'string') {
      encoding = byteOffset;
      byteOffset = 0;
    } else if (byteOffset > 0x7fffffff) {
      byteOffset = 0x7fffffff;
    } else if (byteOffset < -0x80000000) {
      byteOffset = -0x80000000;
    }
    byteOffset = +byteOffset;  // Coerce to Number.
    if (isNaN(byteOffset)) {
      // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
      byteOffset = dir ? 0 : (buffer.length - 1);
    }

    // Normalize byteOffset: negative offsets start from the end of the buffer
    if (byteOffset < 0) { byteOffset = buffer.length + byteOffset; }
    if (byteOffset >= buffer.length) {
      if (dir) { return -1 }
      else { byteOffset = buffer.length - 1; }
    } else if (byteOffset < 0) {
      if (dir) { byteOffset = 0; }
      else { return -1 }
    }

    // Normalize val
    if (typeof val === 'string') {
      val = Buffer$2.from(val, encoding);
    }

    // Finally, search either indexOf (if dir is true) or lastIndexOf
    if (internalIsBuffer(val)) {
      // Special case: looking for empty string/buffer always fails
      if (val.length === 0) {
        return -1
      }
      return arrayIndexOf(buffer, val, byteOffset, encoding, dir)
    } else if (typeof val === 'number') {
      val = val & 0xFF; // Search for a byte value [0-255]
      if (Buffer$2.TYPED_ARRAY_SUPPORT &&
          typeof Uint8Array.prototype.indexOf === 'function') {
        if (dir) {
          return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset)
        } else {
          return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset)
        }
      }
      return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir)
    }

    throw new TypeError('val must be string, number or Buffer')
  }

  function arrayIndexOf (arr, val, byteOffset, encoding, dir) {
    var indexSize = 1;
    var arrLength = arr.length;
    var valLength = val.length;

    if (encoding !== undefined) {
      encoding = String(encoding).toLowerCase();
      if (encoding === 'ucs2' || encoding === 'ucs-2' ||
          encoding === 'utf16le' || encoding === 'utf-16le') {
        if (arr.length < 2 || val.length < 2) {
          return -1
        }
        indexSize = 2;
        arrLength /= 2;
        valLength /= 2;
        byteOffset /= 2;
      }
    }

    function read (buf, i) {
      if (indexSize === 1) {
        return buf[i]
      } else {
        return buf.readUInt16BE(i * indexSize)
      }
    }

    var i;
    if (dir) {
      var foundIndex = -1;
      for (i = byteOffset; i < arrLength; i++) {
        if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
          if (foundIndex === -1) { foundIndex = i; }
          if (i - foundIndex + 1 === valLength) { return foundIndex * indexSize }
        } else {
          if (foundIndex !== -1) { i -= i - foundIndex; }
          foundIndex = -1;
        }
      }
    } else {
      if (byteOffset + valLength > arrLength) { byteOffset = arrLength - valLength; }
      for (i = byteOffset; i >= 0; i--) {
        var found = true;
        for (var j = 0; j < valLength; j++) {
          if (read(arr, i + j) !== read(val, j)) {
            found = false;
            break
          }
        }
        if (found) { return i }
      }
    }

    return -1
  }

  Buffer$2.prototype.includes = function includes (val, byteOffset, encoding) {
    return this.indexOf(val, byteOffset, encoding) !== -1
  };

  Buffer$2.prototype.indexOf = function indexOf (val, byteOffset, encoding) {
    return bidirectionalIndexOf(this, val, byteOffset, encoding, true)
  };

  Buffer$2.prototype.lastIndexOf = function lastIndexOf (val, byteOffset, encoding) {
    return bidirectionalIndexOf(this, val, byteOffset, encoding, false)
  };

  function hexWrite (buf, string, offset, length) {
    offset = Number(offset) || 0;
    var remaining = buf.length - offset;
    if (!length) {
      length = remaining;
    } else {
      length = Number(length);
      if (length > remaining) {
        length = remaining;
      }
    }

    // must be an even number of digits
    var strLen = string.length;
    if (strLen % 2 !== 0) { throw new TypeError('Invalid hex string') }

    if (length > strLen / 2) {
      length = strLen / 2;
    }
    for (var i = 0; i < length; ++i) {
      var parsed = parseInt(string.substr(i * 2, 2), 16);
      if (isNaN(parsed)) { return i }
      buf[offset + i] = parsed;
    }
    return i
  }

  function utf8Write (buf, string, offset, length) {
    return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
  }

  function asciiWrite (buf, string, offset, length) {
    return blitBuffer(asciiToBytes(string), buf, offset, length)
  }

  function latin1Write (buf, string, offset, length) {
    return asciiWrite(buf, string, offset, length)
  }

  function base64Write (buf, string, offset, length) {
    return blitBuffer(base64ToBytes(string), buf, offset, length)
  }

  function ucs2Write (buf, string, offset, length) {
    return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
  }

  Buffer$2.prototype.write = function write (string, offset, length, encoding) {
    // Buffer#write(string)
    if (offset === undefined) {
      encoding = 'utf8';
      length = this.length;
      offset = 0;
    // Buffer#write(string, encoding)
    } else if (length === undefined && typeof offset === 'string') {
      encoding = offset;
      length = this.length;
      offset = 0;
    // Buffer#write(string, offset[, length][, encoding])
    } else if (isFinite(offset)) {
      offset = offset | 0;
      if (isFinite(length)) {
        length = length | 0;
        if (encoding === undefined) { encoding = 'utf8'; }
      } else {
        encoding = length;
        length = undefined;
      }
    // legacy write(string, encoding, offset, length) - remove in v0.13
    } else {
      throw new Error(
        'Buffer.write(string, encoding, offset[, length]) is no longer supported'
      )
    }

    var remaining = this.length - offset;
    if (length === undefined || length > remaining) { length = remaining; }

    if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
      throw new RangeError('Attempt to write outside buffer bounds')
    }

    if (!encoding) { encoding = 'utf8'; }

    var loweredCase = false;
    for (;;) {
      switch (encoding) {
        case 'hex':
          return hexWrite(this, string, offset, length)

        case 'utf8':
        case 'utf-8':
          return utf8Write(this, string, offset, length)

        case 'ascii':
          return asciiWrite(this, string, offset, length)

        case 'latin1':
        case 'binary':
          return latin1Write(this, string, offset, length)

        case 'base64':
          // Warning: maxLength not taken into account in base64Write
          return base64Write(this, string, offset, length)

        case 'ucs2':
        case 'ucs-2':
        case 'utf16le':
        case 'utf-16le':
          return ucs2Write(this, string, offset, length)

        default:
          if (loweredCase) { throw new TypeError('Unknown encoding: ' + encoding) }
          encoding = ('' + encoding).toLowerCase();
          loweredCase = true;
      }
    }
  };

  Buffer$2.prototype.toJSON = function toJSON () {
    return {
      type: 'Buffer',
      data: Array.prototype.slice.call(this._arr || this, 0)
    }
  };

  function base64Slice (buf, start, end) {
    if (start === 0 && end === buf.length) {
      return fromByteArray(buf)
    } else {
      return fromByteArray(buf.slice(start, end))
    }
  }

  function utf8Slice (buf, start, end) {
    end = Math.min(buf.length, end);
    var res = [];

    var i = start;
    while (i < end) {
      var firstByte = buf[i];
      var codePoint = null;
      var bytesPerSequence = (firstByte > 0xEF) ? 4
        : (firstByte > 0xDF) ? 3
        : (firstByte > 0xBF) ? 2
        : 1;

      if (i + bytesPerSequence <= end) {
        var secondByte, thirdByte, fourthByte, tempCodePoint;

        switch (bytesPerSequence) {
          case 1:
            if (firstByte < 0x80) {
              codePoint = firstByte;
            }
            break
          case 2:
            secondByte = buf[i + 1];
            if ((secondByte & 0xC0) === 0x80) {
              tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F);
              if (tempCodePoint > 0x7F) {
                codePoint = tempCodePoint;
              }
            }
            break
          case 3:
            secondByte = buf[i + 1];
            thirdByte = buf[i + 2];
            if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
              tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F);
              if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
                codePoint = tempCodePoint;
              }
            }
            break
          case 4:
            secondByte = buf[i + 1];
            thirdByte = buf[i + 2];
            fourthByte = buf[i + 3];
            if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
              tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F);
              if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
                codePoint = tempCodePoint;
              }
            }
        }
      }

      if (codePoint === null) {
        // we did not generate a valid codePoint so insert a
        // replacement char (U+FFFD) and advance only 1 byte
        codePoint = 0xFFFD;
        bytesPerSequence = 1;
      } else if (codePoint > 0xFFFF) {
        // encode to utf16 (surrogate pair dance)
        codePoint -= 0x10000;
        res.push(codePoint >>> 10 & 0x3FF | 0xD800);
        codePoint = 0xDC00 | codePoint & 0x3FF;
      }

      res.push(codePoint);
      i += bytesPerSequence;
    }

    return decodeCodePointsArray(res)
  }

  // Based on http://stackoverflow.com/a/22747272/680742, the browser with
  // the lowest limit is Chrome, with 0x10000 args.
  // We go 1 magnitude less, for safety
  var MAX_ARGUMENTS_LENGTH = 0x1000;

  function decodeCodePointsArray (codePoints) {
    var len = codePoints.length;
    if (len <= MAX_ARGUMENTS_LENGTH) {
      return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
    }

    // Decode in chunks to avoid "call stack size exceeded".
    var res = '';
    var i = 0;
    while (i < len) {
      res += String.fromCharCode.apply(
        String,
        codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
      );
    }
    return res
  }

  function asciiSlice (buf, start, end) {
    var ret = '';
    end = Math.min(buf.length, end);

    for (var i = start; i < end; ++i) {
      ret += String.fromCharCode(buf[i] & 0x7F);
    }
    return ret
  }

  function latin1Slice (buf, start, end) {
    var ret = '';
    end = Math.min(buf.length, end);

    for (var i = start; i < end; ++i) {
      ret += String.fromCharCode(buf[i]);
    }
    return ret
  }

  function hexSlice (buf, start, end) {
    var len = buf.length;

    if (!start || start < 0) { start = 0; }
    if (!end || end < 0 || end > len) { end = len; }

    var out = '';
    for (var i = start; i < end; ++i) {
      out += toHex(buf[i]);
    }
    return out
  }

  function utf16leSlice (buf, start, end) {
    var bytes = buf.slice(start, end);
    var res = '';
    for (var i = 0; i < bytes.length; i += 2) {
      res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
    }
    return res
  }

  Buffer$2.prototype.slice = function slice (start, end) {
    var len = this.length;
    start = ~~start;
    end = end === undefined ? len : ~~end;

    if (start < 0) {
      start += len;
      if (start < 0) { start = 0; }
    } else if (start > len) {
      start = len;
    }

    if (end < 0) {
      end += len;
      if (end < 0) { end = 0; }
    } else if (end > len) {
      end = len;
    }

    if (end < start) { end = start; }

    var newBuf;
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      newBuf = this.subarray(start, end);
      newBuf.__proto__ = Buffer$2.prototype;
    } else {
      var sliceLen = end - start;
      newBuf = new Buffer$2(sliceLen, undefined);
      for (var i = 0; i < sliceLen; ++i) {
        newBuf[i] = this[i + start];
      }
    }

    return newBuf
  };

  /*
   * Need to make sure that buffer isn't trying to write out of bounds.
   */
  function checkOffset (offset, ext, length) {
    if ((offset % 1) !== 0 || offset < 0) { throw new RangeError('offset is not uint') }
    if (offset + ext > length) { throw new RangeError('Trying to access beyond buffer length') }
  }

  Buffer$2.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
    offset = offset | 0;
    byteLength = byteLength | 0;
    if (!noAssert) { checkOffset(offset, byteLength, this.length); }

    var val = this[offset];
    var mul = 1;
    var i = 0;
    while (++i < byteLength && (mul *= 0x100)) {
      val += this[offset + i] * mul;
    }

    return val
  };

  Buffer$2.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
    offset = offset | 0;
    byteLength = byteLength | 0;
    if (!noAssert) {
      checkOffset(offset, byteLength, this.length);
    }

    var val = this[offset + --byteLength];
    var mul = 1;
    while (byteLength > 0 && (mul *= 0x100)) {
      val += this[offset + --byteLength] * mul;
    }

    return val
  };

  Buffer$2.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 1, this.length); }
    return this[offset]
  };

  Buffer$2.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 2, this.length); }
    return this[offset] | (this[offset + 1] << 8)
  };

  Buffer$2.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 2, this.length); }
    return (this[offset] << 8) | this[offset + 1]
  };

  Buffer$2.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 4, this.length); }

    return ((this[offset]) |
        (this[offset + 1] << 8) |
        (this[offset + 2] << 16)) +
        (this[offset + 3] * 0x1000000)
  };

  Buffer$2.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 4, this.length); }

    return (this[offset] * 0x1000000) +
      ((this[offset + 1] << 16) |
      (this[offset + 2] << 8) |
      this[offset + 3])
  };

  Buffer$2.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
    offset = offset | 0;
    byteLength = byteLength | 0;
    if (!noAssert) { checkOffset(offset, byteLength, this.length); }

    var val = this[offset];
    var mul = 1;
    var i = 0;
    while (++i < byteLength && (mul *= 0x100)) {
      val += this[offset + i] * mul;
    }
    mul *= 0x80;

    if (val >= mul) { val -= Math.pow(2, 8 * byteLength); }

    return val
  };

  Buffer$2.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
    offset = offset | 0;
    byteLength = byteLength | 0;
    if (!noAssert) { checkOffset(offset, byteLength, this.length); }

    var i = byteLength;
    var mul = 1;
    var val = this[offset + --i];
    while (i > 0 && (mul *= 0x100)) {
      val += this[offset + --i] * mul;
    }
    mul *= 0x80;

    if (val >= mul) { val -= Math.pow(2, 8 * byteLength); }

    return val
  };

  Buffer$2.prototype.readInt8 = function readInt8 (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 1, this.length); }
    if (!(this[offset] & 0x80)) { return (this[offset]) }
    return ((0xff - this[offset] + 1) * -1)
  };

  Buffer$2.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 2, this.length); }
    var val = this[offset] | (this[offset + 1] << 8);
    return (val & 0x8000) ? val | 0xFFFF0000 : val
  };

  Buffer$2.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 2, this.length); }
    var val = this[offset + 1] | (this[offset] << 8);
    return (val & 0x8000) ? val | 0xFFFF0000 : val
  };

  Buffer$2.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 4, this.length); }

    return (this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16) |
      (this[offset + 3] << 24)
  };

  Buffer$2.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 4, this.length); }

    return (this[offset] << 24) |
      (this[offset + 1] << 16) |
      (this[offset + 2] << 8) |
      (this[offset + 3])
  };

  Buffer$2.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 4, this.length); }
    return read(this, offset, true, 23, 4)
  };

  Buffer$2.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 4, this.length); }
    return read(this, offset, false, 23, 4)
  };

  Buffer$2.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 8, this.length); }
    return read(this, offset, true, 52, 8)
  };

  Buffer$2.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
    if (!noAssert) { checkOffset(offset, 8, this.length); }
    return read(this, offset, false, 52, 8)
  };

  function checkInt (buf, value, offset, ext, max, min) {
    if (!internalIsBuffer(buf)) { throw new TypeError('"buffer" argument must be a Buffer instance') }
    if (value > max || value < min) { throw new RangeError('"value" argument is out of bounds') }
    if (offset + ext > buf.length) { throw new RangeError('Index out of range') }
  }

  Buffer$2.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
    value = +value;
    offset = offset | 0;
    byteLength = byteLength | 0;
    if (!noAssert) {
      var maxBytes = Math.pow(2, 8 * byteLength) - 1;
      checkInt(this, value, offset, byteLength, maxBytes, 0);
    }

    var mul = 1;
    var i = 0;
    this[offset] = value & 0xFF;
    while (++i < byteLength && (mul *= 0x100)) {
      this[offset + i] = (value / mul) & 0xFF;
    }

    return offset + byteLength
  };

  Buffer$2.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
    value = +value;
    offset = offset | 0;
    byteLength = byteLength | 0;
    if (!noAssert) {
      var maxBytes = Math.pow(2, 8 * byteLength) - 1;
      checkInt(this, value, offset, byteLength, maxBytes, 0);
    }

    var i = byteLength - 1;
    var mul = 1;
    this[offset + i] = value & 0xFF;
    while (--i >= 0 && (mul *= 0x100)) {
      this[offset + i] = (value / mul) & 0xFF;
    }

    return offset + byteLength
  };

  Buffer$2.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 1, 0xff, 0); }
    if (!Buffer$2.TYPED_ARRAY_SUPPORT) { value = Math.floor(value); }
    this[offset] = (value & 0xff);
    return offset + 1
  };

  function objectWriteUInt16 (buf, value, offset, littleEndian) {
    if (value < 0) { value = 0xffff + value + 1; }
    for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
      buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
        (littleEndian ? i : 1 - i) * 8;
    }
  }

  Buffer$2.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 2, 0xffff, 0); }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      this[offset] = (value & 0xff);
      this[offset + 1] = (value >>> 8);
    } else {
      objectWriteUInt16(this, value, offset, true);
    }
    return offset + 2
  };

  Buffer$2.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 2, 0xffff, 0); }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      this[offset] = (value >>> 8);
      this[offset + 1] = (value & 0xff);
    } else {
      objectWriteUInt16(this, value, offset, false);
    }
    return offset + 2
  };

  function objectWriteUInt32 (buf, value, offset, littleEndian) {
    if (value < 0) { value = 0xffffffff + value + 1; }
    for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
      buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff;
    }
  }

  Buffer$2.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 4, 0xffffffff, 0); }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      this[offset + 3] = (value >>> 24);
      this[offset + 2] = (value >>> 16);
      this[offset + 1] = (value >>> 8);
      this[offset] = (value & 0xff);
    } else {
      objectWriteUInt32(this, value, offset, true);
    }
    return offset + 4
  };

  Buffer$2.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 4, 0xffffffff, 0); }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      this[offset] = (value >>> 24);
      this[offset + 1] = (value >>> 16);
      this[offset + 2] = (value >>> 8);
      this[offset + 3] = (value & 0xff);
    } else {
      objectWriteUInt32(this, value, offset, false);
    }
    return offset + 4
  };

  Buffer$2.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) {
      var limit = Math.pow(2, 8 * byteLength - 1);

      checkInt(this, value, offset, byteLength, limit - 1, -limit);
    }

    var i = 0;
    var mul = 1;
    var sub = 0;
    this[offset] = value & 0xFF;
    while (++i < byteLength && (mul *= 0x100)) {
      if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
        sub = 1;
      }
      this[offset + i] = ((value / mul) >> 0) - sub & 0xFF;
    }

    return offset + byteLength
  };

  Buffer$2.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) {
      var limit = Math.pow(2, 8 * byteLength - 1);

      checkInt(this, value, offset, byteLength, limit - 1, -limit);
    }

    var i = byteLength - 1;
    var mul = 1;
    var sub = 0;
    this[offset + i] = value & 0xFF;
    while (--i >= 0 && (mul *= 0x100)) {
      if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
        sub = 1;
      }
      this[offset + i] = ((value / mul) >> 0) - sub & 0xFF;
    }

    return offset + byteLength
  };

  Buffer$2.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 1, 0x7f, -0x80); }
    if (!Buffer$2.TYPED_ARRAY_SUPPORT) { value = Math.floor(value); }
    if (value < 0) { value = 0xff + value + 1; }
    this[offset] = (value & 0xff);
    return offset + 1
  };

  Buffer$2.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 2, 0x7fff, -0x8000); }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      this[offset] = (value & 0xff);
      this[offset + 1] = (value >>> 8);
    } else {
      objectWriteUInt16(this, value, offset, true);
    }
    return offset + 2
  };

  Buffer$2.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 2, 0x7fff, -0x8000); }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      this[offset] = (value >>> 8);
      this[offset + 1] = (value & 0xff);
    } else {
      objectWriteUInt16(this, value, offset, false);
    }
    return offset + 2
  };

  Buffer$2.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000); }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      this[offset] = (value & 0xff);
      this[offset + 1] = (value >>> 8);
      this[offset + 2] = (value >>> 16);
      this[offset + 3] = (value >>> 24);
    } else {
      objectWriteUInt32(this, value, offset, true);
    }
    return offset + 4
  };

  Buffer$2.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
    value = +value;
    offset = offset | 0;
    if (!noAssert) { checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000); }
    if (value < 0) { value = 0xffffffff + value + 1; }
    if (Buffer$2.TYPED_ARRAY_SUPPORT) {
      this[offset] = (value >>> 24);
      this[offset + 1] = (value >>> 16);
      this[offset + 2] = (value >>> 8);
      this[offset + 3] = (value & 0xff);
    } else {
      objectWriteUInt32(this, value, offset, false);
    }
    return offset + 4
  };

  function checkIEEE754 (buf, value, offset, ext, max, min) {
    if (offset + ext > buf.length) { throw new RangeError('Index out of range') }
    if (offset < 0) { throw new RangeError('Index out of range') }
  }

  function writeFloat (buf, value, offset, littleEndian, noAssert) {
    if (!noAssert) {
      checkIEEE754(buf, value, offset, 4);
    }
    write(buf, value, offset, littleEndian, 23, 4);
    return offset + 4
  }

  Buffer$2.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
    return writeFloat(this, value, offset, true, noAssert)
  };

  Buffer$2.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
    return writeFloat(this, value, offset, false, noAssert)
  };

  function writeDouble (buf, value, offset, littleEndian, noAssert) {
    if (!noAssert) {
      checkIEEE754(buf, value, offset, 8);
    }
    write(buf, value, offset, littleEndian, 52, 8);
    return offset + 8
  }

  Buffer$2.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
    return writeDouble(this, value, offset, true, noAssert)
  };

  Buffer$2.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
    return writeDouble(this, value, offset, false, noAssert)
  };

  // copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
  Buffer$2.prototype.copy = function copy (target, targetStart, start, end) {
    if (!start) { start = 0; }
    if (!end && end !== 0) { end = this.length; }
    if (targetStart >= target.length) { targetStart = target.length; }
    if (!targetStart) { targetStart = 0; }
    if (end > 0 && end < start) { end = start; }

    // Copy 0 bytes; we're done
    if (end === start) { return 0 }
    if (target.length === 0 || this.length === 0) { return 0 }

    // Fatal error conditions
    if (targetStart < 0) {
      throw new RangeError('targetStart out of bounds')
    }
    if (start < 0 || start >= this.length) { throw new RangeError('sourceStart out of bounds') }
    if (end < 0) { throw new RangeError('sourceEnd out of bounds') }

    // Are we oob?
    if (end > this.length) { end = this.length; }
    if (target.length - targetStart < end - start) {
      end = target.length - targetStart + start;
    }

    var len = end - start;
    var i;

    if (this === target && start < targetStart && targetStart < end) {
      // descending copy from end
      for (i = len - 1; i >= 0; --i) {
        target[i + targetStart] = this[i + start];
      }
    } else if (len < 1000 || !Buffer$2.TYPED_ARRAY_SUPPORT) {
      // ascending copy from start
      for (i = 0; i < len; ++i) {
        target[i + targetStart] = this[i + start];
      }
    } else {
      Uint8Array.prototype.set.call(
        target,
        this.subarray(start, start + len),
        targetStart
      );
    }

    return len
  };

  // Usage:
  //    buffer.fill(number[, offset[, end]])
  //    buffer.fill(buffer[, offset[, end]])
  //    buffer.fill(string[, offset[, end]][, encoding])
  Buffer$2.prototype.fill = function fill (val, start, end, encoding) {
    // Handle string cases:
    if (typeof val === 'string') {
      if (typeof start === 'string') {
        encoding = start;
        start = 0;
        end = this.length;
      } else if (typeof end === 'string') {
        encoding = end;
        end = this.length;
      }
      if (val.length === 1) {
        var code = val.charCodeAt(0);
        if (code < 256) {
          val = code;
        }
      }
      if (encoding !== undefined && typeof encoding !== 'string') {
        throw new TypeError('encoding must be a string')
      }
      if (typeof encoding === 'string' && !Buffer$2.isEncoding(encoding)) {
        throw new TypeError('Unknown encoding: ' + encoding)
      }
    } else if (typeof val === 'number') {
      val = val & 255;
    }

    // Invalid ranges are not set to a default, so can range check early.
    if (start < 0 || this.length < start || this.length < end) {
      throw new RangeError('Out of range index')
    }

    if (end <= start) {
      return this
    }

    start = start >>> 0;
    end = end === undefined ? this.length : end >>> 0;

    if (!val) { val = 0; }

    var i;
    if (typeof val === 'number') {
      for (i = start; i < end; ++i) {
        this[i] = val;
      }
    } else {
      var bytes = internalIsBuffer(val)
        ? val
        : utf8ToBytes(new Buffer$2(val, encoding).toString());
      var len = bytes.length;
      for (i = 0; i < end - start; ++i) {
        this[i + start] = bytes[i % len];
      }
    }

    return this
  };

  // HELPER FUNCTIONS
  // ================

  var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g;

  function base64clean (str) {
    // Node strips out invalid characters like \n and \t from the string, base64-js does not
    str = stringtrim(str).replace(INVALID_BASE64_RE, '');
    // Node converts strings with length < 2 to ''
    if (str.length < 2) { return '' }
    // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
    while (str.length % 4 !== 0) {
      str = str + '=';
    }
    return str
  }

  function stringtrim (str) {
    if (str.trim) { return str.trim() }
    return str.replace(/^\s+|\s+$/g, '')
  }

  function toHex (n) {
    if (n < 16) { return '0' + n.toString(16) }
    return n.toString(16)
  }

  function utf8ToBytes (string, units) {
    units = units || Infinity;
    var codePoint;
    var length = string.length;
    var leadSurrogate = null;
    var bytes = [];

    for (var i = 0; i < length; ++i) {
      codePoint = string.charCodeAt(i);

      // is surrogate component
      if (codePoint > 0xD7FF && codePoint < 0xE000) {
        // last char was a lead
        if (!leadSurrogate) {
          // no lead yet
          if (codePoint > 0xDBFF) {
            // unexpected trail
            if ((units -= 3) > -1) { bytes.push(0xEF, 0xBF, 0xBD); }
            continue
          } else if (i + 1 === length) {
            // unpaired lead
            if ((units -= 3) > -1) { bytes.push(0xEF, 0xBF, 0xBD); }
            continue
          }

          // valid lead
          leadSurrogate = codePoint;

          continue
        }

        // 2 leads in a row
        if (codePoint < 0xDC00) {
          if ((units -= 3) > -1) { bytes.push(0xEF, 0xBF, 0xBD); }
          leadSurrogate = codePoint;
          continue
        }

        // valid surrogate pair
        codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000;
      } else if (leadSurrogate) {
        // valid bmp char, but last char was a lead
        if ((units -= 3) > -1) { bytes.push(0xEF, 0xBF, 0xBD); }
      }

      leadSurrogate = null;

      // encode utf8
      if (codePoint < 0x80) {
        if ((units -= 1) < 0) { break }
        bytes.push(codePoint);
      } else if (codePoint < 0x800) {
        if ((units -= 2) < 0) { break }
        bytes.push(
          codePoint >> 0x6 | 0xC0,
          codePoint & 0x3F | 0x80
        );
      } else if (codePoint < 0x10000) {
        if ((units -= 3) < 0) { break }
        bytes.push(
          codePoint >> 0xC | 0xE0,
          codePoint >> 0x6 & 0x3F | 0x80,
          codePoint & 0x3F | 0x80
        );
      } else if (codePoint < 0x110000) {
        if ((units -= 4) < 0) { break }
        bytes.push(
          codePoint >> 0x12 | 0xF0,
          codePoint >> 0xC & 0x3F | 0x80,
          codePoint >> 0x6 & 0x3F | 0x80,
          codePoint & 0x3F | 0x80
        );
      } else {
        throw new Error('Invalid code point')
      }
    }

    return bytes
  }

  function asciiToBytes (str) {
    var byteArray = [];
    for (var i = 0; i < str.length; ++i) {
      // Node's code seems to be doing this and not & 0x7F..
      byteArray.push(str.charCodeAt(i) & 0xFF);
    }
    return byteArray
  }

  function utf16leToBytes (str, units) {
    var c, hi, lo;
    var byteArray = [];
    for (var i = 0; i < str.length; ++i) {
      if ((units -= 2) < 0) { break }

      c = str.charCodeAt(i);
      hi = c >> 8;
      lo = c % 256;
      byteArray.push(lo);
      byteArray.push(hi);
    }

    return byteArray
  }


  function base64ToBytes (str) {
    return toByteArray(base64clean(str))
  }

  function blitBuffer (src, dst, offset, length) {
    for (var i = 0; i < length; ++i) {
      if ((i + offset >= dst.length) || (i >= src.length)) { break }
      dst[i + offset] = src[i];
    }
    return i
  }

  function isnan (val) {
    return val !== val // eslint-disable-line no-self-compare
  }


  // the following is from is-buffer, also by Feross Aboukhadijeh and with same lisence
  // The _isBuffer check is for Safari 5-7 support, because it's missing
  // Object.prototype.constructor. Remove this eventually
  function isBuffer$1(obj) {
    return obj != null && (!!obj._isBuffer || isFastBuffer(obj) || isSlowBuffer(obj))
  }

  function isFastBuffer (obj) {
    return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
  }

  // For Node v0.10 support. Remove this eventually.
  function isSlowBuffer (obj) {
    return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isFastBuffer(obj.slice(0, 0))
  }

  // bunch of generic helpers

  /**
   * DIY in Array
   * @param {array} arr to check from
   * @param {*} value to check against
   * @return {boolean} true on found
   */
  var inArray$1 = function (arr, value) { return !!arr.filter(function (a) { return a === value; }).length; };

  // quick and dirty to turn non array to array
  var toArray = function (arg) { return isArray(arg) ? arg : [arg]; };

  /**
   * @param {object} obj for search
   * @param {string} key target
   * @return {boolean} true on success
   */
  var isObjectHasKey$2 = function(obj, key) {
    try {
      var keys = Object.keys(obj);
      return inArray$1(keys, key)
    } catch(e) {
      // @BUG when the obj is not an OBJECT we got some weird output
      return false;
      /*
      console.info('obj', obj)
      console.error(e)
      throw new Error(e)
      */
    }
  };

  // split the contract into the node side and the generic side
  /**
   * Check if the json is a contract file or not
   * @param {object} contract json object
   * @return {boolean} true
   */
  function checkIsContract(contract) {
    return isPlainObject(contract)
    && (
      isObjectHasKey$2(contract, QUERY_NAME)
   || isObjectHasKey$2(contract, MUTATION_NAME)
   || isObjectHasKey$2(contract, SOCKET_NAME)
    )
  }

  /**
   * Wrapper method that check if it's contract then return the contract or false
   * @param {object} contract the object to check
   * @return {boolean | object} false when it's not
   */
  function isContract(contract) {
    return checkIsContract(contract) ? contract : false;
  }

  /**
   * generate a 32bit hash based on the function.toString()
   * _from http://stackoverflow.com/questions/7616461/generate-a-hash-_from-string-in-javascript-jquery
   * @param {string} s the converted to string function
   * @return {string} the hashed function string
   */
  function hashCode(s) {
  	return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
  }
  // wrapper to make sure it string 
  function hashCode2Str(s) {
    return hashCode(s) + ''
  }

  // take only the module part which is what we use here

  /**
   * @param {object} jsonqlInstance the init instance of jsonql client
   * @param {object} contract the static contract
   * @return {object} contract may be from server
   */
  var getContractFromConfig = function(jsonqlInstance, contract) {
    if ( contract === void 0 ) contract = {};

    if (isContract(contract)) {
      return Promise.resolve(contract)
    }
    return jsonqlInstance.getContract()
  };
  // wrapper method to make sure it's a string
  // just alias now
  var hashCode$1 = function (str) { return hashCode2Str(str); };
  var USERDATA_TABLE = 'userdata';
  var CLS_LOCAL_STORE_NAME = 'localStore';
  var CLS_SESS_STORE_NAME = 'sessionStore';
  var CLS_CONTRACT_NAME = 'contract';
  var CLS_PROFILE_IDX = 'prof_idx';
  var LOG_ERROR_SWITCH = '__error__';
  var ZERO_IDX = 0;

  /**
   * The code was extracted from:
   * https://github.com/davidchambers/Base64.js
   */

  var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

  function InvalidCharacterError(message) {
    this.message = message;
  }

  InvalidCharacterError.prototype = new Error();
  InvalidCharacterError.prototype.name = 'InvalidCharacterError';

  function polyfill (input) {
    var str = String(input).replace(/=+$/, '');
    if (str.length % 4 == 1) {
      throw new InvalidCharacterError("'atob' failed: The string to be decoded is not correctly encoded.");
    }
    for (
      // initialize result and counters
      var bc = 0, bs, buffer, idx = 0, output = '';
      // get next character
      buffer = str.charAt(idx++);
      // character found in table? initialize bit storage and add its ascii value;
      ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
        // and if not first of each 4 characters,
        // convert the first 8 bits to one ascii character
        bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
    ) {
      // try to find character in table (0-63, not found => -1)
      buffer = chars.indexOf(buffer);
    }
    return output;
  }


  var atob = typeof window !== 'undefined' && window.atob && window.atob.bind(window) || polyfill;

  function b64DecodeUnicode(str) {
    return decodeURIComponent(atob(str).replace(/(.)/g, function (m, p) {
      var code = p.charCodeAt(0).toString(16).toUpperCase();
      if (code.length < 2) {
        code = '0' + code;
      }
      return '%' + code;
    }));
  }

  var base64_url_decode = function(str) {
    var output = str.replace(/-/g, "+").replace(/_/g, "/");
    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += "==";
        break;
      case 3:
        output += "=";
        break;
      default:
        throw "Illegal base64url string!";
    }

    try{
      return b64DecodeUnicode(output);
    } catch (err) {
      return atob(output);
    }
  };

  function InvalidTokenError(message) {
    this.message = message;
  }

  InvalidTokenError.prototype = new Error();
  InvalidTokenError.prototype.name = 'InvalidTokenError';

  var lib = function (token,options) {
    if (typeof token !== 'string') {
      throw new InvalidTokenError('Invalid token specified');
    }

    options = options || {};
    var pos = options.header === true ? 0 : 1;
    try {
      return JSON.parse(base64_url_decode(token.split('.')[pos]));
    } catch (e) {
      throw new InvalidTokenError('Invalid token specified: ' + e.message);
    }
  };

  var InvalidTokenError_1 = InvalidTokenError;
  lib.InvalidTokenError = InvalidTokenError_1;

  // validate string type
  /**
   * @param {string} value expected value
   * @return {boolean} true if OK
   */
  var checkIsString = function(value) {
    return (trim(value) !== '') ? isString(value) : false;
  };

  // when the user is login with the jwt

  var timestamp = function (sec) {
    if ( sec === void 0 ) sec = false;

    var time = Date.now();
    return sec ? Math.floor( time / 1000 ) : time;
  };

  /**
   * We only check the nbf and exp
   * @param {object} token for checking
   * @return {object} token on success
   */
  function validate(token) {
    var start = token.iat || timestamp(true);
    // we only check the exp for the time being
    if (token.exp) {
      if (start >= token.exp) {
        var expired = new Date(token.exp).toISOString();
        throw new JsonqlError(("Token has expired on " + expired), token)
      }
    }
    return token;
  }

  /**
   * The browser client version it has far fewer options and it doesn't verify it
   * because it couldn't this is the job for the server
   * @TODO we need to add some extra proessing here to check for the exp field
   * @param {string} token to decrypted
   * @return {object} decrypted object
   */
  function jwtDecode(token) {
    if (checkIsString(token)) {
      var t = lib(token);
      return validate(t)
    }
    throw new JsonqlError('Token must be a string!')
  }

  /**
   * Check several parameter that there is something in the param
   * @param {*} param input
   * @return {boolean}
   */
   var isNotEmpty = function (a) {
    if (isArray(a)) {
      return true;
    }
    return a !== undefined && a !== null && trim(a) !== '';
  };

  /** `Object#toString` result references. */
  var numberTag$1 = '[object Number]';

  /**
   * Checks if `value` is classified as a `Number` primitive or object.
   *
   * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are
   * classified as numbers, use the `_.isFinite` method.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a number, else `false`.
   * @example
   *
   * _.isNumber(3);
   * // => true
   *
   * _.isNumber(Number.MIN_VALUE);
   * // => true
   *
   * _.isNumber(Infinity);
   * // => true
   *
   * _.isNumber('3');
   * // => false
   */
  function isNumber(value) {
    return typeof value == 'number' ||
      (isObjectLike(value) && baseGetTag(value) == numberTag$1);
  }

  /**
   * Checks if `value` is `NaN`.
   *
   * **Note:** This method is based on
   * [`Number.isNaN`](https://mdn.io/Number/isNaN) and is not the same as
   * global [`isNaN`](https://mdn.io/isNaN) which returns `true` for
   * `undefined` and other non-number values.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
   * @example
   *
   * _.isNaN(NaN);
   * // => true
   *
   * _.isNaN(new Number(NaN));
   * // => true
   *
   * isNaN(undefined);
   * // => true
   *
   * _.isNaN(undefined);
   * // => false
   */
  function isNaN$1(value) {
    // An `NaN` primitive is the only value that is not equal to itself.
    // Perform the `toStringTag` check first to avoid errors with some
    // ActiveX objects in IE.
    return isNumber(value) && value != +value;
  }

  // validator numbers
  /**
   * @2015-05-04 found a problem if the value is a number like string
   * it will pass, so add a chck if it's string before we pass to next
   * @param {number} value expected value
   * @return {boolean} true if OK
   */
  var checkIsNumber = function(value) {
    return isString(value) ? false : !isNaN$1( parseFloat(value) )
  };

  // check for boolean

  /**
   * @param {boolean} value expected
   * @return {boolean} true if OK
   */
  var checkIsBoolean = function(value) {
    return value !== null && value !== undefined && typeof value === 'boolean'
  };

  // validate any thing only check if there is something

  /**
   * @param {*} value the value
   * @param {boolean} [checkNull=true] strict check if there is null value
   * @return {boolean} true is OK
   */
  var checkIsAny = function(value, checkNull) {
    if ( checkNull === void 0 ) checkNull = true;

    if (value !== undefined && value !== '' && trim(value) !== '') {
      if (checkNull === false || (checkNull === true && value !== null)) {
        return true;
      }
    }
    return false;
  };

  // Good practice rule - No magic number

  var ARGS_NOT_ARRAY_ERR = "args is not an array! You might want to do: ES6 Array.from(arguments) or ES5 Array.prototype.slice.call(arguments)";
  var PARAMS_NOT_ARRAY_ERR = "params is not an array! Did something gone wrong when you generate the contract.json?";
  var EXCEPTION_CASE_ERR = 'Could not understand your arguments and parameter structure!';
  // @TODO the jsdoc return array.<type> and we should also allow array<type> syntax
  var DEFAULT_TYPE$1 = DEFAULT_TYPE;
  var ARRAY_TYPE_LFT$1 = ARRAY_TYPE_LFT;
  var ARRAY_TYPE_RGT$1 = ARRAY_TYPE_RGT;

  var TYPE_KEY$1 = TYPE_KEY;
  var OPTIONAL_KEY$1 = OPTIONAL_KEY;
  var ENUM_KEY$1 = ENUM_KEY;
  var ARGS_KEY$1 = ARGS_KEY;
  var CHECKER_KEY$1 = CHECKER_KEY;
  var ALIAS_KEY$1 = ALIAS_KEY;

  var ARRAY_TYPE$1 = ARRAY_TYPE;
  var OBJECT_TYPE$1 = OBJECT_TYPE;
  var STRING_TYPE$1 = STRING_TYPE;
  var BOOLEAN_TYPE$1 = BOOLEAN_TYPE;
  var NUMBER_TYPE$1 = NUMBER_TYPE;
  var KEY_WORD$1 = KEY_WORD;
  var OR_SEPERATOR$1 = OR_SEPERATOR;

  // not actually in use
  // export const NUMBER_TYPES = JSONQL_CONSTANTS.NUMBER_TYPES;

  // primitive types

  /**
   * this is a wrapper method to call different one based on their type
   * @param {string} type to check
   * @return {function} a function to handle the type
   */
  var combineFn = function(type) {
    switch (type) {
      case NUMBER_TYPE$1:
        return checkIsNumber;
      case STRING_TYPE$1:
        return checkIsString;
      case BOOLEAN_TYPE$1:
        return checkIsBoolean;
      default:
        return checkIsAny;
    }
  };

  // validate array type

  /**
   * @param {array} value expected
   * @param {string} [type=''] pass the type if we encounter array.<T> then we need to check the value as well
   * @return {boolean} true if OK
   */
  var checkIsArray = function(value, type) {
    if ( type === void 0 ) type='';

    if (isArray(value)) {
      if (type === '' || trim(type)==='') {
        return true;
      }
      // we test it in reverse
      // @TODO if the type is an array (OR) then what?
      // we need to take into account this could be an array
      var c = value.filter(function (v) { return !combineFn(type)(v); });
      return !(c.length > 0)
    }
    return false;
  };

  /**
   * check if it matches the array.<T> pattern
   * @param {string} type
   * @return {boolean|array} false means NO, always return array
   */
  var isArrayLike$1 = function(type) {
    // @TODO could that have something like array<> instead of array.<>? missing the dot?
    // because type script is Array<T> without the dot
    if (type.indexOf(ARRAY_TYPE_LFT$1) > -1 && type.indexOf(ARRAY_TYPE_RGT$1) > -1) {
      var _type = type.replace(ARRAY_TYPE_LFT$1, '').replace(ARRAY_TYPE_RGT$1, '');
      if (_type.indexOf(OR_SEPERATOR$1)) {
        return _type.split(OR_SEPERATOR$1)
      }
      return [_type]
    }
    return false;
  };

  /**
   * we might encounter something like array.<T> then we need to take it apart
   * @param {object} p the prepared object for processing
   * @param {string|array} type the type came from <T>
   * @return {boolean} for the filter to operate on
   */
  var arrayTypeHandler = function(p, type) {
    var arg = p.arg;
    // need a special case to handle the OR type
    // we need to test the args instead of the type(s)
    if (type.length > 1) {
      return !arg.filter(function (v) { return (
        !(type.length > type.filter(function (t) { return !combineFn(t)(v); }).length)
      ); }).length;
    }
    // type is array so this will be or!
    return type.length > type.filter(function (t) { return !checkIsArray(arg, t); }).length;
  };

  /**
   * A specialized version of `_.filter` for arrays without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} predicate The function invoked per iteration.
   * @returns {Array} Returns the new filtered array.
   */
  function arrayFilter(array, predicate) {
    var index = -1,
        length = array == null ? 0 : array.length,
        resIndex = 0,
        result = [];

    while (++index < length) {
      var value = array[index];
      if (predicate(value, index, array)) {
        result[resIndex++] = value;
      }
    }
    return result;
  }

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeKeys = overArg(Object.keys, Object);

  /** Used for built-in method references. */
  var objectProto$b = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$9 = objectProto$b.hasOwnProperty;

  /**
   * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   */
  function baseKeys(object) {
    if (!isPrototype(object)) {
      return nativeKeys(object);
    }
    var result = [];
    for (var key in Object(object)) {
      if (hasOwnProperty$9.call(object, key) && key != 'constructor') {
        result.push(key);
      }
    }
    return result;
  }

  /**
   * Creates an array of the own enumerable property names of `object`.
   *
   * **Note:** Non-object values are coerced to objects. See the
   * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
   * for more details.
   *
   * @static
   * @since 0.1.0
   * @memberOf _
   * @category Object
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   * @example
   *
   * function Foo() {
   *   this.a = 1;
   *   this.b = 2;
   * }
   *
   * Foo.prototype.c = 3;
   *
   * _.keys(new Foo);
   * // => ['a', 'b'] (iteration order is not guaranteed)
   *
   * _.keys('hi');
   * // => ['0', '1']
   */
  function keys(object) {
    return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
  }

  /**
   * The base implementation of `_.forOwn` without support for iteratee shorthands.
   *
   * @private
   * @param {Object} object The object to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Object} Returns `object`.
   */
  function baseForOwn(object, iteratee) {
    return object && baseFor(object, iteratee, keys);
  }

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED$2 = '__lodash_hash_undefined__';

  /**
   * Adds `value` to the array cache.
   *
   * @private
   * @name add
   * @memberOf SetCache
   * @alias push
   * @param {*} value The value to cache.
   * @returns {Object} Returns the cache instance.
   */
  function setCacheAdd(value) {
    this.__data__.set(value, HASH_UNDEFINED$2);
    return this;
  }

  /**
   * Checks if `value` is in the array cache.
   *
   * @private
   * @name has
   * @memberOf SetCache
   * @param {*} value The value to search for.
   * @returns {number} Returns `true` if `value` is found, else `false`.
   */
  function setCacheHas(value) {
    return this.__data__.has(value);
  }

  /**
   *
   * Creates an array cache object to store unique values.
   *
   * @private
   * @constructor
   * @param {Array} [values] The values to cache.
   */
  function SetCache(values) {
    var index = -1,
        length = values == null ? 0 : values.length;

    this.__data__ = new MapCache;
    while (++index < length) {
      this.add(values[index]);
    }
  }

  // Add methods to `SetCache`.
  SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
  SetCache.prototype.has = setCacheHas;

  /**
   * A specialized version of `_.some` for arrays without support for iteratee
   * shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} predicate The function invoked per iteration.
   * @returns {boolean} Returns `true` if any element passes the predicate check,
   *  else `false`.
   */
  function arraySome(array, predicate) {
    var index = -1,
        length = array == null ? 0 : array.length;

    while (++index < length) {
      if (predicate(array[index], index, array)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks if a `cache` value for `key` exists.
   *
   * @private
   * @param {Object} cache The cache to query.
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function cacheHas(cache, key) {
    return cache.has(key);
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG = 1,
      COMPARE_UNORDERED_FLAG = 2;

  /**
   * A specialized version of `baseIsEqualDeep` for arrays with support for
   * partial deep comparisons.
   *
   * @private
   * @param {Array} array The array to compare.
   * @param {Array} other The other array to compare.
   * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
   * @param {Function} customizer The function to customize comparisons.
   * @param {Function} equalFunc The function to determine equivalents of values.
   * @param {Object} stack Tracks traversed `array` and `other` objects.
   * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
   */
  function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
    var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
        arrLength = array.length,
        othLength = other.length;

    if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
      return false;
    }
    // Assume cyclic values are equal.
    var stacked = stack.get(array);
    if (stacked && stack.get(other)) {
      return stacked == other;
    }
    var index = -1,
        result = true,
        seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

    stack.set(array, other);
    stack.set(other, array);

    // Ignore non-index properties.
    while (++index < arrLength) {
      var arrValue = array[index],
          othValue = other[index];

      if (customizer) {
        var compared = isPartial
          ? customizer(othValue, arrValue, index, other, array, stack)
          : customizer(arrValue, othValue, index, array, other, stack);
      }
      if (compared !== undefined) {
        if (compared) {
          continue;
        }
        result = false;
        break;
      }
      // Recursively compare arrays (susceptible to call stack limits).
      if (seen) {
        if (!arraySome(other, function(othValue, othIndex) {
              if (!cacheHas(seen, othIndex) &&
                  (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
                return seen.push(othIndex);
              }
            })) {
          result = false;
          break;
        }
      } else if (!(
            arrValue === othValue ||
              equalFunc(arrValue, othValue, bitmask, customizer, stack)
          )) {
        result = false;
        break;
      }
    }
    stack['delete'](array);
    stack['delete'](other);
    return result;
  }

  /**
   * Converts `map` to its key-value pairs.
   *
   * @private
   * @param {Object} map The map to convert.
   * @returns {Array} Returns the key-value pairs.
   */
  function mapToArray(map) {
    var index = -1,
        result = Array(map.size);

    map.forEach(function(value, key) {
      result[++index] = [key, value];
    });
    return result;
  }

  /**
   * Converts `set` to an array of its values.
   *
   * @private
   * @param {Object} set The set to convert.
   * @returns {Array} Returns the values.
   */
  function setToArray(set) {
    var index = -1,
        result = Array(set.size);

    set.forEach(function(value) {
      result[++index] = value;
    });
    return result;
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$1 = 1,
      COMPARE_UNORDERED_FLAG$1 = 2;

  /** `Object#toString` result references. */
  var boolTag$1 = '[object Boolean]',
      dateTag$1 = '[object Date]',
      errorTag$1 = '[object Error]',
      mapTag$1 = '[object Map]',
      numberTag$2 = '[object Number]',
      regexpTag$1 = '[object RegExp]',
      setTag$1 = '[object Set]',
      stringTag$2 = '[object String]',
      symbolTag$1 = '[object Symbol]';

  var arrayBufferTag$1 = '[object ArrayBuffer]',
      dataViewTag$1 = '[object DataView]';

  /** Used to convert symbols to primitives and strings. */
  var symbolProto$1 = Symbol ? Symbol.prototype : undefined,
      symbolValueOf = symbolProto$1 ? symbolProto$1.valueOf : undefined;

  /**
   * A specialized version of `baseIsEqualDeep` for comparing objects of
   * the same `toStringTag`.
   *
   * **Note:** This function only supports comparing values with tags of
   * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
   *
   * @private
   * @param {Object} object The object to compare.
   * @param {Object} other The other object to compare.
   * @param {string} tag The `toStringTag` of the objects to compare.
   * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
   * @param {Function} customizer The function to customize comparisons.
   * @param {Function} equalFunc The function to determine equivalents of values.
   * @param {Object} stack Tracks traversed `object` and `other` objects.
   * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
   */
  function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
    switch (tag) {
      case dataViewTag$1:
        if ((object.byteLength != other.byteLength) ||
            (object.byteOffset != other.byteOffset)) {
          return false;
        }
        object = object.buffer;
        other = other.buffer;

      case arrayBufferTag$1:
        if ((object.byteLength != other.byteLength) ||
            !equalFunc(new Uint8Array$1(object), new Uint8Array$1(other))) {
          return false;
        }
        return true;

      case boolTag$1:
      case dateTag$1:
      case numberTag$2:
        // Coerce booleans to `1` or `0` and dates to milliseconds.
        // Invalid dates are coerced to `NaN`.
        return eq(+object, +other);

      case errorTag$1:
        return object.name == other.name && object.message == other.message;

      case regexpTag$1:
      case stringTag$2:
        // Coerce regexes to strings and treat strings, primitives and objects,
        // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
        // for more details.
        return object == (other + '');

      case mapTag$1:
        var convert = mapToArray;

      case setTag$1:
        var isPartial = bitmask & COMPARE_PARTIAL_FLAG$1;
        convert || (convert = setToArray);

        if (object.size != other.size && !isPartial) {
          return false;
        }
        // Assume cyclic values are equal.
        var stacked = stack.get(object);
        if (stacked) {
          return stacked == other;
        }
        bitmask |= COMPARE_UNORDERED_FLAG$1;

        // Recursively compare objects (susceptible to call stack limits).
        stack.set(object, other);
        var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
        stack['delete'](object);
        return result;

      case symbolTag$1:
        if (symbolValueOf) {
          return symbolValueOf.call(object) == symbolValueOf.call(other);
        }
    }
    return false;
  }

  /**
   * Appends the elements of `values` to `array`.
   *
   * @private
   * @param {Array} array The array to modify.
   * @param {Array} values The values to append.
   * @returns {Array} Returns `array`.
   */
  function arrayPush(array, values) {
    var index = -1,
        length = values.length,
        offset = array.length;

    while (++index < length) {
      array[offset + index] = values[index];
    }
    return array;
  }

  /**
   * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
   * `keysFunc` and `symbolsFunc` to get the enumerable property names and
   * symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {Function} keysFunc The function to get the keys of `object`.
   * @param {Function} symbolsFunc The function to get the symbols of `object`.
   * @returns {Array} Returns the array of property names and symbols.
   */
  function baseGetAllKeys(object, keysFunc, symbolsFunc) {
    var result = keysFunc(object);
    return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
  }

  /**
   * This method returns a new empty array.
   *
   * @static
   * @memberOf _
   * @since 4.13.0
   * @category Util
   * @returns {Array} Returns the new empty array.
   * @example
   *
   * var arrays = _.times(2, _.stubArray);
   *
   * console.log(arrays);
   * // => [[], []]
   *
   * console.log(arrays[0] === arrays[1]);
   * // => false
   */
  function stubArray() {
    return [];
  }

  /** Used for built-in method references. */
  var objectProto$c = Object.prototype;

  /** Built-in value references. */
  var propertyIsEnumerable$1 = objectProto$c.propertyIsEnumerable;

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeGetSymbols = Object.getOwnPropertySymbols;

  /**
   * Creates an array of the own enumerable symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of symbols.
   */
  var getSymbols = !nativeGetSymbols ? stubArray : function(object) {
    if (object == null) {
      return [];
    }
    object = Object(object);
    return arrayFilter(nativeGetSymbols(object), function(symbol) {
      return propertyIsEnumerable$1.call(object, symbol);
    });
  };

  /**
   * Creates an array of own enumerable property names and symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names and symbols.
   */
  function getAllKeys(object) {
    return baseGetAllKeys(object, keys, getSymbols);
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$2 = 1;

  /** Used for built-in method references. */
  var objectProto$d = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$a = objectProto$d.hasOwnProperty;

  /**
   * A specialized version of `baseIsEqualDeep` for objects with support for
   * partial deep comparisons.
   *
   * @private
   * @param {Object} object The object to compare.
   * @param {Object} other The other object to compare.
   * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
   * @param {Function} customizer The function to customize comparisons.
   * @param {Function} equalFunc The function to determine equivalents of values.
   * @param {Object} stack Tracks traversed `object` and `other` objects.
   * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
   */
  function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
    var isPartial = bitmask & COMPARE_PARTIAL_FLAG$2,
        objProps = getAllKeys(object),
        objLength = objProps.length,
        othProps = getAllKeys(other),
        othLength = othProps.length;

    if (objLength != othLength && !isPartial) {
      return false;
    }
    var index = objLength;
    while (index--) {
      var key = objProps[index];
      if (!(isPartial ? key in other : hasOwnProperty$a.call(other, key))) {
        return false;
      }
    }
    // Assume cyclic values are equal.
    var stacked = stack.get(object);
    if (stacked && stack.get(other)) {
      return stacked == other;
    }
    var result = true;
    stack.set(object, other);
    stack.set(other, object);

    var skipCtor = isPartial;
    while (++index < objLength) {
      key = objProps[index];
      var objValue = object[key],
          othValue = other[key];

      if (customizer) {
        var compared = isPartial
          ? customizer(othValue, objValue, key, other, object, stack)
          : customizer(objValue, othValue, key, object, other, stack);
      }
      // Recursively compare objects (susceptible to call stack limits).
      if (!(compared === undefined
            ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
            : compared
          )) {
        result = false;
        break;
      }
      skipCtor || (skipCtor = key == 'constructor');
    }
    if (result && !skipCtor) {
      var objCtor = object.constructor,
          othCtor = other.constructor;

      // Non `Object` object instances with different constructors are not equal.
      if (objCtor != othCtor &&
          ('constructor' in object && 'constructor' in other) &&
          !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
            typeof othCtor == 'function' && othCtor instanceof othCtor)) {
        result = false;
      }
    }
    stack['delete'](object);
    stack['delete'](other);
    return result;
  }

  /* Built-in method references that are verified to be native. */
  var DataView = getNative(root, 'DataView');

  /* Built-in method references that are verified to be native. */
  var Promise$1 = getNative(root, 'Promise');

  /* Built-in method references that are verified to be native. */
  var Set$1 = getNative(root, 'Set');

  /* Built-in method references that are verified to be native. */
  var WeakMap$1 = getNative(root, 'WeakMap');

  /** `Object#toString` result references. */
  var mapTag$2 = '[object Map]',
      objectTag$2 = '[object Object]',
      promiseTag = '[object Promise]',
      setTag$2 = '[object Set]',
      weakMapTag$1 = '[object WeakMap]';

  var dataViewTag$2 = '[object DataView]';

  /** Used to detect maps, sets, and weakmaps. */
  var dataViewCtorString = toSource(DataView),
      mapCtorString = toSource(Map$1),
      promiseCtorString = toSource(Promise$1),
      setCtorString = toSource(Set$1),
      weakMapCtorString = toSource(WeakMap$1);

  /**
   * Gets the `toStringTag` of `value`.
   *
   * @private
   * @param {*} value The value to query.
   * @returns {string} Returns the `toStringTag`.
   */
  var getTag = baseGetTag;

  // Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
  if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag$2) ||
      (Map$1 && getTag(new Map$1) != mapTag$2) ||
      (Promise$1 && getTag(Promise$1.resolve()) != promiseTag) ||
      (Set$1 && getTag(new Set$1) != setTag$2) ||
      (WeakMap$1 && getTag(new WeakMap$1) != weakMapTag$1)) {
    getTag = function(value) {
      var result = baseGetTag(value),
          Ctor = result == objectTag$2 ? value.constructor : undefined,
          ctorString = Ctor ? toSource(Ctor) : '';

      if (ctorString) {
        switch (ctorString) {
          case dataViewCtorString: return dataViewTag$2;
          case mapCtorString: return mapTag$2;
          case promiseCtorString: return promiseTag;
          case setCtorString: return setTag$2;
          case weakMapCtorString: return weakMapTag$1;
        }
      }
      return result;
    };
  }

  var getTag$1 = getTag;

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$3 = 1;

  /** `Object#toString` result references. */
  var argsTag$2 = '[object Arguments]',
      arrayTag$1 = '[object Array]',
      objectTag$3 = '[object Object]';

  /** Used for built-in method references. */
  var objectProto$e = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$b = objectProto$e.hasOwnProperty;

  /**
   * A specialized version of `baseIsEqual` for arrays and objects which performs
   * deep comparisons and tracks traversed objects enabling objects with circular
   * references to be compared.
   *
   * @private
   * @param {Object} object The object to compare.
   * @param {Object} other The other object to compare.
   * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
   * @param {Function} customizer The function to customize comparisons.
   * @param {Function} equalFunc The function to determine equivalents of values.
   * @param {Object} [stack] Tracks traversed `object` and `other` objects.
   * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
   */
  function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
    var objIsArr = isArray(object),
        othIsArr = isArray(other),
        objTag = objIsArr ? arrayTag$1 : getTag$1(object),
        othTag = othIsArr ? arrayTag$1 : getTag$1(other);

    objTag = objTag == argsTag$2 ? objectTag$3 : objTag;
    othTag = othTag == argsTag$2 ? objectTag$3 : othTag;

    var objIsObj = objTag == objectTag$3,
        othIsObj = othTag == objectTag$3,
        isSameTag = objTag == othTag;

    if (isSameTag && isBuffer(object)) {
      if (!isBuffer(other)) {
        return false;
      }
      objIsArr = true;
      objIsObj = false;
    }
    if (isSameTag && !objIsObj) {
      stack || (stack = new Stack);
      return (objIsArr || isTypedArray(object))
        ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
        : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
    }
    if (!(bitmask & COMPARE_PARTIAL_FLAG$3)) {
      var objIsWrapped = objIsObj && hasOwnProperty$b.call(object, '__wrapped__'),
          othIsWrapped = othIsObj && hasOwnProperty$b.call(other, '__wrapped__');

      if (objIsWrapped || othIsWrapped) {
        var objUnwrapped = objIsWrapped ? object.value() : object,
            othUnwrapped = othIsWrapped ? other.value() : other;

        stack || (stack = new Stack);
        return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
      }
    }
    if (!isSameTag) {
      return false;
    }
    stack || (stack = new Stack);
    return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
  }

  /**
   * The base implementation of `_.isEqual` which supports partial comparisons
   * and tracks traversed objects.
   *
   * @private
   * @param {*} value The value to compare.
   * @param {*} other The other value to compare.
   * @param {boolean} bitmask The bitmask flags.
   *  1 - Unordered comparison
   *  2 - Partial comparison
   * @param {Function} [customizer] The function to customize comparisons.
   * @param {Object} [stack] Tracks traversed `value` and `other` objects.
   * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
   */
  function baseIsEqual(value, other, bitmask, customizer, stack) {
    if (value === other) {
      return true;
    }
    if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
      return value !== value && other !== other;
    }
    return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$4 = 1,
      COMPARE_UNORDERED_FLAG$2 = 2;

  /**
   * The base implementation of `_.isMatch` without support for iteratee shorthands.
   *
   * @private
   * @param {Object} object The object to inspect.
   * @param {Object} source The object of property values to match.
   * @param {Array} matchData The property names, values, and compare flags to match.
   * @param {Function} [customizer] The function to customize comparisons.
   * @returns {boolean} Returns `true` if `object` is a match, else `false`.
   */
  function baseIsMatch(object, source, matchData, customizer) {
    var index = matchData.length,
        length = index,
        noCustomizer = !customizer;

    if (object == null) {
      return !length;
    }
    object = Object(object);
    while (index--) {
      var data = matchData[index];
      if ((noCustomizer && data[2])
            ? data[1] !== object[data[0]]
            : !(data[0] in object)
          ) {
        return false;
      }
    }
    while (++index < length) {
      data = matchData[index];
      var key = data[0],
          objValue = object[key],
          srcValue = data[1];

      if (noCustomizer && data[2]) {
        if (objValue === undefined && !(key in object)) {
          return false;
        }
      } else {
        var stack = new Stack;
        if (customizer) {
          var result = customizer(objValue, srcValue, key, object, source, stack);
        }
        if (!(result === undefined
              ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$4 | COMPARE_UNORDERED_FLAG$2, customizer, stack)
              : result
            )) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` if suitable for strict
   *  equality comparisons, else `false`.
   */
  function isStrictComparable(value) {
    return value === value && !isObject(value);
  }

  /**
   * Gets the property names, values, and compare flags of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the match data of `object`.
   */
  function getMatchData(object) {
    var result = keys(object),
        length = result.length;

    while (length--) {
      var key = result[length],
          value = object[key];

      result[length] = [key, value, isStrictComparable(value)];
    }
    return result;
  }

  /**
   * A specialized version of `matchesProperty` for source values suitable
   * for strict equality comparisons, i.e. `===`.
   *
   * @private
   * @param {string} key The key of the property to get.
   * @param {*} srcValue The value to match.
   * @returns {Function} Returns the new spec function.
   */
  function matchesStrictComparable(key, srcValue) {
    return function(object) {
      if (object == null) {
        return false;
      }
      return object[key] === srcValue &&
        (srcValue !== undefined || (key in Object(object)));
    };
  }

  /**
   * The base implementation of `_.matches` which doesn't clone `source`.
   *
   * @private
   * @param {Object} source The object of property values to match.
   * @returns {Function} Returns the new spec function.
   */
  function baseMatches(source) {
    var matchData = getMatchData(source);
    if (matchData.length == 1 && matchData[0][2]) {
      return matchesStrictComparable(matchData[0][0], matchData[0][1]);
    }
    return function(object) {
      return object === source || baseIsMatch(object, source, matchData);
    };
  }

  /** Used to match property names within property paths. */
  var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
      reIsPlainProp = /^\w*$/;

  /**
   * Checks if `value` is a property name and not a property path.
   *
   * @private
   * @param {*} value The value to check.
   * @param {Object} [object] The object to query keys on.
   * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
   */
  function isKey(value, object) {
    if (isArray(value)) {
      return false;
    }
    var type = typeof value;
    if (type == 'number' || type == 'symbol' || type == 'boolean' ||
        value == null || isSymbol(value)) {
      return true;
    }
    return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
      (object != null && value in Object(object));
  }

  /** Error message constants. */
  var FUNC_ERROR_TEXT = 'Expected a function';

  /**
   * Creates a function that memoizes the result of `func`. If `resolver` is
   * provided, it determines the cache key for storing the result based on the
   * arguments provided to the memoized function. By default, the first argument
   * provided to the memoized function is used as the map cache key. The `func`
   * is invoked with the `this` binding of the memoized function.
   *
   * **Note:** The cache is exposed as the `cache` property on the memoized
   * function. Its creation may be customized by replacing the `_.memoize.Cache`
   * constructor with one whose instances implement the
   * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
   * method interface of `clear`, `delete`, `get`, `has`, and `set`.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Function
   * @param {Function} func The function to have its output memoized.
   * @param {Function} [resolver] The function to resolve the cache key.
   * @returns {Function} Returns the new memoized function.
   * @example
   *
   * var object = { 'a': 1, 'b': 2 };
   * var other = { 'c': 3, 'd': 4 };
   *
   * var values = _.memoize(_.values);
   * values(object);
   * // => [1, 2]
   *
   * values(other);
   * // => [3, 4]
   *
   * object.a = 2;
   * values(object);
   * // => [1, 2]
   *
   * // Modify the result cache.
   * values.cache.set(object, ['a', 'b']);
   * values(object);
   * // => ['a', 'b']
   *
   * // Replace `_.memoize.Cache`.
   * _.memoize.Cache = WeakMap;
   */
  function memoize(func, resolver) {
    if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
      throw new TypeError(FUNC_ERROR_TEXT);
    }
    var memoized = function() {
      var args = arguments,
          key = resolver ? resolver.apply(this, args) : args[0],
          cache = memoized.cache;

      if (cache.has(key)) {
        return cache.get(key);
      }
      var result = func.apply(this, args);
      memoized.cache = cache.set(key, result) || cache;
      return result;
    };
    memoized.cache = new (memoize.Cache || MapCache);
    return memoized;
  }

  // Expose `MapCache`.
  memoize.Cache = MapCache;

  /** Used as the maximum memoize cache size. */
  var MAX_MEMOIZE_SIZE = 500;

  /**
   * A specialized version of `_.memoize` which clears the memoized function's
   * cache when it exceeds `MAX_MEMOIZE_SIZE`.
   *
   * @private
   * @param {Function} func The function to have its output memoized.
   * @returns {Function} Returns the new memoized function.
   */
  function memoizeCapped(func) {
    var result = memoize(func, function(key) {
      if (cache.size === MAX_MEMOIZE_SIZE) {
        cache.clear();
      }
      return key;
    });

    var cache = result.cache;
    return result;
  }

  /** Used to match property names within property paths. */
  var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

  /** Used to match backslashes in property paths. */
  var reEscapeChar = /\\(\\)?/g;

  /**
   * Converts `string` to a property path array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the property path array.
   */
  var stringToPath = memoizeCapped(function(string) {
    var result = [];
    if (string.charCodeAt(0) === 46 /* . */) {
      result.push('');
    }
    string.replace(rePropName, function(match, number, quote, subString) {
      result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
    });
    return result;
  });

  /**
   * Casts `value` to a path array if it's not one.
   *
   * @private
   * @param {*} value The value to inspect.
   * @param {Object} [object] The object to query keys on.
   * @returns {Array} Returns the cast property path array.
   */
  function castPath(value, object) {
    if (isArray(value)) {
      return value;
    }
    return isKey(value, object) ? [value] : stringToPath(toString(value));
  }

  /** Used as references for various `Number` constants. */
  var INFINITY$1 = 1 / 0;

  /**
   * Converts `value` to a string key if it's not a string or symbol.
   *
   * @private
   * @param {*} value The value to inspect.
   * @returns {string|symbol} Returns the key.
   */
  function toKey(value) {
    if (typeof value == 'string' || isSymbol(value)) {
      return value;
    }
    var result = (value + '');
    return (result == '0' && (1 / value) == -INFINITY$1) ? '-0' : result;
  }

  /**
   * The base implementation of `_.get` without support for default values.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {Array|string} path The path of the property to get.
   * @returns {*} Returns the resolved value.
   */
  function baseGet(object, path) {
    path = castPath(path, object);

    var index = 0,
        length = path.length;

    while (object != null && index < length) {
      object = object[toKey(path[index++])];
    }
    return (index && index == length) ? object : undefined;
  }

  /**
   * Gets the value at `path` of `object`. If the resolved value is
   * `undefined`, the `defaultValue` is returned in its place.
   *
   * @static
   * @memberOf _
   * @since 3.7.0
   * @category Object
   * @param {Object} object The object to query.
   * @param {Array|string} path The path of the property to get.
   * @param {*} [defaultValue] The value returned for `undefined` resolved values.
   * @returns {*} Returns the resolved value.
   * @example
   *
   * var object = { 'a': [{ 'b': { 'c': 3 } }] };
   *
   * _.get(object, 'a[0].b.c');
   * // => 3
   *
   * _.get(object, ['a', '0', 'b', 'c']);
   * // => 3
   *
   * _.get(object, 'a.b.c', 'default');
   * // => 'default'
   */
  function get(object, path, defaultValue) {
    var result = object == null ? undefined : baseGet(object, path);
    return result === undefined ? defaultValue : result;
  }

  /**
   * The base implementation of `_.hasIn` without support for deep paths.
   *
   * @private
   * @param {Object} [object] The object to query.
   * @param {Array|string} key The key to check.
   * @returns {boolean} Returns `true` if `key` exists, else `false`.
   */
  function baseHasIn(object, key) {
    return object != null && key in Object(object);
  }

  /**
   * Checks if `path` exists on `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {Array|string} path The path to check.
   * @param {Function} hasFunc The function to check properties.
   * @returns {boolean} Returns `true` if `path` exists, else `false`.
   */
  function hasPath(object, path, hasFunc) {
    path = castPath(path, object);

    var index = -1,
        length = path.length,
        result = false;

    while (++index < length) {
      var key = toKey(path[index]);
      if (!(result = object != null && hasFunc(object, key))) {
        break;
      }
      object = object[key];
    }
    if (result || ++index != length) {
      return result;
    }
    length = object == null ? 0 : object.length;
    return !!length && isLength(length) && isIndex(key, length) &&
      (isArray(object) || isArguments(object));
  }

  /**
   * Checks if `path` is a direct or inherited property of `object`.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Object
   * @param {Object} object The object to query.
   * @param {Array|string} path The path to check.
   * @returns {boolean} Returns `true` if `path` exists, else `false`.
   * @example
   *
   * var object = _.create({ 'a': _.create({ 'b': 2 }) });
   *
   * _.hasIn(object, 'a');
   * // => true
   *
   * _.hasIn(object, 'a.b');
   * // => true
   *
   * _.hasIn(object, ['a', 'b']);
   * // => true
   *
   * _.hasIn(object, 'b');
   * // => false
   */
  function hasIn(object, path) {
    return object != null && hasPath(object, path, baseHasIn);
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$5 = 1,
      COMPARE_UNORDERED_FLAG$3 = 2;

  /**
   * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
   *
   * @private
   * @param {string} path The path of the property to get.
   * @param {*} srcValue The value to match.
   * @returns {Function} Returns the new spec function.
   */
  function baseMatchesProperty(path, srcValue) {
    if (isKey(path) && isStrictComparable(srcValue)) {
      return matchesStrictComparable(toKey(path), srcValue);
    }
    return function(object) {
      var objValue = get(object, path);
      return (objValue === undefined && objValue === srcValue)
        ? hasIn(object, path)
        : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$5 | COMPARE_UNORDERED_FLAG$3);
    };
  }

  /**
   * The base implementation of `_.property` without support for deep paths.
   *
   * @private
   * @param {string} key The key of the property to get.
   * @returns {Function} Returns the new accessor function.
   */
  function baseProperty(key) {
    return function(object) {
      return object == null ? undefined : object[key];
    };
  }

  /**
   * A specialized version of `baseProperty` which supports deep paths.
   *
   * @private
   * @param {Array|string} path The path of the property to get.
   * @returns {Function} Returns the new accessor function.
   */
  function basePropertyDeep(path) {
    return function(object) {
      return baseGet(object, path);
    };
  }

  /**
   * Creates a function that returns the value at `path` of a given object.
   *
   * @static
   * @memberOf _
   * @since 2.4.0
   * @category Util
   * @param {Array|string} path The path of the property to get.
   * @returns {Function} Returns the new accessor function.
   * @example
   *
   * var objects = [
   *   { 'a': { 'b': 2 } },
   *   { 'a': { 'b': 1 } }
   * ];
   *
   * _.map(objects, _.property('a.b'));
   * // => [2, 1]
   *
   * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
   * // => [1, 2]
   */
  function property(path) {
    return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
  }

  /**
   * The base implementation of `_.iteratee`.
   *
   * @private
   * @param {*} [value=_.identity] The value to convert to an iteratee.
   * @returns {Function} Returns the iteratee.
   */
  function baseIteratee(value) {
    // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
    // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
    if (typeof value == 'function') {
      return value;
    }
    if (value == null) {
      return identity;
    }
    if (typeof value == 'object') {
      return isArray(value)
        ? baseMatchesProperty(value[0], value[1])
        : baseMatches(value);
    }
    return property(value);
  }

  // validate object type
  /**
   * @TODO if provide with the keys then we need to check if the key:value type as well
   * @param {object} value expected
   * @param {array} [keys=null] if it has the keys array to compare as well
   * @return {boolean} true if OK
   */
  var checkIsObject = function(value, keys) {
    if ( keys === void 0 ) keys=null;

    if (isPlainObject(value)) {
      if (!keys) {
        return true;
      }
      if (checkIsArray(keys)) {
        // please note we DON'T care if some is optional
        // plese refer to the contract.json for the keys
        return !keys.filter(function (key) {
          var _value = value[key.name];
          return !(key.type.length > key.type.filter(function (type) {
            var tmp;
            if (_value !== undefined) {
              if ((tmp = isArrayLike$1(type)) !== false) {
                return !arrayTypeHandler({arg: _value}, tmp)
                // return tmp.filter(t => !checkIsArray(_value, t)).length;
                // @TODO there might be an object within an object with keys as well :S
              }
              return !combineFn(type)(_value)
            }
            return true;
          }).length)
        }).length;
      }
    }
    return false;
  };

  /**
   * fold this into it's own function to handler different object type
   * @param {object} p the prepared object for process
   * @return {boolean}
   */
  var objectTypeHandler = function(p) {
    var arg = p.arg;
    var param = p.param;
    var _args = [arg];
    if (Array.isArray(param.keys) && param.keys.length) {
      _args.push(param.keys);
    }
    // just simple check
    return Reflect.apply(checkIsObject, null, _args)
  };

  // move the index.js code here that make more sense to find where things are
  // import debug from 'debug'
  // const debugFn = debug('jsonql-params-validator:validator')
  // also export this for use in other places

  /**
   * We need to handle those optional parameter without a default value
   * @param {object} params from contract.json
   * @return {boolean} for filter operation false is actually OK
   */
  var optionalHandler = function( params ) {
    var arg = params.arg;
    var param = params.param;
    if (isNotEmpty(arg)) {
      // debug('call optional handler', arg, params);
      // loop through the type in param
      return !(param.type.length > param.type.filter(function (type) { return validateHandler(type, params); }
      ).length)
    }
    return false;
  };

  /**
   * actually picking the validator
   * @param {*} type for checking
   * @param {*} value for checking
   * @return {boolean} true on OK
   */
  var validateHandler = function(type, value) {
    var tmp;
    switch (true) {
      case type === OBJECT_TYPE$1:
        // debugFn('call OBJECT_TYPE')
        return !objectTypeHandler(value)
      case type === ARRAY_TYPE$1:
        // debugFn('call ARRAY_TYPE')
        return !checkIsArray(value.arg)
      // @TODO when the type is not present, it always fall through here
      // so we need to find a way to actually pre-check the type first
      // AKA check the contract.json map before running here
      case (tmp = isArrayLike$1(type)) !== false:
        // debugFn('call ARRAY_LIKE: %O', value)
        return !arrayTypeHandler(value, tmp)
      default:
        return !combineFn(type)(value.arg)
    }
  };

  /**
   * it get too longer to fit in one line so break it out from the fn below
   * @param {*} arg value
   * @param {object} param config
   * @return {*} value or apply default value
   */
  var getOptionalValue = function(arg, param) {
    if (arg !== undefined) {
      return arg;
    }
    return (param.optional === true && param.defaultvalue !== undefined ? param.defaultvalue : null)
  };

  /**
   * padding the arguments with defaultValue if the arguments did not provide the value
   * this will be the name export
   * @param {array} args normalized arguments
   * @param {array} params from contract.json
   * @return {array} merge the two together
   */
  var normalizeArgs = function(args, params) {
    // first we should check if this call require a validation at all
    // there will be situation where the function doesn't need args and params
    if (!checkIsArray(params)) {
      // debugFn('params value', params)
      throw new JsonqlError(PARAMS_NOT_ARRAY_ERR)
    }
    if (params.length === 0) {
      return [];
    }
    if (!checkIsArray(args)) {
      throw new JsonqlError(ARGS_NOT_ARRAY_ERR)
    }
    // debugFn(args, params);
    // fall through switch
    switch(true) {
      case args.length == params.length: // standard
        return args.map(function (arg, i) { return (
          {
            arg: arg,
            index: i,
            param: params[i]
          }
        ); })
      case params[0].variable === true: // using spread syntax
        var type = params[0].type;
        return args.map(function (arg, i) { return (
          {
            arg: arg,
            index: i, // keep the index for reference
            param: params[i] || { type: type, name: '_' }
          }
        ); })
      // with optional defaultValue parameters
      case args.length < params.length:
        return params.map(function (param, i) { return (
          {
            param: param,
            index: i,
            arg: getOptionalValue(args[i], param),
            optional: param.optional || false
          }
        ); })
      // this one pass more than it should have anything after the args.length will be cast as any type
      case args.length > params.length:
        var ctn = params.length;
        // this happens when we have those array.<number> type
        var _type = [ DEFAULT_TYPE$1 ];
        // we only looking at the first one, this might be a @BUG
        /*
        if ((tmp = isArrayLike(params[0].type[0])) !== false) {
          _type = tmp;
        } */
        // if we use the params as guide then the rest will get throw out
        // which is not what we want, instead, anything without the param
        // will get a any type and optional flag
        return args.map(function (arg, i) {
          var optional = i >= ctn ? true : !!params[i].optional;
          var param = params[i] || { type: _type, name: ("_" + i) };
          return {
            arg: optional ? getOptionalValue(arg, param) : arg,
            index: i,
            param: param,
            optional: optional
          }
        })
      // @TODO find out if there is more cases not cover
      default: // this should never happen
        // debugFn('args', args)
        // debugFn('params', params)
        // this is unknown therefore we just throw it!
        throw new JsonqlError(EXCEPTION_CASE_ERR, { args: args, params: params })
    }
  };

  // what we want is after the validaton we also get the normalized result
  // which is with the optional property if the argument didn't provide it
  /**
   * process the array of params back to their arguments
   * @param {array} result the params result
   * @return {array} arguments
   */
  var processReturn = function (result) { return result.map(function (r) { return r.arg; }); };

  /**
   * validator main interface
   * @param {array} args the arguments pass to the method call
   * @param {array} params from the contract for that method
   * @param {boolean} [withResul=false] if true then this will return the normalize result as well
   * @return {array} empty array on success, or failed parameter and reasons
   */
  var validateSync = function(args, params, withResult) {
    var obj;

    if ( withResult === void 0 ) withResult = false;
    var cleanArgs = normalizeArgs(args, params);
    var checkResult = cleanArgs.filter(function (p) {
      // v1.4.4 this fixed the problem, the root level optional is from the last fn
      if (p.optional === true || p.param.optional === true) {
        return optionalHandler(p)
      }
      // because array of types means OR so if one pass means pass
      return !(p.param.type.length > p.param.type.filter(
        function (type) { return validateHandler(type, p); }
      ).length)
    });
    // using the same convention we been using all this time
    return !withResult ? checkResult : ( obj = {}, obj[ERROR_KEY] = checkResult, obj[DATA_KEY] = processReturn(cleanArgs), obj )
  };

  /**
   * A wrapper method that return promise
   * @param {array} args arguments
   * @param {array} params from contract.json
   * @param {boolean} [withResul=false] if true then this will return the normalize result as well
   * @return {object} promise.then or catch
   */
  var validateAsync = function(args, params, withResult) {
    if ( withResult === void 0 ) withResult = false;

    return new Promise(function (resolver, rejecter) {
      var result = validateSync(args, params, withResult);
      if (withResult) {
        return result[ERROR_KEY].length ? rejecter(result[ERROR_KEY])
                                        : resolver(result[DATA_KEY])
      }
      // the different is just in the then or catch phrase
      return result.length ? rejecter(result) : resolver([])
    })
  };

  /**
   * Creates an object with the same keys as `object` and values generated
   * by running each own enumerable string keyed property of `object` thru
   * `iteratee`. The iteratee is invoked with three arguments:
   * (value, key, object).
   *
   * @static
   * @memberOf _
   * @since 2.4.0
   * @category Object
   * @param {Object} object The object to iterate over.
   * @param {Function} [iteratee=_.identity] The function invoked per iteration.
   * @returns {Object} Returns the new mapped object.
   * @see _.mapKeys
   * @example
   *
   * var users = {
   *   'fred':    { 'user': 'fred',    'age': 40 },
   *   'pebbles': { 'user': 'pebbles', 'age': 1 }
   * };
   *
   * _.mapValues(users, function(o) { return o.age; });
   * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
   *
   * // The `_.property` iteratee shorthand.
   * _.mapValues(users, 'age');
   * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
   */
  function mapValues(object, iteratee) {
    var result = {};
    iteratee = baseIteratee(iteratee);

    baseForOwn(object, function(value, key, object) {
      baseAssignValue(result, key, iteratee(value, key, object));
    });
    return result;
  }

  /**
   * The opposite of `_.mapValues`; this method creates an object with the
   * same values as `object` and keys generated by running each own enumerable
   * string keyed property of `object` thru `iteratee`. The iteratee is invoked
   * with three arguments: (value, key, object).
   *
   * @static
   * @memberOf _
   * @since 3.8.0
   * @category Object
   * @param {Object} object The object to iterate over.
   * @param {Function} [iteratee=_.identity] The function invoked per iteration.
   * @returns {Object} Returns the new mapped object.
   * @see _.mapValues
   * @example
   *
   * _.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
   *   return key + value;
   * });
   * // => { 'a1': 1, 'b2': 2 }
   */
  function mapKeys(object, iteratee) {
    var result = {};
    iteratee = baseIteratee(iteratee);

    baseForOwn(object, function(value, key, object) {
      baseAssignValue(result, iteratee(value, key, object), value);
    });
    return result;
  }

  /** Error message constants. */
  var FUNC_ERROR_TEXT$1 = 'Expected a function';

  /**
   * Creates a function that negates the result of the predicate `func`. The
   * `func` predicate is invoked with the `this` binding and arguments of the
   * created function.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Function
   * @param {Function} predicate The predicate to negate.
   * @returns {Function} Returns the new negated function.
   * @example
   *
   * function isEven(n) {
   *   return n % 2 == 0;
   * }
   *
   * _.filter([1, 2, 3, 4, 5, 6], _.negate(isEven));
   * // => [1, 3, 5]
   */
  function negate(predicate) {
    if (typeof predicate != 'function') {
      throw new TypeError(FUNC_ERROR_TEXT$1);
    }
    return function() {
      var args = arguments;
      switch (args.length) {
        case 0: return !predicate.call(this);
        case 1: return !predicate.call(this, args[0]);
        case 2: return !predicate.call(this, args[0], args[1]);
        case 3: return !predicate.call(this, args[0], args[1], args[2]);
      }
      return !predicate.apply(this, args);
    };
  }

  /**
   * The base implementation of `_.set`.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {Array|string} path The path of the property to set.
   * @param {*} value The value to set.
   * @param {Function} [customizer] The function to customize path creation.
   * @returns {Object} Returns `object`.
   */
  function baseSet(object, path, value, customizer) {
    if (!isObject(object)) {
      return object;
    }
    path = castPath(path, object);

    var index = -1,
        length = path.length,
        lastIndex = length - 1,
        nested = object;

    while (nested != null && ++index < length) {
      var key = toKey(path[index]),
          newValue = value;

      if (index != lastIndex) {
        var objValue = nested[key];
        newValue = customizer ? customizer(objValue, key, nested) : undefined;
        if (newValue === undefined) {
          newValue = isObject(objValue)
            ? objValue
            : (isIndex(path[index + 1]) ? [] : {});
        }
      }
      assignValue(nested, key, newValue);
      nested = nested[key];
    }
    return object;
  }

  /**
   * The base implementation of  `_.pickBy` without support for iteratee shorthands.
   *
   * @private
   * @param {Object} object The source object.
   * @param {string[]} paths The property paths to pick.
   * @param {Function} predicate The function invoked per property.
   * @returns {Object} Returns the new object.
   */
  function basePickBy(object, paths, predicate) {
    var index = -1,
        length = paths.length,
        result = {};

    while (++index < length) {
      var path = paths[index],
          value = baseGet(object, path);

      if (predicate(value, path)) {
        baseSet(result, castPath(path, object), value);
      }
    }
    return result;
  }

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeGetSymbols$1 = Object.getOwnPropertySymbols;

  /**
   * Creates an array of the own and inherited enumerable symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of symbols.
   */
  var getSymbolsIn = !nativeGetSymbols$1 ? stubArray : function(object) {
    var result = [];
    while (object) {
      arrayPush(result, getSymbols(object));
      object = getPrototype(object);
    }
    return result;
  };

  /**
   * Creates an array of own and inherited enumerable property names and
   * symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names and symbols.
   */
  function getAllKeysIn(object) {
    return baseGetAllKeys(object, keysIn, getSymbolsIn);
  }

  /**
   * Creates an object composed of the `object` properties `predicate` returns
   * truthy for. The predicate is invoked with two arguments: (value, key).
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Object
   * @param {Object} object The source object.
   * @param {Function} [predicate=_.identity] The function invoked per property.
   * @returns {Object} Returns the new object.
   * @example
   *
   * var object = { 'a': 1, 'b': '2', 'c': 3 };
   *
   * _.pickBy(object, _.isNumber);
   * // => { 'a': 1, 'c': 3 }
   */
  function pickBy(object, predicate) {
    if (object == null) {
      return {};
    }
    var props = arrayMap(getAllKeysIn(object), function(prop) {
      return [prop];
    });
    predicate = baseIteratee(predicate);
    return basePickBy(object, props, function(value, path) {
      return predicate(value, path[0]);
    });
  }

  /**
   * The opposite of `_.pickBy`; this method creates an object composed of
   * the own and inherited enumerable string keyed properties of `object` that
   * `predicate` doesn't return truthy for. The predicate is invoked with two
   * arguments: (value, key).
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Object
   * @param {Object} object The source object.
   * @param {Function} [predicate=_.identity] The function invoked per property.
   * @returns {Object} Returns the new object.
   * @example
   *
   * var object = { 'a': 1, 'b': '2', 'c': 3 };
   *
   * _.omitBy(object, _.isNumber);
   * // => { 'b': '2' }
   */
  function omitBy(object, predicate) {
    return pickBy(object, negate(baseIteratee(predicate)));
  }

  /**
   * Performs a deep comparison between two values to determine if they are
   * equivalent.
   *
   * **Note:** This method supports comparing arrays, array buffers, booleans,
   * date objects, error objects, maps, numbers, `Object` objects, regexes,
   * sets, strings, symbols, and typed arrays. `Object` objects are compared
   * by their own, not inherited, enumerable properties. Functions and DOM
   * nodes are compared by strict equality, i.e. `===`.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to compare.
   * @param {*} other The other value to compare.
   * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
   * @example
   *
   * var object = { 'a': 1 };
   * var other = { 'a': 1 };
   *
   * _.isEqual(object, other);
   * // => true
   *
   * object === other;
   * // => false
   */
  function isEqual(value, other) {
    return baseIsEqual(value, other);
  }

  /**
   * The base implementation of methods like `_.findKey` and `_.findLastKey`,
   * without support for iteratee shorthands, which iterates over `collection`
   * using `eachFunc`.
   *
   * @private
   * @param {Array|Object} collection The collection to inspect.
   * @param {Function} predicate The function invoked per iteration.
   * @param {Function} eachFunc The function to iterate over `collection`.
   * @returns {*} Returns the found element or its key, else `undefined`.
   */
  function baseFindKey(collection, predicate, eachFunc) {
    var result;
    eachFunc(collection, function(value, key, collection) {
      if (predicate(value, key, collection)) {
        result = key;
        return false;
      }
    });
    return result;
  }

  /**
   * This method is like `_.find` except that it returns the key of the first
   * element `predicate` returns truthy for instead of the element itself.
   *
   * @static
   * @memberOf _
   * @since 1.1.0
   * @category Object
   * @param {Object} object The object to inspect.
   * @param {Function} [predicate=_.identity] The function invoked per iteration.
   * @returns {string|undefined} Returns the key of the matched element,
   *  else `undefined`.
   * @example
   *
   * var users = {
   *   'barney':  { 'age': 36, 'active': true },
   *   'fred':    { 'age': 40, 'active': false },
   *   'pebbles': { 'age': 1,  'active': true }
   * };
   *
   * _.findKey(users, function(o) { return o.age < 40; });
   * // => 'barney' (iteration order is not guaranteed)
   *
   * // The `_.matches` iteratee shorthand.
   * _.findKey(users, { 'age': 1, 'active': true });
   * // => 'pebbles'
   *
   * // The `_.matchesProperty` iteratee shorthand.
   * _.findKey(users, ['active', false]);
   * // => 'fred'
   *
   * // The `_.property` iteratee shorthand.
   * _.findKey(users, 'active');
   * // => 'barney'
   */
  function findKey(object, predicate) {
    return baseFindKey(object, baseIteratee(predicate), baseForOwn);
  }

  /**
   * @param {array} arr Array for check
   * @param {*} value target
   * @return {boolean} true on successs
   */
  var isInArray = function(arr, value) {
    return !!arr.filter(function (a) { return a === value; }).length;
  };

  var isObjectHasKey$3 = function(obj, key) {
    var keys = Object.keys(obj);
    return isInArray(keys, key)
  };

  // just not to make my head hurt
  var isEmpty = function (value) { return !isNotEmpty(value); };

  /**
   * Map the alias to their key then grab their value over
   * @param {object} config the user supplied config
   * @param {object} appProps the default option map
   * @return {object} the config keys replaced with the appProps key by the ALIAS
   */
  function mapAliasConfigKeys(config, appProps) {
    // need to do two steps
    // 1. take key with alias key
    var aliasMap = omitBy(appProps, function (value, k) { return !value[ALIAS_KEY$1]; } );
    if (isEqual(aliasMap, {})) {
      return config;
    }
    return mapKeys(config, function (v, key) { return findKey(aliasMap, function (o) { return o.alias === key; }) || key; })
  }

  /**
   * We only want to run the valdiation against the config (user supplied) value
   * but keep the defaultOptions untouch
   * @param {object} config configuraton supplied by user
   * @param {object} appProps the default options map
   * @return {object} the pristine values that will add back to the final output
   */
  function preservePristineValues(config, appProps) {
    // @BUG this will filter out those that is alias key
    // we need to first map the alias keys back to their full key
    var _config = mapAliasConfigKeys(config, appProps);
    // take the default value out
    var pristineValues = mapValues(
      omitBy(appProps, function (value, key) { return isObjectHasKey$3(_config, key); }),
      function (value) { return value.args; }
    );
    // for testing the value
    var checkAgainstAppProps = omitBy(appProps, function (value, key) { return !isObjectHasKey$3(_config, key); });
    // output
    return {
      pristineValues: pristineValues,
      checkAgainstAppProps: checkAgainstAppProps,
      config: _config // passing this correct values back
    }
  }

  /**
   * This will take the value that is ONLY need to check
   * @param {object} config that one
   * @param {object} props map for creating checking
   * @return {object} put that arg into the args
   */
  function processConfigAction(config, props) {
    // debugFn('processConfigAction', props)
    // v.1.2.0 add checking if its mark optional and the value is empty then pass
    return mapValues(props, function (value, key) {
      var obj, obj$1;

      return (
      config[key] === undefined || (value[OPTIONAL_KEY$1] === true && isEmpty(config[key]))
        ? merge({}, value, ( obj = {}, obj[KEY_WORD$1] = true, obj ))
        : ( obj$1 = {}, obj$1[ARGS_KEY$1] = config[key], obj$1[TYPE_KEY$1] = value[TYPE_KEY$1], obj$1[OPTIONAL_KEY$1] = value[OPTIONAL_KEY$1] || false, obj$1[ENUM_KEY$1] = value[ENUM_KEY$1] || false, obj$1[CHECKER_KEY$1] = value[CHECKER_KEY$1] || false, obj$1 )
      );
    }
    )
  }

  /**
   * Quick transform
   * @TODO we should only validate those that is pass from the config
   * and pass through those values that is from the defaultOptions
   * @param {object} opts that one
   * @param {object} appProps mutation configuration options
   * @return {object} put that arg into the args
   */
  function prepareArgsForValidation(opts, appProps) {
    var ref = preservePristineValues(opts, appProps);
    var config = ref.config;
    var pristineValues = ref.pristineValues;
    var checkAgainstAppProps = ref.checkAgainstAppProps;
    // output
    return [
      processConfigAction(config, checkAgainstAppProps),
      pristineValues
    ]
  }

  // breaking the whole thing up to see what cause the multiple calls issue

  // import debug from 'debug';
  // const debugFn = debug('jsonql-params-validator:options:validation')

  /**
   * just make sure it returns an array to use
   * @param {*} arg input
   * @return {array} output
   */
  var toArray$1 = function (arg) { return checkIsArray(arg) ? arg : [arg]; };

  /**
   * DIY in array
   * @param {array} arr to check against
   * @param {*} value to check
   * @return {boolean} true on OK
   */
  var inArray$2 = function (arr, value) { return (
    !!arr.filter(function (v) { return v === value; }).length
  ); };

  /**
   * break out to make the code easier to read
   * @param {object} value to process
   * @param {function} cb the validateSync
   * @return {array} empty on success
   */
  function validateHandler$1(value, cb) {
    var obj;

    // cb is the validateSync methods
    var args = [
      [ value[ARGS_KEY$1] ],
      [( obj = {}, obj[TYPE_KEY$1] = toArray$1(value[TYPE_KEY$1]), obj[OPTIONAL_KEY$1] = value[OPTIONAL_KEY$1], obj )]
    ];
    // debugFn('validateHandler', args)
    return Reflect.apply(cb, null, args)
  }

  /**
   * Check against the enum value if it's provided
   * @param {*} value to check
   * @param {*} enumv to check against if it's not false
   * @return {boolean} true on OK
   */
  var enumHandler = function (value, enumv) {
    if (checkIsArray(enumv)) {
      return inArray$2(enumv, value)
    }
    return true;
  };

  /**
   * Allow passing a function to check the value
   * There might be a problem here if the function is incorrect
   * and that will makes it hard to debug what is going on inside
   * @TODO there could be a few feature add to this one under different circumstance
   * @param {*} value to check
   * @param {function} checker for checking
   */
  var checkerHandler = function (value, checker) {
    try {
      return isFunction(checker) ? checker.apply(null, [value]) : false;
    } catch (e) {
      return false;
    }
  };

  /**
   * Taken out from the runValidaton this only validate the required values
   * @param {array} args from the config2argsAction
   * @param {function} cb validateSync
   * @return {array} of configuration values
   */
  function runValidationAction(cb) {
    return function (value, key) {
      // debugFn('runValidationAction', key, value)
      if (value[KEY_WORD$1]) {
        return value[ARGS_KEY$1]
      }
      var check = validateHandler$1(value, cb);
      if (check.length) {
        // log('runValidationAction', key, value)
        throw new JsonqlTypeError(key, check)
      }
      if (value[ENUM_KEY$1] !== false && !enumHandler(value[ARGS_KEY$1], value[ENUM_KEY$1])) {
        // log(ENUM_KEY, value[ENUM_KEY])
        throw new JsonqlEnumError(key)
      }
      if (value[CHECKER_KEY$1] !== false && !checkerHandler(value[ARGS_KEY$1], value[CHECKER_KEY$1])) {
        // log(CHECKER_KEY, value[CHECKER_KEY])
        throw new JsonqlCheckerError(key)
      }
      return value[ARGS_KEY$1]
    }
  }

  /**
   * @param {object} args from the config2argsAction
   * @param {function} cb validateSync
   * @return {object} of configuration values
   */
  function runValidation(args, cb) {
    var argsForValidate = args[0];
    var pristineValues = args[1];
    // turn the thing into an array and see what happen here
    // debugFn('_args', argsForValidate)
    var result = mapValues(argsForValidate, runValidationAction(cb));
    return merge(result, pristineValues)
  }

  // this is port back from the client to share across all projects

  /**
   * @param {object} config user provide configuration option
   * @param {object} appProps mutation configuration options
   * @param {object} constProps the immutable configuration options
   * @param {function} cb the validateSync method
   * @return {object} Promise resolve merge config object
   */
  function checkOptionsSync(config, appProps, constProps, cb) {
    if ( config === void 0 ) config = {};

    return merge(
      runValidation(
        prepareArgsForValidation(config, appProps),
        cb
      ),
      constProps
    )
  }

  // create function to construct the config entry so we don't need to keep building object
  // import debug from 'debug';
  // const debugFn = debug('jsonql-params-validator:construct-config');
  /**
   * @param {*} args value
   * @param {string} type for value
   * @param {boolean} [optional=false]
   * @param {boolean|array} [enumv=false]
   * @param {boolean|function} [checker=false]
   * @return {object} config entry
   */
  function constructConfig(args, type, optional, enumv, checker, alias) {
    if ( optional === void 0 ) optional=false;
    if ( enumv === void 0 ) enumv=false;
    if ( checker === void 0 ) checker=false;
    if ( alias === void 0 ) alias=false;

    var base = {};
    base[ARGS_KEY] = args;
    base[TYPE_KEY] = type;
    if (optional === true) {
      base[OPTIONAL_KEY] = true;
    }
    if (checkIsArray(enumv)) {
      base[ENUM_KEY] = enumv;
    }
    if (isFunction(checker)) {
      base[CHECKER_KEY] = checker;
    }
    if (isString(alias)) {
      base[ALIAS_KEY] = alias;
    }
    return base;
  }

  // export also create wrapper methods

  /**
   * This has a different interface
   * @param {*} value to supply
   * @param {string|array} type for checking
   * @param {object} params to map against the config check
   * @param {array} params.enumv NOT enum
   * @param {boolean} params.optional false then nothing
   * @param {function} params.checker need more work on this one later
   * @param {string} params.alias mostly for cmd
   */
  var createConfig = function (value, type, params) {
    if ( params === void 0 ) params = {};

    // Note the enumv not ENUM
    // const { enumv, optional, checker, alias } = params;
    // let args = [value, type, optional, enumv, checker, alias];
    var o = params[OPTIONAL_KEY];
    var e = params[ENUM_KEY];
    var c = params[CHECKER_KEY];
    var a = params[ALIAS_KEY];
    return constructConfig.apply(null,  [value, type, o, e, c, a])
  };

  /**
   * copy of above but it's sync, rename with prefix get since 1.5.2
   * @param {function} validateSync validation method
   * @return {function} for performaning the actual valdiation
   */
  var getCheckConfig = function(validateSync) {
    return function(config, appProps, constantProps) {
      if ( constantProps === void 0 ) constantProps = {};

      return checkOptionsSync(config, appProps, constantProps, validateSync)
    }
  };

  // export
  var isString$1 = checkIsString;
  var isNumber$1 = checkIsNumber;
  var validateAsync$1 = validateAsync;

  var createConfig$1 = createConfig;
  var checkConfig = getCheckConfig(validateSync);

  var obj, obj$1, obj$2, obj$3, obj$4, obj$5, obj$6, obj$7, obj$8;

  var appProps = {
    algorithm: createConfig$1(HSA_ALGO, [STRING_TYPE]),
    expiresIn: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj = {}, obj[ALIAS_KEY] = 'exp', obj[OPTIONAL_KEY] = true, obj )),
    notBefore: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj$1 = {}, obj$1[ALIAS_KEY] = 'nbf', obj$1[OPTIONAL_KEY] = true, obj$1 )),
    audience: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$2 = {}, obj$2[ALIAS_KEY] = 'iss', obj$2[OPTIONAL_KEY] = true, obj$2 )),
    subject: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$3 = {}, obj$3[ALIAS_KEY] = 'sub', obj$3[OPTIONAL_KEY] = true, obj$3 )),
    issuer: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$4 = {}, obj$4[ALIAS_KEY] = 'iss', obj$4[OPTIONAL_KEY] = true, obj$4 )),
    noTimestamp: createConfig$1(false, [BOOLEAN_TYPE], ( obj$5 = {}, obj$5[OPTIONAL_KEY] = true, obj$5 )),
    header: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$6 = {}, obj$6[OPTIONAL_KEY] = true, obj$6 )),
    keyid: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$7 = {}, obj$7[OPTIONAL_KEY] = true, obj$7 )),
    mutatePayload: createConfig$1(false, [BOOLEAN_TYPE], ( obj$8 = {}, obj$8[OPTIONAL_KEY] = true, obj$8 ))
  };

  /**
   * @param {boolean} sec return in second or not
   * @return {number} timestamp
   */
  var timestamp$1 = function (sec) {
    if ( sec === void 0 ) sec = false;

    var time = Date.now();
    return sec ? Math.floor( time / 1000 ) : time;
  };

  var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

  function createCommonjsModule(fn, module) {
  	return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var assign = make_assign();
  var create = make_create();
  var trim$1 = make_trim();
  var Global = (typeof window !== 'undefined' ? window : commonjsGlobal);

  var util = {
  	assign: assign,
  	create: create,
  	trim: trim$1,
  	bind: bind,
  	slice: slice,
  	each: each,
  	map: map,
  	pluck: pluck,
  	isList: isList,
  	isFunction: isFunction$1,
  	isObject: isObject$1,
  	Global: Global
  };

  function make_assign() {
  	if (Object.assign) {
  		return Object.assign
  	} else {
  		return function shimAssign(obj, props1, props2, etc) {
  			var arguments$1 = arguments;

  			for (var i = 1; i < arguments.length; i++) {
  				each(Object(arguments$1[i]), function(val, key) {
  					obj[key] = val;
  				});
  			}			
  			return obj
  		}
  	}
  }

  function make_create() {
  	if (Object.create) {
  		return function create(obj, assignProps1, assignProps2, etc) {
  			var assignArgsList = slice(arguments, 1);
  			return assign.apply(this, [Object.create(obj)].concat(assignArgsList))
  		}
  	} else {
  		function F() {} // eslint-disable-line no-inner-declarations
  		return function create(obj, assignProps1, assignProps2, etc) {
  			var assignArgsList = slice(arguments, 1);
  			F.prototype = obj;
  			return assign.apply(this, [new F()].concat(assignArgsList))
  		}
  	}
  }

  function make_trim() {
  	if (String.prototype.trim) {
  		return function trim(str) {
  			return String.prototype.trim.call(str)
  		}
  	} else {
  		return function trim(str) {
  			return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')
  		}
  	}
  }

  function bind(obj, fn) {
  	return function() {
  		return fn.apply(obj, Array.prototype.slice.call(arguments, 0))
  	}
  }

  function slice(arr, index) {
  	return Array.prototype.slice.call(arr, index || 0)
  }

  function each(obj, fn) {
  	pluck(obj, function(val, key) {
  		fn(val, key);
  		return false
  	});
  }

  function map(obj, fn) {
  	var res = (isList(obj) ? [] : {});
  	pluck(obj, function(v, k) {
  		res[k] = fn(v, k);
  		return false
  	});
  	return res
  }

  function pluck(obj, fn) {
  	if (isList(obj)) {
  		for (var i=0; i<obj.length; i++) {
  			if (fn(obj[i], i)) {
  				return obj[i]
  			}
  		}
  	} else {
  		for (var key in obj) {
  			if (obj.hasOwnProperty(key)) {
  				if (fn(obj[key], key)) {
  					return obj[key]
  				}
  			}
  		}
  	}
  }

  function isList(val) {
  	return (val != null && typeof val != 'function' && typeof val.length == 'number')
  }

  function isFunction$1(val) {
  	return val && {}.toString.call(val) === '[object Function]'
  }

  function isObject$1(val) {
  	return val && {}.toString.call(val) === '[object Object]'
  }

  var slice$1 = util.slice;
  var pluck$1 = util.pluck;
  var each$1 = util.each;
  var bind$1 = util.bind;
  var create$1 = util.create;
  var isList$1 = util.isList;
  var isFunction$2 = util.isFunction;
  var isObject$2 = util.isObject;

  var storeEngine = {
  	createStore: createStore
  };

  var storeAPI = {
  	version: '2.0.12',
  	enabled: false,
  	
  	// get returns the value of the given key. If that value
  	// is undefined, it returns optionalDefaultValue instead.
  	get: function(key, optionalDefaultValue) {
  		var data = this.storage.read(this._namespacePrefix + key);
  		return this._deserialize(data, optionalDefaultValue)
  	},

  	// set will store the given value at key and returns value.
  	// Calling set with value === undefined is equivalent to calling remove.
  	set: function(key, value) {
  		if (value === undefined) {
  			return this.remove(key)
  		}
  		this.storage.write(this._namespacePrefix + key, this._serialize(value));
  		return value
  	},

  	// remove deletes the key and value stored at the given key.
  	remove: function(key) {
  		this.storage.remove(this._namespacePrefix + key);
  	},

  	// each will call the given callback once for each key-value pair
  	// in this store.
  	each: function(callback) {
  		var self = this;
  		this.storage.each(function(val, namespacedKey) {
  			callback.call(self, self._deserialize(val), (namespacedKey || '').replace(self._namespaceRegexp, ''));
  		});
  	},

  	// clearAll will remove all the stored key-value pairs in this store.
  	clearAll: function() {
  		this.storage.clearAll();
  	},

  	// additional functionality that can't live in plugins
  	// ---------------------------------------------------

  	// hasNamespace returns true if this store instance has the given namespace.
  	hasNamespace: function(namespace) {
  		return (this._namespacePrefix == '__storejs_'+namespace+'_')
  	},

  	// createStore creates a store.js instance with the first
  	// functioning storage in the list of storage candidates,
  	// and applies the the given mixins to the instance.
  	createStore: function() {
  		return createStore.apply(this, arguments)
  	},
  	
  	addPlugin: function(plugin) {
  		this._addPlugin(plugin);
  	},
  	
  	namespace: function(namespace) {
  		return createStore(this.storage, this.plugins, namespace)
  	}
  };

  function _warn() {
  	var _console = (typeof console == 'undefined' ? null : console);
  	if (!_console) { return }
  	var fn = (_console.warn ? _console.warn : _console.log);
  	fn.apply(_console, arguments);
  }

  function createStore(storages, plugins, namespace) {
  	if (!namespace) {
  		namespace = '';
  	}
  	if (storages && !isList$1(storages)) {
  		storages = [storages];
  	}
  	if (plugins && !isList$1(plugins)) {
  		plugins = [plugins];
  	}

  	var namespacePrefix = (namespace ? '__storejs_'+namespace+'_' : '');
  	var namespaceRegexp = (namespace ? new RegExp('^'+namespacePrefix) : null);
  	var legalNamespaces = /^[a-zA-Z0-9_\-]*$/; // alpha-numeric + underscore and dash
  	if (!legalNamespaces.test(namespace)) {
  		throw new Error('store.js namespaces can only have alphanumerics + underscores and dashes')
  	}
  	
  	var _privateStoreProps = {
  		_namespacePrefix: namespacePrefix,
  		_namespaceRegexp: namespaceRegexp,

  		_testStorage: function(storage) {
  			try {
  				var testStr = '__storejs__test__';
  				storage.write(testStr, testStr);
  				var ok = (storage.read(testStr) === testStr);
  				storage.remove(testStr);
  				return ok
  			} catch(e) {
  				return false
  			}
  		},

  		_assignPluginFnProp: function(pluginFnProp, propName) {
  			var oldFn = this[propName];
  			this[propName] = function pluginFn() {
  				var args = slice$1(arguments, 0);
  				var self = this;

  				// super_fn calls the old function which was overwritten by
  				// this mixin.
  				function super_fn() {
  					if (!oldFn) { return }
  					each$1(arguments, function(arg, i) {
  						args[i] = arg;
  					});
  					return oldFn.apply(self, args)
  				}

  				// Give mixing function access to super_fn by prefixing all mixin function
  				// arguments with super_fn.
  				var newFnArgs = [super_fn].concat(args);

  				return pluginFnProp.apply(self, newFnArgs)
  			};
  		},

  		_serialize: function(obj) {
  			return JSON.stringify(obj)
  		},

  		_deserialize: function(strVal, defaultVal) {
  			if (!strVal) { return defaultVal }
  			// It is possible that a raw string value has been previously stored
  			// in a storage without using store.js, meaning it will be a raw
  			// string value instead of a JSON serialized string. By defaulting
  			// to the raw string value in case of a JSON parse error, we allow
  			// for past stored values to be forwards-compatible with store.js
  			var val = '';
  			try { val = JSON.parse(strVal); }
  			catch(e) { val = strVal; }

  			return (val !== undefined ? val : defaultVal)
  		},
  		
  		_addStorage: function(storage) {
  			if (this.enabled) { return }
  			if (this._testStorage(storage)) {
  				this.storage = storage;
  				this.enabled = true;
  			}
  		},

  		_addPlugin: function(plugin) {
  			var self = this;

  			// If the plugin is an array, then add all plugins in the array.
  			// This allows for a plugin to depend on other plugins.
  			if (isList$1(plugin)) {
  				each$1(plugin, function(plugin) {
  					self._addPlugin(plugin);
  				});
  				return
  			}

  			// Keep track of all plugins we've seen so far, so that we
  			// don't add any of them twice.
  			var seenPlugin = pluck$1(this.plugins, function(seenPlugin) {
  				return (plugin === seenPlugin)
  			});
  			if (seenPlugin) {
  				return
  			}
  			this.plugins.push(plugin);

  			// Check that the plugin is properly formed
  			if (!isFunction$2(plugin)) {
  				throw new Error('Plugins must be function values that return objects')
  			}

  			var pluginProperties = plugin.call(this);
  			if (!isObject$2(pluginProperties)) {
  				throw new Error('Plugins must return an object of function properties')
  			}

  			// Add the plugin function properties to this store instance.
  			each$1(pluginProperties, function(pluginFnProp, propName) {
  				if (!isFunction$2(pluginFnProp)) {
  					throw new Error('Bad plugin property: '+propName+' from plugin '+plugin.name+'. Plugins should only return functions.')
  				}
  				self._assignPluginFnProp(pluginFnProp, propName);
  			});
  		},
  		
  		// Put deprecated properties in the private API, so as to not expose it to accidential
  		// discovery through inspection of the store object.
  		
  		// Deprecated: addStorage
  		addStorage: function(storage) {
  			_warn('store.addStorage(storage) is deprecated. Use createStore([storages])');
  			this._addStorage(storage);
  		}
  	};

  	var store = create$1(_privateStoreProps, storeAPI, {
  		plugins: []
  	});
  	store.raw = {};
  	each$1(store, function(prop, propName) {
  		if (isFunction$2(prop)) {
  			store.raw[propName] = bind$1(store, prop);			
  		}
  	});
  	each$1(storages, function(storage) {
  		store._addStorage(storage);
  	});
  	each$1(plugins, function(plugin) {
  		store._addPlugin(plugin);
  	});
  	return store
  }

  var Global$1 = util.Global;

  var localStorage_1 = {
  	name: 'localStorage',
  	read: read$1,
  	write: write$1,
  	each: each$2,
  	remove: remove,
  	clearAll: clearAll,
  };

  function localStorage() {
  	return Global$1.localStorage
  }

  function read$1(key) {
  	return localStorage().getItem(key)
  }

  function write$1(key, data) {
  	return localStorage().setItem(key, data)
  }

  function each$2(fn) {
  	for (var i = localStorage().length - 1; i >= 0; i--) {
  		var key = localStorage().key(i);
  		fn(read$1(key), key);
  	}
  }

  function remove(key) {
  	return localStorage().removeItem(key)
  }

  function clearAll() {
  	return localStorage().clear()
  }

  // cookieStorage is useful Safari private browser mode, where localStorage
  // doesn't work but cookies do. This implementation is adopted from
  // https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage


  var Global$2 = util.Global;
  var trim$2 = util.trim;

  var cookieStorage = {
  	name: 'cookieStorage',
  	read: read$2,
  	write: write$2,
  	each: each$3,
  	remove: remove$1,
  	clearAll: clearAll$1,
  };

  var doc = Global$2.document;

  function read$2(key) {
  	if (!key || !_has(key)) { return null }
  	var regexpStr = "(?:^|.*;\\s*)" +
  		escape(key).replace(/[\-\.\+\*]/g, "\\$&") +
  		"\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*";
  	return unescape(doc.cookie.replace(new RegExp(regexpStr), "$1"))
  }

  function each$3(callback) {
  	var cookies = doc.cookie.split(/; ?/g);
  	for (var i = cookies.length - 1; i >= 0; i--) {
  		if (!trim$2(cookies[i])) {
  			continue
  		}
  		var kvp = cookies[i].split('=');
  		var key = unescape(kvp[0]);
  		var val = unescape(kvp[1]);
  		callback(val, key);
  	}
  }

  function write$2(key, data) {
  	if(!key) { return }
  	doc.cookie = escape(key) + "=" + escape(data) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
  }

  function remove$1(key) {
  	if (!key || !_has(key)) {
  		return
  	}
  	doc.cookie = escape(key) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
  }

  function clearAll$1() {
  	each$3(function(_, key) {
  		remove$1(key);
  	});
  }

  function _has(key) {
  	return (new RegExp("(?:^|;\\s*)" + escape(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(doc.cookie)
  }

  var defaults = defaultsPlugin;

  function defaultsPlugin() {
  	var defaultValues = {};
  	
  	return {
  		defaults: defaults,
  		get: get
  	}
  	
  	function defaults(_, values) {
  		defaultValues = values;
  	}
  	
  	function get(super_fn, key) {
  		var val = super_fn();
  		return (val !== undefined ? val : defaultValues[key])
  	}
  }

  var bind$2 = util.bind;
  var each$4 = util.each;
  var create$2 = util.create;
  var slice$2 = util.slice;

  var events = eventsPlugin;

  function eventsPlugin() {
  	var pubsub = _newPubSub();

  	return {
  		watch: watch,
  		unwatch: unwatch,
  		once: once,

  		set: set,
  		remove: remove,
  		clearAll: clearAll
  	}

  	// new pubsub functions
  	function watch(_, key, listener) {
  		return pubsub.on(key, bind$2(this, listener))
  	}
  	function unwatch(_, subId) {
  		pubsub.off(subId);
  	}
  	function once(_, key, listener) {
  		pubsub.once(key, bind$2(this, listener));
  	}

  	// overwrite function to fire when appropriate
  	function set(super_fn, key, val) {
  		var oldVal = this.get(key);
  		super_fn();
  		pubsub.fire(key, val, oldVal);
  	}
  	function remove(super_fn, key) {
  		var oldVal = this.get(key);
  		super_fn();
  		pubsub.fire(key, undefined, oldVal);
  	}
  	function clearAll(super_fn) {
  		var oldVals = {};
  		this.each(function(val, key) {
  			oldVals[key] = val;
  		});
  		super_fn();
  		each$4(oldVals, function(oldVal, key) {
  			pubsub.fire(key, undefined, oldVal);
  		});
  	}
  }


  function _newPubSub() {
  	return create$2(_pubSubBase, {
  		_id: 0,
  		_subSignals: {},
  		_subCallbacks: {}
  	})
  }

  var _pubSubBase = {
  	_id: null,
  	_subCallbacks: null,
  	_subSignals: null,
  	on: function(signal, callback) {
  		if (!this._subCallbacks[signal]) {
  			this._subCallbacks[signal] = {};
  		}
  		this._id += 1;
  		this._subCallbacks[signal][this._id] = callback;
  		this._subSignals[this._id] = signal;
  		return this._id
  	},
  	off: function(subId) {
  		var signal = this._subSignals[subId];
  		delete this._subCallbacks[signal][subId];
  		delete this._subSignals[subId];
  	},
  	once: function(signal, callback) {
  		var subId = this.on(signal, bind$2(this, function() {
  			callback.apply(this, arguments);
  			this.off(subId);
  		}));
  	},
  	fire: function(signal) {
  		var args = slice$2(arguments, 1);
  		each$4(this._subCallbacks[signal], function(callback) {
  			callback.apply(this, args);
  		});
  	}
  };

  var lzString = createCommonjsModule(function (module) {
  /* eslint-disable */
  // Copyright (c) 2013 Pieroxy <pieroxy@pieroxy.net>
  // This work is free. You can redistribute it and/or modify it
  // under the terms of the WTFPL, Version 2
  // For more information see LICENSE.txt or http://www.wtfpl.net/
  //
  // For more information, the home page:
  // http://pieroxy.net/blog/pages/lz-string/testing.html
  //
  // LZ-based compression algorithm, version 1.4.4
  var LZString = (function() {

  // private property
  var f = String.fromCharCode;
  var keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
  var keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$";
  var baseReverseDic = {};

  function getBaseValue(alphabet, character) {
    if (!baseReverseDic[alphabet]) {
      baseReverseDic[alphabet] = {};
      for (var i=0 ; i<alphabet.length ; i++) {
        baseReverseDic[alphabet][alphabet.charAt(i)] = i;
      }
    }
    return baseReverseDic[alphabet][character];
  }

  var LZString = {
    compressToBase64 : function (input) {
      if (input == null) { return ""; }
      var res = LZString._compress(input, 6, function(a){return keyStrBase64.charAt(a);});
      switch (res.length % 4) { // To produce valid Base64
      default: // When could this happen ?
      case 0 : return res;
      case 1 : return res+"===";
      case 2 : return res+"==";
      case 3 : return res+"=";
      }
    },

    decompressFromBase64 : function (input) {
      if (input == null) { return ""; }
      if (input == "") { return null; }
      return LZString._decompress(input.length, 32, function(index) { return getBaseValue(keyStrBase64, input.charAt(index)); });
    },

    compressToUTF16 : function (input) {
      if (input == null) { return ""; }
      return LZString._compress(input, 15, function(a){return f(a+32);}) + " ";
    },

    decompressFromUTF16: function (compressed) {
      if (compressed == null) { return ""; }
      if (compressed == "") { return null; }
      return LZString._decompress(compressed.length, 16384, function(index) { return compressed.charCodeAt(index) - 32; });
    },

    //compress into uint8array (UCS-2 big endian format)
    compressToUint8Array: function (uncompressed) {
      var compressed = LZString.compress(uncompressed);
      var buf=new Uint8Array(compressed.length*2); // 2 bytes per character

      for (var i=0, TotalLen=compressed.length; i<TotalLen; i++) {
        var current_value = compressed.charCodeAt(i);
        buf[i*2] = current_value >>> 8;
        buf[i*2+1] = current_value % 256;
      }
      return buf;
    },

    //decompress from uint8array (UCS-2 big endian format)
    decompressFromUint8Array:function (compressed) {
      if (compressed===null || compressed===undefined){
          return LZString.decompress(compressed);
      } else {
          var buf=new Array(compressed.length/2); // 2 bytes per character
          for (var i=0, TotalLen=buf.length; i<TotalLen; i++) {
            buf[i]=compressed[i*2]*256+compressed[i*2+1];
          }

          var result = [];
          buf.forEach(function (c) {
            result.push(f(c));
          });
          return LZString.decompress(result.join(''));

      }

    },


    //compress into a string that is already URI encoded
    compressToEncodedURIComponent: function (input) {
      if (input == null) { return ""; }
      return LZString._compress(input, 6, function(a){return keyStrUriSafe.charAt(a);});
    },

    //decompress from an output of compressToEncodedURIComponent
    decompressFromEncodedURIComponent:function (input) {
      if (input == null) { return ""; }
      if (input == "") { return null; }
      input = input.replace(/ /g, "+");
      return LZString._decompress(input.length, 32, function(index) { return getBaseValue(keyStrUriSafe, input.charAt(index)); });
    },

    compress: function (uncompressed) {
      return LZString._compress(uncompressed, 16, function(a){return f(a);});
    },
    _compress: function (uncompressed, bitsPerChar, getCharFromInt) {
      if (uncompressed == null) { return ""; }
      var i, value,
          context_dictionary= {},
          context_dictionaryToCreate= {},
          context_c="",
          context_wc="",
          context_w="",
          context_enlargeIn= 2, // Compensate for the first entry which should not count
          context_dictSize= 3,
          context_numBits= 2,
          context_data=[],
          context_data_val=0,
          context_data_position=0,
          ii;

      for (ii = 0; ii < uncompressed.length; ii += 1) {
        context_c = uncompressed.charAt(ii);
        if (!Object.prototype.hasOwnProperty.call(context_dictionary,context_c)) {
          context_dictionary[context_c] = context_dictSize++;
          context_dictionaryToCreate[context_c] = true;
        }

        context_wc = context_w + context_c;
        if (Object.prototype.hasOwnProperty.call(context_dictionary,context_wc)) {
          context_w = context_wc;
        } else {
          if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
            if (context_w.charCodeAt(0)<256) {
              for (i=0 ; i<context_numBits ; i++) {
                context_data_val = (context_data_val << 1);
                if (context_data_position == bitsPerChar-1) {
                  context_data_position = 0;
                  context_data.push(getCharFromInt(context_data_val));
                  context_data_val = 0;
                } else {
                  context_data_position++;
                }
              }
              value = context_w.charCodeAt(0);
              for (i=0 ; i<8 ; i++) {
                context_data_val = (context_data_val << 1) | (value&1);
                if (context_data_position == bitsPerChar-1) {
                  context_data_position = 0;
                  context_data.push(getCharFromInt(context_data_val));
                  context_data_val = 0;
                } else {
                  context_data_position++;
                }
                value = value >> 1;
              }
            } else {
              value = 1;
              for (i=0 ; i<context_numBits ; i++) {
                context_data_val = (context_data_val << 1) | value;
                if (context_data_position ==bitsPerChar-1) {
                  context_data_position = 0;
                  context_data.push(getCharFromInt(context_data_val));
                  context_data_val = 0;
                } else {
                  context_data_position++;
                }
                value = 0;
              }
              value = context_w.charCodeAt(0);
              for (i=0 ; i<16 ; i++) {
                context_data_val = (context_data_val << 1) | (value&1);
                if (context_data_position == bitsPerChar-1) {
                  context_data_position = 0;
                  context_data.push(getCharFromInt(context_data_val));
                  context_data_val = 0;
                } else {
                  context_data_position++;
                }
                value = value >> 1;
              }
            }
            context_enlargeIn--;
            if (context_enlargeIn == 0) {
              context_enlargeIn = Math.pow(2, context_numBits);
              context_numBits++;
            }
            delete context_dictionaryToCreate[context_w];
          } else {
            value = context_dictionary[context_w];
            for (i=0 ; i<context_numBits ; i++) {
              context_data_val = (context_data_val << 1) | (value&1);
              if (context_data_position == bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = value >> 1;
            }


          }
          context_enlargeIn--;
          if (context_enlargeIn == 0) {
            context_enlargeIn = Math.pow(2, context_numBits);
            context_numBits++;
          }
          // Add wc to the dictionary.
          context_dictionary[context_wc] = context_dictSize++;
          context_w = String(context_c);
        }
      }

      // Output the code for w.
      if (context_w !== "") {
        if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
          if (context_w.charCodeAt(0)<256) {
            for (i=0 ; i<context_numBits ; i++) {
              context_data_val = (context_data_val << 1);
              if (context_data_position == bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
            }
            value = context_w.charCodeAt(0);
            for (i=0 ; i<8 ; i++) {
              context_data_val = (context_data_val << 1) | (value&1);
              if (context_data_position == bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = value >> 1;
            }
          } else {
            value = 1;
            for (i=0 ; i<context_numBits ; i++) {
              context_data_val = (context_data_val << 1) | value;
              if (context_data_position == bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = 0;
            }
            value = context_w.charCodeAt(0);
            for (i=0 ; i<16 ; i++) {
              context_data_val = (context_data_val << 1) | (value&1);
              if (context_data_position == bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = value >> 1;
            }
          }
          context_enlargeIn--;
          if (context_enlargeIn == 0) {
            context_enlargeIn = Math.pow(2, context_numBits);
            context_numBits++;
          }
          delete context_dictionaryToCreate[context_w];
        } else {
          value = context_dictionary[context_w];
          for (i=0 ; i<context_numBits ; i++) {
            context_data_val = (context_data_val << 1) | (value&1);
            if (context_data_position == bitsPerChar-1) {
              context_data_position = 0;
              context_data.push(getCharFromInt(context_data_val));
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = value >> 1;
          }


        }
        context_enlargeIn--;
        if (context_enlargeIn == 0) {
          context_enlargeIn = Math.pow(2, context_numBits);
          context_numBits++;
        }
      }

      // Mark the end of the stream
      value = 2;
      for (i=0 ; i<context_numBits ; i++) {
        context_data_val = (context_data_val << 1) | (value&1);
        if (context_data_position == bitsPerChar-1) {
          context_data_position = 0;
          context_data.push(getCharFromInt(context_data_val));
          context_data_val = 0;
        } else {
          context_data_position++;
        }
        value = value >> 1;
      }

      // Flush the last char
      while (true) {
        context_data_val = (context_data_val << 1);
        if (context_data_position == bitsPerChar-1) {
          context_data.push(getCharFromInt(context_data_val));
          break;
        }
        else { context_data_position++; }
      }
      return context_data.join('');
    },

    decompress: function (compressed) {
      if (compressed == null) { return ""; }
      if (compressed == "") { return null; }
      return LZString._decompress(compressed.length, 32768, function(index) { return compressed.charCodeAt(index); });
    },

    _decompress: function (length, resetValue, getNextValue) {
      var dictionary = [],
          next,
          enlargeIn = 4,
          dictSize = 4,
          numBits = 3,
          entry = "",
          result = [],
          i,
          w,
          bits, resb, maxpower, power,
          c,
          data = {val:getNextValue(0), position:resetValue, index:1};

      for (i = 0; i < 3; i += 1) {
        dictionary[i] = i;
      }

      bits = 0;
      maxpower = Math.pow(2,2);
      power=1;
      while (power!=maxpower) {
        resb = data.val & data.position;
        data.position >>= 1;
        if (data.position == 0) {
          data.position = resetValue;
          data.val = getNextValue(data.index++);
        }
        bits |= (resb>0 ? 1 : 0) * power;
        power <<= 1;
      }

      switch (next = bits) {
        case 0:
            bits = 0;
            maxpower = Math.pow(2,8);
            power=1;
            while (power!=maxpower) {
              resb = data.val & data.position;
              data.position >>= 1;
              if (data.position == 0) {
                data.position = resetValue;
                data.val = getNextValue(data.index++);
              }
              bits |= (resb>0 ? 1 : 0) * power;
              power <<= 1;
            }
          c = f(bits);
          break;
        case 1:
            bits = 0;
            maxpower = Math.pow(2,16);
            power=1;
            while (power!=maxpower) {
              resb = data.val & data.position;
              data.position >>= 1;
              if (data.position == 0) {
                data.position = resetValue;
                data.val = getNextValue(data.index++);
              }
              bits |= (resb>0 ? 1 : 0) * power;
              power <<= 1;
            }
          c = f(bits);
          break;
        case 2:
          return "";
      }
      dictionary[3] = c;
      w = c;
      result.push(c);
      while (true) {
        if (data.index > length) {
          return "";
        }

        bits = 0;
        maxpower = Math.pow(2,numBits);
        power=1;
        while (power!=maxpower) {
          resb = data.val & data.position;
          data.position >>= 1;
          if (data.position == 0) {
            data.position = resetValue;
            data.val = getNextValue(data.index++);
          }
          bits |= (resb>0 ? 1 : 0) * power;
          power <<= 1;
        }

        switch (c = bits) {
          case 0:
            bits = 0;
            maxpower = Math.pow(2,8);
            power=1;
            while (power!=maxpower) {
              resb = data.val & data.position;
              data.position >>= 1;
              if (data.position == 0) {
                data.position = resetValue;
                data.val = getNextValue(data.index++);
              }
              bits |= (resb>0 ? 1 : 0) * power;
              power <<= 1;
            }

            dictionary[dictSize++] = f(bits);
            c = dictSize-1;
            enlargeIn--;
            break;
          case 1:
            bits = 0;
            maxpower = Math.pow(2,16);
            power=1;
            while (power!=maxpower) {
              resb = data.val & data.position;
              data.position >>= 1;
              if (data.position == 0) {
                data.position = resetValue;
                data.val = getNextValue(data.index++);
              }
              bits |= (resb>0 ? 1 : 0) * power;
              power <<= 1;
            }
            dictionary[dictSize++] = f(bits);
            c = dictSize-1;
            enlargeIn--;
            break;
          case 2:
            return result.join('');
        }

        if (enlargeIn == 0) {
          enlargeIn = Math.pow(2, numBits);
          numBits++;
        }

        if (dictionary[c]) {
          entry = dictionary[c];
        } else {
          if (c === dictSize) {
            entry = w + w.charAt(0);
          } else {
            return null;
          }
        }
        result.push(entry);

        // Add w+entry[0] to the dictionary.
        dictionary[dictSize++] = w + entry.charAt(0);
        enlargeIn--;

        w = entry;

        if (enlargeIn == 0) {
          enlargeIn = Math.pow(2, numBits);
          numBits++;
        }

      }
    }
  };
    return LZString;
  })();

  if(  module != null ) {
    module.exports = LZString;
  }
  });

  var compression = compressionPlugin;

  function compressionPlugin() {
  	return {
  		get: get,
  		set: set,
  	}

  	function get(super_fn, key) {
  		var val = super_fn(key);
  		if (!val) { return val }
  		var decompressed = lzString.decompress(val);
  		// fallback to existing values that are not compressed
  		return (decompressed == null) ? val : this._deserialize(decompressed)
  	}

  	function set(super_fn, key, val) {
  		var compressed = lzString.compress(this._serialize(val));
  		super_fn(key, compressed);
  	}
  }

  // sort of persist on the user side

  var storages = [localStorage_1, cookieStorage];
  var plugins = [defaults, events, compression];

  var localStore = storeEngine.createStore(storages, plugins);

  var Global$3 = util.Global;

  var sessionStorage_1 = {
  	name: 'sessionStorage',
  	read: read$3,
  	write: write$3,
  	each: each$5,
  	remove: remove$2,
  	clearAll: clearAll$2
  };

  function sessionStorage() {
  	return Global$3.sessionStorage
  }

  function read$3(key) {
  	return sessionStorage().getItem(key)
  }

  function write$3(key, data) {
  	return sessionStorage().setItem(key, data)
  }

  function each$5(fn) {
  	for (var i = sessionStorage().length - 1; i >= 0; i--) {
  		var key = sessionStorage().key(i);
  		fn(read$3(key), key);
  	}
  }

  function remove$2(key) {
  	return sessionStorage().removeItem(key)
  }

  function clearAll$2() {
  	return sessionStorage().clear()
  }

  // session store with watch
  // @1.5.0 stop using the expired plugin and deal it ourself
  // import expiredPlugin from 'store/plugins/expire'

  var storages$1 = [sessionStorage_1, cookieStorage];
  var plugins$1 = [defaults, compression];

  var sessionStore = storeEngine.createStore(storages$1, plugins$1);

  // export store interface

  // export back the raw version for development purposes
  var localStore$1 = localStore;
  var sessionStore$1 = sessionStore;

  // ported from jsonql-params-validator

  /**
   * @param {*} args arguments to send
   *@return {object} formatted payload
   */
  var formatPayload = function (args) {
    var obj;

    return (
    ( obj = {}, obj[QUERY_ARG_NAME] = args, obj )
  );
  };

  /**
   * Get name from the payload (ported back from jsonql-koa)
   * @param {*} payload to extract from
   * @return {string} name
   */
  function getNameFromPayload(payload) {
    return Object.keys(payload)[0]
  }

  /**
   * wrapper method to add the timestamp as well
   * @param {string} resolverName
   * @param {*} payload
   * @return {object} delierable
   */
  function createDeliverable(resolverName, payload) {
    var obj;

    return ( obj = {}, obj[resolverName] = payload, obj[TIMESTAMP_PARAM_NAME] = [ timestamp$1() ], obj )
  }

  /**
   * @param {string} resolverName name of function
   * @param {array} [args=[]] from the ...args
   * @param {boolean} [jsonp = false] add v1.3.0 to koa
   * @return {object} formatted argument
   */
  function createQuery(resolverName, args, jsonp) {
    if ( args === void 0 ) args = [];
    if ( jsonp === void 0 ) jsonp = false;

    if (isString(resolverName) && isArray(args)) {
      var payload = formatPayload(args);
      if (jsonp === true) {
        return payload;
      }
      return createDeliverable(resolverName, payload)
    }
    throw new JsonqlValidationError("[createQuery] expect resolverName to be string and args to be array!", { resolverName: resolverName, args: args })
  }

  /**
   * @param {string} resolverName name of function
   * @param {*} payload to send
   * @param {object} [condition={}] for what
   * @param {boolean} [jsonp = false] add v1.3.0 to koa
   * @return {object} formatted argument
   */
  function createMutation(resolverName, payload, condition, jsonp) {
    if ( condition === void 0 ) condition = {};
    if ( jsonp === void 0 ) jsonp = false;

    var _payload = {};
    _payload[PAYLOAD_PARAM_NAME] = payload;
    _payload[CONDITION_PARAM_NAME] = condition;
    if (jsonp === true) {
      return _payload;
    }
    if (isString(resolverName)) {
      return createDeliverable(resolverName, _payload)
    }
    throw new JsonqlValidationError("[createMutation] expect resolverName to be string!", { resolverName: resolverName, payload: payload, condition: condition })
  }

  /**
   * @return {object} _cb as key with timestamp
   */
  var cacheBurst = function () { return ({ _cb: timestamp$1() }); };

  // break up from node-middleware

  // ported from http-client

  /**
   * handle the return data
   * @TODO how to handle the return timestamp and calculate the diff?
   * @param {object} result return from server
   * @return {object} strip the data part out, or if the error is presented
   */
  var resultHandler = function (result) { return (
    (isObjectHasKey$2(result, DATA_KEY) && !isObjectHasKey$2(result, ERROR_KEY)) ? result[DATA_KEY] : result
  ); };

  // new 1.5.0

  // this becomes the base class instead of the HttpCls
  var StoreClass = function StoreClass(opts) {
    this.opts = opts;
    // make it a string
    this.instanceKey = hashCode$1(this.opts.hostname);
    // pass this store for use later
    this.localStore = localStore$1;
    this.sessionStore = sessionStore$1;
    /*
    if (this.opts.debugOn) { // reuse this to clear out the data
      this.log('clear all stores')
      localStore.clearAll()
      sessionStore.clearAll()

      localStore.set('TEST', Date.now())
      sessionStore.set('TEST', Date.now())
    }
    */
  };

  var prototypeAccessors = { lset: { configurable: true },lget: { configurable: true },sset: { configurable: true },sget: { configurable: true } };
  // store in local storage id by the instanceKey
  // values should be an object so with key so we just merge
  // into the existing store without going through the keys
  StoreClass.prototype.__setMethod = function __setMethod (storeType, values) {
      var obj;

    var store = this[storeType];
    var data = this.__getMethod(storeType);
    var skey = this.opts.storageKey;
    var ikey = this.instanceKey;
    store.set(skey, ( obj = {}, obj[ikey] = data ? merge({}, data, values) : values, obj ));
  };
  // return the data id by the instaceKey
  StoreClass.prototype.__getMethod = function __getMethod (storeType) {
    var store = this[storeType];
    var data = store.get(this.opts.storageKey);
    return data ? data[this.instanceKey] : false
  };
  // remove from local store id by instanceKey
  StoreClass.prototype.__delMethod = function __delMethod (storeType, key) {
    var data = this.__getMethod(storeType);
    if (data) {
      var store = {};
      for (var k in data) {
        if (k !== key) {
          store[k] = data[k];
        }
      }
      this.__setMethod(storeType, store);
    }
  };
  // clear everything by this instanceKey
  StoreClass.prototype.__clearMethod = function __clearMethod (storeKey) {
    var skey = this.opts.storageKey;
    var store = this[storeKey];
    var data = store.get(skey);
    if (data) {
      var _store = {};
      for (var k in data) {
        if (k !== this.instanceKey) {
          _store[k] = data[k];
        }
      }
      store.set(skey, _store);
    }
  };
  // Alias for different store
  prototypeAccessors.lset.set = function (values) {
    return this.__setMethod(CLS_LOCAL_STORE_NAME, values)
  };

  prototypeAccessors.lget.get = function () {
    return this.__getMethod(CLS_LOCAL_STORE_NAME)
  };

  StoreClass.prototype.ldel = function ldel (key) {
    return this.__delMethod(CLS_LOCAL_STORE_NAME, key)
  };

  StoreClass.prototype.lclear = function lclear () {
    return this.__clearMethod(CLS_LOCAL_STORE_NAME)
  };

  // store in session store id by the instanceKey
  prototypeAccessors.sset.set = function (values) {
    // this.log('--- sset ---', values)
    return this.__setMethod(CLS_SESS_STORE_NAME, values)
  };

  prototypeAccessors.sget.get = function () {
    return this.__getMethod(CLS_SESS_STORE_NAME)
  };

  StoreClass.prototype.sdel = function sdel (key) {
    return this.__delMethod(CLS_SESS_STORE_NAME, key)
  };

  StoreClass.prototype.sclear = function sclear () {
    return this.__clearMethod(CLS_SESS_STORE_NAME)
  };

  Object.defineProperties( StoreClass.prototype, prototypeAccessors );

  // base HttpClass

  // extract the one we need
  var POST = API_REQUEST_METHODS[0];
  var PUT = API_REQUEST_METHODS[1];

  var HttpClass = /*@__PURE__*/(function (StoreClass) {
    function HttpClass(opts) {
      StoreClass.call(this, opts);
      // change the way how we init Fly
      // flyio now become external depedencies and it makes it easier to switch
      // @BUG should we run test to check if we have the windows object?
      // this.log(opts)
      // this.fly = opts.Fly ? new opts.Fly() : new Fly()
      // to a different environment like WeChat mini app

      this.extraHeader = {};
      // @1.2.1 for adding query to the call on the fly
      this.extraParams = {};
      // this.log('start up opts', opts);
      this.reqInterceptor();
      this.resInterceptor();
    }

    if ( StoreClass ) HttpClass.__proto__ = StoreClass;
    HttpClass.prototype = Object.create( StoreClass && StoreClass.prototype );
    HttpClass.prototype.constructor = HttpClass;

    var prototypeAccessors = { headers: { configurable: true } };

    // set headers for that one call
    prototypeAccessors.headers.set = function (header) {
      this.extraHeader = header;
    };

    /**
     * Create the reusage request method
     * @param {object} payload jsonql payload
     * @param {object} options extra options add the request
     * @param {object} headers extra headers add to the call
     * @return {object} the fly request instance
     */
    HttpClass.prototype.request = function request (payload, options, headers) {
      var obj;

      if ( options === void 0 ) options = {};
      if ( headers === void 0 ) headers = {};
      this.headers = headers;
      var params = merge({}, cacheBurst(), this.extraParams);
      // @TODO need to add a jsonp url and payload
      if (this.opts.enableJsonp) {
        var resolverName = getNameFromPayload(payload);
        params = merge({}, params, ( obj = {}, obj[JSONP_CALLBACK_NAME] = resolverName, obj ));
        payload = payload[resolverName];
      }
      // double up the url param and see what happen @TODO remove later
      var reqParams = merge({}, { method: POST, params: params }, options);
      this.log('request params', reqParams, this.jsonqlEndpoint);

      return this.fly.request(this.jsonqlEndpoint, payload, reqParams)
    };

    /**
     * This will replace the create baseRequest method
     *
     */
    HttpClass.prototype.reqInterceptor = function reqInterceptor () {
      var this$1 = this;

      this.fly.interceptors.request.use(
        function (req) {
          var headers = this$1.getHeaders();
          this$1.log('request interceptor call', headers);

          for (var key in headers) {
            req.headers[key] = headers[key];
          }
          return req;
        }
      );
    };

    // @TODO
    HttpClass.prototype.processJsonp = function processJsonp (result) {
      return resultHandler(result)
    };

    /**
     * This will be replacement of the first then call
     *
     */
    HttpClass.prototype.resInterceptor = function resInterceptor () {
      var this$1 = this;

      var self = this;
      var jsonp = self.opts.enableJsonp;
      this.fly.interceptors.response.use(
        function (res) {
          this$1.log('response interceptor call', res);
          self.cleanUp();
          // now more processing here
          // there is a problem if we throw the result.error here
          // the original data is lost, so we need to do what we did before
          // deal with that error in the first then instead
          var result = isString$1(res.data) ? JSON.parse(res.data) : res.data;
          if (jsonp) {
            return self.processJsonp(result)
          }
          return resultHandler(result)
        },
        // this get call when it's not 200
        function (err) {
          self.cleanUp();
          this$1.log(LOG_ERROR_SWITCH, err);
          throw new JsonqlServerError('Server side error', err)
        }
      );
    };

    /**
     * Get the headers inject into the call
     * @return {object} headers
     */
    HttpClass.prototype.getHeaders = function getHeaders () {
      if (this.opts.enableAuth) {
        return merge({}, DEFAULT_HEADER, this.getAuthHeader(), this.extraHeader)
      }
      return merge({}, DEFAULT_HEADER, this.extraHeader)
    };

    /**
     * Post http call operation to clean up things we need
     */
    HttpClass.prototype.cleanUp = function cleanUp () {
      this.extraHeader = {};
      this.extraParams = {};
    };

    /**
     * GET for contract only
     * @return {promise} resolve the contract
     */
    HttpClass.prototype.getRemoteContract = function getRemoteContract () {
      var this$1 = this;

      if (this.opts.showContractDesc) {
        this.extraParams = merge({}, this.extraParams, SHOW_CONTRACT_DESC_PARAM);
      }
      return this.request({}, {method: 'GET'}, this.contractHeader)
        .then(clientErrorsHandler)
        .then(function (result) {
          this$1.log('get contract result', result);
          // when refresh the window the result is different!
          // @TODO need to check the Koa side about why is that
          // also it should set a flag if we want the description or not
          if (result.cache && result.contract) {
            return result.contract;
          }
          // just the normal result
          return result
        })
        .catch(function (err) {
          this$1.log(LOG_ERROR_SWITCH, 'getRemoteContract err', err);
          throw new JsonqlServerError('getRemoteContract', err)
        })
    };

   /**
    * POST to server - query
    * @param {object} name of the resolver
    * @param {array} args arguments
    * @return {object} promise resolve to the resolver return
    */
   HttpClass.prototype.query = function query (name, args) {
     if ( args === void 0 ) args = [];

     return this.request(createQuery(name, args))
      .then(clientErrorsHandler)
   };

   /**
    * PUT to server - mutation
    * @param {string} name of resolver
    * @param {object} payload what it said
    * @param {object} conditions what it said
    * @return {object} promise resolve to the resolver return
    */
   HttpClass.prototype.mutation = function mutation (name, payload, conditions) {
     if ( payload === void 0 ) payload = {};
     if ( conditions === void 0 ) conditions = {};

     return this.request(createMutation(name, payload, conditions), {method: PUT})
      .then(clientErrorsHandler)
   };

    Object.defineProperties( HttpClass.prototype, prototypeAccessors );

    return HttpClass;
  }(StoreClass));

  // all the contract related methods will be here

  // export
  var ContractClass = /*@__PURE__*/(function (HttpClass) {
    function ContractClass(opts) {
      HttpClass.call(this, opts);
    }

    if ( HttpClass ) ContractClass.__proto__ = HttpClass;
    ContractClass.prototype = Object.create( HttpClass && HttpClass.prototype );
    ContractClass.prototype.constructor = ContractClass;

    var prototypeAccessors = { contractHeader: { configurable: true } };

    /**
     * return the contract public api
     * @return {object} contract
     */
    ContractClass.prototype.getContract = function getContract () {
      var contract = this.readContract();
      this.log('getContract first call', contract);
      return contract ? Promise.resolve(contract)
                      : this.getRemoteContract().then(this.storeContract.bind(this))
    };

    /**
     * We are changing the way how to auth to get the contract.json
     * Instead of in the url, we will be putting that key value in the header
     * @return {object} header
     */
    prototypeAccessors.contractHeader.get = function () {
      var base = {};
      if (this.opts.contractKey !== false) {
        base[this.opts.contractKeyName] = this.opts.contractKey;
      }
      return base;
    };

    /**
     * Save the contract to local store
     * @param {object} contract to save
     * @return {object|boolean} false when its not a contract or contract on OK
     */
    ContractClass.prototype.storeContract = function storeContract (contract) {
      var obj;

      // first need to check if the contract is a contract
      if (!isContract(contract)) {
        throw new JsonqlValidationError("Contract is malformed!")
      }
      this.lset = ( obj = {}, obj[CLS_CONTRACT_NAME] = contract, obj );
      // return it
      this.log('storeContract return result', contract);
      return contract;
    };

    /**
     * return the contract from options or localStore
     * @return {object|boolean} false on not found
     */
    ContractClass.prototype.readContract = function readContract () {
      var contract = isContract(this.opts.contract);
      if (contract !== false) {
        return contract;
      }
      var data = this.lget;
      if (data) {
        return data[CLS_CONTRACT_NAME]
      }
      return false;
    };

    Object.defineProperties( ContractClass.prototype, prototypeAccessors );

    return ContractClass;
  }(HttpClass));

  // this is the new auth class that integrate with the jsonql-jwt
  // export
  var AuthClass = /*@__PURE__*/(function (ContractClass) {
    function AuthClass(opts) {
      ContractClass.call(this, opts);
      if (opts.enableAuth) {
        this.setDecoder = jwtDecode;
      }
      // cache
      this.__userdata__ = null;
    }

    if ( ContractClass ) AuthClass.__proto__ = ContractClass;
    AuthClass.prototype = Object.create( ContractClass && ContractClass.prototype );
    AuthClass.prototype.constructor = AuthClass;

    var prototypeAccessors = { profileIndex: { configurable: true },setDecoder: { configurable: true },saveProfile: { configurable: true },readProfile: { configurable: true },jsonqlToken: { configurable: true },jsonqlUserdata: { configurable: true } };

    /**
     * for overwrite
     * @param {string} token stored token
     * @return {string} token
     */
    AuthClass.prototype.decoder = function decoder (token) {
      return token;
    };

    /**
     * set the profile index
     * @param {number} idx
     */
    prototypeAccessors.profileIndex.set = function (idx) {
      var obj;

      var key = CLS_PROFILE_IDX;
      if (isNumber$1(idx)) {
        this[key] = idx;
        if (this.opts.persistToken) {
          this.lset = ( obj = {}, obj[key] = idx, obj );
        }
        return;
      }
      throw new JsonqlValidationError('profileIndex', ("Expect idx to be number but got " + (typeof idx)))
    };

    /**
     * get the profile index
     * @return {number} idx
     */
    prototypeAccessors.profileIndex.get = function () {
      var key = CLS_PROFILE_IDX;
      if (this.opts.persistToken) {
        var data = this.lget;
        if (data[key]) {
          return data[key]
        }
      }
      return this[key] ? this[key] : ZERO_IDX
    };

    /**
     * Return the token from session store
     * @param {number} [idx=false] profile index
     * @return {string} token
     */
    AuthClass.prototype.rawAuthToken = function rawAuthToken (idx) {
      if ( idx === void 0 ) idx = false;

      if (idx !== false) {
        this.profileIndex = idx;
      }
      // this should return from the base
      return this.jsonqlToken; // see base-cls
    };

    /**
     * Setter to add a decoder when retrieve user token
     * @param {function} d a decoder
     */
    prototypeAccessors.setDecoder.set = function (d) {
      if (typeof d === 'function') {
        this.decoder = d;
      }
    };

    /**
     * getter to return the session or local store set method
     * @param {*} data to save
     * @return {object} set method
     */
    prototypeAccessors.saveProfile.set = function (data) {
      if (this.opts.persistToken) {
        // this.log('--- saveProfile lset ---', data)
        this.lset = data;
      } else {
        // this.log('--- saveProfile sset ---', data)
        this.sset = data;
      }
    };

    /**
     * getter to return the session or local store get method
     * @return {object} get method
     */
    prototypeAccessors.readProfile.get = function () {
      return this.opts.persistToken ? this.lget : this.sget
    };

    // these were in the base class before but it should be here
    /**
     * save token
     * @param {string} token to store
     * @return {string|boolean} false on failed
     */
    prototypeAccessors.jsonqlToken.set = function (token) {
      var obj;

      var data = this.readProfile;
      var key = CREDENTIAL_STORAGE_KEY;
      // @TODO also have to make sure the token is not already existed!
      var tokens = (data && data[key]) ? data[key] : [];
      tokens.push(token);
      this.saveProfile = ( obj = {}, obj[key] = tokens, obj );
      // store the userdata
      this.__userdata__ = this.decoder(token);
      this.jsonqlUserdata = this.__userdata__;
    };

    /**
     * Jsonql token getter
     * 1.5.1 each token associate with the same profileIndex
     * @return {string|boolean} false when failed
     */
    prototypeAccessors.jsonqlToken.get = function () {
      var data = this.readProfile;
      var key = CREDENTIAL_STORAGE_KEY;
      if (data && data[key]) {
        this.log('-- jsonqlToken --', data[key], this.profileIndex, data[key][this.profileIndex]);
        return data[key][this.profileIndex]
      }
      return false
    };

    /**
     * this one will use the sessionStore
     * basically we hook this onto the token store and decode it to store here
     * we only store one decoded user data at a time, but the token can be multiple
     */
    prototypeAccessors.jsonqlUserdata.set = function (userdata) {
      var obj;

      this.sset = ( obj = {}, obj[USERDATA_TABLE] = userdata, obj );
    };

    /**
     * this one store in the session store
     * get login userdata decoded jwt
     * 1.5.1 each userdata associate with the same profileIndex
     * @return {object|null}
     */
    prototypeAccessors.jsonqlUserdata.get = function () {
      var data = this.sget;
      return data ? data[USERDATA_TABLE] : false
    };

    /**
     * Construct the auth header
     * @return {object} header
     */
    AuthClass.prototype.getAuthHeader = function getAuthHeader () {
      var obj;

      var token = this.jsonqlToken; // only call the getter to get the default one
      return token ? ( obj = {}, obj[this.opts.AUTH_HEADER] = (BEARER + " " + token), obj ) : {};
    };

    /**
     * return all the stored token and decode it
     * @param {number} [idx=false] profile index
     * @return {array|boolean|string} false not found or array
     */
    AuthClass.prototype.getProfiles = function getProfiles (idx) {
      if ( idx === void 0 ) idx = false;

      var self = this; // just in case the scope problem
      var data = self.readProfile;
      var key = CREDENTIAL_STORAGE_KEY;
      if (data && data[key]) {
        if (idx !== false && isNumber$1(idx)) {
          return data[key][idx] || false
        }
        return data[key].map(self.decoder.bind(self))
      }
      return false
    };

    /**
     * call after the login
     * @param {string} token return from server
     * @return {object} decoded token to userdata object
     */
    AuthClass.prototype.postLoginAction = function postLoginAction (token) {
      this.jsonqlToken = token;
      
      return { token: token, userdata: this.__userdata__ }
    };

    /**
     * call after the logout @TODO
     */
    AuthClass.prototype.postLogoutAction = function postLogoutAction () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      console.info("postLogoutAction", args);
    };

    Object.defineProperties( AuthClass.prototype, prototypeAccessors );

    return AuthClass;
  }(ContractClass));

  // this the core of the internal storage management

  // This class will only focus on the storage system
  var JsonqlBaseClient = /*@__PURE__*/(function (AuthCls) {
    function JsonqlBaseClient(fly, opts) {
      // change at 1.4.10 pass it directly without init it
      this.fly = fly;
      AuthCls.call(this, opts);
    }

    if ( AuthCls ) JsonqlBaseClient.__proto__ = AuthCls;
    JsonqlBaseClient.prototype = Object.create( AuthCls && AuthCls.prototype );
    JsonqlBaseClient.prototype.constructor = JsonqlBaseClient;

    var prototypeAccessors = { jsonqlEndpoint: { configurable: true } };

    /**
     * construct the end point
     * @return {string} the end point to call
     */
    prototypeAccessors.jsonqlEndpoint.get = function () {
      var baseUrl = this.opts.hostname || '';
      return [baseUrl, this.opts.jsonqlPath].join('/')
    };

    /**
     * simple log control by the debugOn option 
     * @param {array<*>} args
     * @return {void}
     */
    JsonqlBaseClient.prototype.log = function log () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      if (this.opts.debugOn === true) {
        var fns = ['info', 'error'];
        var idx = (args[0] === LOG_ERROR_SWITCH) ? 1 : 0;
        args.splice(0, idx);
        Reflect.apply(console[fns[idx]], console, args);
      }
    };

    Object.defineProperties( JsonqlBaseClient.prototype, prototypeAccessors );

    return JsonqlBaseClient;
  }(AuthClass));

  // export interface

  var NB_EVENT_SERVICE_PRIVATE_STORE = new WeakMap();
  var NB_EVENT_SERVICE_PRIVATE_LAZY = new WeakMap();

  // making all the functionality on it's own
  // import { WatchClass } from './watch'

  var SuspendClass = function SuspendClass() {
    // suspend, release and queue
    this.__suspend__ = null;
    this.queueStore = new Set();
    /*
    this.watch('suspend', function(value, prop, oldValue) {
      this.logger(`${prop} set from ${oldValue} to ${value}`)
      // it means it set the suspend = true then release it
      if (oldValue === true && value === false) {
        // we want this happen after the return happens
        setTimeout(() => {
          this.release()
        }, 1)
      }
      return value; // we need to return the value to store it
    })
    */
  };

  var prototypeAccessors$1 = { $suspend: { configurable: true },$queues: { configurable: true } };

  /**
   * setter to set the suspend and check if it's boolean value
   * @param {boolean} value to trigger
   */
  prototypeAccessors$1.$suspend.set = function (value) {
      var this$1 = this;

    if (typeof value === 'boolean') {
      var lastValue = this.__suspend__;
      this.__suspend__ = value;
      this.logger('($suspend)', ("Change from " + lastValue + " --> " + value));
      if (lastValue === true && value === false) {
        setTimeout(function () {
          this$1.release();
        }, 1);
      }
    } else {
      throw new Error("$suspend only accept Boolean value!")
    }
  };

  /**
   * queuing call up when it's in suspend mode
   * @param {any} value
   * @return {Boolean} true when added or false when it's not
   */
  SuspendClass.prototype.$queue = function $queue () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

    if (this.__suspend__ === true) {
      this.logger('($queue)', 'added to $queue', args);
      // there shouldn't be any duplicate ...
      this.queueStore.add(args);
    }
    return this.__suspend__;
  };

  /**
   * a getter to get all the store queue
   * @return {array} Set turn into Array before return
   */
  prototypeAccessors$1.$queues.get = function () {
    var size = this.queueStore.size;
    this.logger('($queues)', ("size: " + size));
    if (size > 0) {
      return Array.from(this.queueStore)
    }
    return []
  };

  /**
   * Release the queue
   * @return {int} size if any
   */
  SuspendClass.prototype.release = function release () {
      var this$1 = this;

    var size = this.queueStore.size;
    this.logger('(release)', ("Release was called " + size));
    if (size > 0) {
      var queue = Array.from(this.queueStore);
      this.queueStore.clear();
      this.logger('queue', queue);
      queue.forEach(function (args) {
        this$1.logger(args);
        Reflect.apply(this$1.$trigger, this$1, args);
      });
      this.logger(("Release size " + (this.queueStore.size)));
    }
  };

  Object.defineProperties( SuspendClass.prototype, prototypeAccessors$1 );

  // break up the main file because its getting way too long

  var NbEventServiceBase = /*@__PURE__*/(function (SuspendClass) {
    function NbEventServiceBase(config) {
      if ( config === void 0 ) config = {};

      SuspendClass.call(this);
      if (config.logger && typeof config.logger === 'function') {
        this.logger = config.logger;
      }
      this.keep = config.keep;
      // for the $done setter
      this.result = config.keep ? [] : null;
      // we need to init the store first otherwise it could be a lot of checking later
      this.normalStore = new Map();
      this.lazyStore = new Map();
    }

    if ( SuspendClass ) NbEventServiceBase.__proto__ = SuspendClass;
    NbEventServiceBase.prototype = Object.create( SuspendClass && SuspendClass.prototype );
    NbEventServiceBase.prototype.constructor = NbEventServiceBase;

    var prototypeAccessors = { is: { configurable: true },normalStore: { configurable: true },lazyStore: { configurable: true } };

    // for id if the instance is this class
    prototypeAccessors.is.get = function () {
      return 'nb-event-service'
    };

    /**
     * validate the event name(s)
     * @param {string[]} evt event name
     * @return {boolean} true when OK
     */
    NbEventServiceBase.prototype.validateEvt = function validateEvt () {
      var this$1 = this;
      var evt = [], len = arguments.length;
      while ( len-- ) evt[ len ] = arguments[ len ];

      evt.forEach(function (e) {
        if (typeof e !== 'string') {
          this$1.logger('(validateEvt)', e);
          throw new Error("event name must be string type!")
        }
      });
      return true;
    };

    /**
     * Simple quick check on the two main parameters
     * @param {string} evt event name
     * @param {function} callback function to call
     * @return {boolean} true when OK
     */
    NbEventServiceBase.prototype.validate = function validate (evt, callback) {
      if (this.validateEvt(evt)) {
        if (typeof callback === 'function') {
          return true;
        }
      }
      throw new Error("callback required to be function type!")
    };

    /**
     * Check if this type is correct or not added in V1.5.0
     * @param {string} type for checking
     * @return {boolean} true on OK
     */
    NbEventServiceBase.prototype.validateType = function validateType (type) {
      var types = ['on', 'only', 'once', 'onlyOnce'];
      return !!types.filter(function (t) { return type === t; }).length;
    };

    /**
     * Run the callback
     * @param {function} callback function to execute
     * @param {array} payload for callback
     * @param {object} ctx context or null
     * @return {void} the result store in $done
     */
    NbEventServiceBase.prototype.run = function run (callback, payload, ctx) {
      this.logger('(run)', callback, payload, ctx);
      this.$done = Reflect.apply(callback, ctx, this.toArray(payload));
    };

    /**
     * Take the content out and remove it from store id by the name
     * @param {string} evt event name
     * @param {string} [storeName = lazyStore] name of store
     * @return {object|boolean} content or false on not found
     */
    NbEventServiceBase.prototype.takeFromStore = function takeFromStore (evt, storeName) {
      if ( storeName === void 0 ) storeName = 'lazyStore';

      var store = this[storeName]; // it could be empty at this point
      if (store) {
        this.logger('(takeFromStore)', storeName, store);
        if (store.has(evt)) {
          var content = store.get(evt);
          this.logger('(takeFromStore)', ("has " + evt), content);
          store.delete(evt);
          return content;
        }
        return false;
      }
      throw new Error((storeName + " is not supported!"))
    };

    /**
     * The add to store step is similar so make it generic for resuse
     * @param {object} store which store to use
     * @param {string} evt event name
     * @param {spread} args because the lazy store and normal store store different things
     * @return {array} store and the size of the store
     */
    NbEventServiceBase.prototype.addToStore = function addToStore (store, evt) {
      var args = [], len = arguments.length - 2;
      while ( len-- > 0 ) args[ len ] = arguments[ len + 2 ];

      var fnSet;
      if (store.has(evt)) {
        this.logger('(addToStore)', (evt + " existed"));
        fnSet = store.get(evt);
      } else {
        this.logger('(addToStore)', ("create new Set for " + evt));
        // this is new
        fnSet = new Set();
      }
      // lazy only store 2 items - this is not the case in V1.6.0 anymore
      // we need to check the first parameter is string or not
      if (args.length > 2) {
        if (Array.isArray(args[0])) { // lazy store
          // check if this type of this event already register in the lazy store
          var t = args[2];
          if (!this.checkTypeInLazyStore(evt, t)) {
            fnSet.add(args);
          }
        } else {
          if (!this.checkContentExist(args, fnSet)) {
            this.logger('(addToStore)', "insert new", args);
            fnSet.add(args);
          }
        }
      } else { // add straight to lazy store
        fnSet.add(args);
      }
      store.set(evt, fnSet);
      return [store, fnSet.size]
    };

    /**
     * @param {array} args for compare
     * @param {object} fnSet A Set to search from
     * @return {boolean} true on exist
     */
    NbEventServiceBase.prototype.checkContentExist = function checkContentExist (args, fnSet) {
      var list = Array.from(fnSet);
      return !!list.filter(function (l) {
        var hash = l[0];
        if (hash === args[0]) {
          return true;
        }
        return false;
      }).length;
    };

    /**
     * get the existing type to make sure no mix type add to the same store
     * @param {string} evtName event name
     * @param {string} type the type to check
     * @return {boolean} true you can add, false then you can't add this type
     */
    NbEventServiceBase.prototype.checkTypeInStore = function checkTypeInStore (evtName, type) {
      this.validateEvt(evtName, type);
      var all = this.$get(evtName, true);
      if (all === false) {
         // pristine it means you can add
        return true;
      }
      // it should only have ONE type in ONE event store
      return !all.filter(function (list) {
        var t = list[3];
        return type !== t;
      }).length;
    };

    /**
     * This is checking just the lazy store because the structure is different
     * therefore we need to use a new method to check it
     */
    NbEventServiceBase.prototype.checkTypeInLazyStore = function checkTypeInLazyStore (evtName, type) {
      this.validateEvt(evtName, type);
      var store = this.lazyStore.get(evtName);
      this.logger('(checkTypeInLazyStore)', store);
      if (store) {
        return !!Array
          .from(store)
          .filter(function (l) {
            var t = l[2];
            return t !== type;
          }).length
      }
      return false;
    };

    /**
     * wrapper to re-use the addToStore,
     * V1.3.0 add extra check to see if this type can add to this evt
     * @param {string} evt event name
     * @param {string} type on or once
     * @param {function} callback function
     * @param {object} context the context the function execute in or null
     * @return {number} size of the store
     */
    NbEventServiceBase.prototype.addToNormalStore = function addToNormalStore (evt, type, callback, context) {
      if ( context === void 0 ) context = null;

      this.logger('(addToNormalStore)', evt, type, 'try to add to normal store');
      // @TODO we need to check the existing store for the type first!
      if (this.checkTypeInStore(evt, type)) {
        this.logger('(addToNormalStore)', (type + " can add to " + evt + " normal store"));
        var key = this.hashFnToKey(callback);
        var args = [this.normalStore, evt, key, callback, context, type];
        var ref = Reflect.apply(this.addToStore, this, args);
        var _store = ref[0];
        var size = ref[1];
        this.normalStore = _store;
        return size;
      }
      return false;
    };

    /**
     * Add to lazy store this get calls when the callback is not register yet
     * so we only get a payload object or even nothing
     * @param {string} evt event name
     * @param {array} payload of arguments or empty if there is none
     * @param {object} [context=null] the context the callback execute in
     * @param {string} [type=false] register a type so no other type can add to this evt
     * @return {number} size of the store
     */
    NbEventServiceBase.prototype.addToLazyStore = function addToLazyStore (evt, payload, context, type) {
      if ( payload === void 0 ) payload = [];
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = false;

      // this is add in V1.6.0
      // when there is type then we will need to check if this already added in lazy store
      // and no other type can add to this lazy store
      var args = [this.lazyStore, evt, this.toArray(payload), context];
      if (type) {
        args.push(type);
      }
      var ref = Reflect.apply(this.addToStore, this, args);
      var _store = ref[0];
      var size = ref[1];
      this.lazyStore = _store;
      return size;
    };

    /**
     * make sure we store the argument correctly
     * @param {*} arg could be array
     * @return {array} make sured
     */
    NbEventServiceBase.prototype.toArray = function toArray (arg) {
      return Array.isArray(arg) ? arg : [arg];
    };

    /**
     * setter to store the Set in private
     * @param {object} obj a Set
     */
    prototypeAccessors.normalStore.set = function (obj) {
      NB_EVENT_SERVICE_PRIVATE_STORE.set(this, obj);
    };

    /**
     * @return {object} Set object
     */
    prototypeAccessors.normalStore.get = function () {
      return NB_EVENT_SERVICE_PRIVATE_STORE.get(this)
    };

    /**
     * setter to store the Set in lazy store
     * @param {object} obj a Set
     */
    prototypeAccessors.lazyStore.set = function (obj) {
      NB_EVENT_SERVICE_PRIVATE_LAZY.set(this , obj);
    };

    /**
     * @return {object} the lazy store Set
     */
    prototypeAccessors.lazyStore.get = function () {
      return NB_EVENT_SERVICE_PRIVATE_LAZY.get(this)
    };

    /**
     * generate a hashKey to identify the function call
     * The build-in store some how could store the same values!
     * @param {function} fn the converted to string function
     * @return {string} hashKey
     */
    NbEventServiceBase.prototype.hashFnToKey = function hashFnToKey (fn) {
      return hashCode2Str(fn.toString())
    };

    Object.defineProperties( NbEventServiceBase.prototype, prototypeAccessors );

    return NbEventServiceBase;
  }(SuspendClass));

  // The top level
  // export
  var EventService = /*@__PURE__*/(function (NbStoreService) {
    function EventService(config) {
      if ( config === void 0 ) config = {};

      NbStoreService.call(this, config);
    }

    if ( NbStoreService ) EventService.__proto__ = NbStoreService;
    EventService.prototype = Object.create( NbStoreService && NbStoreService.prototype );
    EventService.prototype.constructor = EventService;

    var prototypeAccessors = { $done: { configurable: true } };

    /**
     * logger function for overwrite
     */
    EventService.prototype.logger = function logger () {};

    //////////////////////////
    //    PUBLIC METHODS    //
    //////////////////////////

    /**
     * Register your evt handler, note we don't check the type here,
     * we expect you to be sensible and know what you are doing.
     * @param {string} evt name of event
     * @param {function} callback bind method --> if it's array or not
     * @param {object} [context=null] to execute this call in
     * @return {number} the size of the store
     */
    EventService.prototype.$on = function $on (evt , callback , context) {
      var this$1 = this;
      if ( context === void 0 ) context = null;

      var type = 'on';
      this.validate(evt, callback);
      // first need to check if this evt is in lazy store
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register first then call later
      if (lazyStoreContent === false) {
        this.logger('($on)', (evt + " callback is not in lazy store"));
        // @TODO we need to check if there was other listener to this
        // event and are they the same type then we could solve that
        // register the different type to the same event name

        return this.addToNormalStore(evt, type, callback, context)
      }
      this.logger('($on)', (evt + " found in lazy store"));
      // this is when they call $trigger before register this callback
      var size = 0;
      lazyStoreContent.forEach(function (content) {
        var payload = content[0];
        var ctx = content[1];
        var t = content[2];
        if (t && t !== type) {
          throw new Error(("You are trying to register an event already been taken by other type: " + t))
        }
        this$1.logger("($on)", ("call run on " + evt));
        this$1.run(callback, payload, context || ctx);
        size += this$1.addToNormalStore(evt, type, callback, context || ctx);
      });
      return size;
    };

    /**
     * once only registered it once, there is no overwrite option here
     * @NOTE change in v1.3.0 $once can add multiple listeners
     *       but once the event fired, it will remove this event (see $only)
     * @param {string} evt name
     * @param {function} callback to execute
     * @param {object} [context=null] the handler execute in
     * @return {boolean} result
     */
    EventService.prototype.$once = function $once (evt , callback , context) {
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);
      var type = 'once';
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      var nStore = this.normalStore;
      if (lazyStoreContent === false) {
        this.logger('($once)', (evt + " not in the lazy store"));
        // v1.3.0 $once now allow to add multiple listeners
        return this.addToNormalStore(evt, type, callback, context)
      } else {
        // now this is the tricky bit
        // there is a potential bug here that cause by the developer
        // if they call $trigger first, the lazy won't know it's a once call
        // so if in the middle they register any call with the same evt name
        // then this $once call will be fucked - add this to the documentation
        this.logger('($once)', lazyStoreContent);
        var list = Array.from(lazyStoreContent);
        // should never have more than 1
        var ref = list[0];
        var payload = ref[0];
        var ctx = ref[1];
        var t = ref[2];
        if (t && t !== type) {
          throw new Error(("You are trying to register an event already been taken by other type: " + t))
        }
        this.logger('($once)', ("call run for " + evt));
        this.run(callback, payload, context || ctx);
        // remove this evt from store
        this.$off(evt);
      }
    };

    /**
     * This one event can only bind one callbackback
     * @param {string} evt event name
     * @param {function} callback event handler
     * @param {object} [context=null] the context the event handler execute in
     * @return {boolean} true bind for first time, false already existed
     */
    EventService.prototype.$only = function $only (evt, callback, context) {
      var this$1 = this;
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);
      var type = 'only';
      var added = false;
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      var nStore = this.normalStore;
      if (!nStore.has(evt)) {
        this.logger("($only)", (evt + " add to store"));
        added = this.addToNormalStore(evt, type, callback, context);
      }
      if (lazyStoreContent !== false) {
        // there are data store in lazy store
        this.logger('($only)', (evt + " found data in lazy store to execute"));
        var list = Array.from(lazyStoreContent);
        // $only allow to trigger this multiple time on the single handler
        list.forEach( function (l) {
          var payload = l[0];
          var ctx = l[1];
          var t = l[2];
          if (t && t !== type) {
            throw new Error(("You are trying to register an event already been taken by other type: " + t))
          }
          this$1.logger("($only)", ("call run for " + evt));
          this$1.run(callback, payload, context || ctx);
        });
      }
      return added;
    };

    /**
     * $only + $once this is because I found a very subtile bug when we pass a
     * resolver, rejecter - and it never fire because that's OLD added in v1.4.0
     * @param {string} evt event name
     * @param {function} callback to call later
     * @param {object} [context=null] exeucte context
     * @return {void}
     */
    EventService.prototype.$onlyOnce = function $onlyOnce (evt, callback, context) {
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);
      var type = 'onlyOnce';
      var added = false;
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      var nStore = this.normalStore;
      if (!nStore.has(evt)) {
        this.logger("($onlyOnce)", (evt + " add to store"));
        added = this.addToNormalStore(evt, type, callback, context);
      }
      if (lazyStoreContent !== false) {
        // there are data store in lazy store
        this.logger('($onlyOnce)', lazyStoreContent);
        var list = Array.from(lazyStoreContent);
        // should never have more than 1
        var ref = list[0];
        var payload = ref[0];
        var ctx = ref[1];
        var t = ref[2];
        if (t && t !== 'onlyOnce') {
          throw new Error(("You are trying to register an event already been taken by other type: " + t))
        }
        this.logger("($onlyOnce)", ("call run for " + evt));
        this.run(callback, payload, context || ctx);
        // remove this evt from store
        this.$off(evt);
      }
      return added;
    };

    /**
     * This is a shorthand of $off + $on added in V1.5.0
     * @param {string} evt event name
     * @param {function} callback to exeucte
     * @param {object} [context = null] or pass a string as type
     * @param {string} [type=on] what type of method to replace
     * @return {}
     */
    EventService.prototype.$replace = function $replace (evt, callback, context, type) {
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = 'on';

      if (this.validateType(type)) {
        this.$off(evt);
        var method = this['$' + type];
        this.logger("($replace)", evt, callback);
        return Reflect.apply(method, this, [evt, callback, context])
      }
      throw new Error((type + " is not supported!"))
    };

    /**
     * trigger the event
     * @param {string} evt name NOT allow array anymore!
     * @param {mixed} [payload = []] pass to fn
     * @param {object|string} [context = null] overwrite what stored
     * @param {string} [type=false] if pass this then we need to add type to store too
     * @return {number} if it has been execute how many times
     */
    EventService.prototype.$trigger = function $trigger (evt , payload , context, type) {
      if ( payload === void 0 ) payload = [];
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = false;

      this.validateEvt(evt);
      var found = 0;
      // first check the normal store
      var nStore = this.normalStore;
      this.logger('($trigger)', 'normalStore', nStore);
      if (nStore.has(evt)) {
        // @1.8.0 to add the suspend queue
        var added = this.$queue(evt, payload, context, type);
        this.logger('($trigger)', evt, 'found; add to queue: ', added);
        if (added === true) {
          this.logger('($trigger)', evt, 'not executed. Exit now.');
          return false; // not executed
        }
        var nSet = Array.from(nStore.get(evt));
        var ctn = nSet.length;
        var hasOnce = false;
        for (var i=0; i < ctn; ++i) {
          ++found;
          // this.logger('found', found)
          var ref = nSet[i];
          var _ = ref[0];
          var callback = ref[1];
          var ctx = ref[2];
          var type$1 = ref[3];
          this.logger("($trigger)", ("call run for " + evt));
          this.run(callback, payload, context || ctx);
          if (type$1 === 'once' || type$1 === 'onlyOnce') {
            hasOnce = true;
          }
        }
        if (hasOnce) {
          nStore.delete(evt);
        }
        return found;
      }
      // now this is not register yet
      this.addToLazyStore(evt, payload, context, type);
      return found;
    };

    /**
     * this is an alias to the $trigger
     * @NOTE breaking change in V1.6.0 we swap the parameter around
     * @param {string} evt event name
     * @param {*} params pass to the callback
     * @param {string} type of call
     * @param {object} context what context callback execute in
     * @return {*} from $trigger
     */
    EventService.prototype.$call = function $call (evt, params, type, context) {
      if ( type === void 0 ) type = false;
      if ( context === void 0 ) context = null;

      var args = [evt, params, context, type];
      return Reflect.apply(this.$trigger, this, args)
    };

    /**
     * remove the evt from all the stores
     * @param {string} evt name
     * @return {boolean} true actually delete something
     */
    EventService.prototype.$off = function $off (evt) {
      var this$1 = this;

      this.validateEvt(evt);
      var stores = [ this.lazyStore, this.normalStore ];
      var found = false;
      stores.forEach(function (store) {
        if (store.has(evt)) {
          found = true;
          this$1.logger('($off)', evt);
          store.delete(evt);
        }
      });
      return found;
    };

    /**
     * return all the listener from the event
     * @param {string} evtName event name
     * @param {boolean} [full=false] if true then return the entire content
     * @return {array|boolean} listerner(s) or false when not found
     */
    EventService.prototype.$get = function $get (evt, full) {
      if ( full === void 0 ) full = false;

      this.validateEvt(evt);
      var store = this.normalStore;
      if (store.has(evt)) {
        return Array
          .from(store.get(evt))
          .map( function (l) {
            if (full) {
              return l;
            }
            var key = l[0];
            var callback = l[1];
            return callback;
          })
      }
      return false;
    };

    /**
     * store the return result from the run
     * @param {*} value whatever return from callback
     */
    prototypeAccessors.$done.set = function (value) {
      this.logger('($done)', 'value: ', value);
      if (this.keep) {
        this.result.push(value);
      } else {
        this.result = value;
      }
    };

    /**
     * @TODO is there any real use with the keep prop?
     * getter for $done
     * @return {*} whatever last store result
     */
    prototypeAccessors.$done.get = function () {
      if (this.keep) {
        this.logger('(get $done)', this.result);
        return this.result[this.result.length - 1]
      }
      return this.result;
    };

    Object.defineProperties( EventService.prototype, prototypeAccessors );

    return EventService;
  }(NbEventServiceBase));

  // default

  // this will generate a event emitter and will be use everywhere

  var JsonqlEventEmitter = /*@__PURE__*/(function (NBEventService) {
    function JsonqlEventEmitter(prop) {
      NBEventService.call(this, prop);
    }

    if ( NBEventService ) JsonqlEventEmitter.__proto__ = NBEventService;
    JsonqlEventEmitter.prototype = Object.create( NBEventService && NBEventService.prototype );
    JsonqlEventEmitter.prototype.constructor = JsonqlEventEmitter;

    var prototypeAccessors = { name: { configurable: true } };

    prototypeAccessors.name.get = function () {
      return 'jsonql-event-emitter'
    };

    Object.defineProperties( JsonqlEventEmitter.prototype, prototypeAccessors );

    return JsonqlEventEmitter;
  }(EventService));

  // output
  function getEventEmitter(debugOn) {
    var logger = debugOn ? function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      args.unshift('[NBS]');
      console.log.apply(null, args);
    }: undefined;
    return new JsonqlEventEmitter({ logger: logger })
  }

  /**
   * using just the map reduce to chain multiple functions together
   * @param {function} mainFn the init function
   * @param {array} moreFns as many as you want to take the last value and return a new one
   * @return {function} accept value for the mainFn
   */
  var chainFns = function (mainFn) {
    var moreFns = [], len = arguments.length - 1;
    while ( len-- > 0 ) moreFns[ len ] = arguments[ len + 1 ];

    return (
    function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return (
      moreFns.reduce(function (value, nextFn) { return (
        // change here to check if the return value is array then we spread it 
        Reflect.apply(nextFn, null, toArray(value))
      ); }, Reflect.apply(mainFn, null, args))
    );
    }
  );
  };

  /**
   * check if the object has name property
   * @param {object} obj the object to check
   * @param {string} name the prop name
   * @return {*} the value or undefined
   */
  function objHasProp$1(obj, name) {
    var prop = Object.getOwnPropertyDescriptor(obj, name);
    return prop !== undefined && prop.value ? prop.value : prop;
  }

  /**
   * After the user login we will use this Object.define add a new property
   * to the resolver with the decoded user data
   * @param {function} resolver target resolver
   * @param {string} name the name of the object to get inject also for checking
   * @param {object} data to inject into the function static interface
   * @param {boolean} [overwrite=false] if we want to overwrite the existing data
   * @return {function} added property resolver
   */
  function injectToFn(resolver, name, data, overwrite) {
    if ( overwrite === void 0 ) overwrite = false;

    var check = objHasProp$1(resolver, name);
    if (overwrite === false && check !== undefined) {
      // console.info(`NOT INJECTED`)
      return resolver;
    }
    /* this will throw error!
    if (overwrite === true && check !== undefined) {
      delete resolver[name] // delete this property
    }
    */
    // console.info(`INJECTED`)
    Object.defineProperty(resolver, name, {
      value: data,
      writable: overwrite // if its set to true then we should able to overwrite it
    });

    return resolver;
  }

  // breaking out the inner methods generator in here

  /**
   * generate authorisation specific methods
   * @param {object} jsonqlInstance instance of this
   * @param {string} name of method
   * @param {object} opts configuration
   * @param {object} contract to match
   * @return {function} for use
   */
  var authMethodGenerator = function (jsonqlInstance, name, opts, contract) {
    return function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      var params = contract.auth[name].params;
      var values = params.map(function (p, i) { return args[i]; });
      var header = args[params.length] || {};
      return validateAsync$1(args, params)
        .then(function () { return jsonqlInstance
            .query
            .apply(jsonqlInstance, [name, values, header]); }
        )
        .catch(finalCatch)
    }
  };

  /**
   * Break up the different type each - create query methods
   * @param {object} obj to hold all the objects
   * @param {object} jsonqlInstance jsonql class instance
   * @param {object} ee eventEmitter
   * @param {object} config configuration
   * @param {object} contract json
   * @return {object} modified output for next op
   */
  var createQueryMethods = function (obj, jsonqlInstance, ee, config, contract) {
    var query = {};
    var loop = function ( queryFn ) {
      // to keep it clean we use a param to id the auth method
      // const fn = (_contract.query[queryFn].auth === true) ? 'auth' : queryFn;
      // generate the query method
      query = injectToFn(query, queryFn, function queryFnHandler() {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        var params = contract.query[queryFn].params;
        var _args = params.map(function (param, i) { return args[i]; });
        // debug('query', queryFn, _params);
        // @TODO this need to change to a different way to add an extra header
        var header = {};
        // @TODO validate against the type
        return validateAsync$1(_args, params)
          .then(function () { return jsonqlInstance
              .query
              .apply(jsonqlInstance, [queryFn, _args, header]); }
          )
          .catch(finalCatch)
      });
    };

    for (var queryFn in contract.query) loop( queryFn );
    obj.query = query;
    // create an alias to the helloWorld method
    obj.helloWorld = query.helloWorld;
    return [ obj, jsonqlInstance, ee, config, contract ]
  };

  /**
   * create mutation methods
   * @param {object} obj to hold all the objects
   * @param {object} jsonqlInstance jsonql class instance
   * @param {object} ee eventEmitter
   * @param {object} config configuration
   * @param {object} contract json
   * @return {object} modified output for next op
   */
  var createMutationMethods = function (obj, jsonqlInstance, ee, config, contract) {
    var mutation = {};
    // process the mutation, the reason the mutation has a fixed number of parameters
    // there is only the payload, and conditions parameters
    // plus a header at the end
    var loop = function ( mutationFn ) {
      mutation = injectToFn(mutation, mutationFn, function mutationFnHandler(payload, conditions, header) {
        if ( header === void 0 ) header = {};

        var args = [payload, conditions];
        var params = contract.mutation[mutationFn].params;
        return validateAsync$1(args, params)
          .then(function () { return jsonqlInstance
              .mutation
              .apply(jsonqlInstance, [mutationFn, payload, conditions, header]); }
          )
          .catch(finalCatch)
      });
    };

    for (var mutationFn in contract.mutation) loop( mutationFn );
    obj.mutation = mutation;
    return [ obj, jsonqlInstance, ee, config, contract ]
  };

  /**
   * create auth methods
   * @param {object} obj to hold all the objects
   * @param {object} jsonqlInstance jsonql class instance
   * @param {object} ee eventEmitter
   * @param {object} config configuration
   * @param {object} contract json
   * @return {object} modified output for next op
   */
  var createAuthMethods = function (obj, jsonqlInstance, ee, config, contract) {
    if (config.enableAuth && contract.auth) {
      var auth = {}; // v1.3.1 add back the auth prop name in contract
      var loginHandlerName = config.loginHandlerName;
      var logoutHandlerName = config.logoutHandlerName;
      if (contract.auth[loginHandlerName]) {
        // changing to the name the config specify
        auth[loginHandlerName] = function loginHandlerFn() {
          var args = [], len = arguments.length;
          while ( len-- ) args[ len ] = arguments[ len ];

          var fn = authMethodGenerator(jsonqlInstance, loginHandlerName, config, contract);
          return fn.apply(null, args)
            .then(jsonqlInstance.postLoginAction.bind(jsonqlInstance))
            .then(function (ref) {
              var token = ref.token;
              var userdata = ref.userdata;

              ee.$trigger(LOGIN_NAME, token);
              //  1.5.6 return the decoded userdata instead
              return userdata
            })
        };
      }
      // @TODO allow to logout one particular profile or all of them
      if (contract.auth[logoutHandlerName]) { // this one has a server side logout
        auth[logoutHandlerName] = function logoutHandlerFn() {
          var args = [], len = arguments.length;
          while ( len-- ) args[ len ] = arguments[ len ];

          var fn = authMethodGenerator(jsonqlInstance, logoutHandlerName, config, contract);
          return fn.apply(null, args)
            .then(jsonqlInstance.postLogoutAction.bind(jsonqlInstance))
            .then(function (r) {
              ee.$trigger(LOGOUT_NAME, r);
              return r;
            })
        };
      } else { // this is only for client side logout
        // @TODO should allow to login particular profile
        auth[logoutHandlerName] = function logoutHandlerFn(profileId) {
          if ( profileId === void 0 ) profileId = null;

          jsonqlInstance.postLogoutAction(KEY_WORD, profileId);
          ee.$trigger(LOGOUT_NAME, KEY_WORD);
        };
      }
      obj.auth = auth;
    }

    return obj;
  };

  /**
   * We want the same event emitter that get injected return to the client
   * Therefore we need to take the one been used and return it
   */
  function addPropsToClient(obj, jsonqlInstance, ee, config, contract) {
    obj.eventEmitter = ee;
    obj.contract = contract;
    obj.version = '0.2.0';
    // use this method then we can hook into the debugOn at the same time
    // 1.5.2 change it to a getter to return a method, pass a name to id which one is which
    obj.getLogger = function (name) { return function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return Reflect.apply(jsonqlInstance.log, jsonqlInstance, [name ].concat( args));
   }    };
    // auth
    // create the rest of the methods
    if (config.enableAuth) {
      /**
       * new method to allow retrieve the current login user data
       * @TODO allow to pass an id to switch to different userdata
       * @return {*} userdata
       */
      obj.userdata = function () { return jsonqlInstance.jsonqlUserdata; };
      // allow getting the token for valdiate agains the socket
      // if it's not require auth there is no point of calling getToken
      obj.getToken = function (idx) {
        if ( idx === void 0 ) idx = false;

        return jsonqlInstance.rawAuthToken(idx);
      };
      // switch profile or read back what is the currenct index
      obj.profileIndex = function (idx) {
        if ( idx === void 0 ) idx = false;

        if (idx === false) {
          return jsonqlInstance.profileIndex
        }
        jsonqlInstance.profileIndex = idx;
      };
      // new in 1.5.1 to return different profiles
      obj.getProfiles = function (idx) {
        if ( idx === void 0 ) idx = false;

        return jsonqlInstance.getProfiles(idx);
      };
    }

    return obj;
  }

  /**
   * Here just generate the methods calls
   * @param {object} jsonqlInstance what it said
   * @param {object} ee event emitter
   * @param {object} config configuration
   * @param {object} contract the map
   * @return {object} with mapped methods
   */
  function methodsGenerator(jsonqlInstance, ee, config, contract) {
    var obj = {};
    var fns = [createQueryMethods, createMutationMethods, createAuthMethods];
    var executor = Reflect.apply(chainFns, null, fns);
    return executor(obj, jsonqlInstance, ee, config, contract)
  }

  // Generate the resolver for developer to use

  /**
   * @param {object} jsonqlInstance jsonql class instance
   * @param {object} config options
   * @param {object} contract the contract
   * @param {object} ee eventEmitter
   * @return {object} constructed functions call
   */
  var generator = function (jsonqlInstance, config, contract, ee) {
    // V1.3.0 - now everything wrap inside this method
    var client = methodsGenerator(jsonqlInstance, ee, config, contract);

    // allow developer to access the store api
    if (config.exposeStore) ;
    client = addPropsToClient(client, jsonqlInstance, ee, config, contract);

    // output
    return client
  };

  // all the client configuration options here
  var constProps = {
    contract: false,
    MUTATION_ARGS: ['name', 'payload', 'conditions'], // this seems wrong?
    CONTENT_TYPE: CONTENT_TYPE,
    BEARER: BEARER,
    AUTH_HEADER: AUTH_HEADER
  };

  // grab the localhost name and put into the hostname as default
  var getHostName = function () {
    try {
      return [window.location.protocol, window.location.host].join('//')
    } catch(e) {
      return null
    }
  };

  var appProps$1 = {

    hostname: createConfig$1(getHostName(), [STRING_TYPE]), // required the hostname
    jsonqlPath: createConfig$1(JSONQL_PATH, [STRING_TYPE]), // The path on the server

    loginHandlerName: createConfig$1(ISSUER_NAME, [STRING_TYPE]),
    logoutHandlerName: createConfig$1(LOGOUT_NAME, [STRING_TYPE]),
    // add to koa v1.3.0 - this might remove in the future
    enableJsonp: createConfig$1(false, [BOOLEAN_TYPE]),
    enableAuth: createConfig$1(false, [BOOLEAN_TYPE]),
    // enable useJwt by default @TODO replace with something else and remove them later
    useJwt: createConfig$1(true, [BOOLEAN_TYPE]),
    // when true then store infinity or pass a time in seconds then we check against
    // the token date of creation
    persistToken: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE]),
     // the header
     // v1.2.0 we are using this option during the dev
     // so it won't save anything to the localstorage and fetch a new contract
     // whenever the browser reload
    useLocalstorage: createConfig$1(true, [BOOLEAN_TYPE]), // should we store the contract into localStorage
    storageKey: createConfig$1(CLIENT_STORAGE_KEY, [STRING_TYPE]),// the key to use when store into localStorage
    authKey: createConfig$1(CLIENT_AUTH_KEY, [STRING_TYPE]),// the key to use when store into the sessionStorage
    contractExpired: createConfig$1(0, [NUMBER_TYPE]),// -1 always fetch contract,
                        // 0 never expired,
                        // > 0 then compare the timestamp with the current one to see if we need to get contract again
    // useful during development
    keepContract: createConfig$1(true, [BOOLEAN_TYPE]),
    exposeContract: createConfig$1(false, [BOOLEAN_TYPE]),
    exposeStore: createConfig$1(false, [BOOLEAN_TYPE]), // whether to allow developer to access the store fn 
    // @1.2.1 new option for the contract-console to fetch the contract with description
    showContractDesc: createConfig$1(false, [BOOLEAN_TYPE]),
    contractKey: createConfig$1(false, [BOOLEAN_TYPE]), // if the server side is lock by the key you need this
    contractKeyName: createConfig$1(CONTRACT_KEY_NAME, [STRING_TYPE]), // same as above they go in pairs
    enableTimeout: createConfig$1(false, [BOOLEAN_TYPE]), // @TODO
    timeout: createConfig$1(5000, [NUMBER_TYPE]), // 5 seconds
    returnInstance: createConfig$1(false, [BOOLEAN_TYPE]),
    allowReturnRawToken: createConfig$1(false, [BOOLEAN_TYPE]),
    debugOn: createConfig$1(false, [BOOLEAN_TYPE])
  };

  // this will replace the preConfigCheck in jsonql-koa

  /**
   * the rest of the argument will be functions that
   * need to add to the process chain,
   * finally return a function to accept the config
   * @param {object} defaultOptions prepared before hand
   * @param {object} constProps prepare before hand
   * @param {array} fns arguments see description
   * @return {function} to perform the final configuration check
   */
  function preConfigCheck(defaultOptions1, constProps1) {
    var fns = [], len = arguments.length - 2;
    while ( len-- > 0 ) fns[ len ] = arguments[ len + 2 ];

    // should have just add the method to the last
    var finalFn = function (opt) { return injectToFn(opt, CHECKED_KEY, timestamp$1()); };
    // if there is more than one then chain it otherwise just return the zero idx one
    var fn = Reflect.apply(chainFns, null, fns.concat([finalFn]));
    // 0.8.8 add a default property empty object
    return function preConfigCheckAction(config) {
      if ( config === void 0 ) config = {};

      return fn(config, defaultOptions1, constProps1)
    }
  }

  // new module interface for @jsonql/client

  /**
   * This will combine the socket client options and merge this one
   * then do a pre-check on both at the same time
   * @param {object} [extraProps = {}]
   * @param {object} [extraConstProps = {}]
   * @return {function} to process the developer options
   */
  function getPreConfigCheck(extraProps, extraConstProps) {
    if ( extraProps === void 0 ) extraProps = {};
    if ( extraConstProps === void 0 ) extraConstProps = {};

    // we only want a shallow copy instead of deep merge
    var aProps = Object.assign({}, appProps$1, extraProps);
    var cProps = Object.assign({}, constProps, extraConstProps);
    return preConfigCheck(aProps, cProps, checkConfig)
  }

  // Combine interface to also init the socket client if it's required

  /**
   * Create the custom check options method
   * @param {object} extraDefaultOptions for valdiation
   * @param {object} extraConstProps for merge after
   * @return {function} resolve the clean configuration
   */
  var getCheckConfigFn = function(extraDefaultOptions, extraConstProps) {
    var checkAction = getPreConfigCheck(extraDefaultOptions, extraConstProps);
    return function (config) {
      if ( config === void 0 ) config = {};

      return Promise.resolve(checkAction(config));
    }
  };

  /**
   * Check if the contract has socket field and the socket client is suplied
   * @param {*} [socketClient=null] from the original config
   * @return {function} takes in the extra params then return the client
   */
  function initSocketClient(socketClient) {
    if ( socketClient === void 0 ) socketClient = null;

    /**
     * @param {object} client the http client
     * @param {object} contract the json
     * @param {object} config the checked configuration
     */
    return function (client, contract, config) {
      if (isObjectHasKey$1(contract, SOCKET_NAME)) {
        console.log('contract has socket_name', config);

        var prop = objHasProp(config, CHECKED_KEY);

        console.error('CHECKED_KEY', prop);

        if (socketClient) {
          console.log('has socketClient');
          var constProps = {
            contract: contract,
            log: client.getLogger(("jsonql-client:" + (config.serverType))),
            eventEmitter: client.eventEmitter
          };
          return socketClient(config, constProps)
            .then(function (sc) {
              client[SOCKET_NAME] = sc;
              return client
            })
        } else {
          throw new JsonqlError("initSocketClient", "socketClient is missing!")
        }
      }
      // just return it if there is none
      return client
    }
  }

  /**
   * Main interface for construct the client and return extra options for continue
   * with socket client if any
   * @param {object} Fly the http engine
   * @param {object} [config={}] configuration
   * @return {function} return promise resolve with opts, contract, client
   */
  function getJsonqlClient(fly, extraDefaultOptions, extraConstProps) {
    if ( extraDefaultOptions === void 0 ) extraDefaultOptions = {};
    if ( extraConstProps === void 0 ) extraConstProps = {};

    var checkConfigFn = getCheckConfigFn(extraDefaultOptions, extraConstProps);
    // resolve opts, contract, client
    return function (config) {
      if ( config === void 0 ) config = {};


      return checkConfigFn(config)
        .then(function (opts) { return (
          {
            opts: opts,
            baseClient: new JsonqlBaseClient(fly, opts)
          }
        ); })
        // make sure the contract is presented
        .then(function (ref) {
          var opts = ref.opts;
          var baseClient = ref.baseClient;

          var ee = getEventEmitter(opts.debugOn);
          return getContractFromConfig(baseClient, opts.contract)
            .then(function (contract) { return (
              {
                opts: opts,
                contract: contract,
                client: generator(baseClient, opts, contract, ee)
              }
            ); }
          )
        })
        // @NOTE we only return the opts, contract, client here
        // and allow the client to chain into this to coninue
        // finally generate the websocket client if any
    }
  }

  // constants

  var SOCKET_IO = JS_WS_SOCKET_IO_NAME;

  var MISSING_PROP_ERR = 'Missing property in contract!';

  var EMIT_EVT = EMIT_REPLY_TYPE;

  var UNKNOWN_RESULT = 'UKNNOWN RESULT!';

  var MY_NAMESPACE = 'myNamespace';

  var CB_FN_NAME = 'on';

  // bunch of generic helpers

  /**
   * DIY in Array
   * @param {array} arr to check from
   * @param {*} value to check against
   * @return {boolean} true on found
   */
  var inArray$3 = function (arr, value) { return !!arr.filter(function (a) { return a === value; }).length; };

  // quick and dirty to turn non array to array
  var toArray$2 = function (arg) { return isArray(arg) ? arg : [arg]; };

  /**
   * @param {object} obj for search
   * @param {string} key target
   * @return {boolean} true on success
   */
  var isObjectHasKey$4 = function(obj, key) {
    try {
      var keys = Object.keys(obj);
      return inArray$3(keys, key)
    } catch(e) {
      // @BUG when the obj is not an OBJECT we got some weird output
      return false;
      /*
      console.info('obj', obj)
      console.error(e)
      throw new Error(e)
      */
    }
  };

  /**
   * create a event name
   * @param {string[]} args
   * @return {string} event name for use
   */
  var createEvt = function () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    return args.join('_');
  };

  /**
   * Simple check if the prop is function
   * @param {*} prop input
   * @return {boolean} true on success
   */
  var isFunc = function (prop) {
    if (typeof prop === 'function') {
      return true;
    }
    console.error(("Expect to be Function type! Got " + (typeof prop)));
  };

  /**
   * using just the map reduce to chain multiple functions together
   * @param {function} mainFn the init function
   * @param {array} moreFns as many as you want to take the last value and return a new one
   * @return {function} accept value for the mainFn
   */
  var chainFns$1 = function (mainFn) {
    var moreFns = [], len = arguments.length - 1;
    while ( len-- > 0 ) moreFns[ len ] = arguments[ len + 1 ];

    return (
    function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return (
      moreFns.reduce(function (value, nextFn) { return (
        // change here to check if the return value is array then we spread it 
        Reflect.apply(nextFn, null, toArray$2(value))
      ); }, Reflect.apply(mainFn, null, args))
    );
    }
  );
  };

  // break it out on its own because


  /**
   * This one return a different result from the chainPromises
   * it will be the same like chainFns that take one promise resolve as the next fn parameter
   * @param {function} initPromise a function that accept param and resolve result
   * @param {array} promises array of function pass that resolve promises
   * @return {promise} resolve the processed result
   */
  function chainProcessPromises(initPromise) {
    var promises = [], len = arguments.length - 1;
    while ( len-- > 0 ) promises[ len ] = arguments[ len + 1 ];

    return function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return (
        promises.reduce(function (promiseChain, currentTask) { return (
          promiseChain.then(function (chainResult) { return (
            currentTask(chainResult)
          ); }
        )
      ); }, Reflect.apply(initPromise, null, args))
    );
    }
  }

  /**
   * this is essentially the same as the injectToFn
   * but this will not allow overwrite and set the setter and getter
   * @param {object} obj to get injected
   * @param {string} name of the property
   * @param {function} setter for set
   * @param {function} [getter=null] for get default return null fn
   * @return {object} the injected obj
   */
  function objDefineProps(obj, name, setter, getter) {
    if ( getter === void 0 ) getter = null;

    if (Object.getOwnPropertyDescriptor(obj, name) === undefined) {
      Object.defineProperty(obj, name, {
        set: setter,
        get: getter === null ? function() { return null; } : getter
      });
    }
    return obj
  }

  /**
   * check if the object has name property
   * @param {object} obj the object to check
   * @param {string} name the prop name
   * @return {*} the value or undefined
   */
  function objHasProp$2(obj, name) {
    var prop = Object.getOwnPropertyDescriptor(obj, name);
    return prop !== undefined && prop.value ? prop.value : prop;
  }

  /**
   * After the user login we will use this Object.define add a new property
   * to the resolver with the decoded user data
   * @param {function} resolver target resolver
   * @param {string} name the name of the object to get inject also for checking
   * @param {object} data to inject into the function static interface
   * @param {boolean} [overwrite=false] if we want to overwrite the existing data
   * @return {function} added property resolver
   */
  function injectToFn$1(resolver, name, data, overwrite) {
    if ( overwrite === void 0 ) overwrite = false;

    var check = objHasProp$2(resolver, name);
    if (overwrite === false && check !== undefined) {
      // console.info(`NOT INJECTED`)
      return resolver;
    }
    /* this will throw error!
    if (overwrite === true && check !== undefined) {
      delete resolver[name] // delete this property
    }
    */
    // console.info(`INJECTED`)
    Object.defineProperty(resolver, name, {
      value: data,
      writable: overwrite // if its set to true then we should able to overwrite it
    });

    return resolver;
  }

  // split the contract into the node side and the generic side
  /**
   * Check if the json is a contract file or not
   * @param {object} contract json object
   * @return {boolean} true
   */
  function checkIsContract$1(contract) {
    return isPlainObject(contract)
    && (
      isObjectHasKey$4(contract, QUERY_NAME)
   || isObjectHasKey$4(contract, MUTATION_NAME)
   || isObjectHasKey$4(contract, SOCKET_NAME)
    )
  }

  /**
   * Wrapper method that check if it's contract then return the contract or false
   * @param {object} contract the object to check
   * @return {boolean | object} false when it's not
   */
  function isContract$1(contract) {
    return checkIsContract$1(contract) ? contract : false;
  }

  /**
   * Ported from jsonql-params-validator but different
   * if we don't find the socket part then return false
   * @param {object} contract the contract object
   * @return {object|boolean} false on failed
   */
  function extractSocketPart(contract) {
    if (isObjectHasKey$4(contract, 'socket')) {
      return contract.socket;
    }
    return false;
  }

  /**
   * @BUG we should check the socket part instead of expect the downstream to read the menu!
   * We only need this when the enableAuth is true otherwise there is only one namespace
   * @param {object} contract the socket part of the contract file
   * @param {boolean} [fallback=false] this is a fall back option for old code
   * @return {object} 1. remap the contract using the namespace --> resolvers
   * 2. the size of the object (1 all private, 2 mixed public with private)
   * 3. which namespace is public
   */
  function groupByNamespace(contract, fallback) {
    if ( fallback === void 0 ) fallback = false;

    var socket = extractSocketPart(contract);
    if (socket === false) {
      if (fallback) {
        return contract; // just return the whole contract
      }
      throw new JsonqlError("socket not found in contract!")
    }
    var nspSet = {};
    var size = 0;
    var publicNamespace;
    for (var resolverName in socket) {
      var params = socket[resolverName];
      var namespace = params.namespace;
      if (namespace) {
        if (!nspSet[namespace]) {
          ++size;
          nspSet[namespace] = {};
        }
        nspSet[namespace][resolverName] = params;
        if (!publicNamespace) {
          if (params.public) {
            publicNamespace = namespace;
          }
        }
      }
    }
    return { size: size, nspSet: nspSet, publicNamespace: publicNamespace }
  }

  /**
   * @param {boolean} sec return in second or not
   * @return {number} timestamp
   */
  var timestamp$2 = function (sec) {
    if ( sec === void 0 ) sec = false;

    var time = Date.now();
    return sec ? Math.floor( time / 1000 ) : time;
  };

  // this will replace the preConfigCheck in jsonql-koa

  /**
   * the rest of the argument will be functions that
   * need to add to the process chain,
   * finally return a function to accept the config
   * @param {object} defaultOptions prepared before hand
   * @param {object} constProps prepare before hand
   * @param {array} fns arguments see description
   * @return {function} to perform the final configuration check
   */
  function preConfigCheck$1(defaultOptions1, constProps1) {
    var fns = [], len = arguments.length - 2;
    while ( len-- > 0 ) fns[ len ] = arguments[ len + 2 ];

    // should have just add the method to the last
    var finalFn = function (opt) { return injectToFn$1(opt, CHECKED_KEY, timestamp$2()); };
    // if there is more than one then chain it otherwise just return the zero idx one
    var fn = Reflect.apply(chainFns$1, null, fns.concat([finalFn]));
    // 0.8.8 add a default property empty object
    return function preConfigCheckAction(config) {
      if ( config === void 0 ) config = {};

      return fn(config, defaultOptions1, constProps1)
    }
  }

  /**
   * Make sure everything is in the same page
   * @param {object} defaultOptions configuration option
   * @param {object} constProps add later
   * @param {array} next a list of functions to call if it's not
   * @return {function} resolve the configuration combined
   */
  function postConfigCheck(defaultOptions2, constProps2) {
    var next = [], len = arguments.length - 2;
    while ( len-- > 0 ) next[ len ] = arguments[ len + 2 ];

    return function postConfigCheckAction(config) {
      if ( config === void 0 ) config = {};

      if (objHasProp$2(config, CHECKED_KEY)) {
        return Promise.resolve(merge({}, config, constProps2))
      }
      var fn = Reflect.apply(preConfigCheck$1, null, [defaultOptions2, constProps2 ].concat( next));
      return Promise.resolve(fn(config))
    }
  }

  var NB_EVENT_SERVICE_PRIVATE_STORE$1 = new WeakMap();
  var NB_EVENT_SERVICE_PRIVATE_LAZY$1 = new WeakMap();

  /**
   * generate a 32bit hash based on the function.toString()
   * _from http://stackoverflow.com/questions/7616461/generate-a-hash-_from-string-in-javascript-jquery
   * @param {string} s the converted to string function
   * @return {string} the hashed function string
   */
  function hashCode$2(s) {
  	return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
  }
  // wrapper to make sure it string 
  function hashCode2Str$1(s) {
    return hashCode$2(s) + ''
  }

  // making all the functionality on it's own
  // import { WatchClass } from './watch'

  var SuspendClass$1 = function SuspendClass() {
    // suspend, release and queue
    this.__suspend__ = null;
    this.queueStore = new Set();
    /*
    this.watch('suspend', function(value, prop, oldValue) {
      this.logger(`${prop} set from ${oldValue} to ${value}`)
      // it means it set the suspend = true then release it
      if (oldValue === true && value === false) {
        // we want this happen after the return happens
        setTimeout(() => {
          this.release()
        }, 1)
      }
      return value; // we need to return the value to store it
    })
    */
  };

  var prototypeAccessors$2 = { $suspend: { configurable: true },$queues: { configurable: true } };

  /**
   * setter to set the suspend and check if it's boolean value
   * @param {boolean} value to trigger
   */
  prototypeAccessors$2.$suspend.set = function (value) {
      var this$1 = this;

    if (typeof value === 'boolean') {
      var lastValue = this.__suspend__;
      this.__suspend__ = value;
      this.logger('($suspend)', ("Change from " + lastValue + " --> " + value));
      if (lastValue === true && value === false) {
        setTimeout(function () {
          this$1.release();
        }, 1);
      }
    } else {
      throw new Error("$suspend only accept Boolean value!")
    }
  };

  /**
   * queuing call up when it's in suspend mode
   * @param {any} value
   * @return {Boolean} true when added or false when it's not
   */
  SuspendClass$1.prototype.$queue = function $queue () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

    if (this.__suspend__ === true) {
      this.logger('($queue)', 'added to $queue', args);
      // there shouldn't be any duplicate ...
      this.queueStore.add(args);
    }
    return this.__suspend__;
  };

  /**
   * a getter to get all the store queue
   * @return {array} Set turn into Array before return
   */
  prototypeAccessors$2.$queues.get = function () {
    var size = this.queueStore.size;
    this.logger('($queues)', ("size: " + size));
    if (size > 0) {
      return Array.from(this.queueStore)
    }
    return []
  };

  /**
   * Release the queue
   * @return {int} size if any
   */
  SuspendClass$1.prototype.release = function release () {
      var this$1 = this;

    var size = this.queueStore.size;
    this.logger('(release)', ("Release was called " + size));
    if (size > 0) {
      var queue = Array.from(this.queueStore);
      this.queueStore.clear();
      this.logger('queue', queue);
      queue.forEach(function (args) {
        this$1.logger(args);
        Reflect.apply(this$1.$trigger, this$1, args);
      });
      this.logger(("Release size " + (this.queueStore.size)));
    }
  };

  Object.defineProperties( SuspendClass$1.prototype, prototypeAccessors$2 );

  // break up the main file because its getting way too long

  var NbEventServiceBase$1 = /*@__PURE__*/(function (SuspendClass) {
    function NbEventServiceBase(config) {
      if ( config === void 0 ) config = {};

      SuspendClass.call(this);
      if (config.logger && typeof config.logger === 'function') {
        this.logger = config.logger;
      }
      this.keep = config.keep;
      // for the $done setter
      this.result = config.keep ? [] : null;
      // we need to init the store first otherwise it could be a lot of checking later
      this.normalStore = new Map();
      this.lazyStore = new Map();
    }

    if ( SuspendClass ) NbEventServiceBase.__proto__ = SuspendClass;
    NbEventServiceBase.prototype = Object.create( SuspendClass && SuspendClass.prototype );
    NbEventServiceBase.prototype.constructor = NbEventServiceBase;

    var prototypeAccessors = { is: { configurable: true },normalStore: { configurable: true },lazyStore: { configurable: true } };

    // for id if the instance is this class
    prototypeAccessors.is.get = function () {
      return 'nb-event-service'
    };

    /**
     * validate the event name(s)
     * @param {string[]} evt event name
     * @return {boolean} true when OK
     */
    NbEventServiceBase.prototype.validateEvt = function validateEvt () {
      var this$1 = this;
      var evt = [], len = arguments.length;
      while ( len-- ) evt[ len ] = arguments[ len ];

      evt.forEach(function (e) {
        if (typeof e !== 'string') {
          this$1.logger('(validateEvt)', e);
          throw new Error("event name must be string type!")
        }
      });
      return true;
    };

    /**
     * Simple quick check on the two main parameters
     * @param {string} evt event name
     * @param {function} callback function to call
     * @return {boolean} true when OK
     */
    NbEventServiceBase.prototype.validate = function validate (evt, callback) {
      if (this.validateEvt(evt)) {
        if (typeof callback === 'function') {
          return true;
        }
      }
      throw new Error("callback required to be function type!")
    };

    /**
     * Check if this type is correct or not added in V1.5.0
     * @param {string} type for checking
     * @return {boolean} true on OK
     */
    NbEventServiceBase.prototype.validateType = function validateType (type) {
      var types = ['on', 'only', 'once', 'onlyOnce'];
      return !!types.filter(function (t) { return type === t; }).length;
    };

    /**
     * Run the callback
     * @param {function} callback function to execute
     * @param {array} payload for callback
     * @param {object} ctx context or null
     * @return {void} the result store in $done
     */
    NbEventServiceBase.prototype.run = function run (callback, payload, ctx) {
      this.logger('(run)', callback, payload, ctx);
      this.$done = Reflect.apply(callback, ctx, this.toArray(payload));
    };

    /**
     * Take the content out and remove it from store id by the name
     * @param {string} evt event name
     * @param {string} [storeName = lazyStore] name of store
     * @return {object|boolean} content or false on not found
     */
    NbEventServiceBase.prototype.takeFromStore = function takeFromStore (evt, storeName) {
      if ( storeName === void 0 ) storeName = 'lazyStore';

      var store = this[storeName]; // it could be empty at this point
      if (store) {
        this.logger('(takeFromStore)', storeName, store);
        if (store.has(evt)) {
          var content = store.get(evt);
          this.logger('(takeFromStore)', ("has " + evt), content);
          store.delete(evt);
          return content;
        }
        return false;
      }
      throw new Error((storeName + " is not supported!"))
    };

    /**
     * The add to store step is similar so make it generic for resuse
     * @param {object} store which store to use
     * @param {string} evt event name
     * @param {spread} args because the lazy store and normal store store different things
     * @return {array} store and the size of the store
     */
    NbEventServiceBase.prototype.addToStore = function addToStore (store, evt) {
      var args = [], len = arguments.length - 2;
      while ( len-- > 0 ) args[ len ] = arguments[ len + 2 ];

      var fnSet;
      if (store.has(evt)) {
        this.logger('(addToStore)', (evt + " existed"));
        fnSet = store.get(evt);
      } else {
        this.logger('(addToStore)', ("create new Set for " + evt));
        // this is new
        fnSet = new Set();
      }
      // lazy only store 2 items - this is not the case in V1.6.0 anymore
      // we need to check the first parameter is string or not
      if (args.length > 2) {
        if (Array.isArray(args[0])) { // lazy store
          // check if this type of this event already register in the lazy store
          var t = args[2];
          if (!this.checkTypeInLazyStore(evt, t)) {
            fnSet.add(args);
          }
        } else {
          if (!this.checkContentExist(args, fnSet)) {
            this.logger('(addToStore)', "insert new", args);
            fnSet.add(args);
          }
        }
      } else { // add straight to lazy store
        fnSet.add(args);
      }
      store.set(evt, fnSet);
      return [store, fnSet.size]
    };

    /**
     * @param {array} args for compare
     * @param {object} fnSet A Set to search from
     * @return {boolean} true on exist
     */
    NbEventServiceBase.prototype.checkContentExist = function checkContentExist (args, fnSet) {
      var list = Array.from(fnSet);
      return !!list.filter(function (l) {
        var hash = l[0];
        if (hash === args[0]) {
          return true;
        }
        return false;
      }).length;
    };

    /**
     * get the existing type to make sure no mix type add to the same store
     * @param {string} evtName event name
     * @param {string} type the type to check
     * @return {boolean} true you can add, false then you can't add this type
     */
    NbEventServiceBase.prototype.checkTypeInStore = function checkTypeInStore (evtName, type) {
      this.validateEvt(evtName, type);
      var all = this.$get(evtName, true);
      if (all === false) {
         // pristine it means you can add
        return true;
      }
      // it should only have ONE type in ONE event store
      return !all.filter(function (list) {
        var t = list[3];
        return type !== t;
      }).length;
    };

    /**
     * This is checking just the lazy store because the structure is different
     * therefore we need to use a new method to check it
     */
    NbEventServiceBase.prototype.checkTypeInLazyStore = function checkTypeInLazyStore (evtName, type) {
      this.validateEvt(evtName, type);
      var store = this.lazyStore.get(evtName);
      this.logger('(checkTypeInLazyStore)', store);
      if (store) {
        return !!Array
          .from(store)
          .filter(function (l) {
            var t = l[2];
            return t !== type;
          }).length
      }
      return false;
    };

    /**
     * wrapper to re-use the addToStore,
     * V1.3.0 add extra check to see if this type can add to this evt
     * @param {string} evt event name
     * @param {string} type on or once
     * @param {function} callback function
     * @param {object} context the context the function execute in or null
     * @return {number} size of the store
     */
    NbEventServiceBase.prototype.addToNormalStore = function addToNormalStore (evt, type, callback, context) {
      if ( context === void 0 ) context = null;

      this.logger('(addToNormalStore)', evt, type, 'try to add to normal store');
      // @TODO we need to check the existing store for the type first!
      if (this.checkTypeInStore(evt, type)) {
        this.logger('(addToNormalStore)', (type + " can add to " + evt + " normal store"));
        var key = this.hashFnToKey(callback);
        var args = [this.normalStore, evt, key, callback, context, type];
        var ref = Reflect.apply(this.addToStore, this, args);
        var _store = ref[0];
        var size = ref[1];
        this.normalStore = _store;
        return size;
      }
      return false;
    };

    /**
     * Add to lazy store this get calls when the callback is not register yet
     * so we only get a payload object or even nothing
     * @param {string} evt event name
     * @param {array} payload of arguments or empty if there is none
     * @param {object} [context=null] the context the callback execute in
     * @param {string} [type=false] register a type so no other type can add to this evt
     * @return {number} size of the store
     */
    NbEventServiceBase.prototype.addToLazyStore = function addToLazyStore (evt, payload, context, type) {
      if ( payload === void 0 ) payload = [];
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = false;

      // this is add in V1.6.0
      // when there is type then we will need to check if this already added in lazy store
      // and no other type can add to this lazy store
      var args = [this.lazyStore, evt, this.toArray(payload), context];
      if (type) {
        args.push(type);
      }
      var ref = Reflect.apply(this.addToStore, this, args);
      var _store = ref[0];
      var size = ref[1];
      this.lazyStore = _store;
      return size;
    };

    /**
     * make sure we store the argument correctly
     * @param {*} arg could be array
     * @return {array} make sured
     */
    NbEventServiceBase.prototype.toArray = function toArray (arg) {
      return Array.isArray(arg) ? arg : [arg];
    };

    /**
     * setter to store the Set in private
     * @param {object} obj a Set
     */
    prototypeAccessors.normalStore.set = function (obj) {
      NB_EVENT_SERVICE_PRIVATE_STORE$1.set(this, obj);
    };

    /**
     * @return {object} Set object
     */
    prototypeAccessors.normalStore.get = function () {
      return NB_EVENT_SERVICE_PRIVATE_STORE$1.get(this)
    };

    /**
     * setter to store the Set in lazy store
     * @param {object} obj a Set
     */
    prototypeAccessors.lazyStore.set = function (obj) {
      NB_EVENT_SERVICE_PRIVATE_LAZY$1.set(this , obj);
    };

    /**
     * @return {object} the lazy store Set
     */
    prototypeAccessors.lazyStore.get = function () {
      return NB_EVENT_SERVICE_PRIVATE_LAZY$1.get(this)
    };

    /**
     * generate a hashKey to identify the function call
     * The build-in store some how could store the same values!
     * @param {function} fn the converted to string function
     * @return {string} hashKey
     */
    NbEventServiceBase.prototype.hashFnToKey = function hashFnToKey (fn) {
      return hashCode2Str$1(fn.toString())
    };

    Object.defineProperties( NbEventServiceBase.prototype, prototypeAccessors );

    return NbEventServiceBase;
  }(SuspendClass$1));

  // The top level
  // export
  var EventService$1 = /*@__PURE__*/(function (NbStoreService) {
    function EventService(config) {
      if ( config === void 0 ) config = {};

      NbStoreService.call(this, config);
    }

    if ( NbStoreService ) EventService.__proto__ = NbStoreService;
    EventService.prototype = Object.create( NbStoreService && NbStoreService.prototype );
    EventService.prototype.constructor = EventService;

    var prototypeAccessors = { $done: { configurable: true } };

    /**
     * logger function for overwrite
     */
    EventService.prototype.logger = function logger () {};

    //////////////////////////
    //    PUBLIC METHODS    //
    //////////////////////////

    /**
     * Register your evt handler, note we don't check the type here,
     * we expect you to be sensible and know what you are doing.
     * @param {string} evt name of event
     * @param {function} callback bind method --> if it's array or not
     * @param {object} [context=null] to execute this call in
     * @return {number} the size of the store
     */
    EventService.prototype.$on = function $on (evt , callback , context) {
      var this$1 = this;
      if ( context === void 0 ) context = null;

      var type = 'on';
      this.validate(evt, callback);
      // first need to check if this evt is in lazy store
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register first then call later
      if (lazyStoreContent === false) {
        this.logger('($on)', (evt + " callback is not in lazy store"));
        // @TODO we need to check if there was other listener to this
        // event and are they the same type then we could solve that
        // register the different type to the same event name

        return this.addToNormalStore(evt, type, callback, context)
      }
      this.logger('($on)', (evt + " found in lazy store"));
      // this is when they call $trigger before register this callback
      var size = 0;
      lazyStoreContent.forEach(function (content) {
        var payload = content[0];
        var ctx = content[1];
        var t = content[2];
        if (t && t !== type) {
          throw new Error(("You are trying to register an event already been taken by other type: " + t))
        }
        this$1.logger("($on)", ("call run on " + evt));
        this$1.run(callback, payload, context || ctx);
        size += this$1.addToNormalStore(evt, type, callback, context || ctx);
      });
      return size;
    };

    /**
     * once only registered it once, there is no overwrite option here
     * @NOTE change in v1.3.0 $once can add multiple listeners
     *       but once the event fired, it will remove this event (see $only)
     * @param {string} evt name
     * @param {function} callback to execute
     * @param {object} [context=null] the handler execute in
     * @return {boolean} result
     */
    EventService.prototype.$once = function $once (evt , callback , context) {
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);
      var type = 'once';
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      var nStore = this.normalStore;
      if (lazyStoreContent === false) {
        this.logger('($once)', (evt + " not in the lazy store"));
        // v1.3.0 $once now allow to add multiple listeners
        return this.addToNormalStore(evt, type, callback, context)
      } else {
        // now this is the tricky bit
        // there is a potential bug here that cause by the developer
        // if they call $trigger first, the lazy won't know it's a once call
        // so if in the middle they register any call with the same evt name
        // then this $once call will be fucked - add this to the documentation
        this.logger('($once)', lazyStoreContent);
        var list = Array.from(lazyStoreContent);
        // should never have more than 1
        var ref = list[0];
        var payload = ref[0];
        var ctx = ref[1];
        var t = ref[2];
        if (t && t !== type) {
          throw new Error(("You are trying to register an event already been taken by other type: " + t))
        }
        this.logger('($once)', ("call run for " + evt));
        this.run(callback, payload, context || ctx);
        // remove this evt from store
        this.$off(evt);
      }
    };

    /**
     * This one event can only bind one callbackback
     * @param {string} evt event name
     * @param {function} callback event handler
     * @param {object} [context=null] the context the event handler execute in
     * @return {boolean} true bind for first time, false already existed
     */
    EventService.prototype.$only = function $only (evt, callback, context) {
      var this$1 = this;
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);
      var type = 'only';
      var added = false;
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      var nStore = this.normalStore;
      if (!nStore.has(evt)) {
        this.logger("($only)", (evt + " add to store"));
        added = this.addToNormalStore(evt, type, callback, context);
      }
      if (lazyStoreContent !== false) {
        // there are data store in lazy store
        this.logger('($only)', (evt + " found data in lazy store to execute"));
        var list = Array.from(lazyStoreContent);
        // $only allow to trigger this multiple time on the single handler
        list.forEach( function (l) {
          var payload = l[0];
          var ctx = l[1];
          var t = l[2];
          if (t && t !== type) {
            throw new Error(("You are trying to register an event already been taken by other type: " + t))
          }
          this$1.logger("($only)", ("call run for " + evt));
          this$1.run(callback, payload, context || ctx);
        });
      }
      return added;
    };

    /**
     * $only + $once this is because I found a very subtile bug when we pass a
     * resolver, rejecter - and it never fire because that's OLD added in v1.4.0
     * @param {string} evt event name
     * @param {function} callback to call later
     * @param {object} [context=null] exeucte context
     * @return {void}
     */
    EventService.prototype.$onlyOnce = function $onlyOnce (evt, callback, context) {
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);
      var type = 'onlyOnce';
      var added = false;
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      var nStore = this.normalStore;
      if (!nStore.has(evt)) {
        this.logger("($onlyOnce)", (evt + " add to store"));
        added = this.addToNormalStore(evt, type, callback, context);
      }
      if (lazyStoreContent !== false) {
        // there are data store in lazy store
        this.logger('($onlyOnce)', lazyStoreContent);
        var list = Array.from(lazyStoreContent);
        // should never have more than 1
        var ref = list[0];
        var payload = ref[0];
        var ctx = ref[1];
        var t = ref[2];
        if (t && t !== 'onlyOnce') {
          throw new Error(("You are trying to register an event already been taken by other type: " + t))
        }
        this.logger("($onlyOnce)", ("call run for " + evt));
        this.run(callback, payload, context || ctx);
        // remove this evt from store
        this.$off(evt);
      }
      return added;
    };

    /**
     * This is a shorthand of $off + $on added in V1.5.0
     * @param {string} evt event name
     * @param {function} callback to exeucte
     * @param {object} [context = null] or pass a string as type
     * @param {string} [type=on] what type of method to replace
     * @return {}
     */
    EventService.prototype.$replace = function $replace (evt, callback, context, type) {
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = 'on';

      if (this.validateType(type)) {
        this.$off(evt);
        var method = this['$' + type];
        this.logger("($replace)", evt, callback);
        return Reflect.apply(method, this, [evt, callback, context])
      }
      throw new Error((type + " is not supported!"))
    };

    /**
     * trigger the event
     * @param {string} evt name NOT allow array anymore!
     * @param {mixed} [payload = []] pass to fn
     * @param {object|string} [context = null] overwrite what stored
     * @param {string} [type=false] if pass this then we need to add type to store too
     * @return {number} if it has been execute how many times
     */
    EventService.prototype.$trigger = function $trigger (evt , payload , context, type) {
      if ( payload === void 0 ) payload = [];
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = false;

      this.validateEvt(evt);
      var found = 0;
      // first check the normal store
      var nStore = this.normalStore;
      this.logger('($trigger)', 'normalStore', nStore);
      if (nStore.has(evt)) {
        // @1.8.0 to add the suspend queue
        var added = this.$queue(evt, payload, context, type);
        this.logger('($trigger)', evt, 'found; add to queue: ', added);
        if (added === true) {
          this.logger('($trigger)', evt, 'not executed. Exit now.');
          return false; // not executed
        }
        var nSet = Array.from(nStore.get(evt));
        var ctn = nSet.length;
        var hasOnce = false;
        for (var i=0; i < ctn; ++i) {
          ++found;
          // this.logger('found', found)
          var ref = nSet[i];
          var _ = ref[0];
          var callback = ref[1];
          var ctx = ref[2];
          var type$1 = ref[3];
          this.logger("($trigger)", ("call run for " + evt));
          this.run(callback, payload, context || ctx);
          if (type$1 === 'once' || type$1 === 'onlyOnce') {
            hasOnce = true;
          }
        }
        if (hasOnce) {
          nStore.delete(evt);
        }
        return found;
      }
      // now this is not register yet
      this.addToLazyStore(evt, payload, context, type);
      return found;
    };

    /**
     * this is an alias to the $trigger
     * @NOTE breaking change in V1.6.0 we swap the parameter around
     * @param {string} evt event name
     * @param {*} params pass to the callback
     * @param {string} type of call
     * @param {object} context what context callback execute in
     * @return {*} from $trigger
     */
    EventService.prototype.$call = function $call (evt, params, type, context) {
      if ( type === void 0 ) type = false;
      if ( context === void 0 ) context = null;

      var args = [evt, params, context, type];
      return Reflect.apply(this.$trigger, this, args)
    };

    /**
     * remove the evt from all the stores
     * @param {string} evt name
     * @return {boolean} true actually delete something
     */
    EventService.prototype.$off = function $off (evt) {
      var this$1 = this;

      this.validateEvt(evt);
      var stores = [ this.lazyStore, this.normalStore ];
      var found = false;
      stores.forEach(function (store) {
        if (store.has(evt)) {
          found = true;
          this$1.logger('($off)', evt);
          store.delete(evt);
        }
      });
      return found;
    };

    /**
     * return all the listener from the event
     * @param {string} evtName event name
     * @param {boolean} [full=false] if true then return the entire content
     * @return {array|boolean} listerner(s) or false when not found
     */
    EventService.prototype.$get = function $get (evt, full) {
      if ( full === void 0 ) full = false;

      this.validateEvt(evt);
      var store = this.normalStore;
      if (store.has(evt)) {
        return Array
          .from(store.get(evt))
          .map( function (l) {
            if (full) {
              return l;
            }
            var key = l[0];
            var callback = l[1];
            return callback;
          })
      }
      return false;
    };

    /**
     * store the return result from the run
     * @param {*} value whatever return from callback
     */
    prototypeAccessors.$done.set = function (value) {
      this.logger('($done)', 'value: ', value);
      if (this.keep) {
        this.result.push(value);
      } else {
        this.result = value;
      }
    };

    /**
     * @TODO is there any real use with the keep prop?
     * getter for $done
     * @return {*} whatever last store result
     */
    prototypeAccessors.$done.get = function () {
      if (this.keep) {
        this.logger('(get $done)', this.result);
        return this.result[this.result.length - 1]
      }
      return this.result;
    };

    Object.defineProperties( EventService.prototype, prototypeAccessors );

    return EventService;
  }(NbEventServiceBase$1));

  // default

  // this will generate a event emitter and will be use everywhere
  // create a clone version so we know which one we actually is using
  var JsonqlWsEvt = /*@__PURE__*/(function (NBEventService) {
    function JsonqlWsEvt(logger) {
      if (typeof logger !== 'function') {
        throw new Error("Just die here the logger is not a function!")
      }
      // this ee will always come with the logger
      // because we should take the ee from the configuration
      NBEventService.call(this, { logger: logger });
    }

    if ( NBEventService ) JsonqlWsEvt.__proto__ = NBEventService;
    JsonqlWsEvt.prototype = Object.create( NBEventService && NBEventService.prototype );
    JsonqlWsEvt.prototype.constructor = JsonqlWsEvt;

    var prototypeAccessors = { name: { configurable: true } };

    prototypeAccessors.name.get = function () {
      return 'jsonql-ws-client-core'
    };

    Object.defineProperties( JsonqlWsEvt.prototype, prototypeAccessors );

    return JsonqlWsEvt;
  }(EventService$1));

  // mapping the resolver to their respective nsp

  /**
   * Just make sure the object contain what we are looking for
   * @param {object} opts configuration from checkOptions
   * @return {object} the target content
   */
  var getResolverList = function (contract) {
    var result = extractSocketPart(contract);
    if (result !== false) {
      return result
    }
    throw new JsonqlResolverNotFoundError(MISSING_PROP_ERR)
  };

  /**
   * process the contract first
   * @param {object} opts configuration
   * @return {object} sorted list
   */
  function processContract(opts) {
    var obj, obj$1;

    var contract = opts.contract;
    var enableAuth = opts.enableAuth;
    if (enableAuth) {
      return groupByNamespace(contract)
    }
    return ( obj$1 = {}, obj$1[NSP_SET] = ( obj = {}, obj[JSONQL_PATH] = getResolverList(contract), obj ), obj$1[PUBLIC_NAMESPACE] = JSONQL_PATH, obj$1 )
  }

  // group all the small functions here
  // we shouldn't do this anymore
  var fixWss = function (url, serverType) {
    // ws only allow ws:// path
    if (serverType === JS_WS_NAME) {
      return url.replace('http://', 'ws://')
    }
    return url;
  };

  /**
   * get a stock host name from browser
   */
  var getHostName$1 = function () {
    try {
      return [window.location.protocol, window.location.host].join('//')
    } catch(e) {
      throw new JsonqlValidationError(e)
    }
  };

  /**
   * Unbind the event
   * @param {object} ee EventEmitter
   * @param {string} namespace
   * @return {void}
   */
  var clearMainEmitEvt = function (ee, namespace) {
    var nsps = toArray$2(namespace);
    nsps.forEach(function (n) {
      ee.$off(createEvt(n, EMIT_REPLY_TYPE));
    });
  };

  // breaking it up further to share between methods

  /**
   * break out to use in different places to handle the return from server
   * @param {object} data from server
   * @param {function} resolver NOT from promise
   * @param {function} rejecter NOT from promise
   * @return {void} nothing
   */
  function respondHandler(data, resolver, rejecter) {
    if (isObjectHasKey$4(data, ERROR_KEY)) {
      // debugFn('-- rejecter called --', data[ERROR_KEY])
      rejecter(data[ERROR_KEY]);
    } else if (isObjectHasKey$4(data, DATA_KEY)) {
      // debugFn('-- resolver called --', data[DATA_KEY])
      resolver(data[DATA_KEY]);
    } else {
      // debugFn('-- UNKNOWN_RESULT --', data)
      rejecter({message: UNKNOWN_RESULT, error: data});
    }
  }

  // the actual trigger call method

  /**
   * just wrapper
   * @param {object} ee EventEmitter
   * @param {string} namespace where this belongs
   * @param {string} resolverName resolver
   * @param {boolean} useCallbackStyle use on or not
   * @param {array} args arguments
   * @return {void} nothing
   */
  function actionCall(ee, namespace, resolverName, useCallbackStyle, args) {
    if ( args === void 0 ) args = [];

    var eventName = createEvt(namespace, EMIT_REPLY_TYPE);
    var RESULT_SUBFIX = useCallbackStyle ? RESULT_PROP_NAME : ON_RESULT_PROP_NAME;
    // debugFn(`actionCall: ${eventName} --> ${resolverName}`, args)
    ee.$trigger(eventName, [resolverName, toArray$2(args)]);
    // once we trigger there is nothing return from the resolve
    // @TODO if we need the next then call to have the result back
    // then we need to listen to the event callback here as well
    return new Promise(function (resolver, rejecter) {
      ee.$on(
        createEvt(namespace, resolverName, RESULT_SUBFIX),
        function actionCallResultHandler(result) {
          // debugFn(`got the first result`, result)
          respondHandler(result, resolver, rejecter);
        }
      );
    })
  }

  // pairing with the server vesrion SEND_MSG_PROP_NAME
  // last of the chain so only return the resolver (fn)
  var setupSend = function (fn, ee, namespace, resolverName, params, useCallbackStyle) { return (
    objDefineProps(fn, SEND_MSG_PROP_NAME, function sendSetter(messagePayload) {
      var ERROR_SUBFIX = useCallbackStyle ? ERROR_PROP_NAME : ON_ERROR_PROP_NAME;
      // debugFn('got payload for', messagePayload)
      // @NOTE change from sync interface to async @ 1.0.0
      // this way we will able to catch all the error(s)
      validateAsync$1(toArray$2(messagePayload), params.params, true)
          .then(function (result) {
            // here is the different we don't throw error instead we trigger onError
            if (result[ERROR_KEY] && result[ERROR_KEY].length) {
              // debugFn(`got ERROR_KEY`, result[ERROR_KEY])
              ee.$call(
                createEvt(namespace, resolverName, ERROR_SUBFIX),
                [new JsonqlValidationError(resolverName, result[ERROR_KEY])]
              );
            } else {
              // return it just for the catch to work - if any
              return actionCall(ee, namespace, resolverName, useCallbackStyle, messagePayload)
            }
          })
          .catch(function (err) {
            // debugFn(`error after validateAsync`, err)
            ee.$call(
              createEvt(namespace, resolverName, ERROR_SUBFIX),
              [new JsonqlValidationError(resolverName, err)]
            );
          });
      }, function sendGetter() { // add in 1.1.4
        return function sendGetterAction() {
          var args = [], len = arguments.length;
          while ( len-- ) args[ len ] = arguments[ len ];

          return validateAsync$1(args, params.params, true)
            .then(function (_args) { return actionCall(ee, namespace, resolverName, useCallbackStyle, _args); })
            .catch(finalCatch)
        }
      })
  ); };

  // @TODO using the obj.on syntax to do the same thing

  /**
   * Add extra property to the resolver via the getter
   * @param {function} fn the resolver itself
   * @param {object} ee event emitter
   * @param {string} namespace the namespace this belongs to
   * @param {string} resolverName resolver namee
   * @param {object} params from the contract
   * @return {array} same as what goes in
   */
  function setupCallbackApi(fn, ee, namespace, resolverName, params) {
    return [
      injectToFn$1(fn, CB_FN_NAME, function(evtName, callback) {
        if (isString$1(evtName) && isFunc(callback)) {
          switch(evtName) {
            case RESULT_PROP_NAME:
              ee.$on(
                createEvt(namespace, resolverName, ON_RESULT_PROP_NAME),
                function resultHandler(result) {
                  respondHandler(result, callback, function (error) {
                    ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error);
                  });
                }
              );
              break;
            // register the handler for this message event
            case MESSAGE_PROP_NAME:
              ee.$only(
                createEvt(namespace, resolverName, ON_MESSAGE_PROP_NAME),
                function onMessageCallback(args) {
                  respondHandler(args, callback, function (error) {
                    ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error);
                  });
                }
              );
            break;
            case READY_PROP_NAME:
              ee.$only(
                createEvt(namespace, resolverName, ON_ERROR_PROP_NAME),
                callback
              );
            break;
            default:
              ee.$trigger(
                createEvt(namespace, resolverName, ON_ERROR_PROP_NAME),
                new JsonqlError(resolverName, ("Unknown event name " + evtName + "!"))
              );
          }
        }
      }),
      ee,
      namespace,
      resolverName,
      params
    ]
  }

  // break up the original setup resolver method here

  /**
   * The first one in the chain
   * @return {array}
   */
  var setupNamespace = function (fn, ee, namespace, resolverName, params, useCallbackStyle) { return [
    injectToFn$1(fn, MY_NAMESPACE, namespace),
    ee,
    namespace,
    resolverName,
    params,
    useCallbackStyle
  ]; };

  // onResult handler
  var setupOnResult = function (fn, ee, namespace, resolverName, params, useCallbackStyle) { return [
    objDefineProps(fn, ON_RESULT_PROP_NAME, function(resultCallback) {
      if (isFunc(resultCallback)) {
        ee.$on(
          createEvt(namespace, resolverName, ON_RESULT_PROP_NAME),
          function resultHandler(result) {
            respondHandler(result, resultCallback, function (error) {
              ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error);
            });
          }
        );
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    useCallbackStyle
  ]; };

  // we do need to add the send prop back because it's the only way to deal with
  // bi-directional data stream
  var setupOnMessage = function (fn, ee, namespace, resolverName, params, useCallbackStyle) { return [
    objDefineProps(fn, ON_MESSAGE_PROP_NAME, function(messageCallback) {
      // we expect this to be a function
      if (isFunc(messageCallback)) {
        // did that add to the callback
        var onMessageCallback = function (args) {
          respondHandler(args, messageCallback, function (error) {
            ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error);
          });
        };
        // register the handler for this message event
        ee.$only(createEvt(namespace, resolverName, ON_MESSAGE_PROP_NAME), onMessageCallback);
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    useCallbackStyle
  ]; };

  // add an ON_ERROR_PROP_NAME handler
  var setupOnError = function (fn, ee, namespace, resolverName, params, useCallbackStyle) { return [
    objDefineProps(fn, ON_ERROR_PROP_NAME, function(resolverErrorHandler) {
      if (isFunc(resolverErrorHandler)) {
        // please note ON_ERROR_PROP_NAME can add multiple listners
        ee.$only(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), resolverErrorHandler);
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    useCallbackStyle
  ]; };

  /**
   * Add extra property to the resolver
   * @param {string} namespace where this belongs
   * @param {string} resolverName name as event name
   * @param {object} params from contract
   * @param {function} fn resolver function
   * @param {object} ee EventEmitter
   * @param {boolean} useCallbackStyle new callback style
   * @return {function} resolver
   */
  function setupResolver(namespace, resolverName, params, fn, ee, useCallbackStyle) {
    // also need to setup a getter to get back the namespace of this resolver
    var args = [setupNamespace];
    useCallbackStyle ? args.push(setupCallbackApi) : args.push(setupOnResult, setupOnMessage, setupOnError);
    args.push(setupSend);
    // get the executor
    var executor = Reflect.apply(chainFns$1, null, args);

    return Reflect.apply(executor, null, [fn, ee, namespace, resolverName, params, useCallbackStyle])
  }

  // put all the resolver related methods here to make it more clear

  /**
   * create the actual function to send message to server
   * @param {object} ee EventEmitter instance
   * @param {string} namespace this resolver end point
   * @param {string} resolverName name of resolver as event name
   * @param {object} params from contract
   * @param {boolean} useCallbackStyle on style or not
   * @return {function} resolver
   */
  function createResolver(ee, namespace, resolverName, params, useCallbackStyle) {
    // note we pass the new withResult=true option
    return function() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return validateAsync$1(args, params.params, true)
        .then(function (_args) { return actionCall(ee, namespace, resolverName, useCallbackStyle, _args); })
        .catch(finalCatch)
    }
  }

  /**
   * The problem is the namespace can have more than one
   * and we only have on onError message
   * @param {object} obj the client itself
   * @param {object} ee Event Emitter
   * @param {object} nspSet namespace keys
   * @param {boolean} useCallbackStyle use cb or not
   * @return {object} obj with onError prop
   */
  function createNamespaceErrorHandler(obj, ee, nspSet) {
    // using the onError as name
    // @TODO we should follow the convention earlier
    // make this a setter for the obj itself
    return objDefineProps(obj, ON_ERROR_PROP_NAME, function namespaceErrorCallbackHandler(namespaceErrorHandler) {
      if (isFunc(namespaceErrorHandler)) {
        // please note ON_ERROR_PROP_NAME can add multiple listners
        for (var namespace in nspSet) {
          // this one is very tricky, we need to make sure the trigger is calling
          // with the namespace as well as the error
          ee.$on(createEvt(namespace, ON_ERROR_PROP_NAME), namespaceErrorHandler);
        }
      }
    })
  }

  /**
   * This event will fire when the socket.io.on('connection') and ws.onopen
   * @param {object} obj the client itself
   * @param {object} ee Event Emitter
   * @param {object} nspSet namespace keys
   * @return {object} obj with onReady prop
   */
  function createOnReadyhandler(obj, ee, nspSet) {
    return objDefineProps(obj, ON_READY_PROP_NAME, function onReadyCallbackHandler(onReadyCallback) {
      if (isFunc(onReadyCallback)) {
        // reduce it down to just one flat level
        ee.$on(ON_READY_PROP_NAME, onReadyCallback);
      }
    })
  }

  /**
   * This event will fire when the socket.io.on('connection') and ws.onopen
   * Plus this will check if it's the private namespace that fired the event
   * @param {object} obj the client itself
   * @param {object} ee Event Emitter
   * @param {object} opts configuration
   * @return {object} obj with onLogin prop
   */
  function createOnLoginhandler(obj, ee, opts) {
    if (opts.enableAuth) {
      return objDefineProps(obj, ON_LOGIN_PROP_NAME, function onLoginCallbackHandler(onLoginCallback) {
        if (isFunc(onLoginCallback)) {
          // only one callback can registered with it, TBC
          ee.$only(ON_LOGIN_PROP_NAME, onLoginCallback);
        }
      })
    }
    // just skip it
    return obj
  }

  /**
   * when useCallbackStyle=true use this instead of the above method
   * @param {object} obj the base object to attach to
   * @param {object} ee EventEmitter
   * @param {object} nspSet the map
   * @param {object} opts configuration
   * @return {object} obj
   */
  function createCallbackHandler(obj, ee, nspSet, opts) {
    return injectToFn$1(obj, CB_FN_NAME, function onHandler(evtName, callback) {
      if (isString$1(evtName) && isFunc(callback)) {
        switch (evtName) {
          case ERROR_PROP_NAME:
            for (var namespace in nspSet) {
              // this one is very tricky, we need to make sure the trigger is calling
              // with the namespace as well as the error
              ee.$on(createEvt(namespace, ERROR_PROP_NAME), callback);
            }
            break;
          case LOGIN_PROP_NAME: // @TODO should only available when enableAuth=true
            ee.$only(LOGIN_PROP_NAME, callback);
            break;
          case READY_PROP_NAME:
            ee.$on(READY_PROP_NAME, callback);
            break;
          default:
            ee.$trigger(ERROR_PROP_NAME, new JsonqlError(CB_FN_NAME, ("Unknown event name " + evtName + "!")));
        }
      }
      // @TODO need to issue another error here!
    })
  }

  // take out from the resolver-methods

  /**
   * @TODO this is now become unnecessary because the login is a slave to the
   * http-client - but keep this for now and see what we want to do with it later
   * break out from createAuthMethods to allow chaining call
   * @param {object} obj the main client object
   * @param {object} ee event emitter
   * @param {object} opts configuration
   * @return {array} modified input
   */
  var setupLoginHandler = function (obj, ee, opts) { return [
    injectToFn$1(obj, opts.loginHandlerName, function loginHandler(token) {
      if (token && isString$1(token)) {
        return ee.$trigger(LOGIN_EVENT_NAME, [token])
      }
      // should trigger a global error instead @TODO
      throw new JsonqlValidationError(opts.loginHandlerName, ("Unexpected token " + token))
    }),
    ee,
    opts
  ]; };

  /**
   * break out from createAuthMethods to allow chaining call - final in chain
   * @param {object} obj the main client object
   * @param {object} ee event emitter
   * @param {object} opts configuration
   * @return {object} the modified obj
   */
  var setupLogoutHandler = function (obj, ee, opts) { return (
    injectToFn$1(obj, opts.logoutHandlerName, function logoutHandler() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      ee.$trigger(LOGOUT_EVENT_NAME, args);
    })
  ); };

  /**
   * Create auth related methods
   * @param {object} obj the client itself
   * @param {object} ee Event Emitter
   * @param {object} opts configuration
   * @return {object} obj with auth methods if any
   */
  function createAuthMethods$1(obj, ee, opts) {
    if (opts.enableAuth) {
      return chainFns$1(setupLoginHandler, setupLogoutHandler)(obj, ee, opts)
    }
    return obj;
  }

  // resolvers generator

  /**
   * step one get the obj map with the namespace
   * @param {object} opts configuration
   * @param {object} nspMap resolvers index by their namespace
   * @param {object} ee EventEmitter
   * @return {promise} resolve the obj mapped, and start the chain
   */
  function getMappedObj(opts, nspMap, ee) {
    var obj = {};
    // let resolverNames = [];
    var nspSet = nspMap.nspSet;
    var useCallbackStyle = opts.useCallbackStyle; // @1.2.1
    for (var namespace in nspSet) {
      var list = nspSet[namespace];
      for (var resolverName in list) {
        // resolverNames.push(resolverName)
        var params = list[resolverName];
        var fn = createResolver(ee, namespace, resolverName, params, useCallbackStyle);
        // this should set as a getter therefore can not be overwrite by accident
        // obj[resolverName] = setupResolver(namespace, resolverName, params, fn, ee)
        obj = injectToFn$1(obj, resolverName, setupResolver(namespace, resolverName, params, fn, ee, useCallbackStyle));
      }
    }
    // move this two method down to helpers
    // we want these methods visible in debug or console.log
    obj.devHelpers = {
      // this is a helper method for the developer to know the namespace inside
      getNsp: function () { return Object.keys(nspSet); },
      // simple get version trick
      getVer: function () { return opts.version || 'NOT SET'; }
      // not really necessary because the dev can query the contract
      // getResolverNames: () => resolverNames
    };
    // resolve the obj to start the chain
    // chain the result to allow the chain processing
    return Promise.resolve(obj)
  }

  /**
   * prepare the methods
   * @param {object} opts configuration
   * @param {object} nspMap resolvers index by their namespace
   * @param {object} ee EventEmitter
   * @return {object} of resolvers
   * @public
   */
  function generator$1(opts, nspMap, ee) {
    var nspSet = nspMap.nspSet;
    var useCallbackStyle = opts.useCallbackStyle;
    var enableAuth = opts.enableAuth;
    var args = [getMappedObj];
    if (useCallbackStyle) {
      args.push(function (obj) { return createCallbackHandler(obj, ee, nspSet); });
    } else {
      args.push(
        function (obj1) { return createNamespaceErrorHandler(obj1, ee, nspSet); },
        function (obj2) { return createOnReadyhandler(obj2, ee); }
      );
      if (enableAuth) {
        args.push(function (obj3) { return createOnLoginhandler(obj3, ee, opts); });
      }
    }
    // this only apply to when enableAuth = true
    if (enableAuth) {
      args.push(function (obj4) { return createAuthMethods$1(obj4, ee, opts); });
    }
    // run it
    var executor = Reflect.apply(chainProcessPromises, null, args);
    return executor(opts, nspMap, ee)
  }

  var obj$9, obj$1$1;
  var AVAILABLE_METHODS = [IO_ROUNDTRIP_LOGIN, IO_HANDSHAKE_LOGIN];

  var defaultOptions = {
    useCallbackStyle: createConfig$1(false, [BOOLEAN_TYPE]),
    loginHandlerName: createConfig$1(ISSUER_NAME, [STRING_TYPE]),
    logoutHandlerName: createConfig$1(LOGOUT_NAME, [STRING_TYPE]),
    // this is for socket.io
    loginMethod: createConfig$1(IO_HANDSHAKE_LOGIN, [STRING_TYPE], ( obj$9 = {}, obj$9[ENUM_KEY] = AVAILABLE_METHODS, obj$9 )),
    // we will use this for determine the socket.io client type as well - @TODO remove or rename 
    useJwt: createConfig$1(true, [BOOLEAN_TYPE, STRING_TYPE]),
    hostname: createConfig$1(false, [STRING_TYPE]),
    namespace: createConfig$1(JSONQL_PATH, [STRING_TYPE]),
    wsOptions: createConfig$1({}, [OBJECT_TYPE]),
    // make this null as default don't set this here, only set in the down stream
    // serverType: createConfig(null, [STRING_TYPE], {[ENUM_KEY]: AVAILABLE_SERVERS}),
    // we require the contract already generated and pass here
    contract: createConfig$1({}, [OBJECT_TYPE], ( obj$1$1 = {}, obj$1$1[CHECKER_KEY] = isContract$1, obj$1$1 )),
    enableAuth: createConfig$1(false, [BOOLEAN_TYPE]),
    token: createConfig$1(false, [STRING_TYPE])
  };

  // create options

  // constant props
  var defaultConstProps = {
    eventEmitter: null,
    // we unify the two different client into one now
    // only expect different parameter
    nspClient: null,
    nspAuthClient: null,
    // contructed path
    wssPath: ''
  };

  /**
   * wrapper method to check this already did the pre check
   * @param {object} config user supply config
   * @param {object} cProps user supply const props
   * @return {promise} resolve to the checked opitons
   */
  function checkOptionsWrapper(config, cProps) {
    var constProps = Object.assign(defaultConstProps, cProps);
    var fn = postConfigCheck(defaultOptions, constProps, checkConfig);
    return fn(config)
  }

  /**
   * check the configuration
   * @param {object} config user supplied configuration
   * @param {object} constProps developer supplied configuration
   * @return {object} after checked
   */
  function checkOptions(config, constProps) {
    return checkOptionsWrapper(config, constProps)
      .then(function (opts) {
        if (!opts.hostname) {
          opts.hostname = getHostName$1();
        }
        // @TODO the contract now will supply the namespace information
        // and we need to use that to group the namespace call
        opts.wssPath = fixWss([opts.hostname, opts.namespace].join('/'), opts.serverType);
        // debug('CONFIGURATION OPTIONS', opts)
        return opts
      })
  }

  // for export
  var jsonqlWsDefaultOptions = defaultOptions;
  var jsonqlWsDefaultConstProps = defaultConstProps;

  // the top level API

  /**
   * @TODO remove this implmenetation once the test is done
   */
  var dummyLogger = function () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Reflect.apply(console.info, console, ['[jsonql-ws-client-core]' ].concat( args));
  };

  /**
   * getting the event emitter
   * @param {object} opts configuration
   * @return {object} the event emitter instance
   */
  var getEventEmitter$1 = function (opts) {
    var log = opts.log;
    var eventEmitter = opts.eventEmitter;
    if (eventEmitter) {
      log("eventEmitter is:", eventEmitter.name);
      return eventEmitter
    }
    log("Create a new Event Emitter");
    return new JsonqlWsEvt( opts.log )
  };

  /**
   * Make sure there is a log method
   * @param {object} opts configuration
   * @return {object} opts
   */
  var getLogFn = function (opts) {
    var log = opts.log; // 1.3.9 if we pass a log method here then we use this
    if (!log || typeof log !== 'function') {
      opts.log = dummyLogger;
    }
    opts.log('--- wsClientCore config opts ---', opts);
    return opts
  };

  /**
   * The main interface which will generate the socket clients and map all events
   * @param {object} socketClientResolver this is the one method export by various clients
   * @param {object} [constProps={}] add this to supply the constProps from the downstream client
   * @return {object} the wsClient instance with all the available API
   */
  function wsClientCore(socketClientResolver, constProps) {
    if ( constProps === void 0 ) constProps = {};

    // we need to inject property to this client later
    // therefore we need to do it this way
    return function (config) { return checkOptions(config, constProps)
        .then(getLogFn)
        .then(function (opts) { return ({
          opts: opts,
          nspMap: processContract(opts),
          ee: getEventEmitter$1(opts)
        }); })
        .then(
          function (ref) {
            var opts = ref.opts;
            var nspMap = ref.nspMap;
            var ee = ref.ee;

            return socketClientResolver(opts, nspMap, ee);
      }
        )
        .then(
          function (ref) {
            var opts = ref.opts;
            var nspMap = ref.nspMap;
            var ee = ref.ee;

            return generator$1(opts, nspMap, ee);
      }
        )
        .catch(function (err) {
          console.error("jsonql-ws-client init error", err);
        }); }
  }

  // this is all the isormophic-ws is
  var ws = null;

  if (typeof WebSocket !== 'undefined') {
    ws = WebSocket;
  } else if (typeof MozWebSocket !== 'undefined') {
    ws = MozWebSocket;
  } else if (typeof global$1 !== 'undefined') {
    ws = global$1.WebSocket || global$1.MozWebSocket;
  } else if (typeof window !== 'undefined') {
    ws = window.WebSocket || window.MozWebSocket;
  } else if (typeof self !== 'undefined') {
    ws = self.WebSocket || self.MozWebSocket;
  }

  var WebSocket$1 = ws;

  // pass the different type of ws to generate the client
  /**
   * WebSocket is strict about the path, therefore we need to make sure before it goes in
   * @param {string} url input url
   * @return {string} url with correct path name
   */
  var fixWss$1 = function (url) {
    var uri = url.toLowerCase();
    if (uri.indexOf('http') > -1) {
      if (uri.indexOf('https') > -1) {
        return uri.replace('https', 'wss')
      }
      return uri.replace('http', 'ws')
    }
    return uri
  };

  /**
   * The bug was in the wsOptions where ws doesn't need it but socket.io do
   * therefore the object was pass as second parameter!
   * @param {object} WebSocket the client or node version of ws
   * @param {boolean} auth if it's auth then 3 param or just one
   */
  function createWsClient(WebSocket, auth) {
    if ( auth === void 0 ) auth = false;

    if (auth === false) {
      return function createWsClientHandler(url) {
        return new WebSocket(fixWss$1(url))
      }
    }

    /**
     * Create a client with auth token
     * @param {string} url start with ws:// @TODO check this?
     * @param {string} token the jwt token
     * @return {object} ws instance
     */
    return function createWsAuthClientHandler(url, token) {
      var ws_url = fixWss$1(url);
      // console.log('what happen here?', url, ws_url, token)
      var uri = token && typeof token === 'string' ? (ws_url + "?" + TOKEN_PARAM_NAME + "=" + token) : ws_url;
      try {
        return new WebSocket(uri)
      } catch(e) {
        console.error('WebSocket Connection Error', e);
        return false
      }
    }
  }

  // bunch of generic helpers

  /**
   * DIY in Array
   * @param {array} arr to check from
   * @param {*} value to check against
   * @return {boolean} true on found
   */
  var inArray$4 = function (arr, value) { return !!arr.filter(function (a) { return a === value; }).length; };

  // quick and dirty to turn non array to array
  var toArray$3 = function (arg) { return isArray(arg) ? arg : [arg]; };

  /**
   * @param {object} obj for search
   * @param {string} key target
   * @return {boolean} true on success
   */
  var isObjectHasKey$5 = function(obj, key) {
    try {
      var keys = Object.keys(obj);
      return inArray$4(keys, key)
    } catch(e) {
      // @BUG when the obj is not an OBJECT we got some weird output
      return false;
      /*
      console.info('obj', obj)
      console.error(e)
      throw new Error(e)
      */
    }
  };

  /**
   * create a event name
   * @param {string[]} args
   * @return {string} event name for use
   */
  var createEvt$1 = function () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    return args.join('_');
  };

  /**
   * using just the map reduce to chain multiple functions together
   * @param {function} mainFn the init function
   * @param {array} moreFns as many as you want to take the last value and return a new one
   * @return {function} accept value for the mainFn
   */
  var chainFns$2 = function (mainFn) {
    var moreFns = [], len = arguments.length - 1;
    while ( len-- > 0 ) moreFns[ len ] = arguments[ len + 1 ];

    return (
    function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return (
      moreFns.reduce(function (value, nextFn) { return (
        // change here to check if the return value is array then we spread it 
        Reflect.apply(nextFn, null, toArray$3(value))
      ); }, Reflect.apply(mainFn, null, args))
    );
    }
  );
  };

  /**
   * check if the object has name property
   * @param {object} obj the object to check
   * @param {string} name the prop name
   * @return {*} the value or undefined
   */
  function objHasProp$3(obj, name) {
    var prop = Object.getOwnPropertyDescriptor(obj, name);
    return prop !== undefined && prop.value ? prop.value : prop;
  }

  /**
   * After the user login we will use this Object.define add a new property
   * to the resolver with the decoded user data
   * @param {function} resolver target resolver
   * @param {string} name the name of the object to get inject also for checking
   * @param {object} data to inject into the function static interface
   * @param {boolean} [overwrite=false] if we want to overwrite the existing data
   * @return {function} added property resolver
   */
  function injectToFn$2(resolver, name, data, overwrite) {
    if ( overwrite === void 0 ) overwrite = false;

    var check = objHasProp$3(resolver, name);
    if (overwrite === false && check !== undefined) {
      // console.info(`NOT INJECTED`)
      return resolver;
    }
    /* this will throw error!
    if (overwrite === true && check !== undefined) {
      delete resolver[name] // delete this property
    }
    */
    // console.info(`INJECTED`)
    Object.defineProperty(resolver, name, {
      value: data,
      writable: overwrite // if its set to true then we should able to overwrite it
    });

    return resolver;
  }

  // split the contract into the node side and the generic side

  /**
   * @NOTE ported from jsonql-ws-client
   * Got to make sure the connection order otherwise
   * it will hang
   * @param {object} nspSet contract
   * @param {string} publicNamespace like the name said
   * @return {array} namespaces in order
   */
  function getNamespaceInOrder(nspSet, publicNamespace) {
    var names = []; // need to make sure the order!
    for (var namespace in nspSet) {
      if (namespace === publicNamespace) {
        names[1] = namespace;
      } else {
        names[0] = namespace;
      }
    }
    return names;
  }

  /**
   * @param {boolean} sec return in second or not
   * @return {number} timestamp
   */
  var timestamp$3 = function (sec) {
    if ( sec === void 0 ) sec = false;

    var time = Date.now();
    return sec ? Math.floor( time / 1000 ) : time;
  };

  // ported from jsonql-params-validator

  /**
   * @param {*} args arguments to send
   *@return {object} formatted payload
   */
  var formatPayload$1 = function (args) {
    var obj;

    return (
    ( obj = {}, obj[QUERY_ARG_NAME] = args, obj )
  );
  };

  /**
   * Get name from the payload (ported back from jsonql-koa)
   * @param {*} payload to extract from
   * @return {string} name
   */
  function getNameFromPayload$1(payload) {
    return Object.keys(payload)[0]
  }

  /**
   * wrapper method to add the timestamp as well
   * @param {string} resolverName
   * @param {*} payload
   * @return {object} delierable
   */
  function createDeliverable$1(resolverName, payload) {
    var obj;

    return ( obj = {}, obj[resolverName] = payload, obj[TIMESTAMP_PARAM_NAME] = [ timestamp$3() ], obj )
  }

  /**
   * @param {string} resolverName name of function
   * @param {array} [args=[]] from the ...args
   * @param {boolean} [jsonp = false] add v1.3.0 to koa
   * @return {object} formatted argument
   */
  function createQuery$1(resolverName, args, jsonp) {
    if ( args === void 0 ) args = [];
    if ( jsonp === void 0 ) jsonp = false;

    if (isString(resolverName) && isArray(args)) {
      var payload = formatPayload$1(args);
      if (jsonp === true) {
        return payload;
      }
      return createDeliverable$1(resolverName, payload)
    }
    throw new JsonqlValidationError("[createQuery] expect resolverName to be string and args to be array!", { resolverName: resolverName, args: args })
  }

  /**
   * string version of the createQuery
   * @return {string}
   */
  function createQueryStr(resolverName, args, jsonp) {
    if ( args === void 0 ) args = [];
    if ( jsonp === void 0 ) jsonp = false;

    return JSON.stringify(createQuery$1(resolverName, args, jsonp))
  }

  // this will replace the preConfigCheck in jsonql-koa

  /**
   * the rest of the argument will be functions that
   * need to add to the process chain,
   * finally return a function to accept the config
   * @param {object} defaultOptions prepared before hand
   * @param {object} constProps prepare before hand
   * @param {array} fns arguments see description
   * @return {function} to perform the final configuration check
   */
  function preConfigCheck$2(defaultOptions1, constProps1) {
    var fns = [], len = arguments.length - 2;
    while ( len-- > 0 ) fns[ len ] = arguments[ len + 2 ];

    // should have just add the method to the last
    var finalFn = function (opt) { return injectToFn$2(opt, CHECKED_KEY, timestamp$3()); };
    // if there is more than one then chain it otherwise just return the zero idx one
    var fn = Reflect.apply(chainFns$2, null, fns.concat([finalFn]));
    // 0.8.8 add a default property empty object
    return function preConfigCheckAction(config) {
      if ( config === void 0 ) config = {};

      return fn(config, defaultOptions1, constProps1)
    }
  }

  /**
   * Make sure everything is in the same page
   * @param {object} defaultOptions configuration option
   * @param {object} constProps add later
   * @param {array} next a list of functions to call if it's not
   * @return {function} resolve the configuration combined
   */
  function postConfigCheck$1(defaultOptions2, constProps2) {
    var next = [], len = arguments.length - 2;
    while ( len-- > 0 ) next[ len ] = arguments[ len + 2 ];

    return function postConfigCheckAction(config) {
      if ( config === void 0 ) config = {};

      if (objHasProp$3(config, CHECKED_KEY)) {
        return Promise.resolve(merge({}, config, constProps2))
      }
      var fn = Reflect.apply(preConfigCheck$2, null, [defaultOptions2, constProps2 ].concat( next));
      return Promise.resolve(fn(config))
    }
  }

  // since both the ws and io version are
  // pre-defined in the client-generator
  // and this one will have the same parameters
  // and the callback is identical

  /**
   * wrapper method to create a nsp without login
   * @param {string|boolean} namespace namespace url could be false
   * @param {object} opts configuration
   * @return {object} ws client instance
   */
  function createNspClient(namespace, opts) {
    var hostname = opts.hostname;
    var wssPath = opts.wssPath;
    var wsOptions = opts.wsOptions;
    var nspClient = opts.nspClient;
    var url = namespace ? [hostname, namespace].join('/') : wssPath;
    return nspClient(url, wsOptions)
  }

  /**
   * wrapper method to create a nsp with token auth
   * @param {string} namespace namespace url
   * @param {object} opts configuration
   * @return {object} ws client instance
   */
  function createNspAuthClient(namespace, opts) {
    var hostname = opts.hostname;
    var wssPath = opts.wssPath;
    var token = opts.token;
    var wsOptions = opts.wsOptions;
    var nspAuthClient = opts.nspAuthClient;
    var url = namespace ? [hostname, namespace].join('/') : wssPath;
    if (token && typeof token !== 'string') {
      throw new Error(("Expect token to be string, but got " + token))
    }
    return nspAuthClient(url, token, wsOptions)
  }

  // this use by client-event-handler

  /**
   * trigger errors on all the namespace onError handler
   * @param {object} ee Event Emitter
   * @param {array} namespaces nsps string
   * @param {string} message optional
   * @param {object} opts configuration
   * @return {void}
   */
  function triggerNamespacesOnError(ee, namespaces, message, opts) {
    if ( opts === void 0 ) opts = {};

    var useCallbackStyle = opts.useCallbackStyle;
    var ERROR_SUBFIX = useCallbackStyle ? ERROR_PROP_NAME : ON_ERROR_PROP_NAME;
    namespaces.forEach( function (namespace) {
      ee.$call(createEvt(namespace, ERROR_SUBFIX), [{ message: message, namespace: namespace }]);
    });
  }

  // This is share between different clients so we export it

  /**
   * A fake ee handler
   * @param {string} namespace nsp
   * @param {object} ee EventEmitter
   * @param {object} opts configuration
   * @return {void}
   */
  var notLoginWsHandler = function (namespace, ee, opts) {
    var useCallbackStyle = opts.useCallbackStyle;
    var log = opts.log;
    var ERROR_SUBFIX = useCallbackStyle ? ERROR_PROP_NAME : ON_ERROR_PROP_NAME;
    var RESULT_SUBFIX = useCallbackStyle ? RESULT_PROP_NAME : ON_RESULT_PROP_NAME;
    ee.$only(
      createEvt(namespace, EMIT_EVT),
      function notLoginHandlerCallback(resolverName, args) {

        log('[notLoginHandler] hijack the ws call', namespace, resolverName, args);

        var error = {
          message: NOT_LOGIN_ERR_MSG
        };
        // It should just throw error here and should not call the result
        // because that's channel for handling normal event not the fake one
        ee.$call(createEvt(namespace, resolverName, ERROR_SUBFIX), [error]);
        // also trigger the result handler, but wrap inside the error key
        ee.$call(createEvt(namespace, resolverName, RESULT_SUBFIX), [{ error: error }]);
      }
    );
  };

  /**
   * get the private namespace
   * @param {array} namespaces array
   * @return {*} string on success
   */
  var getPrivateNamespace = function (namespaces) { return (
    namespaces.length > 1 ? namespaces[0] : false
  ); };

  /**
   * centralize all the comm in one place
   * @param {object} opts configuration
   * @param {array} namespaces namespace(s)
   * @param {object} ee Event Emitter instance
   * @param {function} bindWsHandler binding the ee to ws --> this is the core bit
   * @param {array} namespaces array of namespace available
   * @param {object} nsps namespaced nsp
   * @return {void} nothing
   */
  function clientEventHandler(opts, nspMap, ee, bindWsHandler, namespaces, nsps) {
    // @1.1.3 add isPrivate prop to id which namespace is the private nsp
    // then we can use this prop to determine if we need to fire the ON_LOGIN_PROP_NAME event
    var privateNamespace = getPrivateNamespace(namespaces);
    var isPrivate = false;
    var log = opts.log;
    // loop
    // @BUG for io this has to be in order the one with auth need to get call first
    // The order of login is very import we need to run a waterfall here to make sure
    // one is execute then the other
    namespaces.forEach(function (namespace) {
      isPrivate = privateNamespace === namespace;
      if (nsps[namespace]) {
        log('[call bindWsHandler]', isPrivate, namespace);
        var args = [namespace, nsps[namespace], ee, isPrivate, opts];
        if (opts.serverType === SOCKET_IO) {
          var nspSet = nspMap.nspSet;
          args.push(nspSet[namespace]);
        }
        Reflect.apply(bindWsHandler, null, args);
      } else {
        // a dummy placeholder
        notLoginWsHandler(namespace, ee, opts);
      }
    });
    // this will be available regardless enableAuth
    // because the server can log the client out
    ee.$on(LOGOUT_EVENT_NAME, function logoutEvtHandler() {
      log('LOGOUT_EVENT_NAME');
      // disconnect(nsps, opts.serverType)
      // we need to issue error to all the namespace onError handler
      triggerNamespacesOnError(ee, namespaces, LOGOUT_EVENT_NAME);
      // rebind all of the handler to the fake one
      namespaces.forEach( function (namespace) {
        clearMainEmitEvt(ee, namespace);
        // clear out the nsp
        nsps[namespace] = false;
        // add a NOT LOGIN error if call
        notLoginWsHandler(namespace, ee, opts);
      });
    });
  }

  // take the ws reply data for use

  var keys$1 = [ WS_REPLY_TYPE, WS_EVT_NAME, WS_DATA_NAME ];

  /**
   * @param {object} payload should be string when reply but could be transformed
   * @return {boolean} true is OK
   */
  var isWsReply = function (payload) {
    var data = payload.data;
    if (data) {
      var result = keys$1.filter(function (key) { return isObjectHasKey$5(data, key); });
      return (result.length === keys$1.length) ? data : false
    }
    return false
  };

  /**
   * @param {object} payload This is the entire ws Event Object
   * @return {object} false on failed
   */
  var extractWsPayload = function (payload) {
    var data = payload.data;
    var json = isString$1(data) ? JSON.parse(data) : data;
    var fdata;
    if ((fdata = isWsReply(json)) !== false) {
      return {
        resolverName: fdata[WS_EVT_NAME],
        data: fdata[WS_DATA_NAME],
        type: fdata[WS_REPLY_TYPE]
      }
    }
    throw new JsonqlError('payload can not be decoded', payload)
  };

  // the WebSocket main handler


  /**
   * under extremely circumstances we might not even have a resolverName, then
   * we issue a global error for the developer to catch it
   * @param {object} ee event emitter
   * @param {string} namespace nsp
   * @param {string} resolverName resolver
   * @param {object} json decoded payload or error object
   * @param {string} ERROR_EVT_NAME the error event name
   * @return {undefined} nothing return
   */
  var errorTypeHandler = function (ee, namespace, resolverName, json, ERROR_EVT_NAME) {
    var evt = [namespace];
    if (resolverName) {
      evt.push(resolverName);
    }
    evt.push(ERROR_EVT_NAME);
    var evtName = Reflect.apply(createEvt$1, null, evt);
    // test if there is a data field
    var payload = json.data || json;
    ee.$trigger(evtName, [payload]);
  };

  /**
   * Binding the even to socket normally
   * @param {string} namespace
   * @param {object} ws the nsp
   * @param {object} ee EventEmitter
   * @param {boolean} isPrivate to id if this namespace is private or not
   * @param {object} opts configuration
   * @return {object} promise resolve after the onopen event
   */
  function wsMainHandler(namespace, ws, ee, isPrivate, opts) {
    var useCallbackStyle = opts.useCallbackStyle;
    var log = opts.log;

    log("wsMainHandler log test");

    var READY_EVT_NAME = useCallbackStyle ? READY_PROP_NAME : ON_READY_PROP_NAME;
    var LOGIN_EVT_NAME = useCallbackStyle ? LOGIN_PROP_NAME : ON_LOGIN_PROP_NAME;
    var MESSAGE_EVT_NAME = useCallbackStyle ? MESSAGE_PROP_NAME : ON_MESSAGE_PROP_NAME;
    var RESULT_EVT_NAME = useCallbackStyle ? RESULT_PROP_NAME : ON_RESULT_PROP_NAME;
    var ERROR_EVT_NAME = useCallbackStyle ? ERROR_PROP_NAME : ON_ERROR_PROP_NAME;
    // connection open
    ws.onopen = function onOpenCallback() {
      log('ws.onopen listened');
      // we just call the onReady
      ee.$call(READY_EVT_NAME, namespace);
      // need an extra parameter here to id the private nsp
      if (isPrivate) {
        log(("isPrivate and fire the " + LOGIN_EVT_NAME));
        ee.$call(LOGIN_EVT_NAME, namespace);
      }
      // add listener only after the open is called
      ee.$only(
        createEvt$1(namespace, EMIT_REPLY_TYPE),
        function wsMainOnEvtHandler(resolverName, args) {
          log('calling server', resolverName, args);
          ws.send(
            createQueryStr(resolverName, args)
          );
        }
      );
    };

    // reply
    // If we change it to the event callback style
    // then the payload will just be the payload and fucks up the extractWsPayload call @TODO
    ws.onmessage = function onMessageCallback(payload) {
      // console.log(`on.message`, typeof payload, payload)
      try {
        var json = extractWsPayload(payload);
        var resolverName = json.resolverName;
        var type = json.type;
        log('Hear from server', type, json);
        switch (type) {
          case EMIT_REPLY_TYPE:
            var e1 = createEvt$1(namespace, resolverName, MESSAGE_EVT_NAME);
            var r = ee.$trigger(e1, [json]);
            log("EMIT_REPLY_TYPE", e1, r);
            break;
          case ACKNOWLEDGE_REPLY_TYPE:
            var e2 = createEvt$1(namespace, resolverName, RESULT_EVT_NAME);
            var x = ee.$trigger(e2, [json]);
            // log(`ACKNOWLEDGE_REPLY_TYPE`, e2, json)
            break;
          case ERROR_TYPE:
            // this is handled error and we won't throw it
            // we need to extract the error from json
            log("ERROR_TYPE");
            errorTypeHandler(ee, namespace, resolverName, json, ERROR_EVT_NAME);
            break;
          // @TODO there should be an error type instead of roll into the other two types? TBC
          default:
            // if this happen then we should throw it and halt the operation all together
            log('Unhandled event!', json);
            errorTypeHandler(ee, namespace, resolverName, json, ERROR_EVT_NAME);
            // let error = {error: {'message': 'Unhandled event!', type}};
            // ee.$trigger(createEvt(namespace, resolverName, ON_RESULT_PROP_NAME), [error])
        }
      } catch(e) {
        console.error("ws.onmessage error", e);
        errorTypeHandler(ee, namespace, false, e, ERROR_EVT_NAME);
      }
    };
    // when the server close the connection
    ws.onclose = function onCloseCallback() {
      log('ws.onclose callback');
      // @TODO what to do with this
      // ee.$trigger(LOGOUT_EVENT_NAME, [namespace])
    };
    // listen to the LOGOUT_EVENT_NAME
    ee.$on(LOGOUT_EVENT_NAME, function closeEvtHandler() {
      try {
        log('terminate ws connection');
        ws.terminate();
      } catch(e) {
        console.error('ws.terminate error', e);
      }
    });
  }

  // actually binding the event client to the socket client

  /**
   * Because the nsps can be throw away so it doesn't matter the scope
   * this will get reuse again
   * @param {object} opts configuration
   * @param {object} nspMap from contract
   * @param {string|null} token whether we have the token at run time
   * @return {object} nsps namespace with namespace as key
   */
  var createNsps = function(opts, nspMap, token) {
    var nspSet = nspMap.nspSet;
    var publicNamespace = nspMap.publicNamespace;
    var login = false;
    var namespaces = [];
    var nsps = {};
    // first we need to binding all the events handler
    if (opts.enableAuth) { // && opts.useJwt
      login = true; // just saying we need to listen to login event
      namespaces = getNamespaceInOrder(nspSet, publicNamespace);
      nsps = namespaces.map(function (namespace, i) {
        var obj, obj$1, obj$2;

        if (i === 0) {
          if (token) {
            opts.token = token;
            // console.log('create createNspAuthClient at run time', opts)
            return ( obj = {}, obj[namespace] = createNspAuthClient(namespace, opts), obj )
          }
          return ( obj$1 = {}, obj$1[namespace] = false, obj$1 )
        }
        return ( obj$2 = {}, obj$2[namespace] = createNspClient(namespace, opts), obj$2 )
      }).reduce(function (first, next) { return Object.assign(first, next); }, {});
    } else {
      var namespace = getNameFromPayload$1(nspSet);
      namespaces.push(namespace);
      // standard without login
      // the stock version should not have a namespace
      nsps[namespace] = createNspClient(false, opts);
    }
    // return
    return { nsps: nsps, namespaces: namespaces, login: login }
  };

  /**
   * create a ws client
   * @param {object} opts configuration
   * @param {object} nspMap namespace with resolvers
   * @param {object} ee EventEmitter to pass through
   * @return {object} what comes in what goes out
   */
  function createClient(opts, nspMap, ee) {
    // arguments that don't change
    var args = [opts, nspMap, ee, wsMainHandler];
    // now create the nsps
    var token = opts.token;
    var log = opts.log;
    var ref = createNsps(opts, nspMap, token);
    var nsps = ref.nsps;
    var namespaces = ref.namespaces;
    var login = ref.login;
    // binding the listeners - and it will listen to LOGOUT event
    // to unbind itself, and the above call will bind it again
    Reflect.apply(clientEventHandler, null, args.concat([namespaces, nsps]));
    // setup listener
    if (login) {
      ee.$only(LOGIN_EVENT_NAME, function loginEventHandler(tokenLater) {

        log('createClient LOGIN_EVENT_NAME');

        // debugFn(`LOGIN_EVENT_NAME called with token:`, tokenLater)
        // @BUG this keep causing an "Disconnect call failed TypeError: Cannot read property 'readyState' of null"
        // I think that is because it's not login then it can not be disconnect
        // how do we track this state globally
        // disconnect(nsps, JS_WS_NAME)

        // @TODO should we trigger error on this one?
        // triggerNamespacesOnError(ee, namespaces, LOGIN_EVENT_NAME)
        clearMainEmitEvt(ee, namespaces);
        // console.log('LOGIN_EVENT_NAME', token)
        var newNsps = createNsps(opts, nspMap, tokenLater);
        // rebind it
        Reflect.apply(
          clientEventHandler,
          null,
          args.concat([newNsps.namespaces, newNsps.nsps])
        );
      });
    }
    // return what input
    return { opts: opts, nspMap: nspMap, ee: ee }
  }

  // share method to create the wsClientResolver

  /**
   * combine the create client resolver
   * @param {object} ws the different WebSocket module
   * @return {function} the wsClientResolver
   */
  function createClientResolver(ws) {
    var client = createWsClient(ws);
    var authClient = createWsClient(ws, true);
    /**
     * wsClientResolver
     * @param {object} opts configuration
     * @param {object} nspMap from the contract
     * @param {object} ee instance of the eventEmitter
     * @return {object} passing the same 3 input out with additional in the opts
     */
    return function(opts, nspMap, ee) {
      opts.nspClient = client;
      opts.nspAuthClient = authClient;
      // @1.0.7 remove later once everything fixed 
      var log = opts.log;
      if (log && typeof log === 'function') {
        log('@jsonql/ws ee', ee.name);
        log('@jsonql/ws createClientResolver', opts);
      }
      // console.log(`contract`, opts.contract)
      return createClient(opts, nspMap, ee)
    }
  }

  // this will be the news style interface that will pass to the jsonql-ws-client

  /**
   * @param {object} opts configuration
   * @param {object} nspMap from the contract
   * @param {object} ee instance of the eventEmitter
   * @return {object} passing the same 3 input out with additional in the opts
   */
  var wsClientResolver = createClientResolver(WebSocket$1);

  // where all the base options are
  // constant props
  var wsClientConstProps = {
    version: '__PLACEHOLDER__', // will get replace
    serverType: JS_WS_NAME
  };

  /**
   * The bug is the interface because
   * jsonqlWsClientCore takes the constProps at init
   * then only accept the config, therefore if we pass
   * the constProps as second param then it got ignore
   * what we need to do here is to preCheckConfig first
   * @param {object} config supply by dev
   * @param {object} [constProps={}] prop don't want to get check
   * @return {promise} resolve to the final config
   */
  function checkWsClientOptions(config, constProps) {
    if ( constProps === void 0 ) constProps = {};

    var localConstProps = Object.assign(jsonqlWsDefaultConstProps, wsClientConstProps, constProps);
    var checkFn = postConfigCheck$1(jsonqlWsDefaultOptions, localConstProps, checkConfig);
    return checkFn(config)
  }

  // this is the module entry point for ES6 for client

  // export back the function and that's it
  function wsClient(config, constProps) {
    if ( config === void 0 ) config = {};
    if ( constProps === void 0 ) constProps = {};

    return checkWsClientOptions(config)
      .then(wsClientCore(wsClientResolver, constProps))
  }

  // this is the jsonql client with ws

  /**
   * The top API to get the jsonql client with WebSocket client
   * @param {object} fly the fly instance NOT the Fly class itself
   * @param {object} [config={}] developer supply options
   * @return {object} the jsonql browser client with ws socket
   * @public
   */
  function createJsonqlHttpWsClient(fly, config) {
    if ( config === void 0 ) config = {};

    // get the client generator function
    var clientGenFn = getJsonqlClient(fly, jsonqlWsDefaultOptions, {});
    // now run it with the config input
    return clientGenFn(config)
      .then(function (ref) {
        var client = ref.client;
        var contract = ref.contract;
        var opts = ref.opts;

        // console.info('contract', contract)
        // console.info('opts', opts)
        // return client
        var fn = initSocketClient(wsClient);
        return fn(client, contract, opts)
      })
  }

  return createJsonqlHttpWsClient;

})));
//# sourceMappingURL=jsonql-client-ws.debug.js.map
