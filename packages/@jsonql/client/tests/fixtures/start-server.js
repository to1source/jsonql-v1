
const jsonqlKoaServer = require('@jsonql/koa')
const { port, contractDir, resolverDir, keysDir } = require('./options')
const debug = require('debug')('jsonql-client:fixtures:start-server')

module.exports = () => jsonqlKoaServer({
  port,
  contractDir,
  resolverDir,
  keysDir,
  enableAuth: true,
  serverType: 'ws'
})
