/**
 * public api to get a list of news
 * @return {array} list of news
 */
module.exports = function getNews() {

  return [
    {title: 'News of today'},
    {title: 'News of yesterday'},
    {title: 'News of tomorrow'}
  ]
}
