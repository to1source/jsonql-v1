const koaServer = require('../fixtures/start-server')
const runQunitSetup = require('./run-qunit-setup')
const config = {
  "port": 8081,
  "webroot": [
    "./tests/dist",
    "./tests/qunit/webroot",
    "./tests/qunit/files",
    "./node_modules"
  ],
  "open": true,
  "reload": true,
  "testFilePattern": "*-test.js",
  "baseDir": "/home/joel/projects/open-source/jsonql/packages/@jsonql/client/tests"
}

const srv = koaServer()
srv.start()
// gives it 3 seconds
setTimeout(() => {
  runQunitSetup(config)
}, 3000)
