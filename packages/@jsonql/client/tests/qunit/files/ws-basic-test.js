QUnit.test('jsonql client should have http and ws clients', function(assert) {
  var done = []
  var ctn = 4; // just change this number as the test increase
  for (var i = 0; i < ctn; ++i) {
    done.push(assert.async())
  }

  jsonqlClientWs(fly, {
    hostname: 'http://localhost:7890',
    enableAuth: true,
    debugOn: true
  })
  .then(function(client) {
    console.log(client)

    assert.equal(true, client.socket !== undefined, "Init client test to check if there is a socket")
    done[0]()
    // test login - first one should failed
    client.auth.login('john')
      .catch((err) => {
        console.error('login error', err)
        // @BUG this is actually wrong but need to fix this in jsonql-koa
        assert.equal('JsonqlResolverAppError', err.className, 'When throw a JsonqlAuthorisationError @BUG! inside the resolver should able to catch it back')
        done[1]()
        // now test the correct login
        client.auth.login('joel')
          .then(result => {
            console.info('login result', result)
            assert.equal('joel', result.name, 'Expect the login result to be the user object return from resolver on the server side')
            done[2]()
          })
      })
      // listen to the onLogin event

      client.socket.onLogin = function testOnLoginHandler(ns) {
        console.info('--- onLogin ---', ns)
        done[3]()
      }

      client.socket.onReady = function testOnReadyHandler(ns) {
        console.info('--- onReady ---', ns)

      }

  })
  .catch(function(err) {
    console.error('Init jsonqlClientWs error', err)
  })


  // console.log(fly)

  // console.log(client)



})
