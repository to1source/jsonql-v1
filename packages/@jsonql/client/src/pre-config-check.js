// custom version for debugging

import { preConfigCheck } from 'jsonql-utils/module'
import { checkConfig } from 'jsonql-params-validator'
import {
  jsonqlHttpClientAppProps,
  jsonqlHttpClientConstProps
} from 'jsonql-client/module'
/**
 * This will combine the socket client options and merge this one
 * then do a pre-check on both at the same time
 * @param {object} [extraProps = {}]
 * @param {object} [extraConstProps = {}]
 * @return {function} to process the developer options
 */
function getPreConfigCheck(extraProps = {}, extraConstProps = {}) {
  // we only want a shallow copy instead of deep merge
  const aProps = Object.assign({}, jsonqlHttpClientAppProps, extraProps)
  const cProps = Object.assign({}, jsonqlHttpClientConstProps, extraConstProps)

  console.info('aProps', aProps)
  console.info(`cProps`, cProps)
  delete aProps.token // just delete this and it works @TODO why?

  const fn =  preConfigCheck(aProps, cProps, checkConfig)

  return (config = {}) => {
    console.info('config', config)
    const result = fn(config)
    console.info('result', result)
    return result
  }
}
