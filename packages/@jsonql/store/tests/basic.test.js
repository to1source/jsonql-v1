const test = require('ava')

const { JsonqlStore } = require('../index')
const debug = require('debug')('jsonql-store:test:basic')

test.before(t => {
  t.context.store = new JsonqlStore({ logger: debug })
})


test(`It should able to store anything using a key`, t => {
  const store = t.context.store 
  const someKey = 'some-key'
  const someValue = {msg: 'Store an object and check the content later'}

  const result = store.set(someKey, someValue)
  t.true(result)

  const value = store.get(someKey)

  t.deepEqual(someValue, value)
})


test(`It should able to store a function and return it still a function`, t => {

  const store = t.context.store 

  const key = 'some-fn-key'
  const fn = function(msg) {
    return `got your ${msg}`
  }

  const result = store.set(key, fn)
  t.true(result)

  const fn1 = store.get(key)
  t.true(typeof fn1 === 'function')
  t.true(fn1('na na na').indexOf('na na na') > -1)

  const fn2 = store.get(key)

  t.true(typeof fn2 === 'function')
  // check size 

  const size = store.size()

  debug('size', size)
  t.truthy(size)

  store.clear()

  const size1 = store.size()

  t.is(size1, 0)

})


test(`Test the rest of the methods take, delete, all etc`, t => {
  const store = t.context.store 
  const someKey = 'some-key'
  const someValue = 'some-value'

  const someKey1 = 'some-key-1'
  const someValue1 = 'some-value-1'

  store.set(someKey, someValue)
  store.set(someKey1, someValue1)

  const size = store.size()

  t.is(size, 2)

  const arr = store.all()
  const arr1 = store.all(true)
  debug('all', arr)
  debug('array', arr1)

  store.delete(someKey)

  const check = store.has(someKey)

  t.false(check)

  const value2 = store.take(someKey1)

  t.is(value2, someValue1)

  t.is(store.size(), 0)

})