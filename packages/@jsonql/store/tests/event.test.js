// this will focus on the event hook testing 

const test = require('ava')
const { 
  JsonqlStore, 
  HAS_EVT,
  SET_EVT,
  GET_EVT,
  DELETE_EVT,
  TAKE_EVT,
  ALL_EVT,
  SIZE_EVT,
  CLEAR_EVT
} = require('../dist/jsonql-store.cjs')
const debug = require('debug')('jsonql-store:test:event')
const Event = require('@to1source/event')

test.before(t => {
  t.context.store = new JsonqlStore({ logger: debug })
  t.context.event = new Event()
})

test.cb(`It should able to hook into the store using the on method`, t => {

  t.plan(2)

  const store = t.context.store 

  // we add the on before calling it 
  store.on(SET_EVT, function(...args) {
    debug(SET_EVT, args)
    t.pass()
  })

  store.on(GET_EVT, function() {
    t.pass()
    t.end()
  })

  const somekey1 = 'key-1'
  const someval1 = 'value-1'

  store.set(somekey1, someval1)

  store.get(somekey1)
})

test.cb(`Test all the event hooks`, t => {
  const total = 7 
  t.plan(total)
  let ctn = 0
  // listen to the count 
  const evt = t.context.event 
  const store = new JsonqlStore()
  evt.$on('add', function(type) {
    ++ctn 
    debug('ctn -------------------->', ctn, type)
    if (ctn >= total) {
      t.end()
    }
  })
  const add = (type) => evt.$trigger('add', [type])

  const somekey2 = 'key-2'
  const someval2 = 'value-2'

  const somekey3 = 'key-3'
  const someval3 = 'value-3'

  store.set(somekey2, someval2)
  store.set(somekey3, someval3)

  store.all(true)

  store.delete(somekey2)
  store.take(somekey3)
  store.size()

  store.clear()

  store.on(SET_EVT, function(...args) {
    debug(SET_EVT, args)
    t.pass()
    add(SET_EVT)
  })

  store.on(DELETE_EVT, function() {
    t.pass()
    add(DELETE_EVT)
  })

  store.on(TAKE_EVT, function() {
    t.pass()
    add(TAKE_EVT)
  })

  store.on(SIZE_EVT, function() {
    t.pass()
    add(SIZE_EVT)
  })

  store.on(ALL_EVT, function(values) {
    debug(values)
    t.pass()
    add(ALL_EVT)
  })

  store.on(CLEAR_EVT, function() {
    t.pass()
    add(CLEAR_EVT)
  })

})
