# @jsonql/store 

> A super simple in-memory store with an event hook 

What that mean is, whenever you call this store, you can hook into it by listen to the event, 
and do your thing; such as return value from the store of your own choice, save value to your store etc.

The event hook is using [@to1source/event](https://npmjs.com/package/@to1source/event)

## Example 

```js
const { 
  JsonqlStore, 
  SET_EVT   
} from '@jsonql/store' 

const cache = new JsonqlStore()

cache.set('key', 'some value')

// listen to the event 

cache.$on(SET_EVT, function(value) {
  // here you will get the value just set 
  // which is 'some value' 
})
```

## API 

This module exports the follow:

- JsonqlStore
- HAS_EVT
- SET_EVT
- GET_EVT
- DELETE_EVT
- TAKE_EVT
- ALL_EVT
- SIZE_EVT
- CLEAR_EVT
- AVAILABLE_EVTS

### JsonqlStore 

The main class interface

```js

const store = new JsonqlStore()

// or if you want to take a look inside 
// example using debug 

const logger = require('debug')('your-key')

const store = new JsonqlStore({ logger })

```

#### Init options 

##### logger: allow you to pass a logging function 


```js 
// store.js 
const debug = require('debug')('a-key-to-id-your-logger')
const { JsonqlStore } = require('@jsonql/store')

const store = new JsonqlStore({ logger })

```

Then when you run it like:

```sh
$ DEBUG=a-key-to-id-your-logger node ./store.js 
```

You will see a lot of output from your console, that show what is going on internally.

##### eventOff: turn off the event listener 

You can turn off the event listener

```js
const { JsonqlStore, SET_EVT } = require('@jsonql/store')
const store = new JsonqlStore({ eventOff: true})

// if you try to listen to the event 
const isEventOn = store.on(SET_EVT, function somecallback() {})

```

In the above example, the on will do nothing, `isEventOn` is `undefined` 

For more about the it's parent the [@to1source/event](https://npmjs.com/package/@to1source/event), please check their documentation.

### store.on(eventName, callback)

Allow you to listen to the event happening inside, it's better using the exported constants as event name

```js
const { JsonqlStore, SET_EVT } = require('@jsonql/store')
const store = new JsonqlStore()

store.set('some-key', 'some-value')

// the magic is we don't care about when you start listening
store.on(SET_EVT, function(value) {
  // you will get back the key and value 
  // also you can add as many event listener as you need to
})

```

### store.has(key) HAS_EVT 

Check if if certain key existed in the store 

```js

const check = store.has(key)
// listen to this event
store.on(HAS_EVT, function(value) {
  // you will get back the key and the check value
})

```

### store.set(key,value) SET_EVT 

Store data id by key (key can be anything)

```js
store.set('some-key', 'some-value')
store.on(SET_EVT, function(result) {
  // get back the key and value in an array
})

```

### store.get(key) GET_EVT 

Get the data id by key 

```js
const data = store.get('some-key')
store.on(GET_EVT, function(result) {
  // get back the key and value in an array 
})

```

### store.delete(key) DELETE_EVT 

Delete data id by key 

```js
const result = store.delete('some-key')
store.on(DELETE_EVT, function(result) {
  // get back the key and value in an array
})

```

### store.take(key) TAKE_EVT 

Get the data id by key, then remove it from the store 

```js
const result = store.take('some-key')
store.on(TAKE_EVT, function(result) {
  // get back the key and value in an array
})
const existed = store.has('some-key')
// existed is false
```

### store.all(arr=true) ALL_EVT 

Return everything from the store as an array, if you pass `arr=false` then return the raw `Map` object

```js
const result = store.all()
// it's an array so you can perform your ops here
result.map(x => console.log(x))

```

### store.size() SIZE_EVT (alias JsonqlStore.length)

Return the size (how many) of the store 

```js
store.set('a', 1)
store.set('b', 2)
store.set('c', 3)

const ctn = store.size() // it will be 3 

store.length === ctn // same as above 

```


### store.clear() CLEAR_EVT 

Clear out everything from the store 

```js
store.clear()
const check = store.all()
store.length === 0

```

--- 

Joel Chu (c) 2020 

[NEWBRAN LTD](https://newbran.ch)

[TO1SOURCE](https://to1source.com)