
// create constants for all the event names 
// 'has', 'set', 'get', 'remove', 'take', 'all', 'size'
export const HAS_EVT = 'has'
export const SET_EVT = 'set'
export const GET_EVT = 'get'
export const DELETE_EVT = 'delete'
export const TAKE_EVT = 'take'
export const ALL_EVT = 'all'
export const SIZE_EVT = 'size'
export const CLEAR_EVT = 'clear'

export const AVAILABLE_EVTS = [
  HAS_EVT,
  SET_EVT,
  GET_EVT,
  DELETE_EVT,
  TAKE_EVT,
  ALL_EVT,
  SIZE_EVT,
  CLEAR_EVT
]