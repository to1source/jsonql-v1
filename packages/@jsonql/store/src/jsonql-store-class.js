// just fucking around with the cache object 
// NodeCache is a piece of shit 
import EventClass from '@to1source/event'
import {
  HAS_EVT,
  SET_EVT,
  GET_EVT,
  DELETE_EVT,
  TAKE_EVT,
  ALL_EVT,
  SIZE_EVT,
  CLEAR_EVT,
  AVAILABLE_EVTS
} from './constants'


export class JsonqlStore extends EventClass {

  /**
   * constructor init the instance
   * @param {*} opts 
   */
  constructor(opts = {}) {
    super(opts)
    // @0.9.1 add a switch to turn off the event
    this.__eventOff = !!opts.eventOff    
    this.$store = new Map()
  }

  //////////////////////////////////
  //        PRIVATE METHODS       //
  //////////////////////////////////


  /**
   * Wrap all event emit here and can be turn off via the eventOff = true
   * @param {*} evtName 
   * @param {array} payload expect an array but you need to diy it! 
   * @return {*} undefined means did nothing 
   */
  __emit(evtName, payload) {
    if (this.__eventOff === false) {
      return this.$trigger(evtName, payload)
    }
  }

  /**
   * a wraper method to create the stored items 
   * @TODO add timestamp using a Set etc
   * @param {*} value whatever you want to store 
   * @return {*} TBC  
   */
  __wrap(value) {
    return value
  }

  /**
   * Unwrap our internal store and return the value
   * @param {*} value
   * @return {*} TBC  
   */
  __unwrap(value) {
    return value
  }

  //////////////////////////////////
  //        PUBLIC METHODS        //
  //////////////////////////////////

  /**
   * Check key exist 
   * @param {*} key 
   * @return {*}
   */
  has(key) {
    const result = this.$store.has(key) || false
    this.__emit(HAS_EVT, [key, result])

    return result
  }

  /**
   * note this actually return the value itself 
   * @param {*} key 
   * @param {*} value 
   * @return {*}
   */
  set(key, value) {
    this.$store.set(key, value)
    this.__emit(SET_EVT, [key, value])

    return this.has(key)
  }

  /**
   * get it and make sure it return boolen false when nothing
   * @param {*} key
   * @return {*} 
   */
  get(key) {
    const result = this.$store.get(key) || false
    this.__emit(GET_EVT, [key, result])

    return result
  }

  /**
   * Remove from store 
   * @param {*} key
   * @return {*} 
   */
  delete(key) {
    const result = this.$store.delete(key)
    this.__emit(DELETE_EVT, [key, result])
    
    return result
  }

  /**
   * return the key value then remove it
   * @param {*} key
   * @return {*} 
   */
  take(key) {
    if (this.has(key)) {
      const value = this.$store.get(key)
      this.__emit(TAKE_EVT, [key, value])
      // we call the native method because we don't want to trigger the delete event again
      this.$store.delete(key)
      return value
    }
    this.__emit(TAKE_EVT, [key])
    return false
  }

  /**
   * alias to entries return interable object
   * @param {boolean} [arr=true] return as array or not
   * @return {array} this is the default now
   */
  all(arr = true) {
    const result = this.offload()
    const returnResult = arr ? Array.from(result) : result 

    this.__emit(ALL_EVT, [returnResult])

    return returnResult
  }

  /**
   * Offloading the store return just the raw data 
   * @return {*} <Map> object 
   */
  offload() {
    return this.$store.entries()
  }

  /**
   * Get the size (length) of the store
   * @return {number} 
   */
  size() {
    const result = this.$store.size
    this.__emit(SIZE_EVT, [result])

    return result
  }

  /**
   * Alias to size as a getter
   * @return {number} 
   */
  get length() {
    return this.size()
  }

  /**
   * Clear out everything from store
   * @return {void}
   */
  clear() {
    this.$store.clear()
    this.__emit(CLEAR_EVT)
    // @TODO should we clear out all the event listeners as well? 
  }

  /** 
   * event hook 
   * @param {string} evtName
   * @param {function} callback
   * @param {*} context
   * @return {*} return undefined means it did nothing  
   */
  on(evtName, callback, context = null) {
    if (this.__eventOff === false) {
      const ok = !!AVAILABLE_EVTS.filter(e => e === evtName).length
      if (ok) {
        return this.$on(evtName, callback, context)
      } 
      throw new Error(`${evtName} is not one of ours!`)
    }
  }
}


