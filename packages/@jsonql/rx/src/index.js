// The final putting them together

import generator from './generator';

import JsonqlRxClientClass from './rx-client';
/**
 * @param {object} jsonqlRxClient the init instance of jsonql client
 * @param {object} contract the static contract
 * @return {object} contract may be from server
 */
const getContract = (jsonqlRxClient, contract = {}) => {
  if (contract && contract.query && contract.mutation) {
    return Promise.resolve(contract);
  }
  return jsonqlRxClient.getContract();
};

/**
 * The final interface using the contract to generate callable methods
 * @param {object} config for the jsonql instance
 * @param {object} contract the json file it's empty object then we call the get contract
 * @return {object} key value path of callable methods
 */
export default function(config = {}) {
  const jsonqlRx = new JsonqlRXClientClass(config);

  return getContract(
    jsonqlRx,
    config.contract || false
  ).then(
    contract => generator(contract, jsonqlRx, config)
  ).then(
    jsonqlRxClient =>
  )
}
