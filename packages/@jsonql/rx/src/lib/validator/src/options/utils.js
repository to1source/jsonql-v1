import {
  isFunction,
  isUndefined,
  merge,
  mapValues,
  omitBy
} from 'lodash-es';
import {
  TYPE_KEY,
  OPTIONAL_KEY,
  ENUM_KEY,
  ARGS_KEY,
  CHECKER_KEY,
  KEY_WORD,
  ALIAS_KEY
} from '../constants';
import {
  JsonqlEnumError,
  JsonqlTypeError,
  JsonqlCheckerError
} from 'jsonql-errors';

import { checkIsArray } from '../array';
import checkIsString from '../string';
import notEmpty from '../not-empty';

// import debug from 'debug';
// const debugFn = debug('jsonql-params-validator:options:utils');

/**
 * just make sure it returns an array to use
 * @param {*} arg input
 * @return {array} output
 */
const toArray = arg => checkIsArray(arg) ? arg : [arg];
// just not to make my head hurt
const isEmpty = value => !notEmpty(value);

/**
 * DIY in array
 * @param {array} arr to check against
 * @param {*} value to check
 * @return {boolean} true on OK
 */
const inArray = (arr, value) => {
  return !!arr.filter(v => v === value).length;
}

/**
 * Map the alias to their key then grab their value over
 * @param {object} config the user supplied config
 * @param {object} appProps the default option map
 * @return {object} the value replaced config
 */
const mapAliasValue = (config, appProps) => {
  return mapValues(appProps, (value, key) => {
    let configValue;
    if (checkIsString(value[ALIAS_KEY]) && !isUndefined(configValue = config[value[ALIAS_KEY]])) {
      value[ARGS_KEY] = configValue;
    }
    return value;
  });
};

/**
 * break out to make the code easier to read
 * @param {object} value to process
 * @param {function} cb the validateSync
 * @return {array} empty on success
 */
export const validateHandler = (value, cb) => {
  // cb is the validateSync methods
  return cb([ value[ARGS_KEY] ],
            [{
                [TYPE_KEY]: toArray(value[TYPE_KEY]),
                [OPTIONAL_KEY]: value[OPTIONAL_KEY]
            }]
          );
};

/**
 * Check against the enum value if it's provided
 * @param {*} value to check
 * @param {*} enumv to check against if it's not false
 * @return {boolean} true on OK
 */
export const enumHandler = (value, enumv) => {
  if (checkIsArray(enumv)) {
    return inArray(enumv, value);
  }
  return true;
};

/**
 * Allow passing a function to check the value
 * There might be a problem here if the function is incorrect
 * and that will makes it hard to debug what is going on inside
 * @TODO there could be a few feature add to this one under different circumstance
 * @param {*} value to check
 * @param {function} checker for checking
 */
export const checkerHandler = (value, checker) => {
  try {
    return isFunction(checker) ? checker.apply(null, [value]) : false;
  } catch (e) {
    return false;
  }
};

/**
 * Quick transform
 * @param {object} config that one
 * @param {object} appProps mutation configuration options
 * @return {object} put that arg into the args
 */
export const config2argsAction = (config, appProps) => {
  const props = mapAliasValue(config, appProps);
  // we need to filter out those option that has no params field
  // i.e. user just pass something not in our map
  const filtered = omitBy(config, (value, key) => !props[key]);
  // now we map the value to the validator style
  // v.1.2.0 add checking if its mark optional and the value is empty then pass
  return mapValues(props, (value, key) => (
    isUndefined(filtered[key]) || (value[OPTIONAL_KEY] === true && isEmpty(filtered[key]))
      ? merge({}, value, {[KEY_WORD]: true})
      : {
          [ARGS_KEY]: filtered[key],
          [TYPE_KEY]: value[TYPE_KEY],
          [OPTIONAL_KEY]: value[OPTIONAL_KEY] || false,
          [ENUM_KEY]: value[ENUM_KEY] || false,
          [CHECKER_KEY]: value[CHECKER_KEY] || false
        }
    )
  );
};

/**
 * @param {array} args from the config2argsAction
 * @param {function} cb validateSync
 * @return {array} of configuration values
 */
export const runValidation = (args, cb) => {
  return mapValues(args, (value, key) => {
    if (value[KEY_WORD]) {
      return value[ARGS_KEY];
    }
    const check = validateHandler(value, cb);
    if (check.length) {
      throw new JsonqlTypeError(key, check);
    }
    if (value[ENUM_KEY] !== false && !enumHandler(value[ARGS_KEY], value[ENUM_KEY])) {
      throw new JsonqlEnumError(key);
    }
    if (value[CHECKER_KEY] !== false && !checkerHandler(value[ARGS_KEY], value[CHECKER_KEY])) {
      throw new JsonqlCheckerError(key);
    }
    // next check enum value
    return value[ARGS_KEY];
  });
}
