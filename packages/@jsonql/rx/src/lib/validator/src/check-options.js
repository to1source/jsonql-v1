// this is port back from the client to share across all projects
import { merge, mapValues, transform, omitBy } from 'lodash-es';
import { checkIsArray } from './array';
import debugFn from './debug';
const debug = debugFn('check-options');

/**
 * just make sure it returns an array to use
 * @param {*} arg input
 * @return {array} output
 */
const toArray = arg => checkIsArray(arg) ? arg : [arg];

/**
 * Quick transform
 * @param {object} config that one
 * @param {object} appProps mutation configuration options
 * @return {object} put that arg into the args
 */
const config2args = (config, appProps) => {
  const props = merge({}, appProps);
  // we need to filter out those option that has no params field
  // i.e. user just pass something not in our map
  const filtered = omitBy(config, (value, key) => !props[key]);
  // now we map the value to the validator style
  return Promise.resolve(
    mapValues(props, (value, key) => (
      filtered[key] !== undefined
      ? { args: filtered[key], type: value.type }
      : value
    ))
  );
};

/**
 * @param {object} config user provide configuration option
 * @param {object} appProps mutation configuration options
 * @param {object} constProps the immutable configuration options
 * @param {function} cb the validateSync method
 * @return {object} Promise resolve merge config object
 */
export default function(config = {}, appProps, constProps, cb) {
  return config2args(config, appProps)
    .then(args => {
      return mapValues(args, (value, key) => {
        const { args, type } = value;
        const params = toArray(type).map(param => ({type: [ param ]}));
        const check = cb([args], params);
        if (check.length) {
          // Just throw the key back so we could id which one is wrong!
          throw new Error(key);
        }
        return value.args;
      });
    })
    // next if every good then pass to final merging
    .then(args => merge({}, args, constProps));
}
