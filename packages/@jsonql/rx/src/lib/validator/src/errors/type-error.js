
export default class JsonqlTypeError extends Error {
  constructor(...args) {
    super(...args);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlTypeError);
    }
  }
}
