const test = require('ava')

const JsonqlEventService = require('../dist/jsonql-event.cjs')
const logger = require('debug')('nb-event-service')
const debug  = require('debug')('nb-event-service:test:suspend')


test.before(t => {
  t.context.evtSrv = new JsonqlEventService(logger)
})

test('It should able to suspend the $trigger', t => {

  const evtSrv = t.context.evtSrv;
  const evt = 'to-suspend-event'
  evtSrv.$on(evt, function(num) {
    return num + 1;
  })

  evtSrv.$suspend = true;
  evtSrv.$trigger(evt, 100)

  debug(evtSrv.$queues)

  t.false(evtSrv.$done === 101)

})
