// The main class
import './interfaces.ts';
import JsonqlStoreService from './store-service';

// main
export default class JsonqlEvent extends JsonqlStoreService {

  /**
   * Create EventService instance
   * @param {configObj} config configuration object
   * @return {void} nothing - don't do :void this fuck typescript up what a lot of shit
   */
  constructor(logger: any) {
    super(logger)
  }

  /**
   * Register your evt handler, note we don't check the type here,
   * we expect you to be sensible and know what you are doing.
   * @param {string} evt name of event
   * @param {function} callback bind method --> if it's array or not
   * @param {object} [context=null] to execute this call in
   * @return {number} the size of the store
   */
  public $on(evt: string, callback: myCallbackType, context: any = null): any {
    this.validate(callback, evt)
    const type = 'on';
    // first need to check if this evt is in lazy store
    let lazyStoreContent = this.takeFromStore(evt)
    // this is normal register first then call later
    if (lazyStoreContent === false) {
      this.logger('$on', `${evt} callback is not in lazy store`)
      // @TODO we need to check if there was other listener to this
      // event and are they the same type then we could solve that
      // register the different type to the same event name

      return this.addToNormalStore(evt, type, callback, context)
    }
    this.logger('$on', `${evt} found in lazy store`)
    // this is when they call $trigger before register this callback
    let size = 0;
    lazyStoreContent.forEach((content: any[]): void => {
      let [ payload, ctx, t ] = content;
      if (t && t !== type) {
        throw new Error(`You are trying to register an event already been taken by other type: ${t}`)
      }
      this.run(callback, payload, context || ctx)
      let result = this.addToNormalStore(evt, type, callback, context || ctx)
      if (typeof result !== 'boolean') {
        size += result;
      }
    })
    return size;
  }

  /**
   * once only registered it once, there is no overwrite option here
   * @NOTE change in v1.3.0 $once can add multiple listeners
   *       but once the event fired, it will remove this event (see $only)
   * @param {string} evt name
   * @param {function} callback to execute
   * @param {object} [context=null] the handler execute in
   * @return {number|boolean} result
   */
  public $once(evt: string , callback: myCallbackType , context: any = null): any {
    this.validate(callback, evt)
    const type = 'once';
    let lazyStoreContent = this.takeFromStore(evt)
    // this is normal register before call $trigger
    let nStore = this.normalStore;
    if (lazyStoreContent === false) {
      this.logger('$once', `${evt} not in the lazy store`)
      // v1.3.0 $once now allow to add multiple listeners
      return this.addToNormalStore(evt, type, callback, context)
    } else {
      // now this is the tricky bit
      // there is a potential bug here that cause by the developer
      // if they call $trigger first, the lazy won't know it's a once call
      // so if in the middle they register any call with the same evt name
      // then this $once call will be fucked - add this to the documentation
      this.logger('$once', lazyStoreContent)
      const list = this.arrayFrom(lazyStoreContent)
      // should never have more than 1
      const [ payload, ctx, t ] = list[0]
      if (t && t !== type) {
        throw new Error(`You are trying to register an event already been taken by other type: ${t}`)
      }
      this.run(callback, payload, context || ctx)
      // remove this evt from store
      this.$off(evt)
    }
    return false;
  }

  /**
   * This one event can only bind one callbackback
   * @param {string} evt event name
   * @param {function} callback event handler
   * @param {object} [context=null] the context the event handler execute in
   * @return {boolean} true bind for first time, false already existed
   */
  public $only(evt: string, callback: myCallbackType, context: any = null): boolean {
    this.validate(callback, evt)
    const type = 'only';
    // keep getting TS232, this is why typescript is a joke
    // it will just ended up with everything is any type and back to square one
    let added: any = false;
    let lazyStoreContent = this.takeFromStore(evt)
    // this is normal register before call $trigger
    let nStore = this.normalStore;
    if (!nStore.has(evt)) {
      this.logger(`$only`, `${evt} add to store`)
      added = this.addToNormalStore(evt, type, callback, context)
    }
    if (lazyStoreContent !== false) {
      // there are data store in lazy store
      this.logger('$only', `${evt} found data in lazy store to execute`)
      const list = this.arrayFrom(lazyStoreContent)
      // $only allow to trigger this multiple time on the single handler
      list.forEach( l => {
        const [ payload, ctx, t ] = l;
        if (t && t !== type) {
          throw new Error(`You are trying to register an event already been taken by other type: ${t}`)
        }
        this.run(callback, payload, context || ctx)
      })
    }
    return added;
  }

  /**
   * $only + $once this is because I found a very subtile bug when we pass a
   * resolver, rejecter - and it never fire because that's OLD added in v1.4.0
   * @param {string} evt event name
   * @param {function} callback to call later
   * @param {object} [context=null] exeucte context
   * @return {boolean} same as above
   */
  public $onlyOnce(evt: string, callback: myCallbackType, context: any = null): boolean {
    this.validate(callback, evt)
    const type = 'onlyOnce';
    let added: any = false;
    let lazyStoreContent = this.takeFromStore(evt)
    // this is normal register before call $trigger
    let nStore = this.normalStore;
    if (!nStore.has(evt)) {
      this.logger(`$onlyOnce`, `${evt} add to store`)
      added = this.addToNormalStore(evt, type, callback, context)
    }
    if (lazyStoreContent !== false) {
      // there are data store in lazy store
      this.logger('$onlyOnce', lazyStoreContent)
      const list = this.arrayFrom(lazyStoreContent)
      // should never have more than 1
      const [ payload, ctx, t ] = list[0]
      if (t && t !== 'onlyOnce') {
        throw new Error(`You are trying to register an event already been taken by other type: ${t}`)
      }
      this.run(callback, payload, context || ctx)
      // remove this evt from store
      this.$off(evt)
    }
    return added;
  }

  /**
   * This is a shorthand of $off + $on added in V1.5.0
   * @param {string} evt event name
   * @param {function} callback to exeucte
   * @param {object} [context = null] or pass a string as type
   * @param {string} [type=on] what type of method to replace
   * @return {any} whatever the call method return
   */
  public $replace(evt: string, callback: myCallbackType, context: any = null, type: string = 'on'): any {
    this.$off(evt)
    // this will become a problem
    let method = this.getMethodBy(type)
    return Reflect.apply(method, this, [evt, callback, context])
  }

  /**
   * trigger the event
   * @param {string} evt name NOT allow array anymore!
   * @param {mixed} [payload = []] pass to fn
   * @param {object|string} [context = null] overwrite what stored
   * @param {string} [type=false] if pass this then we need to add type to store too
   * @return {number} if it has been execute how many times
   */
  public $trigger(evt: string , payload: any[] = [] , context: any = null, type: string|boolean = false): number {
    this.validateEvt(evt)
    let found = 0;
    // first check the normal store
    let nStore = this.normalStore;
    this.logger('$trigger, from normalStore: ', nStore)
    if (nStore.has(evt)) {
      let added = this.$queue(evt, payload, context, type)
      this.logger('$trigger', evt, 'found; add to queue: ', added)
      if (added === true) {
        return found; // just exit
      }
      let nSet = this.arrayFrom(nStore.get(evt))
      let ctn = nSet.length;
      let hasOnce = false;
      // let hasOnly = false;
      for (let i=0; i < ctn; ++i) {
        ++found;
        // this.logger('found', found)
        let [ _, callback, ctx, type ] = nSet[i]
        this.run(callback, payload, context || ctx)
        if (type === 'once' || type === 'onlyOnce') {
          hasOnce = true;
        }
      }
      if (hasOnce) {
        nStore.delete(evt)
      }
      return found;
    }
    // now this is not register yet
    this.addToLazyStore(evt, payload, context, type)
    return found;
  }

  /**
   * this is an alias to the $trigger
   * @NOTE breaking change in V1.6.0 we swap the parameter around
   * @param {string} evt event name
   * @param {*} params pass to the callback
   * @param {string} type of call
   * @param {object} context what context callback execute in
   * @return {*} from $trigger
   */
  public $call(evt: string, params: any[], type: any = false, context: any = null): any {
    this.validateEvt(evt)
    let args = [evt, params]
    args.push(context, type)
    return Reflect.apply(this.$trigger, this, args)
  }

  /**
   * remove the evt from all the stores
   * @param {string} evt name
   * @return {boolean} true actually delete something
   */
  public $off(evt: string): boolean {
    this.validateEvt(evt)
    let stores = [ this.lazyStore, this.normalStore ]
    let found = false;
    stores.forEach(store => {
      if (store.has(evt)) {
        found = true;
        store.delete(evt)
      }
    })
    return found;
  }

  /**
   * When using ES6 you just need this[$+type] but typescript no such luck
   * @param {string} type the 4 types
   * @return {*} the method
   */
  protected getMethodBy(type: string): any {
    switch(type) {
      case 'on':
        return this.$on;
      case 'only':
        return this.$only;
      case 'once':
        return this.$once;
      case 'onlyOnce':
        return this.$onlyOnce;
      default:
        throw new Error(`${type} is not supported!`)
    }
  }
  
  /**
   * Add $trigger args to the queue during suspend state
   * @param {array} args from the $trigger call parameters
   * @return {boolean} true when added
   */
  private $queue(...args: any[]): boolean {
    if (this.suspend) {
      this.queue.add(args)
    }
    return this.suspend;
  }

  /**
   * whenever the suspend change from true to false
   * then we check the queue to see if there is anything to do
   * @return {boolean} true if there is queue
   */
  private releaseQueue(): boolean {
    let size = this.queue.size;
    if (size > 0) {
      let queue = this.arrayFrom(this.queue)
      this.queue.clear()
      queue.forEach(args => {
        Reflect.apply(this.$trigger, this, args)
      })
    }
    return !!size;
  }

  /**
   * This is the main interface to see if anything happens
   * @param {boolean} state to change the suspend state
   * @return {boolean} what comes in what goes out
   */
  public set $suspend(state: boolean) {
    if (typeof state !== 'boolean') {
      throw new Error(`Typescript is a fucking joke! BTW we want a Boolean which fucking Typescript can't figure it out!!!! ${typeof state}`)
    }
    const lastSuspendState = this.suspend;
    this.suspend = state;
    if (lastSuspendState === true && state === false) {
      setTimeout(() => {
        this.releaseQueue()
      }, 1)
    }
  }
}
