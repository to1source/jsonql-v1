/**
 * generate a 32bit hash based on the function.toString()
 * _from http://stackoverflow.com/questions/7616461/generate-a-hash-_from-string-in-javascript-jquery
 * @param {function} fn the converted to string function
 * @return {string} the hashed function string
 */
export default function hashCode(fn: Function): String;
