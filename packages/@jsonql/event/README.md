# @jsonql/event

This is ported from [nb-event-service](https://npmjs.com/package/nb-event-service) and rewritten with Typescript

The different is - no alias options

## Installation

You don't usually use this directly, this is part of the `@jsonql` modules.

But it can work standalone:

```sh
$ npm i @jsonql/event
```

When using it directly in browser you have to do this:

```js
var eventService = new JsonqlEvent()
// the rest of the code
```

I would recommend that you don't use this module (Typescript is not as magical as most keep saying it is);
you should use [nb-event-service](https://npmjs.org/package/nb-event-service) instead, if you only use this one module.
If you are using Typescript, then suck it up then ...

For the other that use node build tool, just do this:

```js
import JsonqlEvent from '@jsonql/event'
```

## API

Same as `nb-event-service` only without the alias.

#### $on(eventName, callback, context)

* eventName (string) The event name you want to handle. You can call this multiple times to add different listeners
* callback (function) it will receive the `params` that call
* context (object|null) optional, we will pass it like this `Reflect.apply(callback, context, args)`

It will return the total number of events that get registered.

#### $once(eventName , callback, context)

* eventName (string) the event you want to listen to once, you can call this more than once to add more listener
* callback (function) it will receive the `params` that call
* context (object|null) optional same as above

$once allow you to bind one or more listener to the same event. But once this event fired (triggered)
it will remove itself from the event store, and no longer available. This behavior is changed in V1.3.0.

There is a potential problem with $once you can see below. It's no really a bug per se, but due to our
own unique feature that can call event before even it existed (yeah, it's magic)


```js
// trigger event before it register with a handler
ee.$trigger('someEvent')
// now it register with a regular $on
ee.$on('someEvent', function() {
  console.log('call me second')
})
// but some where else you try to register it with $once
ee.$once('someEvent', function() {
  console.log('call me first')
})
```

In v1.3.0 we change the behavior of $once, now you can register more than one handler.
But if you look at the above example, you register it with `$on` then `$once`.

What happen is, the `$once` call execute the `$trigger` from the earlier call, then it will
remove this event from the event handler store. Therefore, you `$on` will never fire again.

So you have to make sure which event you **REALLY** want to register with what.

#### $only(eventName , callback, context)

This is a new method in v1.3.0

* eventName (string) the event you want to listen to once, this is first come first serve, and only **ONE** listener
* callback (function) it will receive the `params` that call
* context (object|null) optional same as above

Example:

```js
es.$only('only-event', function(message) {
  console.log('ONLY', message)
})
// now if you try to add another
es.$only('only-event', function(message) {
  console.log('AGAIN', message)
})

// execute it
es.$trigger('only-event', 'A little cat jumping through the window')

```

You will only get `ONLY A little cat jumping through the window` but the second callback never add to the event store.
Although we develop this feature purposely for our other library to use, but it has a lot real world usage.

#### $onlyOnce(eventName , callback, context)

Just like what it said on the tin; its `$only` + `$once`. You should able to figure out what it does.

#### $off(eventName)

* eventName (string) event to remove from internal store  

It will return

* true - event been clear
* false - such even doesn't exist

#### $replace(eventName, callback, context = null, type = 'on')

This is `$off` + event register function

Type can be `on`, `only`, `once`, `onlyOnce` default value is `on`

#### $trigger(eventName, params , context, type)

* eventName (string) this will trigger the callback that register with this `eventName` whether that actually exist or not
* params (mixed) optional - data you want to pass to your callback method
* context (object || null) optional - When we execute the callback, we will add this context to the `Reflect.apply` or default to null
* type (string) available types are `on`, `only`, `once`, `onlyOnce` this is for trigger event before it get register and prevent other type to register it

This method will return

* false - if there is nothing to call
* i - the total events been called

#### $call(eventName, params, type, context)

* eventName (string) this will trigger the callback that register with this `eventName` whether that actually exist or not
* params (mixed) optional - data you want to pass to your callback method
* type (string) available types are `on`, `only`, `once`, `onlyOnce` this is for trigger event before it get register and prevent other type to register it
* context (object || null) optional - When we execute the callback, we will add this context to the `Reflect.apply` or default to null

This basically it's a shorthand of `$trigger` if you know that your callback only execute in `null` and purposely register a type to prevent
other to register it later

This is useful shorthand, also trigger event before its register

Example:

```js
// call before event register
es.$call('some-event', 1001, 'only')
// now try to register it with a different event handler
es.$on('some-event', function(num) {
  return ++num;
})
// it will throw Error that tells you it has been register with `only` type already

```

#### $get(evt)

* return all the listeners for that particular event name from the internal store. Handy for debug.

Or it will return `false` if there is nothing

#### $suspend

This is a setter inside the class; When you set this to `true` it suspend all the `$trigger` and `call` operation.
They will be store inside a queue, and release when you set it to `false`

```js
const evtSrv = new JsonqlEvent()

evtSrv.$on('add-something', function(value) {
  return value + 1;
})
evtSrv.$suspend = true;

evtSrv.$trigger('add-something', 100)

evtSrv.$done === 101; // false

evtSrv.$suspend = false;

evtSrv.$done === 101; // true

```



---

ISC

[Joel Chu](https://joelchu.com) (c) 2019 - [NEWBRAN LTD UK](https://newbran.ch) / [TO1SOURCE CN](https://to1source.cn)
