
/**
 * create a login method for testing
 * @param {string} username username
 * @return {object} userdata
 */
module.exports = function login(username) {
  return {name: username}
}
