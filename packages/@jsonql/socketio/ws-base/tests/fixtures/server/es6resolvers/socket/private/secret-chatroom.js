
/**
 * @param {string} room room name
 * @param {*} msg message to that room
 * @return {*} depends
 */
export default function secretChatroom(room, msg) {

  let userdata = secretChatroom.userdata;
  // @TODO
  return `send ${msg+''} to ${room} room from ${userdata.name}`;
}
