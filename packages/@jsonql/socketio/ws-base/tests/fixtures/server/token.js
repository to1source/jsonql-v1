// generate token for use for auth
const { join } = require('path')
const fsx = require('fs-extra')
const privateKey = fsx.readFileSync(join(__dirname, 'keys', 'privateKey.pem'))
const { jwtRsaToken, jwtToken } = require('jsonql-jwt')
const { HSA_ALGO } = require('jsonql-constants')

module.exports = function(payload, key = false) {
  if (key === false) {
    return jwtRsaToken(payload, privateKey)
  }
  return jwtToken(payload, key, {
    algorithm: HSA_ALGO
  })
}
