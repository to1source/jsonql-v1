// this one will provide all the configuration options including the contract
const { join } = require('path')
const fsx = require('fs-extra')
const { getDebug } = require('../../lib/share/helpers')
const debug = getDebug('test:full-setup')
const resolverDir = join(__dirname, 'resolvers')

const wsServer = require('../../index')
const http = require('http')

// start
const server = http.createServer(function(req, res) {
  res.writeHead(200, { 'Content-Type': 'text/plain' })
  res.write('request successfully proxied!' + '\n' + JSON.stringify(req.headers, true, 2))
  res.end()
})

module.exports = function(_config = {}) {
  const config = {
    resolverDir,
    serverType: 'socket.io'
  }
  const opts = Object.assign(config, _config)
  return new Promise(resolver => {
    wsServer(opts, server)
      .then(io => {
        debug('socket setup completed')
        resolver({
          app: server,
          io
        })
      })
  })
}
