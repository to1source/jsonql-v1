// this is a standalone test to try the Object.defineProperty
const test = require('ava')

test.before( t => {
  const fn = require('./fixtures/fn');
  Object.defineProperty(fn, 'ctx', {
    value: function() {
      return 'I am ctx';
    }
  })
  t.context.fn = fn;
})

test("I should able to defined an object property on a anonymous function", t => {
  let fn1 = t.context.fn;
  const msg = fn1()

  // t.is('This is a fn', msg);
  t.is('I am ctx', msg)
})
