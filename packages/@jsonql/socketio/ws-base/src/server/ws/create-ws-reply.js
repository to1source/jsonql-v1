// for some really really weird reason if I put this into the utils/helpers
// its unable to import into the module here
// since this is for ws only then we could just put in here instead
const {
  WS_REPLY_TYPE,
  WS_EVT_NAME,
  WS_DATA_NAME
} = require('jsonql-constants')
// const debug = require('debug')('jsonql-ws-server:create-ws-reply');
/**
 * The ws doesn't have a acknowledge callback like socket.io
 * so we have to DIY one for ws and other that doesn't have it
 * @param {string} type of reply
 * @param {string} resolverName which is replying
 * @param {*} data payload
 * @return {string} stringify json
 */
const createWsReply = (type, resolverName, data) => {
  return JSON.stringify({
    data: {
      [WS_REPLY_TYPE]: type,
      [WS_EVT_NAME]: resolverName,
      [WS_DATA_NAME]: data
    }
  })
}

module.exports = createWsReply
