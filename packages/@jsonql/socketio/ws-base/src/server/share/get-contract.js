// if they didn't pass the contract then we need to grab it from the contractDir
const { join } = require('path')
const fsx = require('fs-extra')
const { DEFAULT_CONTRACT_FILE_NAME } = require('jsonql-constants')
const { isContract } = require('jsonql-params-validator')
const { JsonqlError } = require('jsonql-errors')
const { CONTRACT_NOT_FOUND_ERR  } = require('./constants')
/**
 * @param {object} config configuration
 * @return {object} config with the contract
 */
module.exports = function(config) {
  if (config.contract && isContract(config.contract)) {
    return config;
  }
  let c = fsx.readJsonSync(join(config.contractDir, DEFAULT_CONTRACT_FILE_NAME))
  if (!isContract(c)) {
    throw new JsonqlError(CONTRACT_NOT_FOUND_ERR )
  }
  config.contract = c;
  return config;
}
