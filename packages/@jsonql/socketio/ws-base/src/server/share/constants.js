
const SOCKET_IO = 'socket.io';
const WS = 'ws';

const AVAILABLE_SERVERS = [SOCKET_IO, WS];

const SOCKET_NOT_DEFINE_ERR = 'socket is not define in the contract file!';

const SERVER_NOT_SUPPORT_ERR = 'is not supported server name!';

const SECRET_MISSING_ERR = 'Secret is required!';

const MODULE_NAME = 'jsonql-ws-server';

const CONTRACT_NOT_FOUND_ERR = `No contract presented!`;

module.exports = {
    SOCKET_IO,
    WS,
    AVAILABLE_SERVERS,
    SOCKET_NOT_DEFINE_ERR,
    SERVER_NOT_SUPPORT_ERR,
    SECRET_MISSING_ERR,
    MODULE_NAME,
    CONTRACT_NOT_FOUND_ERR 
};
