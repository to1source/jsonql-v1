// import { JsonqlError } from 'jsonql-errors'
/*
this have moved out of this package
@TODO need to figure out how to inject them back here
import createWsClient from './ws'
import createIoClient from './io'
*/
// import { SOCKET_IO, WS, SOCKET_NOT_DEFINE_ERR } from './utils/constants'

/**
 * get the create client instance function
 * @param {string} type of client
 * @return {function} the actual methods
 * @public
 */
export default function createSocketClient(opts, nspMap, ee) {
  // idea, instead of serverType we pass the create client method via the opts
  return Reflect.apply(opts.createClientMethod, null, [opts, nspMap, ee])
  /*
  switch (opts.serverType) {
    case SOCKET_IO:
      return createIoClient(opts, nspMap, ee)
    case WS:
      return createWsClient(opts, nspMap, ee)
    default:
      throw new JsonqlError(SOCKET_NOT_DEFINE_ERR)
  }
  */
}
