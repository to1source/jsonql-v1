// constants

import {
  EMIT_REPLY_TYPE,
  JS_WS_SOCKET_IO_NAME,
  JS_WS_NAME,
  ON_MESSAGE_PROP_NAME,
  ON_RESULT_PROP_NAME
} from 'jsonql-constants'

const SOCKET_IO = JS_WS_SOCKET_IO_NAME;
const WS = JS_WS_NAME;

const AVAILABLE_SERVERS = [SOCKET_IO, WS]

const SOCKET_NOT_DEFINE_ERR = 'socket is not define in the contract file!';

const SERVER_NOT_SUPPORT_ERR = 'is not supported server name!';

const MISSING_PROP_ERR = 'Missing property in contract!';

const UNKNOWN_CLIENT_ERR = 'Unknown client type!';

const EMIT_EVT = EMIT_REPLY_TYPE;

const NAMESPACE_KEY = 'namespaceMap';

const UNKNOWN_RESULT = 'UKNNOWN RESULT!';

const NOT_ALLOW_OP = 'This operation is not allow!';

const MY_NAMESPACE = 'myNamespace'

export {
  SOCKET_IO,
  WS,
  AVAILABLE_SERVERS,
  SOCKET_NOT_DEFINE_ERR,
  SERVER_NOT_SUPPORT_ERR,
  MISSING_PROP_ERR,
  UNKNOWN_CLIENT_ERR,
  EMIT_EVT,
  ON_MESSAGE_PROP_NAME,
  ON_RESULT_PROP_NAME,
  NAMESPACE_KEY,
  UNKNOWN_RESULT,
  NOT_ALLOW_OP,
  MY_NAMESPACE
}
