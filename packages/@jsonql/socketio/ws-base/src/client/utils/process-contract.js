// mapping the resolver to their respective nsp

import { JSONQL_PATH } from 'jsonql-constants'
import { groupByNamespace } from 'jsonql-jwt'
import { JsonqlResolverNotFoundError } from 'jsonql-errors'

import { MISSING_PROP_ERR } from './constants'
import getDebug from './get-debug'
const debug = getDebug('process-contract')

/**
 * Just make sure the object contain what we are looking for
 * @param {object} opts configuration from checkOptions
 * @return {object} the target content
 */
const getResolverList = contract => {
  if (contract) {
    const { socket } = contract;
    if (socket) {
      return socket;
    }
  }
  throw new JsonqlResolverNotFoundError(MISSING_PROP_ERR)
}

/**
 * process the contract first
 * @param {object} opts configuration
 * @return {object} sorted list
 */
export default function processContract(opts) {
  const { contract, enableAuth } = opts;
  if (enableAuth) {
    return groupByNamespace(contract)
  }
  return {
    nspSet: { [JSONQL_PATH]: getResolverList(contract) },
    publicNamespace: JSONQL_PATH,
    size: 1 // this prop is pretty meaningless now
  }
}
