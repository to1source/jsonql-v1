// export the util methods
import { isArray } from 'jsonql-params-validator'
import { toArray, createEvt, formatPayload } from 'jsonql-utils'

import ee from './ee'
import getDebug from './get-debug'
import * as constants from './constants'
import checkOptions from './check-options'
import processContract from './process-contract'
import getNamespaceInOrder from './get-namespace-in-order'
import { clearMainEmitEvt, disconnect } from './events'

// export
export {
  getNamespaceInOrder,
  createEvt,
  clearMainEmitEvt,
  checkOptions,
  ee,
  constants,
  getDebug,
  processContract,
  toArray,
  formatPayload,
  disconnect
}
