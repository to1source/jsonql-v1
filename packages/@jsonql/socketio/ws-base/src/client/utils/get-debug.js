// not using the jsonql-utils version
import debug from 'debug'
const MODULE_NAME = 'jsonql-ws-base'
/**
 * Try to normalize it to use between browser and node
 * @param {string} name for the debug output
 * @return {function} debug
 */
const getDebug = name => {
  if (debug) {
    console.info(`using node debug`)
    return debug(MODULE_NAME).extend(name)
  }
  return (...args) => {
    console.info.apply(null, [name].concat(args))
  }
}
try {
  // only when we set a global DEBUG=true 
  if (window && window.DEBUG && window.localStorage) {
    localStorage.setItem('DEBUG', `${MODULE_NAME}*`)
  }
} catch(e) {}
// export it
export default getDebug;
