// moved from the index.js
import { JS_WS_SOCKET_IO_NAME } from 'jsonql-constants'
import { EMIT_EVT } from './constants'
/**
 * Unbind the event
 * @param {object} ee EventEmitter
 * @param {string} namespace
 * @return {void}
 */
export const clearMainEmitEvt = (ee, namespace) => {
  let nsps = isArray(namespace) ? namespace : [namespace]
  nsps.forEach(n => {
    ee.$off(createEvt(n, EMIT_EVT))
  })
}

/**
 * exeucte a disconnect call for different client
 * @param {object} nsps namespace as key
 * @param {string} type of server
 */
export const disconnect = (nsps, type = JS_WS_SOCKET_IO_NAME) => {
  try {
    // @TODO what happen to others?
    const method = type === JS_WS_SOCKET_IO_NAME ? 'disconnect' : 'terminate';
    for (let namespace in nsps) {
      let nsp = nsps[namespace]
      if (nsp && nsp[method]) {
        Reflect.apply(nsp[method], null, [])
      }
    }
  } catch(e) {
    // socket.io throw a this.destroy of undefined?
    console.error('disconnect', e)
  }
}
