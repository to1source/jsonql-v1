

/**
 * Got to make sure the connection order otherwise
 * it will hang
 * @param {object} nspSet contract
 * @param {string} publicNamespace like the name said
 * @return {array} namespaces in order
 */
export default function getNamespaceInOrder(nspSet, publicNamespace) {
  let names = []; // need to make sure the order!
  for (let namespace in nspSet) {
    if (namespace === publicNamespace) {
      names[1] = namespace;
    } else {
      names[0] = namespace;
    }
  }
  return names;
}
