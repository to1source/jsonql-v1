# jsonql-ws-base 

This module is the foundation for all the jsonql socket related code. 
This is NOT intend to use directly. 

Please checkout our main documentation site at [jsonql.org](https://jsonql.js.org)

---

Joel Chu (c) 2019 

Co-develop by NEWBRAN LTD UK and TO1SOURCE China 
