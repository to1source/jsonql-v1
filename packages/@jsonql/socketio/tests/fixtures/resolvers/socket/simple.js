// just simple send and process

/**
 * @param {number} i a number
 * @return {number} a number + 1;
 */
module.exports = function(i) {
  return ++i;
}
