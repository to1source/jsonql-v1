// this will grab the list of test files
// inject into the html and run the server
const glob = require('glob')
const { join, resolve } = require('path')
const wsServer = require('jsonql-ws-server')
const serverIoCore = require('server-io-core')

const baseDir = join(__dirname, 'files')
const appBaseDir = resolve(join(__dirname, '..', 'fixtures'))
const contractDir = join(appBaseDir, 'contract')
const resolverDir = join(appBaseDir, 'resolvers')
const contractAuthDir = join(contractDir, 'auth')
const keysDir = join(appBaseDir, 'keys')

const port = 8001;

const getConfig = (env) => {
  let opts = {
    contractDir,
    resolverDir,
    keysDir,
    hostname: `ws://localhost:${port}`,
  }

  let envOpts = {}
  switch (env) {
    case 'io':
      envOpts.serverType = 'socket.io';
    break;
    case 'hs':
      envOpts.serverType = 'socket.io';
      envOpts.enableAuth = true;
      envOpts.useJwt = true;
    break;
    case 'rt':
      envOpts.serverType = 'socket.io';
      envOpts.enableAuth = true;
      envOpts.useJwt = '12345678';
    break;
    case 'ws-auth':
      envOpts.enableAuth = true;
      envOpts.useJwt = true;
      envOpts.serverType = 'ws';
    break;
    case 'ws':
    default:
      envOpts.serverType = 'ws'
    break;
  }
  return Object.assign(opts, envOpts)
}

const wsServerSetup = (server) => {
  const wss = wsServer(getConfig(process.env.NODE_ENV), server)
  return wss; // not in use
}

const runQunit = (open = true) => {
  return new Promise((resolver, rejecter) => {
    glob(join(baseDir, '*-test.js'), function(err, files) {
      if (err) {
        console.log('FAILED!', err)
        return rejecter(err)
        // return process.exit();
      }
      // now start the server
      const { webserver, app, start } = serverIoCore({
        autoStart: false,
        port: port,
        webroot: [
          join(__dirname),
          join(__dirname, 'webroot'),
          join(__dirname, '..', '..', 'node_modules'),
          join(__dirname, '..', '..', 'dist')
        ],
        // open: open,
        // this will interface with jsonql-ws-server
        socket:false,
        reload: false,
        debugger: false,
        inject: {
          insertBefore: false,
          target: {
            body: files.map( file => file.replace(__dirname, '') )
          }
        }
      })
      wsServerSetup(webserver)
      // now we have a webserver then we could setup the jsonql-ws-server
      resolver({ webserver, app, start })
    })
  })
}

runQunit()
  .then(({ start }) => {
    start()
  })
