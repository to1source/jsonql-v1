// standard ws client test without auth
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const { JS_WS_SOCKET_IO_NAME, IO_ROUNDTRIP_LOGIN } = require('jsonql-constants')
const debug = require('debug')('jsonql-ws-client:test:io-rt-login')

const wsClient = require('../main')
const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))
const keysDir = join(__dirname, 'fixtures', 'keys')
const secret = '123456789';
const port = 8006;

test.before(async t => {
  const { io, app } = await serverSetup({
    contract,
    contractDir,
    keysDir,
    serverType: JS_WS_SOCKET_IO_NAME,
    enableAuth: true,
    useJwt: secret // <-- this is the secret
  })

  t.context.server = app.listen(port)

  t.context.client = await wsClient({
    serverType: JS_WS_SOCKET_IO_NAME,
    hostname: `ws://localhost:${port}`,
    contract: publicContract,
    enableAuth: true,
    useJwt: IO_ROUNDTRIP_LOGIN // <-- DON'T PASS THE SERVER SECRET!
  })
})

test.after(t => {
  t.context.server.close()
})


test.serial.cb('It should able to connect to the socket.io server public namespace', t => {
  t.plan(1)
  let client = t.context.client;
  client.pinging('Hello')
  client.pinging.onResult = (res) => {
      debug('res', res)
      t.is(res, 'connection established')
      t.end()
    }
})

test.serial.cb.skip('It should not able to connect to the socket.io private namespace at this point', t => {
  t.plan(1)
  let client = t.context.client;
  client.sendExtraMsg(100)
  // @TODO this one need to take a look at 
  client.sendExtraMsg.onError = error => {
      debug('NOT_LOGIN_ERR_MSG', NOT_LOGIN_ERR_MSG, error.message)
      t.is(NOT_LOGIN_ERR_MSG, error.message)
      t.end()
    }
})

test.serial.cb('It should able to call the login method and establish socket.io connection using the round trip method', t => {
  t.plan(1)
  let client = t.context.client;
  const payload = {name: 'Joel'}
  const token = genToken(payload, secret)
  debug('call login', token)

  client.login(token)

  // add onReady and wait for the login to happen

  client.onReady = function(namespace) {
    debug('--> onReady', namespace)
    if (namespace === 'jsonql/private') {
      client.sendExtraMsg(200)
      client.sendExtraMsg.onResult = (result) => {
          debug('login success', result)
          t.is(201, result)
          t.end()
        }
      // this should work
      // but this will happen before the above Promise.resolve
      client.sendExtraMsg.onMessage = function(result) {
        debug('onMessge got news', result)
      }
    }
  }
})
