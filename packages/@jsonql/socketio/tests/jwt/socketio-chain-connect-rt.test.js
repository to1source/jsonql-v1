// testing the socket.io auth with client and server
const test = require('ava')
const http = require('http')
const socketIo = require('socket.io')
// const socketIoClient = require('socket.io-client')
const debug = require('debug')('jsonql-jwt:test:socketio-auth')
// import server
const {
  socketIoJwtAuth,
  socketIoNodeLogin,
  socketIoNodeClientAsync
} = require('../main')
// import data
const { secret, token, namespace, msg } = require('./fixtures/options')

const namespace1 = '/' + namespace + '/a';
const namespace2 = '/' + namespace + '/b';
const port = 3004;

const url = `http://localhost:${port}`;

test.before( t => {
  // setup server
  const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.write('Hello world!')
    res.end()
  })
  t.context.server = server;
  // setup socket.io
  const io = socketIo(server)
  const nsp1 = io.of(namespace1)
  const nsp2 = io.of(namespace2)

  t.context.nsp1 = socketIoJwtAuth(nsp1, { secret })
  // doesn't require login
  t.context.nsp2 = nsp2;

  t.context.server.listen(port)
})

test.after( t => {
  t.context.server.close()
})


test.cb.skip('It should able to connect to a public namespace using the socketIoNodeClientAsync', t => {
  t.plan(1)
  t.pass()
  t.end()
})
