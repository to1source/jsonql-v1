// basic io client test without auth

const test = require('ava')

const { JS_WS_SOCKET_IO_NAME } = require('jsonql-constants')
const wsClient = require('../main')
const serverSetup = require('./fixtures/server-setup')

const { join } = require('path')
const fsx = require('fs-extra')

const publicContract = fsx.readJsonSync(join(__dirname, 'fixtures', 'contract', 'public-contract.json'))
const contract = fsx.readJsonSync(join(__dirname, 'fixtures', 'contract', 'contract.json'))

const debug = require('debug')('jsonql-ws-client:test:io')

const port = 8004;

test.before(async t => {

  const { app } = await serverSetup({
    contract,
    serverType: JS_WS_SOCKET_IO_NAME
  })

  t.context.server = app.listen(port)

  t.context.client = await wsClient({
    hostname: `http://localhost:${port}`,
    serverType: JS_WS_SOCKET_IO_NAME,
    contract: publicContract
  })
})

test.after(async t => {
  t.context.server.close();
});

test.cb('It should able to connect to the socket.io server', t => {

  t.plan(2)
  let client = t.context.client

  t.truthy(wsClient.CLIENT_TYPE_INFO)

  client.simple(100)

  client.simple.onResult = (result) => {
    t.is(101, result)
    t.end()
  }
})

test.cb('It should able to send message back while its talking to the server', t => {
  t.plan(1)
  let c = 0;
  let client = t.context.client;
  client.continuous('Jumping')
  client.continuous.onResult = (msg) => {
      ++c;
      debug(c)
      debug('onResult', c , msg)
    }
  // add a event handler
  client.continuous.onMessage = function(msg) {
    ++c;
    debug(c)
    if (c === 3) {
      debug(c)
      client.continuous.send = 'terminate'
      t.pass()
      t.end()
    }
    debug('onMessage', c , msg)
  }
})
