// server side auth with jwt
const socketioJwt = require('socketio-jwt')
const { JsonqlValidationError } = require('jsonql-errors')
const { isObject } = require('jsonql-params-validator')
const { DEFAULT_WS_WAIT_TIME } = require('jsonql-constants')

/**
 * The actual call
 * @param {object} io io or nsp instance with the on method
 * @param {object} config configuration
 * @param {function} onAuthenticated callback
 */
function socketIoJwtAuthAction(io , config, onAuthenticated, onUnauthorized) {
  io
    .on('connection', socketioJwt.authorize(config))
    .on('authenticated', onAuthenticated)
    .on('unauthorized', onUnauthorized)
}

/**
 * porting the auth server over - this is a server side cjs call back
 * @param {object} io io or nsp instance with the on method
 * @param {object} config configuration
 * @param {function} [onAuthenticated=false] callback
 * @param {functoin} [onUnauthorized= false] callback
 * @return {object|void} if not provide a callback then return a promise
 */
module.exports = function socketIoJwtAuth(io , config = {}, onAuthenticated = false, onUnauthorized = false) {
  if (!isObject(config)) {
    throw new JsonqlValidationError(`config is not object format!`)
  }
  const opts = Object.assign({}, { timeout: DEFAULT_WS_WAIT_TIME }, config)
  if (!onAuthenticated && !onUnauthorized) {
    return new Promise((resolver, rejecter) => {
      socketIoJwtAuthAction(io, opts, socket => {
        resolver(socket)
      }, rejecter)
    })
  }
  if (typeof onAuthenticated !== 'function') {
    throw new JsonqlValidationError(`callback supplied is not a function!`)
  }
  if (!onUnauthorized) {
    onUnauthorized = () => {
      console.error('onUnauthorized')
    }
  }
  socketIoJwtAuthAction(io, opts, onAuthenticated, onUnauthorized)
}
