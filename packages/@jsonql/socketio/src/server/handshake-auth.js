const socketioJwt = require('socketio-jwt')
const { JsonqlValidationError } = require('jsonql-errors')
const { isObject } = require('jsonql-params-validator')
const debug = require('debug')('jsonql-jwt:handshake-auth')
/**
 * Server side auth when handshake
 * @param {object} io the socket io or nsp instance
 * @param {object} config configuration
 * @return {object} return the io back
 */
module.exports = function socketIoHandshakeAuth(io, config = {}) {
  if (!io || !io.use) {
    throw new JsonqlValidationError(`The io object supplied doesn't have use method!`)
  }
  if (!isObject(config)) {
    throw new JsonqlValidationError(`The supplied config is not object format!`)
  }
  const opts = Object.assign({}, config, { handshake: true })
  debug('configuration', opts)
  io.use(socketioJwt.authorize(opts))
  return io;
}
