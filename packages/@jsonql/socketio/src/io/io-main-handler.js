import {
  ON_MESSAGE_PROP_NAME,
  ON_RESULT_PROP_NAME,
  EMIT_EVT
} from '../utils/constants'
import {
  ON_ERROR_PROP_NAME,
  LOGOUT_EVENT_NAME,
  ERROR_TYPE,
  ERROR_KEY,
  DATA_KEY,
  ON_READY_PROP_NAME
} from 'jsonql-constants'
import { isObjectHasKey } from 'jsonql-params-validator'
import { getDebug, createEvt, formatPayload } from '../utils'

const debugFn = getDebug('io-main-handler')

/**
 * @param {object} ee Event Emitter
 * @param {string} namespace namespace of this nsp
 * @param {string} resolverName resolver to handle this call
 * @return {function} capture the result
 */
const resultHandler = (ee, namespace, resolverName, evt = ON_RESULT_PROP_NAME) => {
  return (result) => {
    ee.$trigger(createEvt(namespace, resolverName, evt), [result])
  }
}

/**
 * @param {object} nspSet resolver list
 * @param {object} nsp nsp instance
 * @param {object} ee Event Emitter
 * @param {string} namespace name of this nsp
 * @return {void}
 */
const createResolverListener = (nspSet, nsp, ee, namespace) => {
  for (let resolverName in nspSet) {
    nsp.on(
      resolverName,
      resultHandler(ee, namespace, resolverName, ON_MESSAGE_PROP_NAME)
    )
  }
}

/**
 * @param {object} nsp instance
 * @param {object} ee Event Emitter
 * @param {string} namespace name of this nsp
 * @return {void}
 */
const mainEventHandler = (nsp, ee, namespace) => {
  ee.$only(
    createEvt(namespace, EMIT_EVT),
    function resolverEmitHandler(resolverName, args) {
      debugFn('mainEventHandler', resolverName, args)
      nsp.emit(
        resolverName,
        formatPayload(args),
        resultHandler(ee, namespace, resolverName)
      )
    }
  )
}

/**
 * it makes no different at this point if we know its connection establish or not
 * We should actually know this before hand before we call here
 * @param {string} namespace of this client
 * @param {object} socket this is the resolved nsp connection object
 * @param {object} ee Event Emitter
 * @param {object} nspSet the list of resolvers
 * @param {object} opts configuration
 */
export default function ioMainHandler(namespace, socket, ee, nspSet, opts) {
  // the main listener for all the client resolvers
  mainEventHandler(socket, ee, namespace)
  // it doesn't make much different between inside the connect or not
  // loop through to create the listeners
  createResolverListener(nspSet, socket, ee, namespace)
  //@TODO next we need to add a ERROR handler
  // The server side is not implementing a global ERROR call yet
  // and the result or message error will be handle individually by their callback
  // listen to the server close event
  socket.on('disconnect', function disconnect() {
    debugFn('io.disconnect')
    // TBC what to do with this
    // ee.$trigger(LOGOUT_EVENT_NAME, [namespace])
  })
  // listen to the logout event
  ee.$on(LOGOUT_EVENT_NAME, function logoutHandler() {
    try {
      debugFn('terminate ws connection')
      socket.close()
    } catch(e) {
      debugFn('terminate ws error', e)
    }
  })
  // the last one to fire
  ee.$trigger(ON_READY_PROP_NAME, namespace)
}
