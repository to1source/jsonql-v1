// socket.io server setup
const socketIo = require('socket.io')
const { socketIoHandshakeAuth } = require('jsonql-jwt')
const { RSA_ALGO } = require('jsonql-constants')

const { getNamespace, getDebug } = require('../share/helpers')
const debug = getDebug('create-socket.io')


/**
 * @TODO need to check how to support the com style verification
 * @param {object} config options
 * @param {object} server the http server
 * @return {object} ws server instance with namespace as key
 */
module.exports = function(config, server) {
  // init the io instance
  const io = socketIo(server)
  const namespaces = getNamespace(config)
  let added = false;
  // always use a namespace
  return namespaces.map((namespace, i) => {
    // this is different from WebSocket start with a slash
    let nsp = io.of('/' + namespace)
    if (config.enableAuth && config.publicKey && i > 0) {
      // if they use the config.secret then
      // we init the socketIoJwtAuth later
      nsp = socketIoHandshakeAuth(nsp, {
        timeout: config.authTimeout,
        secret: config.publicKey,
        algorithms: RSA_ALGO
      })
      debug(`create nsp using socketIoHandshakeAuth`)
    }
    // return the key
    return { [namespace]: nsp }
  })
  .reduce((last, next) => Object.assign(last, next), {});
}
