// this will combine two namespaces and chain them together in a promises chain
// to ensure the auth connection established first
import { chainPromises } from 'jsonql-utils'
import { IO_ROUNDTRIP_LOGIN, IO_HANDSHAKE_LOGIN } from 'jsonql-constants'
import { JsonqlValidationError } from 'jsonql-errors'
import { isObject } from 'jsonql-params-validator'

import {
  socketIoHandshakeLogin,
  socketIoClientAsync
} from './handshake-login'
import socketIoRoundtripLogin from './roundtrip-login'

/**
 * Type of client
 * @param {string} type for checking
 * @return {function} or throw error
 */
function getAuthClient(type) {
  console.info('client type: ', type)
  switch (type) {
    case IO_ROUNDTRIP_LOGIN:
      return socketIoRoundtripLogin;
    case IO_HANDSHAKE_LOGIN:
      return socketIoHandshakeLogin;
    default:
      throw new JsonqlValidationError('socketIoChainConnect', {message: `Unknown ${type} of client!`})
  }
}

/**
 * break it out to make it easier to read
 * @param {number} idx index
 * @param {string} baseUrl same as below
 * @param {array} namespaces same as below
 * @param {string} token for validation
 * @param {array} options same as below
 * @return {array} of args
 */
function getArgs(idx, baseUrl, namespaces, token, options) {
  let args = [
    [baseUrl, namespaces[idx] ].join('') // the way the io namespace require a slash at the beginning
  ]
  if (idx === 0) {
    args.push(token)
  }
  if (isObject(options[idx])) {
    args.push(options[idx])
  }
  return args;
}

/**
 * @param {object} io socket.io-client
 * @param {string} baseUrl to connect
 * @param {array} namespaces to append to baseUrl
 * @param {string} token for validation
 * @param {array} options passing to the clients
 * @param {string} [ type = IO_HANDSHAKE_LOGIN ] of client to use
 * @return {object} promise resolved n*nsps in order
 */
export default function socketIoChainConnect(io, baseUrl, namespaces, token, type = IO_HANDSHAKE_LOGIN, options = []) {
  // we expect the order is auth url first
  const clients = [ getAuthClient(type), socketIoClientAsync ]
  return chainPromises(
    clients.map( (client, i) => {
      let args = getArgs(i, baseUrl, namespaces, token, options)
      console.info(i, args, clients[i])
      return Reflect.apply(clients[i], null, [io, ...args])
    })
  )
}
