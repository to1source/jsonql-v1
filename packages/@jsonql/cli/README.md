# @jsonql/cli

This is an interactive command line client to setup project,
also comes with a command line interface to access a jsonql server.

## Installation

You **really should** install this globally:

```sh
$ npm install --global @jsonql/cli
```

## jsonql

This is the setup tool

```sh
$ jsonql
```

## jsonql-cli

This is the command line client

```sh
$ jsonql-cli http://host-to-your-jsonql
```
