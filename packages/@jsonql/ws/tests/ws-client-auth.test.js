// standard ws client test without auth
const test = require('ava')
const EventClass = require('@to1source/event')
const modulePath = process.env.JSONQL_DEBUG ? '../node-module' : '../node-ws-client'
const wsClient = require(modulePath)

const serverSetup = require('./fixtures/server-setup')

const { join } = require('path')
const fsx = require('fs-extra')

const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))

const _debug = require('debug')('jsonql-ws-client:test:ws-auth')
const colors = require('colors/safe')

const debug = (str, ...args) => {
  Reflect.apply(_debug, null, [colors.black.bgBrightMagenta(str+'')].concat(args))
}

const localDebug = (str, ...args) => {
  Reflect.apply(debug, null, [ colors.black.bgBrightGreen(str+'') ].concat(args) )
}

const payload = {name: 'Joel'}
const token = genToken(payload)
const port = 8003

test.before(async t => {

  const { app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    serverType: 'ws',
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })
  t.context.server = app.listen(port)

  t.context.client = await wsClient({
    token,
    hostname: `ws://localhost:${port}`,
    contract: publicContract,
    enableAuth: true
  }, { log: debug }) // debug

  t.context.evt = new EventClass()
})

test.after(t => {
  t.context.server.close()
})

// @NOTE here might be the problem, what we might have to do is to suspend all the
// event in the queue, call login, or trigger by the host module to login
// then after the login, we release the queue
test.serial.cb(`Should able to handle multiple onMessage and one onLogin callback`, t => {
  t.plan(3)
  const evt = t.context.evt
  let ctn = 0
  evt.$on('run', function() {
    ++ctn
    if (ctn > 2) {
      t.end()
    }
  })

  let client = t.context.client

  client.onReady = function onReadyCallback(w) {
    localDebug('onReady -->', w)
    t.pass()
    evt.$trigger('run', [])
  }

  client.onReady = function onReadyAgainCallback(w) {
    localDebug('onReady again SHOULD NOT GET FIRE', w)
  }

  client.onLogin = function onLoginCallback(n) {
    localDebug(`onLogin -->`, n)
    t.pass()
    evt.$trigger('run', [])
  }

})


test.serial.cb('It should able to connect to the WebSocket public namespace', t => {
  let ctn = 2
  t.plan(ctn)
  let client = t.context.client
  const evtName = 'run-1'
  const evt = t.context.evt

  evt.$on(evtName, function() {
    --ctn
    if (ctn === 0) {
      t.end()
    }
  })

  client.pinging('Hello')

  client.pinging.onResult = res => {
      localDebug('res', res)
      t.is(res, 'connection established')

      evt.$trigger(evtName, [])

      client.pinging.send('ping')
    }
  // start the ping pong game
  client.pinging.onMessage = function(msg) {
    if (msg==='pong') {
      client.pinging.send('pong')
    } else {
      client.pinging.send('giving up')
      t.is(msg, 'ping')

      evt.$trigger(evtName, [])
    }
  }
})

test.serial.cb('It should able to connect to a WebSocket private namespace', t => {

  let ctn = 2
  t.plan(ctn)
  let client = t.context.client
  const evtName = 'run-2'
  const evt = t.context.evt

  const add = () => evt.$trigger(evtName, [])

  evt.$on(evtName, function() {
    --ctn
    if (ctn === 0) {
      t.end()
    }
  })

  // run test

  client.sendExtraMsg(100)

  client.sendExtraMsg.onResult = function onResultCb(result) {
    t.is(101, result)
    add()
  }

  client.sendExtraMsg.onMessage = function onMessageCb(num) {
    t.is(102, num)
    add()
  }

})
