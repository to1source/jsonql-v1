// just for debugging purposes

// since both the ws and io version are
// pre-defined in the client-generator
// and this one will have the same parameters
// and the callback is identical
const debugFn = console.info
/**
 * wrapper method to create a nsp without login
 * @param {string|boolean} namespace namespace url could be false
 * @param {object} opts configuration
 * @return {object} ws client instance
 */
export function createNspClient(namespace, opts) {
  const { wssPath, wsOptions, hostname } = opts;
  const url = namespace ? [hostname, namespace].join('/') : wssPath;
  debugFn('nspClient url', url)
  // the wsOptions is the BUG!
  return opts.nspClient(url, wsOptions)
}

/**
 * wrapper method to create a nsp with token auth
 * @param {string} namespace namespace url
 * @param {object} opts configuration
 * @return {object} ws client instance
 */
export function createNspAuthClient(namespace, opts) {
  const { wssPath, token, wsOptions, hostname } = opts;
  const url = namespace ? [hostname, namespace].join('/') : wssPath;
  debugFn('nspAuthClient url', url)
  return opts.nspAuthClient(url, token, wsOptions)
}
