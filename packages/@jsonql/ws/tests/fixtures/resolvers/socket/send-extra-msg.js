// this one will use the property send to send an different message


/**
 * in the mean time we send extra message back first
 * @param {number} x a number for process
 * @return {number} x + ?
 */
module.exports = function sendExtraMsg(x) {
  sendExtraMsg.send(x + 2)
  
  return x + 1
}
