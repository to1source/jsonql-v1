// this is a public method always avaialble
const debug = require('debug')('jsonql-ws-client:socket:pinging')
const colors = require('colors/safe')

const log = (...args) => {
  Reflect.apply(debug, null, [colors.white.bgCyan('=== SEND ==='), ...args])
}

let ctn = 0
/**
 * @param {string} msg message
 * @return {string} reply message based on your message
 */
module.exports = function pinging(msg) {
  log(ctn, msg)
  if (ctn > 0) {
    switch (msg) {
      case 'ping':
        log('pinging.send.pong')
        pinging.send('pong')
      case 'pong':
        log('pinging.send.ping')
        pinging.send('ping')
      default:
        log('nothing')
        return
        //pinging.send = 'You lose!';
    }
  }
  ++ctn
  return 'connection established'
}
