// this will keep sending out message until received a terminate call
let timer
let ctn = 0


/**
 * @param {string} msg a message
 * @return {string} a message with timestamp
 */
module.exports = function continuous(msg) {
  // use the send setter instead
  timer = setInterval(() => {
    const ts = Date.now()
    console.log('---------> calling send with <------------', ts)
    continuous.send(msg + ` [${++ctn}] ${ts}`)
  }, 500)

  if (msg === 'terminate') {
    return clearInterval(timer)
  }

  // return result
  return Promise.resolve(`=========================== start at ${Date.now()} =============================`)
}
