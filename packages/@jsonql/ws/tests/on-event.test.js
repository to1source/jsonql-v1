// take the onlogin onReady event out test on it's own 
const test = require('ava')

const { join } = require('path')
const fsx = require('fs-extra')
const rainbow = require('./fixtures/rainbow')
const wsClient = require('../node-ws-client')
const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')
const EventClass = require('@to1source/event')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))

/// const { NOT_LOGIN_ERR_MSG } = require('jsonql-constants')

const _debug = require('debug')
const colors = require('colors/safe')

const debug = (str, ...args) => {
  const fn = _debug('jsonql-ws-client:test:on-event')

  Reflect.apply(fn , null, [colors.blue.bgBrightGreen(str), ...args])
}

const payload = {name: 'Jack'}
const token = genToken(payload)
const port = 8001

test.before(async t => {

  t.context.tmpEvt = new EventClass()

  const { app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })
  t.context.server = app.listen(port)
  // without the token
  t.context.client = await wsClient({
    token,
    hostname: `ws://localhost:${port}`,
    contract: publicContract,
    enableAuth: true
  }, { 
    log: () => {} //debug 
  })
})

test.after(t => {
  t.context.server.close()
})


// this login method need to redo because there is no login call here
test.serial.cb('It should trigger the login call here', t => {
  const plan = 3
  
  t.plan(plan)
  
  const tmpEvt = t.context.tmpEvt
  const add = () => tmpEvt.$trigger('add')
  const client = t.context.client
  let ctn = 0
  
  tmpEvt.$on('add', () => {
    ++ctn 
    if (ctn >= plan) {
      t.end()
    }
  })

  // rainbow('Just try to see if this works at all?')

  client.onLogin = function onLoginCallback(namespace) {
    rainbow('onLogin -->', namespace)
    add()
    t.pass()
  }

  // add onReady and wait for the login to happen
  client.onReady = function testTwoOnReadyCallback(namespace) {
    rainbow('client.onReady -->', namespace, client.simple.myNamespace)
    
    if (namespace === client.simple.myNamespace) {

      client.simple(200)
      
      client.simple.onResult = function simpleOnResultCallback(result) {
        debug('simple onResult pass', result)
        t.pass()
        add()
      }
      // try to call the static method 
      client.simple.send(1000)
      // this was never call because the resolver never use the send() method
      client.simple.onMessage = function simpleOnMessageCallback(result) {
        debug(`simple onMessage`, result)
        t.pass()
        add()
      }
    }
  }

})