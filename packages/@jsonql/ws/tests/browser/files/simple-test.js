
QUnit.test('It should able to use the client to contact the server with static contract', function(assert) {
  var done1 = assert.async()

  var jclient = jsonqlWsClient({
    hostname: 'http://localhost:8101',
    contract: contract
  }).catch(function(error) {
    console.error('init error', error)
  })

  jclient.then(function(client) {

    console.log('client', client)

    client.simple(1)

    client.simple.onResult = function(result) {
      console.log('result', 2)
      assert.equal(2, result, "Hello world test done")
      done1()
    }
  })
})
