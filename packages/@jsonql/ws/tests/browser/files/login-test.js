// QUnit.config.autostart = false

QUnit.test('It should able to connect to the auth enabled server', function(assert) {
  // var plan = 2
  var done = assert.async()
  /*
  for (let i = 0; i < plan; ++i) {
    done.push(assert.async())
  }
  */
  var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9lbCIsImlhdCI6MTU3MTkwODg5N30.Cp53a29nuFCKjEqUT8MmO2pAKsUVOHogBLuiGg-WOYetqgW48HvEudpO1KdBnZMzJPQcbdqVHVQlhv720GaYfE33ZOgC6o37LTTrPPKzXgynJ9kTfWI-Mct28teKN7SMKl2rACHn5OBogFkUE1zDB4wADoF77ClgnhehXkv-XlM"
  
  jsonqlWsClient({
    // debugOn: true,
    hostname: 'http://localhost:8103',
    enableAuth: true,
    contract: authContract
  })
  .then(client => {
    // const { eventEmitter } = client

    // at this point it should only have a public nsp connected 
    client.onReady = function(namespace) {
      console.info(namespace, Date.now())
      
      assert.equal('jsonql/public', namespace, "Expect ONLY public namespace")
      // done[0]()
      setTimeout(function() {
        client.login(token)
      }, 500)
    }
    // @TODO next get the eventEmitter and trigger a login 
    
    // console.info('client', client)

    //eventEmitter.$trigger('__login__', token)
    let ctn = 0

    /**
     * @TODO @NOTE 
     * Aftering calling the login, we only expect this onLogin get call once 
     * but this is not the case, it get call twice, therefore we need to 
     * go back and look at the hold event process, and when exactly the onLogin get trigger 
     */
    client.onLogin = function() {
      console.log(`onLogin`, Date.now())
      assert.ok(true)

      ++ctn 
      if (ctn > 1) {
        done()
      }

    }


  })



})