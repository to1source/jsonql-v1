;(function() {
  // testing the QUnit.log method here
  // also testing the debug here
  var errors = [], i = 0;

  QUnit.log(function(res){
    i++;
    if (!res || !res.result){
      // Failure:
      errors.push(res)
    }
    if (i%50 == 0){
      var data = {
        tests_run: i,
        tracebacks: errors,
        url : window.location.pathname
      }
      errors = [];
    }
  })

  QUnit.done(function(results){
    results.tracebacks = errors;
    results.url = window.location.pathname;

    console.info('QUnit done', results)
  })

})();
