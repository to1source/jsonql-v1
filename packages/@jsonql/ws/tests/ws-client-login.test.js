// need to test the way how it suppose to connect to public interface first 
// then we trigger a login and have the and ONLY the onLogin trigger next 
// then we need to try to logout and then log back in again to see what 
// is the effect of the logout 

const test = require('ava')
const EventClass = require('@to1source/event')
const modulePath = process.env.JSONQL_DEBUG ? '../node-module' : '../node-ws-client'
const wsClient = require(modulePath)

const serverSetup = require('./fixtures/server-setup')

const { join } = require('path')
const fsx = require('fs-extra')

const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))

const _debug = require('debug')('jsonql-ws-client:test:ws-auth')
const colors = require('colors/safe')

const debug = (str, ...args) => {
  Reflect.apply(_debug, null, [colors.black.bgBrightMagenta(str+'')].concat(args))
}

const localDebug = (str, ...args) => {
  Reflect.apply(debug, null, [ colors.black.bgBrightGreen(str+'') ].concat(args) )
}

const payload = {name: 'Joel'}
const token = genToken(payload)
const port = 8006

test.before(async t => {

  const { app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    serverType: 'ws',
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })
  t.context.server = app.listen(port)

  t.context.client = await wsClient({
    token,
    hostname: `ws://localhost:${port}`,
    contract: publicContract,
    enableAuth: true
  }, { log: debug }) // debug

  t.context.evt = new EventClass()
})

test.after(t => {
  t.context.server.close()
})


test.serial.cb(`Should have a onReady event fired`, t => {
  const client = t.context.client 
  client.onReady(namespace => {
    localDebug('isLogin?', client.isLogin, namespace)
    t.is('jsonql/public', namespace)
    t.end()
  }) 
})