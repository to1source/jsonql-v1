// this is a new interface that takes the already check config 
// and init the ws client
// this is the new interface that will create the combine client 
import {
  createCombineWsClientConstructor,
  // straight export
  checkSocketClientType,
  getEventEmitter, 
  EventEmitterClass
} from './src/create-combine-client'
import { setupSocketClientListener } from './src/core/setup-socket-client-listener'

const createCombineWsClient = createCombineWsClientConstructor(setupSocketClientListener)

export {
  checkSocketClientType,
  getEventEmitter, 
  EventEmitterClass,

  createCombineWsClient
}

