/**
 * Rollup config for browser version of the
 */
import { join } from 'path'

// import buble from '@rollup/plugin-buble'
import replace from '@rollup/plugin-replace'
import commonjs from '@rollup/plugin-commonjs'
import json from '@rollup/plugin-json'
import nodeResolve from '@rollup/plugin-node-resolve'

import { terser } from "rollup-plugin-terser"
import size from 'rollup-plugin-bundle-size'

import babel from 'rollup-plugin-babel'

/*
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
// support async functions
import async from 'rollup-plugin-async'
// get the version info
*/

import { version } from './package.json'

const env = process.env.NODE_ENV
const target = process.env.TARGET

let plugins = [
  commonjs({
    include: 'node_modules/**'
  }),
  nodeResolve(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__PLACEHOLDER__': `version: ${version} module: ${target}`
  }),
  json({
    preferConst: true
  }),
  babel({
    exclude: 'node_modules/**'
  })
  // buble()
  // nodeGlobals(),
  // builtins(),
  // async(),
]

if (env === 'production') {
  plugins.push( terser() )
}
plugins.push( size() )

let inFile, outFile, _target 

switch (target) {
  case 'cjs':
    inFile = 'src/node-ws-client.js'
    outFile = join(__dirname, 'node-ws-client.js')
    _target = 'cjs'
    break
  case 'cjs-module':
    inFile = 'src/node/module.js'
    outFile = join(__dirname, 'node-module.js')
    _target = 'cjs'
    break   
  case 'umd':
  default:
    inFile = 'index.js'
    outFile = join(__dirname, 'dist', `jsonql-ws-client.${target}.js`)
    _target = 'umd'
    break 
}

let config = {
  input: join(__dirname, inFile),
  output: {
    name: 'jsonqlWsClient',
    file: outFile,
    format: _target,
    sourcemap: true,
    globals: {
      'path': 'path',
      'fs': 'fs',
      'WebSocket': 'ws',
      'socket.io-client': 'io',
      'promise-polyfill': 'Promise',
      'debug': 'debug'
    }
  },
  external: [
    'path',
    'fs',
    'ws',
    'WebSocket',
    'socket.io-client',
    'io',
    'debug',
    'Promise',
    'promise-polyfill'
  ],
  plugins: plugins
};

export default config;
