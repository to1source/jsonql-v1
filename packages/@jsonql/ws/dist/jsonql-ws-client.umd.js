(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('jsonql-constants'), require('jsonql-utils/module'), require('jsonql-errors'), require('jsonql-params-validator'), require('@to1source/event'), require('jsonql-utils/src/generic'), require('jsonql-utils/src/obj-define-props'), require('jsonql-errors/src/validation-error'), require('jsonql-utils/src/namespace'), require('@jsonql/security'), require('js-cookie'), require('jsonql-utils/src/chain-promises')) :
  typeof define === 'function' && define.amd ? define(['jsonql-constants', 'jsonql-utils/module', 'jsonql-errors', 'jsonql-params-validator', '@to1source/event', 'jsonql-utils/src/generic', 'jsonql-utils/src/obj-define-props', 'jsonql-errors/src/validation-error', 'jsonql-utils/src/namespace', '@jsonql/security', 'js-cookie', 'jsonql-utils/src/chain-promises'], factory) :
  (global = global || self, global.jsonqlWsClient = factory(global.jsonqlConstants, global.module, global.jsonqlErrors, global.jsonqlParamsValidator, global.EventEmitterClass, global.generic, global.objDefineProps, global.JsonqlValidationError, global.namespace, global.security, global.cookies, global.chainPromises));
}(this, (function (jsonqlConstants, module, jsonqlErrors, jsonqlParamsValidator, EventEmitterClass, generic, objDefineProps, JsonqlValidationError, namespace, security, cookies, chainPromises) { 'use strict';

  EventEmitterClass = EventEmitterClass && Object.prototype.hasOwnProperty.call(EventEmitterClass, 'default') ? EventEmitterClass['default'] : EventEmitterClass;
  JsonqlValidationError = JsonqlValidationError && Object.prototype.hasOwnProperty.call(JsonqlValidationError, 'default') ? JsonqlValidationError['default'] : JsonqlValidationError;
  cookies = cookies && Object.prototype.hasOwnProperty.call(cookies, 'default') ? cookies['default'] : cookies;

  // this will be part of the init client sequence
  var CSRF_HEADER_NOT_EXIST_ERR = 'CSRF header is not in the received payload';

  /**
   * Util method 
   * @param {string} payload return from server
   * @return {object} the useful bit 
   */
  function extractSrvPayload(payload) {
    var json = module.toJson(payload);
    
    if (json && typeof json === 'object') {
      // note this method expect the json.data inside
      return module.extractWsPayload(json)
    }
    
    throw new jsonqlErrors.JsonqlError('extractSrvPayload', json)
  }

  /**
   * call the server to get a csrf token 
   * @return {string} formatted payload to send to the server 
   */
  function createInitPing() {
    var ts = module.timestamp();

    return module.createQueryStr(jsonqlConstants.INTERCOM_RESOLVER_NAME, [jsonqlConstants.SOCKET_PING_EVENT_NAME, ts])
  }

  /**
   * Take the raw on.message result back then decoded it 
   * @param {*} payload the raw result from server
   * @return {object} the csrf payload
   */
  function extractPingResult(payload) {
    var obj;

    var result = extractSrvPayload(payload);
    
    if (result && result[jsonqlConstants.DATA_KEY] && result[jsonqlConstants.DATA_KEY][jsonqlConstants.CSRF_HEADER_KEY]) {
      return ( obj = {}, obj[jsonqlConstants.HEADERS_KEY] = result[jsonqlConstants.DATA_KEY], obj )
    }

    throw new jsonqlErrors.JsonqlError('extractPingResult', CSRF_HEADER_NOT_EXIST_ERR)
  }


  /**
   * Create a generic intercom method
   * @param {string} type the event type 
   * @param {array} args if any 
   * @return {string} formatted payload to send
   */
  function createIntercomPayload(type) {
    var args = [], len = arguments.length - 1;
    while ( len-- > 0 ) args[ len ] = arguments[ len + 1 ];

    var ts = module.timestamp();
    var payload = [type].concat(args);
    payload.push(ts);
    return module.createQueryStr(jsonqlConstants.INTERCOM_RESOLVER_NAME, payload)
  }

  // move the get logger stuff here

  // it does nothing
  var dummyLogger = function () {};

  /**
   * re-use the debugOn prop to control this log method
   * @param {object} opts configuration
   * @return {function} the log function
   */
  var getLogger = function (opts) {
    var debugOn = opts.debugOn;  
    if (debugOn) {
      return function () {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        Reflect.apply(console.info, console, ['[jsonql-ws-client-core]' ].concat( args));
      }
    }
    return dummyLogger
  };

  /**
   * Make sure there is a log method
   * @param {object} opts configuration
   * @return {object} opts
   */
  var getLogFn = function (opts) {
    var log = opts.log; // 1.3.9 if we pass a log method here then we use this
    if (!log || typeof log !== 'function') {
      return getLogger(opts)
    }
    opts.log('---> getLogFn user supplied log function <---', opts);
    return log
  };

  // this will generate a event emitter and will be use everywhere
  // create a clone version so we know which one we actually is using
  var JsonqlWsEvt = /*@__PURE__*/(function (EventEmitterClass) {
    function JsonqlWsEvt(logger) {
      if (typeof logger !== 'function') {
        throw new Error("Just die here the logger is not a function!")
      }
      logger("---> Create a new EventEmitter <---");
      // this ee will always come with the logger
      // because we should take the ee from the configuration
      EventEmitterClass.call(this, { logger: logger });
    }

    if ( EventEmitterClass ) JsonqlWsEvt.__proto__ = EventEmitterClass;
    JsonqlWsEvt.prototype = Object.create( EventEmitterClass && EventEmitterClass.prototype );
    JsonqlWsEvt.prototype.constructor = JsonqlWsEvt;

    var prototypeAccessors = { name: { configurable: true } };

    prototypeAccessors.name.get = function () {
      return 'jsonql-ws-client-core'
    };

    Object.defineProperties( JsonqlWsEvt.prototype, prototypeAccessors );

    return JsonqlWsEvt;
  }(EventEmitterClass));

  /**
   * getting the event emitter
   * @param {object} opts configuration
   * @return {object} the event emitter instance
   */
  var getEventEmitter = function (opts) {
    var log = opts.log;
    var eventEmitter = opts.eventEmitter;
    
    if (eventEmitter) {
      log("eventEmitter is:", eventEmitter.name);
      return eventEmitter
    }
    
    return new JsonqlWsEvt( opts.log )
  };

  // group all the small functions here

  /**
   * WebSocket is strict about the path, therefore we need to make sure before it goes in
   * @param {string} url input url
   * @return {string} url with correct path name
   */
  var fixWss = function (url) {
    var pattern = new RegExp('^(http|https)\:\/\/', 'i');
    var result = url.match(pattern);
    if (result && result.length) {
      var target = result[1].toLowerCase();
      var replacement = target === 'https' ? 'wss' : 'ws';
      
      return url.replace(pattern, replacement + '://')
    }

    return url 
  };


  /**
   * get a stock host name from browser
   */
  var getHostName = function () {
    try {
      return [window.location.protocol, window.location.host].join('//')
    } catch(e) {
      throw new JsonqlValidationError(e)
    }
  };

  /**
   * Unbind the event, this should only get call when it's disconnected
   * @param {object} ee EventEmitter
   * @param {string} namespace
   * @return {void}
   */
  var clearMainEmitEvt = function (ee, namespace) {
    var nsps = generic.toArray(namespace);
    nsps.forEach(function (n) {
      ee.$off(generic.createEvt(n, jsonqlConstants.EMIT_REPLY_TYPE));
    });
  };

  /**
   * Setup the IS_LOGIN_PROP_KEY IS_READY_PROP_KEY 
   * @param {*} client 
   * @return {*} client 
   */
  function setupStatePropKeys(client) {
    return [jsonqlConstants.IS_READY_PROP_KEY, jsonqlConstants.IS_LOGIN_PROP_KEY]
      // .map(key => injectToFn(client, key, false, true))
      .reduce(function (c, key) {
        return objDefineProps.injectToFn(c, key, false, true)
      }, client)
  }

  // constants

  var EMIT_EVT = jsonqlConstants.EMIT_REPLY_TYPE;

  var UNKNOWN_RESULT = 'UKNNOWN RESULT!';

  var MY_NAMESPACE = 'myNamespace';

  // breaking it up further to share between methods

  /**
   * break out to use in different places to handle the return from server
   * @param {object} data from server
   * @param {function} resolver NOT from promise
   * @param {function} rejecter NOT from promise
   * @return {void} nothing
   */
  function respondHandler(data, resolver, rejecter) {
    if (module.isObjectHasKey(data, jsonqlConstants.ERROR_KEY)) {
      // debugFn('-- rejecter called --', data[ERROR_KEY])
      rejecter(data[jsonqlConstants.ERROR_KEY]);
    } else if (module.isObjectHasKey(data, jsonqlConstants.DATA_KEY)) {
      // debugFn('-- resolver called --', data[DATA_KEY])
      // @NOTE we change from calling it directly to use reflect 
      // this could have another problem later when the return data is no in an array structure
      Reflect.apply(resolver, null, [].concat( data[jsonqlConstants.DATA_KEY] ));
    } else {
      // debugFn('-- UNKNOWN_RESULT --', data)
      rejecter({message: UNKNOWN_RESULT, error: data});
    }
  }

  // the actual trigger call method

  /**
   * just wrapper
   * @param {object} ee EventEmitter
   * @param {string} namespace where this belongs
   * @param {string} resolverName resolver
   * @param {array} args arguments
   * @param {function} log function 
   * @return {void} nothing
   */
  function actionCall(ee, namespace, resolverName, args, log) {
    if ( args === void 0 ) args = [];

    // reply event 
    var outEventName = module.createEvt(namespace, jsonqlConstants.EMIT_REPLY_TYPE);

    log(("actionCall: " + outEventName + " --> " + resolverName), args);
    // This is the out going call 
    ee.$trigger(outEventName, [resolverName, module.toArray(args)]);
    
    // then we need to listen to the event callback here as well
    return new Promise(function (resolver, rejecter) {
      var inEventName = module.createEvt(namespace, resolverName, jsonqlConstants.ON_RESULT_FN_NAME);
      // this cause the onResult got the result back first 
      // and it should be the promise resolve first
      // @TODO we need to rewrote the respondHandler to change the problem stated above 
      ee.$on(
        inEventName,
        function actionCallResultHandler(result) {
          log("got the first result", result);
          respondHandler(result, resolver, rejecter);
        }
      );
    })
  }

  // setting up the send method 

  /** 
   * pairing with the server vesrion SEND_MSG_FN_NAME
   * last of the chain so only return the resolver (fn)
   * This is now change to a getter / setter method 
   * and call like this: resolver.send(...args)
   * @param {function} fn the resolver function 
   * @param {object} ee event emitter instance 
   * @param {string} namespace the namespace it belongs to 
   * @param {string} resolverName name of the resolver 
   * @param {object} params from contract 
   * @param {function} log a logger function
   * @return {function} return the resolver itself 
   */ 
  var setupSendMethod = function (fn, ee, namespace, resolverName, params, log) { return (
    module.objDefineProps(
      fn, 
      jsonqlConstants.SEND_MSG_FN_NAME, 
      module.nil, 
      function sendHandler() {
        log("running call getter method");
        // let _log = (...args) => Reflect.apply(console.info, console, ['[SEND]'].concat(args))
        /** 
         * This will follow the same pattern like the resolver 
         * @param {array} args list of unknown argument follow the resolver 
         * @return {promise} resolve the result 
         */
        return function sendCallback() {
          var args = [], len = arguments.length;
          while ( len-- ) args[ len ] = arguments[ len ];

          return jsonqlParamsValidator.validateAsync(args, params.params, true)
            .then(function (_args) {
              // @TODO check the result 
              // because the validation could failed with the list of fail properties 
              log('execute send', namespace, resolverName, _args);
              return actionCall(ee, namespace, resolverName, _args, log)
            })
            .catch(function (err) {
              // @TODO it shouldn't be just a validation error 
              // it could be server return error, so we need to check 
              // what error we got back here first 
              log('send error', err);
              // @TODO it might not an validation error need the finalCatch here
              ee.$call(
                module.createEvt(namespace, resolverName, jsonqlConstants.ON_ERROR_FN_NAME),
                [new jsonqlErrors.JsonqlValidationError(resolverName, err)]
              );
            })
        }    
      })
  ); };

  // break up the original setup resolver method here


  /**
   * moved back from generator-methods  
   * create the actual function to send message to server
   * @param {object} ee EventEmitter instance
   * @param {string} namespace this resolver end point
   * @param {string} resolverName name of resolver as event name
   * @param {object} params from contract
   * @param {function} log pass the log function
   * @return {function} resolver
   */
  function createResolver(ee, namespace, resolverName, params, log) {
    // note we pass the new withResult=true option
    return function resolver() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return jsonqlParamsValidator.validateAsync(args, params.params, true)
        .then(function (_args) { return actionCall(ee, namespace, resolverName, _args, log); })
        .catch(jsonqlErrors.finalCatch)
    }
  }

  /**
   * The first one in the chain, just setup a namespace prop
   * the rest are passing through
   * @param {function} fn the resolver function
   * @param {object} ee the event emitter
   * @param {string} resolverName what it said
   * @param {object} params for resolver from contract
   * @param {function} log the logger function
   * @return {array}
   */
  var setupNamespace = function (fn, ee, namespace, resolverName, params, log) { return [
    module.injectToFn(fn, MY_NAMESPACE, namespace),
    ee,
    namespace,
    resolverName,
    params,
    log
  ]; };

  /**
   * onResult handler
   */
  var setupOnResult = function (fn, ee, namespace, resolverName, params, log) { return [
    module.objDefineProps(fn, jsonqlConstants.ON_RESULT_FN_NAME, function(resultCallback) {
      if (module.isFunc(resultCallback)) {
        ee.$on(
          module.createEvt(namespace, resolverName, jsonqlConstants.ON_RESULT_FN_NAME),
          function resultHandler(result) {
            respondHandler(result, resultCallback, function (error) {
              log(("Catch error: \"" + resolverName + "\""), error);
              ee.$trigger(
                module.createEvt(namespace, resolverName, jsonqlConstants.ON_ERROR_FN_NAME),
                error
              );
            });
          }
        );
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    log
  ]; };

  /**
   * we do need to add the send prop back because it's the only way to deal with
   * bi-directional data stream
   */
  var setupOnMessage = function (fn, ee, namespace, resolverName, params, log) { return [
    module.objDefineProps(fn, jsonqlConstants.ON_MESSAGE_FN_NAME, function(messageCallback) {
      // we expect this to be a function
      if (module.isFunc(messageCallback)) {
        // did that add to the callback
        var onMessageCallback = function (args) {
          log("onMessageCallback", args);
          respondHandler(
            args, 
            messageCallback, 
            function (error) {
              log(("Catch error: \"" + resolverName + "\""), error);
              ee.$trigger(
                module.createEvt(namespace, resolverName, jsonqlConstants.ON_ERROR_FN_NAME),
                error
              );
            });
          };
        // register the handler for this message event
        ee.$only(
          module.createEvt(namespace, resolverName, jsonqlConstants.ON_MESSAGE_FN_NAME),
          onMessageCallback
        );
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    log
  ]; };

  /**
   * ON_ERROR_FN_NAME handler
   */
  var setupOnError = function (fn, ee, namespace, resolverName, params, log) { return [
    module.objDefineProps(fn, jsonqlConstants.ON_ERROR_FN_NAME, function(resolverErrorHandler) {
      if (module.isFunc(resolverErrorHandler)) {
        // please note ON_ERROR_FN_NAME can add multiple listners
        ee.$only(
          module.createEvt(namespace, resolverName, jsonqlConstants.ON_ERROR_FN_NAME),
          resolverErrorHandler
        );
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    log
  ]; };

  /**
   * Add extra property / listeners to the resolver
   * @param {string} namespace where this belongs
   * @param {string} resolverName name as event name
   * @param {object} params from contract
   * @param {function} fn resolver function
   * @param {object} ee EventEmitter
   * @param {function} log function
   * @return {function} resolver
   */  
  function setupResolver(namespace, resolverName, params, fn, ee, log) {
    var fns = [
      setupNamespace,
      setupOnResult,
      setupOnMessage,
      setupOnError,
      setupSendMethod
    ];
    var executor = Reflect.apply(module.chainFns, null, fns);
    // get the executor
    return executor(fn, ee, namespace, resolverName, params, log)
  }

  // put all the resolver related methods here to make it more clear

  /**
   * step one get the clientmap with the namespace
   * @param {object} opts configuration
   * @param {object} ee EventEmitter
   * @param {object} nspGroup resolvers index by their namespace
   * @return {promise} resolve the clientmapped, and start the chain
   */
  function generateResolvers(opts, ee, nspGroup) {
    var log = opts.log;
    var client= {};
    
    for (var namespace in nspGroup) {
      var list = nspGroup[namespace];
      for (var resolverName in list) {
        // resolverNames.push(resolverName)
        var params = list[resolverName];
        var fn = createResolver(ee, namespace, resolverName, params, log);
        // this should set as a getter therefore can not be overwrite by accident
        client = module.injectToFn(
          client,
          resolverName,
          setupResolver(namespace, resolverName, params, fn, ee, log)
        );
      }
    }
    // resolve the clientto start the chain
    // chain the result to allow the chain processing
    return [ client, opts, ee, nspGroup ]
  }

  // move from generator-methods 

  /**
   * This event will fire when the socket.io.on('connection') and ws.onopen
   * @param {object} client  client itself
   * @param {object} opts configuration
   * @param {object} ee Event Emitter
   * @return {array} [ obj, opts, ee ]
   */
  function setupOnReadyListener(client, opts, ee) {
    return [
      module.objDefineProps(
        client,
        jsonqlConstants.ON_READY_FN_NAME,
        function onReadyCallbackHandler(onReadyCallback) {
          if (module.isFunc(onReadyCallback)) {
            // reduce it down to just one flat level
            // @2020-03-19 only allow ONE onReady callback otherwise
            // it will get fire multiple times which is not what we want
            ee.$only(jsonqlConstants.ON_READY_FN_NAME, onReadyCallback);
          }
        }
      ),
      opts,
      ee
    ]
  }

  /**
   * The problem is the namespace can have more than one
   * and we only have on onError message
   * @param {object} clientthe client itself
   * @param {object} opts configuration
   * @param {object} ee Event Emitter
   * @param {object} nspGroup namespace keys
   * @return {array} [obj, opts, ee]
   */
  function setupNamespaceErrorListener(client, opts, ee, nspGroup) {
    return [
      module.objDefineProps(
        client,
        jsonqlConstants.ON_ERROR_FN_NAME,
        function namespaceErrorCallbackHandler(namespaceErrorHandler) {
          if (module.isFunc(namespaceErrorHandler)) {
            // please note ON_ERROR_FN_NAME can add multiple listners
            for (var namespace in nspGroup) {
              // this one is very tricky, we need to make sure the trigger is calling
              // with the namespace as well as the error
              ee.$on(module.createEvt(namespace, jsonqlConstants.ON_ERROR_FN_NAME), namespaceErrorHandler);
            }
          }
        }
      ),
      opts,
      ee
    ]
  }

  // take out from the resolver-methods
  // import { validateAsync } from 'jsonql-params-validator'

  /**
   * @UPDATE it might be better if we decoup the two http-client only emit a login event
   * Here should catch it and reload the ws client @TBC
   * break out from createAuthMethods to allow chaining call
   * @TODO when this is using the standalone mode then this will have to be disable
   * or delegated to the login method from the contract
   * @param {object} obj the main client object
   * @param {object} opts configuration
   * @param {object} ee event emitter
   * @return {array} [ obj, opts, ee ] what comes in what goes out
   */
  var setupLoginHandler = function (obj, opts, ee) { return [
    module.injectToFn(
      obj, 
      opts.loginHandlerName, 
      function loginHandler(token) {
        if (token && jsonqlParamsValidator.isString(token)) {
          opts.log(("Received " + jsonqlConstants.LOGIN_EVENT_NAME + " with " + token));
          // @TODO add the interceptor hook
          return ee.$trigger(jsonqlConstants.LOGIN_EVENT_NAME, [token])
        }
        // should trigger a global error instead @TODO
        throw new jsonqlErrors.JsonqlValidationError(opts.loginHandlerName, ("Unexpected token " + token))
      }
    ),
    opts,
    ee
  ]; };

  /**
   * Switch to this one when standalone mode is enable 
   * @NOTE we don't actually have this because the public contract not included it
   * so we need to figure out a different way to include the auth method 
   * that get expose to the public before we can continue with this standalone mode
   * const { contract, loginHandlerName } = opts 
   * const params = contract[SOCKET_AUTH_NAME][loginHandlerName]
   * @param {*} obj 
   * @param {*} opts 
   * @param {*} ee 
   */
  var setupStandaloneLoginHandler = function (obj, opts, ee) { return [
    module.injectToFn(
      obj, 
      loginHandlerName,
      function standaloneLoginHandler() {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        // we only need to pass the argument 
        // let the listener to handle the rest
        ee.$trigger(jsonqlConstants.LOGIN_EVENT_NAME, args);
      }
    ),
    opts,
    ee 
  ]; };

  /**
   * break out from createAuthMethods to allow chaining call - final in chain
   * @param {object} obj the main client object
   * @param {object} opts configuration
   * @param {object} ee event emitter
   * @return {array} [ obj, opts, ee ] what comes in what goes out
   */
  var setupLogoutHandler = function (obj, opts, ee) { return [
    module.injectToFn(
      obj, 
      opts.logoutHandlerName, 
      function logoutHandler() {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        opts[jsonqlConstants.IS_LOGIN_PROP_KEY] = true;
        ee.$trigger(jsonqlConstants.LOGOUT_EVENT_NAME, args);
      }
    ),
    opts,
    ee
  ]; };

  /**
   * This event will fire when the socket.io.on('connection') and ws.onopen
   * Plus this will check if it's the private namespace that fired the event
   * @param {object} obj the client itself
   * @param {object} ee Event Emitter
   * @return {array} [ obj, opts, ee] what comes in what goes out
   */
  var setupOnLoginListener = function (obj, opts, ee) { return [
    module.objDefineProps(
      obj, 
      jsonqlConstants.ON_LOGIN_FN_NAME, 
      function onLoginCallbackHandler(onLoginCallback) {
        if (module.isFunc(onLoginCallback)) {
          // only one callback can registered with it, TBC
          // Should this be a $onlyOnce listener after the logout 
          // we add it back? 
          ee.$only(jsonqlConstants.ON_LOGIN_FN_NAME, onLoginCallback);
        }
      }
    ),
    opts,
    ee
  ]; };

  /**
   * Main Create auth related methods
   * @param {object} obj the client itself
   * @param {object} opts configuration
   * @param {object} ee Event Emitter
   * @return {array} [ obj, opts, ee ] what comes in what goes out
   */
  function setupAuthMethods(obj, opts, ee) {
    return module.chainFns(
      opts[jsonqlConstants.STANDALONE_PROP_KEY] === true ? setupStandaloneLoginHandler : setupLoginHandler, 
      setupLogoutHandler,
      setupOnLoginListener
    )(obj, opts, ee)
  }

  // this is a new method that will create several

  /**
   * Set up the CONNECTED_PROP_KEY to the client
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   */
  function setupConnectPropKey(client, opts, ee) {
    var log = opts.log; 
    log('[1] setupConnectPropKey');
      // we just inject a helloWorld method here
    // set up the init state of the connect
    client = module.injectToFn(client, jsonqlConstants.CONNECTED_PROP_KEY , false, true);
    return [ client, opts, ee ]
  }


  /**
   * setup listener to the connect event 
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   */
  function setupConnectEvtListener(client, opts, ee) {
    // @TODO do what at this point?
    var log = opts.log; 

    log("[2] setupConnectEvtListener");

    ee.$on(jsonqlConstants.CONNECT_EVENT_NAME, function() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      log("setupConnectEvtListener pass and do nothing at the moment", args);
    });
    
    return [client, opts, ee]
  }

  /**
   * setup listener to the connected event 
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   */
  function setupConnectedEvtListener(client, opts, ee) {
    var log = opts.log; 

    log("[3] setupConnectedEvtListener");

    ee.$on(jsonqlConstants.CONNECTED_EVENT_NAME, function() {
      var obj;

      client[jsonqlConstants.CONNECTED_PROP_KEY] = true;
      // new action to take release the holded event queue 
      var ctn = ee.$release();

      log("CONNECTED_EVENT_NAME", true, 'queue count', ctn);

    return ( obj = {}, obj[jsonqlConstants.CONNECTED_PROP_KEY] = true, obj )
    });

    return [client, opts, ee]
  }

  /**
   * Listen to the disconnect event and set the property to the client 
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   */
  function setupDisconnectListener(client, opts, ee) {
    var log = opts.log; 

    log("[4] setupDisconnectListener");

    ee.$on(jsonqlConstants.DISCONNECT_EVENT_NAME, function() {
      var obj;

      client[jsonqlConstants.CONNECTED_PROP_KEY] = false;
      log("CONNECTED_EVENT_NAME", false);

    return ( obj = {}, obj[jsonqlConstants.CONNECTED_PROP_KEY] = false, obj )
    });

    return [client, opts, ee]
  }

  /**
   * disconnect action
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   * @return {object} this is the final step to return the client
   */
  function setupDisconectAction(client, opts, ee) {
    var disconnectHandlerName = opts.disconnectHandlerName;
    var log = opts.log;
    log("[5] setupDisconectAction");

    return module.injectToFn(
      client,
      disconnectHandlerName,
      function disconnectHandler() {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        ee.$trigger(jsonqlConstants.DISCONNECT_EVENT_NAME, args);
      }
    )
  }

  /**
   * this is the new method that setup the intercom handler
   * also this serve as the final call in the then chain to
   * output the client
   * @param {object} client the client
   * @param {object} opts configuration
   * @param {object} ee the event emitter
   * @return {object} client
   */
  function setupInterCom(client, opts, ee) {
    var fns = [
      setupConnectPropKey,
      setupConnectEvtListener,
      setupConnectedEvtListener,
      setupDisconnectListener,
      setupDisconectAction
    ];

    var executor = Reflect.apply(module.chainFns, null, fns);
    return executor(client, opts, ee)
  }

  // The final step of the setup before it returns the client


  /**
   * The final step to return the client
   * @param {object} obj the client
   * @param {object} opts configuration
   * @param {object} ee the event emitter
   * @return {object} client
   */
  function setupFinalStep(obj, opts, ee) {
    
    var client = setupInterCom(obj, opts, ee);
    // opts.log(`---> The final step to return the ws-client <---`)
    // add some debug functions
    client.verifyEventEmitter = function () { return ee.is; };
    // we add back the two things into the client
    // then when we do integration, we run it in reverse,
    // create the ws client first then the host client
    client.eventEmitter = opts.eventEmitter;
    client.log = opts.log;

    // now at this point, we are going to call the connect event
    ee.$trigger(jsonqlConstants.CONNECT_EVENT_NAME, [opts, ee]); // just passing back the entire opts object
    // also we can release the queue here 
    if (opts[jsonqlConstants.SUSPEND_EVENT_PROP_KEY] === true) {
      opts.$releaseNamespace();
    }

    // finally inject some prop keys to the client 
    return setupStatePropKeys(obj)
  }

  // resolvers generator

  /**
   * This is the starting point of binding callable method to the 
   * ws, and we have to determine which is actually active 
   * if enableAuth but we don't have a token, when the connection 
   * to ws establish we only release the public related event 
   * and the private one remain hold until the LOGIN_EVENT get trigger
   * @param {object} opts configuration
   * @param {object} nspMap resolvers index by their namespace
   * @param {object} ee EventEmitter
   * @return {object} of resolvers
   * @public
   */
  function callersGenerator(opts, nspMap, ee) {
    
    var fns = [
      generateResolvers,
      setupOnReadyListener,
      setupNamespaceErrorListener
    ];
    if (opts.enableAuth) {
      // there is a problem here, when this is a public namespace
      // it should not have a login logout event attach to it
      fns.push(setupAuthMethods);
    }
    // we will always get back the [ obj, opts, ee ]
    // then we only return the obj (wsClient)
    // This has move outside of here, into the main method
    // the reason is we could switch around the sequence much easier
    fns.push(setupFinalStep);
    // stupid reaon!!!
    var executer = Reflect.apply(module.chainFns, null, fns);
    // run it
    return executer(opts, ee, nspMap[jsonqlConstants.NSP_GROUP])
  }

  var obj, obj$1;



  var configCheckMap = {};
  configCheckMap[jsonqlConstants.STANDALONE_PROP_KEY] = jsonqlParamsValidator.createConfig(false, [jsonqlConstants.BOOLEAN_TYPE]);
  configCheckMap[jsonqlConstants.DEBUG_ON_PROP_KEY] = jsonqlParamsValidator.createConfig(false, [jsonqlConstants.BOOLEAN_TYPE]);
  configCheckMap[jsonqlConstants.LOGIN_FN_NAME_PROP_KEY] = jsonqlParamsValidator.createConfig(jsonqlConstants.LOGIN_FN_NAME, [jsonqlConstants.STRING_TYPE]);
  configCheckMap[jsonqlConstants.LOGOUT_FN_NAME_PROP_KEY] = jsonqlParamsValidator.createConfig(jsonqlConstants.LOGOUT_FN_NAME, [jsonqlConstants.STRING_TYPE]);
  configCheckMap[jsonqlConstants.DISCONNECT_FN_NAME_PROP_KEY] = jsonqlParamsValidator.createConfig(jsonqlConstants.DISCONNECT_FN_NAME, [jsonqlConstants.STRING_TYPE]);
  configCheckMap[jsonqlConstants.SWITCH_USER_FN_NAME_PROP_KEY] = jsonqlParamsValidator.createConfig(jsonqlConstants.SWITCH_USER_FN_NAME, [jsonqlConstants.STRING_TYPE]);
  configCheckMap[jsonqlConstants.HOSTNAME_PROP_KEY] = jsonqlParamsValidator.createConfig(false, [jsonqlConstants.STRING_TYPE]);
  configCheckMap[jsonqlConstants.NAMESAPCE_PROP_KEY] = jsonqlParamsValidator.createConfig(jsonqlConstants.JSONQL_PATH, [jsonqlConstants.STRING_TYPE]);
  configCheckMap[jsonqlConstants.WS_OPT_PROP_KEY] = jsonqlParamsValidator.createConfig({}, [jsonqlConstants.OBJECT_TYPE]);
  configCheckMap[jsonqlConstants.CONTRACT_PROP_KEY] = jsonqlParamsValidator.createConfig({}, [jsonqlConstants.OBJECT_TYPE], ( obj = {}, obj[jsonqlConstants.CHECKER_KEY] = module.isContract, obj ));
  configCheckMap[jsonqlConstants.ENABLE_AUTH_PROP_KEY] = jsonqlParamsValidator.createConfig(false, [jsonqlConstants.BOOLEAN_TYPE]);
  configCheckMap[jsonqlConstants.TOKEN_PROP_KEY] = jsonqlParamsValidator.createConfig(false, [jsonqlConstants.STRING_TYPE]);
  configCheckMap[jsonqlConstants.CSRF_PROP_KEY] = jsonqlParamsValidator.createConfig(jsonqlConstants.CSRF_HEADER_KEY, [jsonqlConstants.STRING_TYPE]);
  configCheckMap[jsonqlConstants.SUSPEND_EVENT_PROP_KEY] = jsonqlParamsValidator.createConfig(false, [jsonqlConstants.BOOLEAN_TYPE]);

  // socket client
  var socketCheckMap = {};
  socketCheckMap[jsonqlConstants.SOCKET_TYPE_PROP_KEY] = jsonqlParamsValidator.createConfig(null, [jsonqlConstants.STRING_TYPE], ( obj$1 = {}, obj$1[jsonqlConstants.ALIAS_KEY] = jsonqlConstants.SOCKET_TYPE_CLIENT_ALIAS, obj$1 ));

  var wsCoreCheckMap = Object.assign(configCheckMap, socketCheckMap);

  // constant props
  var wsCoreConstProps = {};
  wsCoreConstProps[jsonqlConstants.USE_JWT_PROP_KEY] = true;
  wsCoreConstProps.log = null;
  wsCoreConstProps.eventEmitter = null;
  wsCoreConstProps.nspClient = null;
  wsCoreConstProps.nspAuthClient = null;
  wsCoreConstProps.wssPath = '';
  wsCoreConstProps.publicNamespace = jsonqlConstants.PUBLIC_KEY;
  wsCoreConstProps.privateNamespace = jsonqlConstants.PRIVATE_KEY;

  // create options


  /**
   * wrapper method to check this already did the pre check
   * @param {object} config user supply config
   * @param {object} defaultOptions for checking
   * @param {object} constProps user supply const props
   * @return {promise} resolve to the checked opitons
   */
  function checkConfiguration(config, defaultOptions, constProps) {
    var defaultCheckMap= Object.assign(wsCoreCheckMap, defaultOptions);
    var wsConstProps = Object.assign(wsCoreConstProps, constProps);

    return jsonqlParamsValidator.checkConfigAsync(config, defaultCheckMap, wsConstProps)
  }

  /**
   * Taking the `then` part from the method below
   * @param {object} opts
   * @return {promise} opts all done
   */
  function postCheckInjectOpts(opts) {
    
    return Promise.resolve(opts)
      .then(function (opts) {
        if (!opts.hostname) {
          opts.hostname = getHostName();
        }
        // @TODO the contract now will supply the namespace information
        // and we need to use that to group the namespace call
        
        opts.wssPath = fixWss([opts.hostname, opts.namespace].join('/'), opts.serverType);
        // get the log function here
        opts.log = getLogFn(opts);

        opts.eventEmitter = getEventEmitter(opts);
      
        return opts
      })
  }

  /**
   * Don't want to make things confusing
   * Breaking up the opts process in one place
   * then generate the necessary parameter in another step
   * @2020-3-20 here we suspend operation by it's namespace first
   * Then in the framework part, after the connection establish we release
   * the queue
   * @param {object} opts checked --> merge --> injected
   * @return {object} {opts, nspMap, ee}
   */
  function createRequiredParams(opts) {
    var nspMap = module.getNspInfoByConfig(opts);
    var ee = opts.eventEmitter;
    // @TODO here we are going to add suspend event to the namespace related methods
    var log = opts.log; 
    var namespaces = nspMap.namespaces;
    var publicNamespace = nspMap.publicNamespace;
    log("namespaces", namespaces);
    // next we loop the namespace and suspend all the events prefix with namespace 
    if (opts[jsonqlConstants.SUSPEND_EVENT_PROP_KEY] === true) {
      // ON_READY_FN_NAME
      // ON_LOGIN_FN_NAME

      // we create this as a function then we can call it again 
      // @TODO we need to add the onReady and the onLogin in the suspend list as well
      opts.$suspendNamepsace = function () {
        var eventNames = [jsonqlConstants.ON_RESULT_FN_NAME ].concat( namespaces);
        if (opts.enableAuth) {
          eventNames.push(jsonqlConstants.ON_LOGIN_FN_NAME);
        }
        // [ON_READY_FN_NAME, ON_LOGIN_FN_NAME, ...namespaces]
        Reflect.apply(ee.$suspendEvent, ee, eventNames);
      };
      // then we create a new method to releas the queue 
      // we prefix it with the $ to notify this is not a jsonql part methods
      opts.$releaseNamespace = function () {
        ee.$releaseEvent(jsonqlConstants.ON_RESULT_FN_NAME, publicNamespace);
      };

      if (opts.enableAuth) {
        opts.$releasePrivateNamespace = function () {
          ee.$releaseEvent(jsonqlConstants.ON_LOGIN_FN_NAME, namespaces[1]);
        };
      }

      // now run it 
      opts.$suspendNamepsace();
    }
    
    return { opts: opts, nspMap: nspMap, ee: ee }
  }

  // the top level API


  /**
   * 0.5.0 we break up the wsClientCore in two parts one without the config check
   * @param {function} setupSocketClientListener just make sure what it said it does
   * @return {function} to actually generate the client
   */
  function wsClientCoreAction(setupSocketClientListener) {
    /**
     * This is a breaking change, to continue the onion skin design
     * @param {object} config the already checked config
     * @return {promise} resolve the client
     */
    return function createClientAction(config) {
      if ( config === void 0 ) config = {};


      return postCheckInjectOpts(config)
        .then(createRequiredParams)
        .then(
          function (ref) {
            var opts = ref.opts;
            var nspMap = ref.nspMap;
            var ee = ref.ee;

            return setupSocketClientListener(opts, nspMap, ee);
      }
        )
        .then(
          function (ref) {
            var opts = ref.opts;
            var nspMap = ref.nspMap;
            var ee = ref.ee;

            return callersGenerator(opts, nspMap, ee);
      }
        )
        .catch(function (err) {
          console.error("[jsonql-ws-core-client init error]", err);
        })
    }
  }

  /**
   * The main interface which will generate the socket clients and map all events
   * @param {object} socketClientListerner this is the one method export by various clients
   * @param {object} [configCheckMap={}] we should do all the checking in the core instead of the client
   * @param {object} [constProps={}] add this to supply the constProps from the downstream client
   * @return {function} accept a config then return the wsClient instance with all the available API
   */
  function wsClientCore(socketClientListener, configCheckMap, constProps) {
    if ( configCheckMap === void 0 ) configCheckMap = {};
    if ( constProps === void 0 ) constProps = {};

    // we need to inject property to this client later
    return function (config) {
      if ( config === void 0 ) config = {};

      return checkConfiguration(config, configCheckMap, constProps)
                              .then(
                                wsClientCoreAction(socketClientListener)
                              );
    }
  }

  // this use by client-event-handler

  /**
   * trigger errors on all the namespace onError handler
   * @param {object} ee Event Emitter
   * @param {array} namespaces nsps string
   * @param {string} message optional
   * @return {void}
   */
  function triggerNamespacesOnError(ee, namespaces, message) {
    namespaces.forEach( function (namespace) {
      ee.$trigger(
        module.createEvt(namespace, jsonqlConstants.ON_ERROR_FN_NAME), 
        [{ message: message, namespace: namespace }]
      );
    });
  }

  /**
   * Handle the onerror callback 
   * @param {object} ee event emitter  
   * @param {string} namespace which namespace has error 
   * @param {*} err error object
   * @return {void} 
   */
  var handleNamespaceOnError = function (ee, namespace, err) {
    ee.$trigger(module.createEvt(namespace, jsonqlConstants.ON_ERROR_FN_NAME), [err]);
  };

  // NOT IN USE AT THE MOMENT JUST KEEP IT HERE FOR THE TIME BEING 

  /**
   * A Event Listerner placeholder when it's not connect to the private nsp
   * @param {string} namespace nsp
   * @param {object} ee EventEmitter
   * @param {object} opts configuration
   * @return {void}
   */
  var notLoginListener = function (namespace, ee, opts) {
    var log = opts.log; 

    ee.$only(
      module.createEvt(namespace, EMIT_EVT),
      function notLoginListernerCallback(resolverName, args) {
        log('[notLoginListerner] hijack the ws call', namespace, resolverName, args);
        var error = { message: jsonqlConstants.NOT_LOGIN_ERR_MSG };
        // It should just throw error here and should not call the result
        // because that's channel for handling normal event not the fake one
        ee.$call(module.createEvt(namespace, resolverName, jsonqlConstants.ON_ERROR_FN_NAME), [ error ]);
        // also trigger the result Listerner, but wrap inside the error key
        ee.$call(module.createEvt(namespace, resolverName, jsonqlConstants.ON_RESULT_FN_NAME), [{ error: error }]);
      }
    );
  };

  /**
   * Only when there is a private namespace then we bind to this event
   * @param {object} nsps the available nsp(s)
   * @param {array} namespaces available namespace 
   * @param {object} ee eventEmitter 
   * @param {object} opts configuration
   * @return {void}
   */
  var logoutEvtListener = function (nsps, namespaces, ee, opts) {
    var log = opts.log; 
    // this will be available regardless enableAuth
    // because the server can log the client out
    ee.$on(
      jsonqlConstants.LOGOUT_EVENT_NAME, 
      function logoutEvtCallback() {
        var privateNamespace = getPrivateNamespace(namespaces);
        log((jsonqlConstants.LOGOUT_EVENT_NAME + " event triggered"));
        // disconnect(nsps, opts.serverType)
        // we need to issue error to all the namespace onError Listerner
        triggerNamespacesOnError(ee, [privateNamespace], jsonqlConstants.LOGOUT_EVENT_NAME);
        // rebind all of the Listerner to the fake one
        log(("logout from " + privateNamespace));

        clearMainEmitEvt(ee, privateNamespace);
        // we need to issue one more call to the server before we disconnect
        // now this is a catch 22, here we are not suppose to do anything platform specific
        // so that should fire before trigger this event
        // clear out the nsp
        nsps[privateNamespace] = null; 
        // add a NOT LOGIN error if call
        notLoginWsListerner(privateNamespace, ee, opts);
      }
    );
  };

  // This is share between different clients so we export it

  /**
   * centralize all the comm in one place
   * @param {function} bindSocketEventHandler binding the ee to ws --> this is the core bit
   * @param {object} nsps namespaced nsp
   * @return {void} nothing
   */
  function namespaceEventListener(bindSocketEventListener, nsps) {
    /**
     * BREAKING CHANGE instead of one flat structure
     * we return a function to accept the two
     * @param {object} opts configuration
     * @param {object} nspMap this is not in the opts
     * @param {object} ee Event Emitter instance
     * @return {array} although we return something but this is the last step and nothing to do further
     */
    return function (opts, nspMap, ee) {
      // since all these params already in the opts
      var log = opts.log;
      var namespaces = nspMap.namespaces;
      // @1.1.3 add isPrivate prop to id which namespace is the private nsp
      // then we can use this prop to determine if we need to fire the ON_LOGIN_PROP_NAME event
      var privateNamespace = namespace.getPrivateNamespace(namespaces);
      // The total number of namespaces (which is 2 at the moment) minus the private namespace number
      var ctn = namespaces.length - 1;   
      // @NOTE we need to somehow id if this namespace already been connected 
      // and the event been released before 
      return namespaces.map(function (namespace) {
        var isPrivate = privateNamespace === namespace;
        log(namespace, (" --> " + (isPrivate ? 'private': 'public') + " nsp --> "), nsps[namespace] !== false);
        if (nsps[namespace]) {
          log('[call bindWsHandler]', isPrivate, namespace);
          // we need to add one more property here to tell the bindSocketEventListener 
          // how many times it should call the onReady 
          var args = [namespace, nsps[namespace], ee, isPrivate, opts, --ctn];
          // Finally we binding everything together
          Reflect.apply(bindSocketEventListener, null, args);
          
        } else {
          log(("binding notLoginWsHandler to " + namespace));
          // a dummy placeholder
          // @TODO but it should be a not connect handler
          // when it's not login (or fail) this should be handle differently
          notLoginListener(namespace, ee, opts);
        }
        if (isPrivate) {
          log("Has private and add logoutEvtHandler");
          logoutEvtListener(nsps, namespaces, ee, opts);
        }
        // just return something its not going to get use anywhere
        return isPrivate
      })
    }
  }

  /*
  This two client is the final one that gets call 
  all it does is to create the url that connect to 
  and actually trigger the connection and return the socket 
  therefore they are as generic as it can be
  */

  /**
   * wrapper method to create a nsp without login
   * @param {string|boolean} namespace namespace url could be false
   * @param {object} opts configuration
   * @return {object} ws client instance
   */
  function createNspClient(namespace, opts) {
    var hostname = opts.hostname;
    var wssPath = opts.wssPath;
    var nspClient = opts.nspClient;
    var log = opts.log;
    var url = namespace ? [hostname, namespace].join('/') : wssPath;
    log("createNspClient with URL --> ", url);

    return nspClient(url, opts)
  }

  /**
   * wrapper method to create a nsp with token auth
   * @param {string} namespace namespace url
   * @param {object} opts configuration
   * @return {object} ws client instance
   */
  function createNspAuthClient(namespace, opts) {
    var hostname = opts.hostname;
    var wssPath = opts.wssPath;
    var token = opts.token;
    var nspAuthClient = opts.nspAuthClient;
    var log = opts.log;
    var url = namespace ? [hostname, namespace].join('/') : wssPath;
    
    log("createNspAuthClient with URL -->", url);

    if (token && typeof token !== 'string') {
      throw new Error(("Expect token to be string, but got " + token))
    }
    // now we need to get an extra options for framework specific method, which is not great
    // instead we just pass the entrie opts to the authClient 

    return nspAuthClient(url, opts, token)
  }

  var obj$2;

  var AVAILABLE_PLACES = [
    jsonqlConstants.TOKEN_IN_URL,
    jsonqlConstants.TOKEN_IN_HEADER
  ];

  // constant props
  var wsClientConstProps = {
    version: 'version: 1.2.0 module: umd', // will get replace
    serverType: jsonqlConstants.JS_WS_NAME
  };

  var wsClientCheckMap = {};
  wsClientCheckMap[jsonqlConstants.TOKEN_DELIVER_LOCATION_PROP_KEY] = jsonqlParamsValidator.createConfig(jsonqlConstants.TOKEN_IN_URL, [jsonqlConstants.STRING_TYPE], ( obj$2 = {}, obj$2[jsonqlConstants.ENUM_KEY] = AVAILABLE_PLACES, obj$2 ));

  // this is all the isormophic-ws is
  var ws = null;

  if (typeof WebSocket !== 'undefined') {
    ws = WebSocket;
  } else if (typeof MozWebSocket !== 'undefined') {
    ws = MozWebSocket;
  } else if (typeof global !== 'undefined') {
    ws = global.WebSocket || global.MozWebSocket;
  } else if (typeof window !== 'undefined') {
    ws = window.WebSocket || window.MozWebSocket;
  } else if (typeof self !== 'undefined') {
    ws = self.WebSocket || self.MozWebSocket;
  }

  var ws$1 = ws;

  /**
   * This will get re-use when we start using the disconnect event method
   * @param {*} ws 
   * @return {void}
   */
  function disconnect(ws) {
    if (ws.terminate && module.isFunc(ws.terminate)) {
      ws.terminate();
    } else if (ws.close && module.isFunc(ws.close)) {
      ws.close();
    }
  }

  // pass the different type of ws to generate the client

  /**
   * 
   * @param {*} WebSocketClass 
   * @param {*} url 
   * @param {*} type 
   * @param {*} options 
   */
  function createWs(WebSocketClass, type, url, options) {
    if (type === 'browser') {
      var headers = options.headers; 
      if (headers) {
        for (var key in headers) {
          if (!cookies.get(key)) {
            cookies.set(key, headers[key]);
          }
        }
      }
    }
    var wsUrl = fixWss(url);
    return type === 'node' ? new WebSocketClass(wsUrl, options) : new WebSocketClass(wsUrl) 
  }

  /**
   * Group the ping and get respond create new client in one
   * @param {object} ws 
   * @param {object} WebSocket 
   * @param {string} url
   * @param {function} resolver 
   * @param {function} rejecter 
   * @param {boolean} auth client or not
   * @return {promise} resolve the confirm client
   */
  function initPingAction(ws, WebSocketClass, type, url, wsOptions, resolver, rejecter) {

    // @TODO how to we id this client can issue a CSRF
    // by origin? 
    ws.onopen = function onOpenCallback() {
      ws.send(createInitPing());
    };

    ws.onmessage = function onMessageCallback(payload) {
      try {
        var header = extractPingResult(payload.data);
        // delay or not show no different but just on the safe side
        setTimeout(function () { 
          disconnect(ws);
        }, 50);
        var newWs = createWs(WebSocketClass, type, url, Object.assign(wsOptions, header)); 
        
        resolver(newWs);  
        
      } catch(e) {
        rejecter(e);
      }
    };

    ws.onerror = function onErrorCallback(err) {
      rejecter(err);
    };
  }

  /**
   * less duplicated code the better 
   * @param {object} WebSocket 
   * @param {string} type we need to change the way how it deliver header for different platform
   * @param {string} url formatted url
   * @param {object} options or not
   * @return {promise} resolve the actual verified client
   */
  function asyncConnect(WebSocketClass, type, url, options) {
    
    return new Promise(function (resolver, rejecter) {  
      var unconfirmClient = createWs(WebSocketClass, type, url, options);
                                
      return initPingAction(unconfirmClient, WebSocketClass, type, url, options, resolver, rejecter)
    })
  }

  /**
   * The bug was in the wsOptions where ws don't need it but socket.io do
   * therefore the object was pass as second parameter!
   * @NOTE here we only return a method to create the client, it might not get call 
   * @param {object} WebSocket the client or node version of ws
   * @param {string} [type = 'browser'] we need to tell if this is browser or node  
   * @param {boolean} [auth = false] if it's auth then 3 param or just one
   * @return {function} the client method to connect to the ws socket server
   */
  function setupWebsocketClientFn(WebSocketClass, type, auth) {
    if ( type === void 0 ) type = 'browser';
    if ( auth === void 0 ) auth = false;

    if (auth === false) {
      /**
       * Create a non-protected client
       * @param {string} uri already constructed url  
       * @param {object} config from the ws-client-core this will be wsOptions taken out from opts 
       * @return {promise} resolve to the confirmed client
       */
      return function createWsClient(uri, config) {
        // const { log } = config
        var ref = security.prepareConnectConfig(uri, config, false);
        var url = ref.url;
        var opts = ref.opts;

        return asyncConnect(WebSocketClass, type, url, opts)
      }
    }

    /**
     * Create a client with auth token
     * @param {string} uri start with ws:// @TODO check this?
     * @param {object} config this is the full configuration because we need something from it
     * @param {string} token the jwt token
     * @return {object} ws instance
     */
    return function createWsAuthClient(uri, config, token) {
      // const { log } = config
      var ref = security.prepareConnectConfig(uri, config, token);
      var url = ref.url;
      var opts = ref.opts;

      return asyncConnect(WebSocketClass, type, url, opts)
    }
  }

  // @BUG when call disconnected

  /**
   * when we received a login event 
   * from the http-client or the standalone login call 
   * we received a token here --> update the opts then trigger 
   * the CONNECT_EVENT_NAME again
   * @param {object} opts configurations
   * @param {object} nspMap contain all the required info
   * @param {object} ee event emitter
   * @return {void}
   */
  function loginEventListener(opts, nspMap, ee) {
    var log = opts.log;
    var namespaces = nspMap.namespaces;

    log("[4] loginEventHandler");

    ee.$only(jsonqlConstants.LOGIN_EVENT_NAME, function loginEventHandlerCallback(tokenFromLoginAction) {

      log('createClient LOGIN_EVENT_NAME $only handler');
      // @NOTE this is wrong because whenever start it will connect the to the public 
      // but when login it should just start binding the private event
      clearMainEmitEvt(ee, namespaces);
      // reload the nsp and rebind all the events
      opts.token = tokenFromLoginAction; 
      ee.$trigger(jsonqlConstants.CONNECT_EVENT_NAME, [opts, ee]); // don't need to pass the nspMap 
    });
  }

  // actually binding the event client to the socket client

  /**
   * Because the nsps can be throw away so it doesn't matter the scope
   * this will get reuse again
   * @NOTE when we enable the standalone method this sequence will not change 
   * only call and reload
   * @param {object} opts configuration
   * @param {object} nspMap from contract
   * @param {string|null} token whether we have the token at run time
   * @return {promise} resolve the nsps namespace with namespace as key
   */
  var createNsp = function(opts, nspMap, token) {
    if ( token === void 0 ) token = null;

    // we leave the token param out because it could get call by another method
    token = token || opts.token; 
    var publicNamespace = nspMap.publicNamespace;
    var namespaces = nspMap.namespaces;
    var log = opts.log; 
    log("createNspAction", 'publicNamespace', publicNamespace, 'namespaces', namespaces);
    
    // reverse the namespaces because it got stuck for some reason
    // const reverseNamespaces = namespaces.reverse()
    if (opts.enableAuth) {
      return chainPromises.chainPromises(
        namespaces.map(function (namespace, i) {
          if (i === 0) {
            if (token) {
              opts.token = token;
              log('create createNspAuthClient at run time');
              return createNspAuthClient(namespace, opts)
            }
            return Promise.resolve(false)
          }
          return createNspClient(namespace, opts)
        })
      )
      .then(function (results) { return results.map(function (result, i) {
            var obj;

            return (( obj = {}, obj[namespaces[i]] = result, obj ));
          })
            .reduce(function (a, b) { return Object.assign(a, b); }, {}); }
      )
    }
    
    return createNspClient(false, opts)
      .then(function (nsp) {
        var obj;

        return (( obj = {}, obj[publicNamespace] = nsp, obj ));
    })
  };

  // taken out from the bind-socket-event-handler 

  /**
   * This is the actual logout (terminate socket connection) handler 
   * There is another one that is handle what should do when this happen 
   * @param {object} ee eventEmitter
   * @param {object} ws the WebSocket instance
   * @return {void}
   */
  function disconnectEventListener(ee, ws) {
    // listen to the LOGOUT_EVENT_NAME when this is a private nsp
    ee.$on(jsonqlConstants.DISCONNECT_EVENT_NAME, function closeEvtHandler() {
      try {
        // @TODO we need find a way to get the userdata
        ws.send(createIntercomPayload(LOGOUT_EVENT_NAME));
        log('terminate ws connection');
        ws.terminate();
      } catch(e) {
        console.error('ws.terminate error', e);
      }
    });
  }

  // the WebSocket main handler

  /**
   * in some edge case we might not even have a resolverName, then
   * we issue a global error for the developer to catch it
   * @param {object} ee event emitter
   * @param {string} namespace nsp
   * @param {string} resolverName resolver
   * @param {object} json decoded payload or error object
   * @return {undefined} nothing return
   */
  var errorTypeHandler = function (ee, namespace, resolverName, json) {
    var evt = [namespace];
    if (resolverName) {
      evt.push(resolverName);
    }
    evt.push(jsonqlConstants.ON_ERROR_FN_NAME);
    var evtName = Reflect.apply(module.createEvt, null, evt);
    // test if there is a data field
    var payload = json.data || json;
    ee.$trigger(evtName, [payload]);
  };

  /**
   * Binding the event to socket normally
   * @param {string} namespace
   * @param {object} ws the nsp
   * @param {object} ee EventEmitter
   * @param {boolean} isPrivate to id if this namespace is private or not
   * @param {object} opts configuration
   * @param {number} ctnLeft in the namespaceEventListener count down how many namespace left to call 
   * @return {object} promise resolve after the onopen event
   */
  function bindSocketEventHandler(namespace, ws, ee, isPrivate, opts, ctnLeft) {
    var log = opts.log;
    // setup the logut event listener 
    // this will hear the event and actually call the ws.terminate
    if (isPrivate) {
      log('Private namespace', namespace, ' binding to the DISCONNECT ws.terminate');
      disconnectEventListener(ee, ws);
    }
    // log(`log test, isPrivate:`, isPrivate)
    // connection open
    ws.onopen = function onOpenCallback() {

      log('client.ws.onopen listened -->', namespace);
      /*
      This is the change from before about the hooks 
      now only the public namespace will get trigger the onReady 
      and the private one will get the onLogin 
      this way, we won't get into the problem of multiple event 
      get trigger, and for developer they just have to place 
      the callback inside the right hook
      */
      if (isPrivate) {
        log(("isPrivate and fire the " + jsonqlConstants.ON_LOGIN_FN_NAME));
        ee.$call(jsonqlConstants.ON_LOGIN_FN_NAME)(namespace);
      } else {
        ee.$call(jsonqlConstants.ON_READY_FN_NAME)(namespace, ctnLeft);
        // The namespaceEventListener will count this for here
        // and this count will only count the public namespace 
        // it will always be 1 for now
        log("isPublic onReady hook executed", ctnLeft);
        if (ctnLeft === 0) {
          ee.$off(jsonqlConstants.ON_READY_FN_NAME);
        }
      }
      // add listener only after the open is called
      ee.$only(
        module.createEvt(namespace, jsonqlConstants.EMIT_REPLY_TYPE),
        /**
         * actually send the payload to server
         * @param {string} resolverName
         * @param {array} args NEED TO CHECK HOW WE PASS THIS!
         */
        function wsMainOnEvtHandler(resolverName, args) {
          var payload = module.createQueryStr(resolverName, args);
          log('ws.onopen.send', resolverName, args, payload);

          ws.send(payload);
        }
      );
    };

    // reply
    // If we change it to the event callback style
    // then the payload will just be the payload and fucks up the extractWsPayload call @TODO
    ws.onmessage = function onMessageCallback(payload) {

      log("client.ws.onmessage raw payload", payload.data);
      
      // console.log(`on.message`, typeof payload, payload)
      try {
        // log(`ws.onmessage raw payload`, payload)
        // @TODO the payload actually contain quite a few things - is that changed?
        // type: message, data: data_send_from_server
        var json = module.extractWsPayload(payload.data);
        var resolverName = json.resolverName;
        var type = json.type;

        log('Respond from server', type, json);

        switch (type) {
          case jsonqlConstants.EMIT_REPLY_TYPE:
            var e1 = module.createEvt(namespace, resolverName, jsonqlConstants.ON_MESSAGE_FN_NAME);
            var r = ee.$call(e1)(json);
            
            log("EMIT_REPLY_TYPE", e1, r);
            break
          case jsonqlConstants.ACKNOWLEDGE_REPLY_TYPE:
            var e2 = module.createEvt(namespace, resolverName, jsonqlConstants.ON_RESULT_FN_NAME);
            var x2 = ee.$call(e2)(json);

            log("ACKNOWLEDGE_REPLY_TYPE", e2, x2);
            break
          case jsonqlConstants.ERROR_KEY:
            // this is handled error and we won't throw it
            // we need to extract the error from json
            log("ERROR_KEY");
            errorTypeHandler(ee, namespace, resolverName, json);
            break
          // @TODO there should be an error type instead of roll into the other two types? TBC
          default:
            // if this happen then we should throw it and halt the operation all together
            log('Unhandled event!', json);
            errorTypeHandler(ee, namespace, resolverName, json);
            // let error = {error: {'message': 'Unhandled event!', type}};
            // ee.$trigger(createEvt(namespace, resolverName, ON_RESULT_FN_NAME), [error])
        }
      } catch(e) {
        log("client.ws.onmessage error", e);
        errorTypeHandler(ee, namespace, false, e);
      }
    };
    // when the server close the connection
    ws.onclose = function onCloseCallback() {
      log('client.ws.onclose callback');
      // @TODO what to do with this
      // ee.$trigger(LOGOUT_EVENT_NAME, [namespace])
    };
    // add a onerror event handler here
    ws.onerror = function onErrorCallback(err) {
      // trigger a global error event
      log("client.ws.onerror", err);
      handleNamespaceOnError(ee, namespace, err);
    };
    
    // we don't bind the logut here and just return the ws 
    return ws 
  }

  // take out from the bind-framework-to-jsonql 

  /**
   * This is the hard of establishing the connection and binding to the jsonql events 
   * @param {*} nspMap 
   * @param {*} ee event emitter
   * @param {function} log function to show internal 
   * @return {void}
   */
  function connectEventListener(nspMap, ee, log) {
    log("[2] setup the CONNECT_EVENT_NAME");
    // this is a automatic trigger from within the framework
    ee.$only(jsonqlConstants.CONNECT_EVENT_NAME, function connectEventNameHandler($config, $ee) {
      log("[3] CONNECT_EVENT_NAME", $config);

      return createNsp($config, nspMap)
              .then(function (nsps) { return namespaceEventListener(bindSocketEventHandler, nsps); })
              .then(function (listenerFn) { return listenerFn($config, nspMap, $ee); })
    });
  }

  // share method to create the wsClientResolver

  /**
   * Create the framework <---> jsonql client binding
   * @param {object} WebSocketClass the different WebSocket module
   * @param {string} [type=browser] we need different setup for browser or node
   * @return {function} the wsClientResolver
   */
  function setupConnectClient(WebSocketClass, type) {
    if ( type === void 0 ) type = 'browser';

    /**
     * wsClientResolver
     * @param {object} opts configuration
     * @param {object} nspMap from the contract
     * @param {object} ee instance of the eventEmitter
     * @return {object} passing the same 3 input out with additional in the opts
     */
    return function createClientBindingAction(opts, nspMap, ee) {
      var log = opts.log;

      log("There is problem here with passing the opts", opts);
      // this will put two callable methods into the opts 
      opts[jsonqlConstants.NSP_CLIENT] = setupWebsocketClientFn(WebSocketClass, type);
      // we don't need this one unless enableAuth === true 
      if (opts[jsonqlConstants.ENABLE_AUTH_PROP_KEY] === true) {
        opts[jsonqlConstants.NSP_AUTH_CLIENT] = setupWebsocketClientFn(WebSocketClass, type, true);
      }    
      // debug 
      log("[1] bindWebsocketToJsonql", ee.$name, nspMap);
      // @2020-03-20 @NOTE 
      
      connectEventListener(nspMap, ee, log);
    
      // next we need to setup the login event handler
      // But the same design (see above) when we received a login event 
      // from the http-client or the standalone login call 
      // we received a token here --> update the opts then trigger 
      // the CONNECT_EVENT_NAME again
      loginEventListener(opts, nspMap, ee);

      log("just before returing the values for the next operation from createClientBindingAction");

      // we just return what comes in
      return { opts: opts, nspMap: nspMap, ee: ee }
    }
  }

  // this will be the news style interface that will pass to the jsonql-ws-client

  var setupSocketClientListener = setupConnectClient(ws$1);

  // this is the module entry point for ES6 for client

  // export back the function and that's it
  function wsBrowserClient(config, constProps) {
    if ( config === void 0 ) config = {};
    if ( constProps === void 0 ) constProps = {};

    return wsClientCore(
      setupSocketClientListener, 
      wsClientCheckMap, 
      Object.assign({}, wsClientConstProps, constProps)
    )(config)
  }

  // just export interface for browser

  return wsBrowserClient;

})));
//# sourceMappingURL=jsonql-ws-client.umd.js.map
