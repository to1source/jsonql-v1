// this will be the news style interface that will pass to the jsonql-ws-client
// then return a function for accepting an opts to generate the final
// client api
import ws from './ws'
import { setupConnectClient } from './setup-connect-client'

const setupSocketClientListener = setupConnectClient(ws)

/**
 * We export it as a default and use the file name to id the function 
 * but it just way too confusing, therefore we change it to a name export 
 * @param {object} opts configuration
 * @param {object} nspMap from the contract
 * @param {object} ee instance of the eventEmitter
 * @return {object} passing the same 3 input out with additional in the opts
 */
export {
  setupSocketClientListener
} 
