// We keep all the import from jsonql-ws-client-core in place 
// if we need to switch then just switch in one place
import {
  wsCoreCheckMap,
  wsCoreConstProps,
  jsonqlWsConstants,
  // generator methods
  wsClientCore,
  wsClientCoreAction,
  // helper methods
  checkSocketClientType,
  
  createCombineConfigCheck,

  triggerNamespacesOnError,
  handleNamespaceOnError,

  clearMainEmitEvt,
  getEventEmitter, 
  EventEmitterClass,

  namespaceEventListener,
  createCombineClient,

  createInitPing, 
  extractPingResult,
  createIntercomPayload,

  prepareConnectConfig,

  createNspClient,
  createNspAuthClient,
  fixWss
} from '../../../../ws-client-core/index' 
// 'jsonql-ws-client-core'
 
// export 
export {
  wsCoreCheckMap,
  wsCoreConstProps,
  jsonqlWsConstants,
  // generator methods
  wsClientCore,
  wsClientCoreAction,
  // helper methods
  checkSocketClientType,
  
  createCombineConfigCheck,

  triggerNamespacesOnError,
  handleNamespaceOnError,

  clearMainEmitEvt,
  getEventEmitter, 
  EventEmitterClass,

  namespaceEventListener,
  createCombineClient,

  createInitPing,  
  extractPingResult,
  createIntercomPayload,

  prepareConnectConfig,

  createNspClient,
  createNspAuthClient,
  fixWss
}