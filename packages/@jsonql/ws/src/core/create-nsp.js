// actually binding the event client to the socket client
import {
  createNspClient,
  createNspAuthClient
} from './modules'
import {
  chainPromises 
} from 'jsonql-utils/src/chain-promises'

/**
 * Because the nsps can be throw away so it doesn't matter the scope
 * this will get reuse again
 * @NOTE when we enable the standalone method this sequence will not change 
 * only call and reload
 * @param {object} opts configuration
 * @param {object} nspMap from contract
 * @param {string|null} token whether we have the token at run time
 * @return {promise} resolve the nsps namespace with namespace as key
 */
const createNsp = function(opts, nspMap, token = null) {
  // we leave the token param out because it could get call by another method
  token = token || opts.token 
  let { publicNamespace, namespaces } = nspMap
  const { log } = opts 
  log(`createNspAction`, 'publicNamespace', publicNamespace, 'namespaces', namespaces)
  
  // reverse the namespaces because it got stuck for some reason
  // const reverseNamespaces = namespaces.reverse()
  if (opts.enableAuth) {
    return chainPromises(
      namespaces.map((namespace, i) => {
        if (i === 0) {
          if (token) {
            opts.token = token
            log('create createNspAuthClient at run time')
            return createNspAuthClient(namespace, opts)
          }
          return Promise.resolve(false)
        }
        return createNspClient(namespace, opts)
      })
    )
    .then(results => 
      results.map((result, i) => 
        ({ [namespaces[i]]: result }))
          .reduce((a, b) => Object.assign(a, b), {})
    )
  }
  
  return createNspClient(false, opts)
    .then(nsp => ({[publicNamespace]: nsp}))
}

export { createNsp }
