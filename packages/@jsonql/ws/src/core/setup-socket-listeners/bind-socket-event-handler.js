// the WebSocket main handler
import {
  ACKNOWLEDGE_REPLY_TYPE,
  EMIT_REPLY_TYPE,
  ERROR_KEY,
  ON_MESSAGE_FN_NAME,
  ON_RESULT_FN_NAME,
  ON_READY_FN_NAME,
  ON_LOGIN_FN_NAME,
  ON_ERROR_FN_NAME
} from 'jsonql-constants'
import {
  createQueryStr,
  createEvt,
  extractWsPayload
} from 'jsonql-utils/module'
import {
  handleNamespaceOnError
} from '../modules'
import { 
  disconnectEventListener
} from './disconnect-event-listener'

/**
 * in some edge case we might not even have a resolverName, then
 * we issue a global error for the developer to catch it
 * @param {object} ee event emitter
 * @param {string} namespace nsp
 * @param {string} resolverName resolver
 * @param {object} json decoded payload or error object
 * @return {undefined} nothing return
 */
export const errorTypeHandler = (ee, namespace, resolverName, json) => {
  let evt = [namespace]
  if (resolverName) {
    evt.push(resolverName)
  }
  evt.push(ON_ERROR_FN_NAME)
  let evtName = Reflect.apply(createEvt, null, evt)
  // test if there is a data field
  let payload = json.data || json
  ee.$trigger(evtName, [payload])
}

/**
 * Binding the event to socket normally
 * @param {string} namespace
 * @param {object} ws the nsp
 * @param {object} ee EventEmitter
 * @param {boolean} isPrivate to id if this namespace is private or not
 * @param {object} opts configuration
 * @param {number} ctnLeft in the namespaceEventListener count down how many namespace left to call 
 * @return {object} promise resolve after the onopen event
 */
export function bindSocketEventHandler(namespace, ws, ee, isPrivate, opts, ctnLeft) {
  const { log } = opts
  // setup the logut event listener 
  // this will hear the event and actually call the ws.terminate
  if (isPrivate) {
    log('Private namespace', namespace, ' binding to the DISCONNECT ws.terminate')
    disconnectEventListener(ee, ws)
  }
  // log(`log test, isPrivate:`, isPrivate)
  // connection open
  ws.onopen = function onOpenCallback() {

    log('client.ws.onopen listened -->', namespace)
    /*
    This is the change from before about the hooks 
    now only the public namespace will get trigger the onReady 
    and the private one will get the onLogin 
    this way, we won't get into the problem of multiple event 
    get trigger, and for developer they just have to place 
    the callback inside the right hook
    */
    if (isPrivate) {
      log(`isPrivate and fire the ${ON_LOGIN_FN_NAME}`)
      ee.$call(ON_LOGIN_FN_NAME)(namespace)
    } else {
      ee.$call(ON_READY_FN_NAME)(namespace, ctnLeft)
      // The namespaceEventListener will count this for here
      // and this count will only count the public namespace 
      // it will always be 1 for now
      log(`isPublic onReady hook executed`, ctnLeft)
      if (ctnLeft === 0) {
        ee.$off(ON_READY_FN_NAME)
      }
    }
    // add listener only after the open is called
    ee.$only(
      createEvt(namespace, EMIT_REPLY_TYPE),
      /**
       * actually send the payload to server
       * @param {string} resolverName
       * @param {array} args NEED TO CHECK HOW WE PASS THIS!
       */
      function wsMainOnEvtHandler(resolverName, args) {
        const payload = createQueryStr(resolverName, args)
        log('ws.onopen.send', resolverName, args, payload)

        ws.send(payload)
      }
    )
  }

  // reply
  // If we change it to the event callback style
  // then the payload will just be the payload and fucks up the extractWsPayload call @TODO
  ws.onmessage = function onMessageCallback(payload) {

    log(`client.ws.onmessage raw payload`, payload.data)
    
    // console.log(`on.message`, typeof payload, payload)
    try {
      // log(`ws.onmessage raw payload`, payload)
      // @TODO the payload actually contain quite a few things - is that changed?
      // type: message, data: data_send_from_server
      const json = extractWsPayload(payload.data)
      const { resolverName, type } = json

      log('Respond from server', type, json)

      switch (type) {
        case EMIT_REPLY_TYPE:
          let e1 = createEvt(namespace, resolverName, ON_MESSAGE_FN_NAME)
          let r = ee.$call(e1)(json)
          
          log(`EMIT_REPLY_TYPE`, e1, r)
          break
        case ACKNOWLEDGE_REPLY_TYPE:
          let e2 = createEvt(namespace, resolverName, ON_RESULT_FN_NAME)
          let x2 = ee.$call(e2)(json)

          log(`ACKNOWLEDGE_REPLY_TYPE`, e2, x2)
          break
        case ERROR_KEY:
          // this is handled error and we won't throw it
          // we need to extract the error from json
          log(`ERROR_KEY`)
          errorTypeHandler(ee, namespace, resolverName, json)
          break
        // @TODO there should be an error type instead of roll into the other two types? TBC
        default:
          // if this happen then we should throw it and halt the operation all together
          log('Unhandled event!', json)
          errorTypeHandler(ee, namespace, resolverName, json)
          // let error = {error: {'message': 'Unhandled event!', type}};
          // ee.$trigger(createEvt(namespace, resolverName, ON_RESULT_FN_NAME), [error])
      }
    } catch(e) {
      log(`client.ws.onmessage error`, e)
      errorTypeHandler(ee, namespace, false, e)
    }
  }
  // when the server close the connection
  ws.onclose = function onCloseCallback() {
    log('client.ws.onclose callback')
    // @TODO what to do with this
    // ee.$trigger(LOGOUT_EVENT_NAME, [namespace])
  }
  // add a onerror event handler here
  ws.onerror = function onErrorCallback(err) {
    // trigger a global error event
    log(`client.ws.onerror`, err)
    handleNamespaceOnError(ee, namespace, err)
  }
  
  // we don't bind the logut here and just return the ws 
  return ws 
}
