// taken out from the bind-socket-event-handler 
import { DISCONNECT_EVENT_NAME } from 'jsonql-constants'
import { createIntercomPayload } from '../modules'

/**
 * This is the actual logout (terminate socket connection) handler 
 * There is another one that is handle what should do when this happen 
 * @param {object} ee eventEmitter
 * @param {object} ws the WebSocket instance
 * @return {void}
 */
export function disconnectEventListener(ee, ws) {
  // listen to the LOGOUT_EVENT_NAME when this is a private nsp
  ee.$on(DISCONNECT_EVENT_NAME, function closeEvtHandler() {
    try {
      // @TODO we need find a way to get the userdata
      ws.send(createIntercomPayload(LOGOUT_EVENT_NAME))
      log('terminate ws connection')
      ws.terminate()
    } catch(e) {
      console.error('ws.terminate error', e)
    }
  })
}