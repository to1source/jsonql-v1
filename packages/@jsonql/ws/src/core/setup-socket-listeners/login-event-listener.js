// @BUG when call disconnected
// this keep causing an "Disconnect call failed TypeError: Cannot read property 'readyState' of null"
// I think that is because it's not login then it can not be disconnect
// how do we track this state globally
import { 
  LOGIN_EVENT_NAME, 
  CONNECT_EVENT_NAME 
} from 'jsonql-constants'
import { clearMainEmitEvt } from '../modules'

/**
 * when we received a login event 
 * from the http-client or the standalone login call 
 * we received a token here --> update the opts then trigger 
 * the CONNECT_EVENT_NAME again
 * @param {object} opts configurations
 * @param {object} nspMap contain all the required info
 * @param {object} ee event emitter
 * @return {void}
 */
export function loginEventListener(opts, nspMap, ee) {
  const { log } = opts
  const { namespaces } = nspMap

  log(`[4] loginEventHandler`)

  ee.$only(LOGIN_EVENT_NAME, function loginEventHandlerCallback(tokenFromLoginAction) {

    log('createClient LOGIN_EVENT_NAME $only handler')
    // @NOTE this is wrong because whenever start it will connect the to the public 
    // but when login it should just start binding the private event
    clearMainEmitEvt(ee, namespaces)
    // reload the nsp and rebind all the events
    opts.token = tokenFromLoginAction 
    ee.$trigger(CONNECT_EVENT_NAME, [opts, ee]) // don't need to pass the nspMap 
  })
}
