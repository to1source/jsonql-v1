// take out from the bind-framework-to-jsonql 
import { CONNECT_EVENT_NAME } from 'jsonql-constants'
import { createNsp } from '../create-nsp'
import { namespaceEventListener } from '../modules'
import { bindSocketEventHandler } from './bind-socket-event-handler'

/**
 * This is the hard of establishing the connection and binding to the jsonql events 
 * @param {*} nspMap 
 * @param {*} ee event emitter
 * @param {function} log function to show internal 
 * @return {void}
 */
export function connectEventListener(nspMap, ee, log) {
  log(`[2] setup the CONNECT_EVENT_NAME`)
  // this is a automatic trigger from within the framework
  ee.$only(CONNECT_EVENT_NAME, function connectEventNameHandler($config, $ee) {
    log(`[3] CONNECT_EVENT_NAME`, $config)

    return createNsp($config, nspMap)
            .then(nsps => namespaceEventListener(bindSocketEventHandler, nsps))
            .then(listenerFn => listenerFn($config, nspMap, $ee))
  })
}