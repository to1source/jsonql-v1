// this is for node upstream client to include and construct their own client 
// this will get build by rollup and becomes the node.js output
import {
  createCombineWsClientConstructor,
  // straight export
  checkSocketClientType,
  getEventEmitter, 
  EventEmitterClass
} from '../create-combine-client'
import { setupSocketClientListener } from './setup-socket-client-listener'

const createCombineWsClient = createCombineWsClientConstructor(setupSocketClientListener)

export {
  createCombineWsClient,
  checkSocketClientType,
  getEventEmitter, 
  EventEmitterClass
}