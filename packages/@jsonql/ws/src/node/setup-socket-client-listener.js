// this will be the news style interface that will pass to the jsonql-ws-client
// then return a function for accepting an opts to generate the final
// client api
import WebSocket from 'ws'
import { setupConnectClient } from '../core/setup-connect-client'

const setupSocketClientListener = setupConnectClient(WebSocket, 'node')

/**
 * @param {object} opts configuration
 * @param {object} nspMap from the contract
 * @param {object} ee instance of the eventEmitter
 * @return {object} passing the same 3 input out with additional in the opts
 */
export {
  setupSocketClientListener
} 
