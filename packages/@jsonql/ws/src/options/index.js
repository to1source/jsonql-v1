// jsonql-ws-core takes over the check configuration
// here we only have to supply the options that is unique to this client
// create options
import { 
  JS_WS_NAME, 
  ENUM_KEY,
  TOKEN_DELIVER_LOCATION_PROP_KEY, 
  TOKEN_IN_URL,
  TOKEN_IN_HEADER,
  STRING_TYPE
} from 'jsonql-constants'
import { 
  createConfig 
} from 'jsonql-params-validator'

const AVAILABLE_PLACES = [
  TOKEN_IN_URL,
  TOKEN_IN_HEADER
]

// constant props
const wsClientConstProps = {
  version: '__PLACEHOLDER__', // will get replace
  serverType: JS_WS_NAME
}

const wsClientCheckMap = {
  [TOKEN_DELIVER_LOCATION_PROP_KEY]: createConfig(TOKEN_IN_URL, [STRING_TYPE], {
    [ENUM_KEY]: AVAILABLE_PLACES
  })
}

export { 
  wsClientCheckMap, 
  wsClientConstProps 
}
