// this is the module entry point for node client
import {
  wsClientCore
} from './core/modules'
import { 
  wsClientCheckMap,
  wsClientConstProps
} from './options'
import { 
  setupSocketClientListener 
} from './node/setup-socket-client-listener'

// export back the function and that's it
export default function wsNodeClient(config = {}, constProps = {}) {
  
  return wsClientCore(
    setupSocketClientListener, 
    wsClientCheckMap, 
    Object.assign({}, wsClientConstProps, constProps)
  )(config)
}
