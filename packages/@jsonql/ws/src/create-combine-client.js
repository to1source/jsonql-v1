// this is a wrapper 
// then different version (e.g. node or browser)
// just pass the engine here to create the client

import { 
  wsClientCheckMap,
  wsClientConstProps
} from './options'
import {
  checkSocketClientType,
  createCombineClient,
  getEventEmitter, 
  EventEmitterClass
} from './core/modules'

/**
 * Overload the createCombineCreate to create a combine http / ws client
 * @param {object} setupSocketClientListener 
 * @return {function} to accept config then generate the client
 */
function createCombineWsClientConstructor(setupSocketClientListener) {
  /**
   * @param {object} httpClientConstructor
   * @param {object} configCheckMap
   * @param {object} constProps
   * @return {object} the ws client combine with http client
   */
  return function createCombineWsClient(httpClientConstructor, configCheckMap, constProps) {
    const localConstProps = Object.assign({}, constProps, wsClientConstProps)
    const localCheckMap = Object.assign({}, configCheckMap, wsClientCheckMap)
    
    return createCombineClient(
      httpClientConstructor,
      setupSocketClientListener,
      localCheckMap, 
      localConstProps
    )
  }
}

export {
  createCombineWsClientConstructor,
  // straight export
  checkSocketClientType,
  getEventEmitter, 
  EventEmitterClass
}