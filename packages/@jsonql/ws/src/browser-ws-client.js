// this is the module entry point for ES6 for client
// the main will point to the node.js server side setup
import { 
  wsClientCore
} from './core/modules' 
import { 
  wsClientCheckMap,
  wsClientConstProps
} from './options'
import { 
  setupSocketClientListener 
} from './core/setup-socket-client-listener'

// export back the function and that's it
export default function wsBrowserClient(config = {}, constProps = {}) {
  return wsClientCore(
    setupSocketClientListener, 
    wsClientCheckMap, 
    Object.assign({}, wsClientConstProps, constProps)
  )(config)
}
