#!/usr/bin/env node
// https://nodejs.org/api/crypto.html
// https://stackoverflow.com/questions/8520973/how-to-create-a-pair-private-public-keys-using-node-js-crypto
const { argv } = require('yargs')
const colors = require('colors/safe')
// const fsx = require('fs-extra')
// const debug = require('debug')('jsonql-jwt:cmd')

const { version } = require('./package.json')
const { rsaKeys, jwtToken, rsaPemKeys } = require('./main')

const saveMsg = () => {
  console.info('-----------------------------------------')
  console.info(colors.bgRed.white(`Please make a copy of these keys NOW!`))
}

// main method
const run = argv => {
  let args = [];
  console.info(colors.white(`Running jsonql-jwt cli version ${version}`))
  switch (argv._[0]) {
    case 'rsa-keys':
      args[0] = argv.format || undefined;
      args[1] = argv.len || undefined;
      const { publicKey, privateKey } = Reflect.apply(rsaKeys, null, args)
      console.log(colors.yellow('[RSA key pairs result]'))
      console.log('PUBLIC KEY: ', colors.bgCyan.black(publicKey))
      console.log('PRIVATE KEY: ', colors.bgYellow.black(privateKey))
      saveMsg()
    break
    case 'rsa-pem':
      args[0] = argv.len || undefined;
      args[1] = argv.outputDir || '';
      const p = Reflect.apply(rsaPemKeys, null, args)
      p.then(result => {
        if (!argv.outputDir) {
          console.log('PUBLIC PEM KEY: \r\n', colors.bgCyan.black(result.publicKey))
          console.log('PRIVATE PEM KEY: \r\n', colors.bgYellow.black(result.privateKey))
          saveMsg()
        }
      })
      .catch(err => {
        console.error(colors.red(err.message || err))
      });
    break
    case 'token':
      if (argv.secret && argv.payload) {
        console.log(argv.payload)
        try {
          const payload = JSON.parse(JSON.stringify(argv.payload))
          const token = jwtToken(payload, argv.secret)

          console.log('JWT TOKEN: ', colors.bgCyan.black(token))

        } catch(e) {
          console.error('error!', colors.red(e))
        }
      }
    break
    default:
      console.log(colors.red('You need to tell me what you want!'))
  }
}

// now run it
run(argv)
