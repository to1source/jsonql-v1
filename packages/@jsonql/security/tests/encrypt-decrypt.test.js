const test = require('ava')
// const fsx = require('fs-extra')
const { join } = require('path')
const {
  encryptWithPublicPem,
  decryptWithPrivatePem
} = require('../main')
const base = join(__dirname, 'fixtures', 'keys')

test.before( t => {

  t.context.publicKeyPath = join(base, 'publicKey.pem')
  t.context.privateKeyPath = join(base, 'privateKey.pem')
})

test('It should able to encrypt with public key and decrypt with private key', t => {
  const text = 'A little fox jump over the fence and drop dead!';

  const cryptedText = encryptWithPublicPem(text, t.context.publicKeyPath)

  t.is(text, decryptWithPrivatePem(cryptedText, t.context.privateKeyPath))

})
