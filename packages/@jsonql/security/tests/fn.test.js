// just testing if some function exist or not 
const test = require('ava')
const {
  BEARER,
  AUTH_CHECK_HEADER,
  HEADERS_KEY,
  CSRF_HEADER_KEY,
  AUTH_HEADER,
  TOKEN_DELIVER_LOCATION_PROP_KEY,
  TOKEN_IN_HEADER
} = require('jsonql-constants')
const {
  extractConfig,
  prepareConnectConfig,

  getTokenFromUrl,
  processAuthTokenHeader,
  getTokenFromHeader,

  getWsAuthToken,

  extractHeaderValue
} = require('../main')
const debug = require('debug')('jsonql-security:test:fn')

const isf = f => typeof f === 'function'

test(`import bunch of methods should be in the export for node`, t => {
  t.true(isf(extractConfig))
  t.true(isf(prepareConnectConfig)) 
  t.true(isf(getTokenFromUrl)) 
  t.true(isf(processAuthTokenHeader)) 
  t.true(isf(getTokenFromHeader)) 
  t.true(isf(getWsAuthToken)) 
})

test(`Test the header config to able to get the CSRF token header as well`, t => {

  const token = 'justadummytokenstringwhatever'
  const csrf = 'whatever'
  const url1 = 'http://whatever.com'
  const config1 = {}
  const config2 = {
    [TOKEN_DELIVER_LOCATION_PROP_KEY]: TOKEN_IN_HEADER,
    [HEADERS_KEY]: {[CSRF_HEADER_KEY]: csrf}
  }

  const set1 = prepareConnectConfig(url1, config1)
  t.deepEqual({[HEADERS_KEY]: {}}, set1.opts)

  
  const set2 = prepareConnectConfig(url1, config2)
  t.truthy(set2.opts[HEADERS_KEY][CSRF_HEADER_KEY])

  const set3 = prepareConnectConfig(url1, config2, token)

  // debug(config2)
  // debug(set3)

  t.is(set3.opts[HEADERS_KEY][CSRF_HEADER_KEY], csrf)
  t.is(set3.opts[HEADERS_KEY][AUTH_HEADER], `${BEARER} ${token}`)

})


test(`Try to see if we pass different authorisation header and come out with the same token`, t => {
  
  const fakeUrl = 'http://whatever.com'
  const token = '123456789'
  const header1 = {[AUTH_CHECK_HEADER]: token}
  const header2 = {[AUTH_CHECK_HEADER]: `${BEARER} ${token}`}

  const result1 = getTokenFromHeader(header1)
  const result2 = getTokenFromHeader(header2)

  t.is(result1, result2)

  const result3 = prepareConnectConfig(fakeUrl)

  t.is(result3.url, fakeUrl)

})

test(`Test why the token is not present but the getWsAuthToken return a string false result`, t => {
  
  const fakeUrl = 'https://whatever.com'
  const req = {
    url: fakeUrl,
    headers: {}
  }
  const opts1 = {}
  const opts2 = {[TOKEN_DELIVER_LOCATION_PROP_KEY]: TOKEN_IN_HEADER}

  
  const result1 = getWsAuthToken(opts1, req)

  t.false(result1)

  const result2 = getWsAuthToken(opts2, req)

  t.false(result2)

})

test.only(`Test the extract cookie from header`, t => {
  const headers = { 
    host: 'localhost:8001',
    connection: 'Upgrade',
    pragma: 'no-cache',
    'cache-control': 'no-cache',
    'user-agent':
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
    upgrade: 'websocket',
    origin: 'http://localhost:8001',
    'sec-websocket-version': '13',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language':
    'en-GB,en-US;q=0.9,en;q=0.8,zh-CN;q=0.7,zh-TW;q=0.6,zh;q=0.5',
    cookie:
    'X-CSRF-Token=MDUBgJIGVWEjvjYjGMnMN; dummy=some%20dummy%20value',
    'sec-websocket-key': 'CqR2kzG/4+rPiWwwKCX9+g==',
    'sec-websocket-extensions': 'permessage-deflate; client_max_window_bits' 
  }

  const csrf = extractHeaderValue(headers, CSRF_HEADER_KEY)

  debug(csrf)

  t.is(csrf, 'MDUBgJIGVWEjvjYjGMnMN')

})