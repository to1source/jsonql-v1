const { nanoid } = require('nanoid')
const { JsonqlError } = require('jsonql-errors')
// const { getNodeCache } = require('./get-node-cache')
// call this first
// const store = getNodeCache()
const STORE_FAIL_ERR = 'unable to store the token in node cache!'
const { getCache, isCacheObj } = require('./get-cache')

/**
 * Check if the key existed
 * @param {object} store the nodeCache 
 * @param {string} token token to check
 * @return {boolean}
 */
function isCSRFTokenExist(store, token) {
  return store.has(token)
}


/**
 * Generate a token and check if it's in the store
 * @param {object} store explicitly pass the store instead of using the global one
 * @return {string} token
 */
function getToken(store) {
  // therefore we don't need to pass the init cache object here
  store = isCacheObj(store) ? store : getCache() // this could create a confusion unsync different store
  let token = nanoid()

  // test if the token exist
  if (store.has(token)) {
    // call itself again until find a key not in store
    return getToken(store)
  }
  return token
}

/**
 * create a set the token in store
 * @param {object} store same as getToken
 * @return {string} token
 */
function createCSRFToken(store) {
  let token = getToken(store)
  // not setting the TTL at the moment, but we should in the future
  if (store.set(token, Date.now())) {
    return token
  }
  throw new JsonqlError('createCSRFToken', STORE_FAIL_ERR)
}




module.exports = { getToken, createCSRFToken, isCSRFTokenExist }
