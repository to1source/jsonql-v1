// generate the pem style keys
const { generateKeyPair } = require('crypto')
const fsx = require('fs-extra')
const { join, resolve } = require('path')
const colors = require('colors/safe')
const {
  PEM_EXT,
  RSA_MIN_MODULE_LEN,
  RSA_MAX_MODULE_LEN,
  DEFAULT_PRIVATE_KEY_FILE,
  DEFAULT_PUBLIC_KEY_FILE
} = require('jsonql-constants')
const { JsonqlValidationError, JsonqlError } = require('jsonql-errors')

/**
 * Double check the input len
 * @param {number} len modulusLength
 * @return {number} on OK
 */
const checkLen = len => {
  len = parseInt(len, 10)
  if (isNaN(len)) {
    throw new JsonqlValidationError(`modulusLength needs to be an integer!`);
  }
  if (len === RSA_MIN_MODULE_LEN || len === RSA_MAX_MODULE_LEN) {
    return len;
  }
  if (len > RSA_MIN_MODULE_LEN && len < RSA_MAX_MODULE_LEN) {
    let d = len / RSA_MIN_MODULE_LEN
    if (d%2 === 0) {
      return len
    }
  }
  throw new JsonqlValidationError(`modulusLength needs to be squre by 2 and larger or equal to ${RSA_MIN_MODULE_LEN} and less than or equal to ${RSA_MAX_MODULE_LEN}`)
}

/**
 * write the files out to outputDir
 * @param {object} result
 * @param {string} result.publicKey
 * @param {string} result.privateKey
 * @return {object} promise resolve to publicKey / privateKey
 */
const outputToDir = (outputDir, result) => {
  const dir = resolve(outputDir)
  const { publicKey, privateKey } = result
  const files = [publicKey, privateKey]
  // @TODO use provide names
  const names = [
    DEFAULT_PUBLIC_KEY_FILE,
    DEFAULT_PRIVATE_KEY_FILE
  ]
  // the way this check is wrong ...
  return Promise.all(
    files.map((file, i) => {
      let filePath = join(dir, names[i])
      return fsx.outputFile(filePath, file)
        .then(() => {
          let key = names[i].replace('.' + PEM_EXT, '')
          return {[key]: filePath}
        })
      })
  ).then(result => {
    if (files.length === result.length) {
      console.info(colors.green(`pem files written to ${dir}`))
      // we need to return an object instead of array
      return result.reduce((next, last) => Object.assign(next, last), {})
    }
    throw new JsonqlError('Something went wrong, some of the file are not written!', { files, result })
  })
  .catch(err => {
    console.error(`[rsa-pem-keys] an error occuried!`, err)
  })
}

/**
 * generate a pair of public and private rsa keys in pem format
 * @param {integer} len the length of the key
 * @return {promise} resolve the publicKey privateKey
 */
function rsaPemKeysAction(len) {
  return new Promise((resolver, rejecter) => {
    generateKeyPair('rsa', {
      modulusLength: checkLen(len),
      publicKeyEncoding: {
        type: 'spki',
        format: 'pem'
      },
      privateKeyEncoding: {
        type: 'pkcs1',
        format: 'pem'
        // cipher: 'aes-256-cbc',
        // passphrase: passphrase
      }
    }, (err, publicKey, privateKey) => {
      if (err) {
        return rejecter(err);
      }
      resolver({ publicKey, privateKey })
    })
  })
}

/**
 * Wrap two steps into one with different parameters to control it
 * @param {*} passphrase for rsa
 * @param {number} [len = = RSA_MIN_MODULE_LEN] 1024
 * @param {*} outputDir for writing the files to
 */
module.exports = function rsaPemKeys(len = RSA_MIN_MODULE_LEN, outputDir = '') {
  return rsaPemKeysAction(len)
    .then(result => outputDir ? outputToDir(outputDir, result) : result)
}
