const crypto = require('crypto')
const { BASE64_FORMAT, RSA_FORMATS } = require('jsonql-constants')
const { inArray } = require('jsonql-params-validator')
const { JsonqlError } = require('jsonql-errors')
// HEX_FORMAT, 
const PRIME_LENGTH = 60

/**
 * create RSA public / private key pair
 * @param {string} [format=BASE64_FORMAT] what format of key
 * @param {number} [len=PRIME_LENGTH] prime length
 * @return {object} publicKey, privateKey
 */
module.exports = function rsaKeys(format = BASE64_FORMAT, len = PRIME_LENGTH) {
  if (len > 0 && inArray(RSA_FORMATS, format)) {
    const diffHell = crypto.createDiffieHellman(len)
    diffHell.generateKeys(format);
    return {
      publicKey: diffHell.getPublicKey(format),
      privateKey: diffHell.getPrivateKey(format)
    }
  }
  throw new JsonqlError(`Len(gth) must be greater than zero and format must be one of these two ${RSA_FORMATS}`)
}
