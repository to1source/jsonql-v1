const jwt = require('jsonwebtoken')
// HSA_ALGO, , BASE64_FORMAT
const { RSA_ALGO } = require('jsonql-constants')
const { buff } = require('jsonql-utils')
const { tokenValidator } = require('./decode-token')

// const baseOptions = { algorithm: HSA_ALGO }

/*
NOTES about the options:

algorithm
expiresIn --> exp
notBefore --> nbf
audience --> aud
issuer --> iss
subject --> sub
jwtid
noTimestamp
header
keyid
mutatePayload
*/

/**
 * Generate a jwt token
 * @param {object} payload object
 * @param {string} secret private key or share secret
 * @param {object} [options=baseOptions] configuration options
 * @return {string} the token
 */
module.exports = function jwtToken(payload, secret, options = {}) {
  if (options.algorithm === RSA_ALGO) {
    secret = buff(secret)
  }
  const opts = tokenValidator(options)
  return jwt.sign(payload, secret, opts)
}
