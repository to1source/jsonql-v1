const jwt = require('jsonwebtoken')
const { RSA_ALGO } = require('jsonql-constants')
const { isString } = require('jsonql-params-validator')
const { JsonqlError } = require('jsonql-errors')
const { buff } = require('jsonql-utils')
// variable
const baseOptions = { algorithms: RSA_ALGO }
/**
 * @param {string} token to decrypted
 * @param {string} key public key
 * @param {object} [options=baseOptions] configuration options
 * @return {object} decrypted object
 */
module.exports = function jwtDecode(token, key, options = {}) {
  let opts = Object.assign({}, baseOptions, options)
  if (isString(token)) {
    if (options.algorithms === RSA_ALGO) {
      key = buff(key)
    }
    return jwt.verify(token, key, opts)
  }
  throw new JsonqlError('Token must be a string!')
}
