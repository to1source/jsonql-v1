/* moved to @jsonql/socketio
const socketIoJwtAuth = require('./socketio/socketio-jwt-auth')
const socketIoHandshakeAuth = require('./socketio/handshake-auth')
const socketIoGetUserdata = require('./socketio/get-userdata')
*/
/* moved to @jsonql/ws
const wsVerifyClient = require('./ws/verify-client')
const wsGetUserdata = require('./ws/get-userdata')
*/
// auth
const loginResultToJwt = require('./auth/login-result-to-jwt')
const provideUserdata = require('./auth/provide-userdata')
const createTokenValidator = require('./auth/create-token-validator')


module.exports = {
  loginResultToJwt,
  provideUserdata,
  createTokenValidator
}
