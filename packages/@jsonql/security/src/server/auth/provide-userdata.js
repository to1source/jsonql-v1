const { injectToFn } = require('jsonql-utils')
const { USERDATA_PROP_NAME } = require('jsonql-constants')
/**
 * After the user login we will use this Object.define add a new property
 * to the resolver with the decoded user data
 * @param {function} resolver target resolver
 * @param {object} userdata to add
 * @return {function} added property resolver
 */
module.exports = function(resolver, userdata) {
  return injectToFn(resolver, USERDATA_PROP_NAME, userdata, true)
}
