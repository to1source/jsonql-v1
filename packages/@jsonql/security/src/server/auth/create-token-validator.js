// const { join, resolve } = require('path')
const { JsonqlValidationError } = require('jsonql-errors')
const { isString } = require('jsonql-params-validator')
const { RSA_ALGO, HSA_ALGO } = require('jsonql-constants')
const jwtDecode = require('../../jwt/jwt-decode')

/**
 * This will overwrite the developer provide one when useJwt is enable
 * @param {object} config configuration from the jsonql-koa
 * @return {function} the token decode function
 */
module.exports = function createTokenValidator(config) {
  const { useJwt, publicKey } = config
  // const tokenOpts = tokenValidator(tokenOptions);
  // debug('config', useJwt, publicKey, tokenOpts)
  let key, opts;
  if (isString(useJwt)) {
    key = useJwt;
    opts = { algorithms: HSA_ALGO }
  } else {
    key = publicKey;
    opts = { algorithms: RSA_ALGO }
  }
  if (!key) {
    throw new JsonqlValidationError(`key is not provided!`)
  }
  return function tokenValidator(token) {
    return jwtDecode(token, key, opts)
  }
}
