'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/* base.js */
var HEADERS_KEY = 'headers';

var AUTH_HEADER = 'Authorization';
var BEARER = 'Bearer';

var WS_OPT_PROP_KEY = 'wsOptions';
// we could pass the token in the header instead when init the WebSocket 
var TOKEN_DELIVER_LOCATION_PROP_KEY = 'tokenDeliverLocation';

var COOKIE_PROP_KEY = 'cookie';/* socket.js */
var TOKEN_PARAM_NAME = 'token';

// this is the value for TOKEN_DELIVER_LOCATION_PROP_KEY
var TOKEN_IN_HEADER = 'header';
var TOKEN_IN_URL = 'url';

// this method is re-use in several clients 
// move back to jsonql-constants later

/**
 * extract the new options for authorization
 * @param {*} opts configuration
 * @return {string} the header option
 */
function extractConfig(opts) {
  // we don't really need to do any validation here 
  // because the opts should be clean before calling here
  return opts[TOKEN_DELIVER_LOCATION_PROP_KEY] || TOKEN_IN_URL
}

/**
 * When running the CSRF token, and have a Auth token 
 * the csrf is missing so we need to take that into account as well
 * @param {object} config configuration
 * @param {string} token auth token 
 * @return {object} constructed the full options to pass to the WS object 
 */
function prepareHeaderOpts(config, token) {
  var obj, obj$1;

  if ( token === void 0 ) token = false;
  var wsOptions = config[WS_OPT_PROP_KEY] || {};
  var headers = config[HEADERS_KEY] || {};
  if (token) {
    headers = Object.assign(headers, ( obj = {}, obj[AUTH_HEADER] = (BEARER + " " + token), obj ));
  }
  // we might have to use the merge here
  return Object.assign({}, wsOptions, ( obj$1 = {}, obj$1[HEADERS_KEY] = headers, obj$1 ))
}

/**
 * prepare the url and options to the WebSocket
 * @param {*} url 
 * @param {*} config 
 * @param {*} [token = false] 
 * @return {object} with url and opts key  
 */
function prepareConnectConfig(url, config, token) {
  if ( config === void 0 ) config = {};
  if ( token === void 0 ) token = false;

  var tokenOpt = extractConfig(config);

  switch (tokenOpt) {
    case TOKEN_IN_URL:
      return {
        url: token ? (url + "?" + TOKEN_PARAM_NAME + "=" + token) : url,
        opts: prepareHeaderOpts(config, false)
      }
    case TOKEN_IN_HEADER:
    default: 
      return {
        url: url,
        opts: prepareHeaderOpts(config, token)
      }
  }
}

/**
 * Extract the cookie part from the header
 * @param {object} headers
 * @return {*} false when failed to find or object contain the break down cookie content 
 */
function getCookie(headers) {
  if (headers[COOKIE_PROP_KEY]) {
    var parts = headers[COOKIE_PROP_KEY].split(';');
  
    return parts.map(function (part) {
      var obj;

      var values = part.split('=');
      return ( obj = {}, obj[values[0]] = values[1], obj )
    }).reduce(function (a, b) { return Object.assign(a, b); }, {})
  }

  return false 
}

/**
 * When we use the browser WebSocket object we need to set the header via cookie 
 * And the server will see it like this "cookie: 'X-CSRF-Token=MDUBgJIGVWEjvjYjGMnMN'"
 * Therefore, we need a different way to extract the csrf / authorisation headers 
 * @param {object} headers the server received the headers 
 * @param {string} target  the target header we are looking for
 * @param {boolean} term just don't want to run into an infinite loop 
 * @return {*} the value of the target headers or false when failed to find  
 */
function extractHeaderValue(headers, target, term) {
  if ( term === void 0 ) term = false;

  if (headers[target]) {

    return headers[target]
  } else if (headers[target.toLowerCase()]) {
    
    return headers[target.toLowerCase()]
  }
  if (term) { // if we still couldn't find anything then just exit here

    return false
  }

  var cookies = getCookie(headers); 
  if (cookies) {
    // call myself again
    return extractHeaderValue(cookies, target, true)
  }

  return false
}

exports.extractConfig = extractConfig;
exports.extractHeaderValue = extractHeaderValue;
exports.prepareConnectConfig = prepareConnectConfig;
