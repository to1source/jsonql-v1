// rewrote this in class syntax instead of the function prototype 

/*
function InvalidTokenError(message) {
  this.message = message
}

InvalidTokenError.prototype = new Error()
InvalidTokenError.prototype.name = 'InvalidTokenError'
*/

class InvalidTokenError extends Error {

  constructor(message) {
    this.message = message 
  }

  get name() {
    return 'InvalidTokenError'
  }
}

export { InvalidTokenError }