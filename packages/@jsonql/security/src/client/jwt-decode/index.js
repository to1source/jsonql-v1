// JWT library entry point 

import { base64UrlDecode } from './base64_url_decode'
import { InvalidTokenError } from './invalid-token-error'


/**
 * the main interface to decode a jwt token 
 * @param {string} token 
 * @param {object} options
 * @return {*} decoded token payload  
 */
function jwtDecode(token,options) {
  if (typeof token !== 'string') {
    throw new InvalidTokenError('Invalid token specified')
  }

  options = options || {};
  var pos = options.header === true ? 0 : 1
  try {
    return JSON.parse(base64UrlDecode(token.split('.')[pos]))
  } catch (e) {
    throw new InvalidTokenError('Invalid token specified: ' + e.message)
  }
}

// using name export 
export {
  jwtDecode,
  InvalidTokenError
}

