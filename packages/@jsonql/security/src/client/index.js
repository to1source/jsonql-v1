// this is the client side code interface using ES6
import decodeToken from './decode-token/decode-token'
import tokenValidator from './decode-token/token-validator'

export {
  decodeToken,
  tokenValidator
}
