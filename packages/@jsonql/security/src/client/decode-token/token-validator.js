// we need to create a validator to check the token fields
// to determine if this token is valid of not
import {
  BOOLEAN_TYPE,
  STRING_TYPE,
  NUMBER_TYPE,
  OPTIONAL_KEY,
  ALIAS_KEY,
  HSA_ALGO
} from 'jsonql-constants'

import { createConfig, checkConfig, isObject } from 'jsonql-params-validator'

const appProps = {
  algorithm: createConfig(HSA_ALGO, [STRING_TYPE]),
  expiresIn: createConfig(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], {
    [ALIAS_KEY]: 'exp' , [OPTIONAL_KEY]: true
  }),
  notBefore: createConfig(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], {
    [ALIAS_KEY]: 'nbf', [OPTIONAL_KEY]: true
  }),
  audience: createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], {
    [ALIAS_KEY]: 'iss', [OPTIONAL_KEY]: true
  }),
  subject: createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], {
    [ALIAS_KEY]: 'sub', [OPTIONAL_KEY]: true
  }),
  issuer: createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], {
    [ALIAS_KEY]: 'iss', [OPTIONAL_KEY]: true
  }),
  noTimestamp: createConfig(false, [BOOLEAN_TYPE], {
    [OPTIONAL_KEY]: true
  }),
  header: createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], {
    [OPTIONAL_KEY]: true
  }),
  keyid: createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], {
    [OPTIONAL_KEY]: true
  }),
  mutatePayload: createConfig(false, [BOOLEAN_TYPE], {
    [OPTIONAL_KEY]: true
  })
}

/**
 * validate the token type 
 * @param {object} config options
 * @return {object} tested result  
 */
export default function tokenValidator(config) {
  if (!isObject(config)) {
    return {} // @TODO we just ignore it all together?
  }
  let result = {}
  let opts = checkConfig(config, appProps)
  // need to remove options that is false
  for (let key in opts) {
    if (opts[key]) {
      result[key] = opts[key]
    }
  }
  return result
}
