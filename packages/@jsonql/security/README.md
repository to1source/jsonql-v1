# @jsonql/security

> A collection of security related tools for jsonql 

This library provides 

- several JWT authenticatioin methods
- CSRF token management 
- Header check implementation 

and a few other things

**This is not for general use, because it included a lot jsonql specific features**

## Installation

```sh
$ npm i @jsonql/security
```

### Node command line utility

When you install this globally, you can use the command line utility.
You don't usually need to do this manually, this is build into each module
that require to generate the keys (i.e. [jsonql-koa](https://npmjs.com/package/jsonql-koa))

```sh
$ jsonql-security rsa-pem

```

or

```sh
$ node ./node_modules/@jsonql/security/cmd.js -- rsa-pem
```

This will create a pair of `RSA256` public / private keys in `PEM` format.

Or you can output that into a folder

```sh
$ jsonql-security rsa-pem --outputDir ./path/to/directory

```

### Browser tools

This module export several different modules, when you use in node, it points to the `main.js`
When you including the `dist/jsonql-security.js` in your HTML file,
it will get you a whole different tool set for browser only. And it has a global namespace `jsonqlSecurity`.

### Note to ourself 

- CSRF using `node-cache` but we need to add a interceptor to allow developer to use the storage of their choice
- JWT token should allow to do the same thing like CSRF 
- Implement check headers / Origin 

---

[Joel Chu](https://joelchu.com)

[NEWBRAN LTD](https://newbran.ch) &amp; [TO1SOURCE LTD](https://to1source.com) (c) 2020
