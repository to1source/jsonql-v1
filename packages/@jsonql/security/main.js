// main export interface for node modules
//  import export
const rsaKeys = require('./src/crypto/rsa-keys')
const rsaPemKeys = require('./src/crypto/rsa-pem-keys')
// const shaKey = require('./crypto/shaKey');
const jwtToken = require('./src/jwt/jwt-token')
const jwtDecode = require('./src/jwt/jwt-decode')
const jwtRsaToken = require('./src/jwt/jwt-rsa-token')

const { 
  decodeToken, 
  tokenValidator
} = require('./src/jwt/decode-token')
const { 
  encryptWithPublicPem, 
  decryptWithPrivatePem 
} = require('./src/crypto/encrypt-decrypt')
const {
  loginResultToJwt,
  provideUserdata,
  createTokenValidator
} = require('./src/server')
const { 
  getCache,
  isCacheObj,
  getToken,
  createCSRFToken,
  isCSRFTokenExist
} = require('./src/cache')  
const {
  extractConfig,
  prepareConnectConfig,
  extractHeaderValue
} = require('./src/socket/index.cjs')
const {
  getTokenFromUrl,
  processAuthTokenHeader,
  getTokenFromHeader,
  getWsAuthToken
} = require('./src/socket/ws-token-fn')

// output
module.exports = {
  rsaKeys,
  jwtToken,
  jwtDecode,
  jwtRsaToken,

  rsaPemKeys,

  loginResultToJwt,
  provideUserdata,
  createTokenValidator,

  decodeToken,
  tokenValidator,

  encryptWithPublicPem,
  decryptWithPrivatePem,

  // new csrf and node cache related stuff 
  getCache,
  isCacheObj,
  
  getToken,
  createCSRFToken,
  isCSRFTokenExist,

  extractConfig,
  prepareConnectConfig,
  extractHeaderValue,
  
  getTokenFromUrl,
  processAuthTokenHeader,
  getTokenFromHeader,
  getWsAuthToken
}
