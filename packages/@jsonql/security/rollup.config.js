/**
 * Rollup config for building the client package
 */
import { join } from 'path'

import buble from '@rollup/plugin-buble'
import replace from '@rollup/plugin-replace'
import commonjs from '@rollup/plugin-commonjs'
import json from '@rollup/plugin-json'
import nodeResolve from '@rollup/plugin-node-resolve'

import { terser } from "rollup-plugin-terser"

import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
// support async functions
import async from 'rollup-plugin-async'
// get the version info
import { version } from './package.json'

const env = process.env.NODE_ENV

let plugins = [
  json({
    preferConst: true
  }),
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({ preferBuiltins: true }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__PLACEHOLDER__': `version: ${version} module: UMD`
  })
]
plugins.push(terser())
if (env === 'production') {

}
plugins.push(size())

let config = {
  input: join(__dirname, 'index.js'),
  output: {
    name: 'jsonqlSecurity',
    file: join(__dirname, 'dist', 'jsonql-security.js'),
    format: 'umd',
    sourcemap: true,
    globals: {
      'socket.io-client': 'io',
      'promise-polyfill': 'Promise',
      'debug': 'debug'
    }
  },
  external: [
    'WebSocket',
    'socket.io-client',
    'io',
    'debug',
    'Promise',
    'promise-polyfill'
  ],
  plugins: plugins
};

export default config
