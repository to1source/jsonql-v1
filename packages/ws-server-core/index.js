// Not going to use the koa-socket-2 due to it's lack of support namespace
// which is completely useless for us if there is no namespace
const {
  SOCKET_STATE_KEY,
  NO_TOKEN_ERR_MSG,
  // rename them
  wsServerDefaultOptions,
  wsServerConstProps,
  // rest of the exports
  checkSocketServerType,
  jsonqlWsServerCoreAction,
  jsonqlWsServerCore
} = require('./src')
// we also need to export all the share methods here because they will get use
// in the respective external methods
const {
  getTokenFromUrl,
  processAuthTokenHeader,
  getTokenFromHeader,
  getWsAuthToken,
  extractConfig,
  prepareConnectConfig,
  getCache,
  isCacheObj
} = require('@jsonql/security')
const { 
  getContract 
} = require('./src/share/get-contract')

const {
  createWsReply,
  getDebug,
  getRainbowDebug,

  extractWsPayload,
  nil,
  getUserdata,
  isUserdata,
  prepareUserdata
} = require('./src/share/helpers')

// const debug = getDebug('main')
// also report the constants
const jsonqlWsCoreConstants = require('./src/options/constants')

const {
  getSocketHandler
} = require('./src/handles')

// export every bits out then the downstream build as they want
module.exports = {
  SOCKET_STATE_KEY,
  NO_TOKEN_ERR_MSG,
  // for the server to use
  // some of these might not be needed to export anymore @TODO
 
  getContract,
  createWsReply,
  
  extractWsPayload,

  nil,
  getUserdata,
  isUserdata,
  prepareUserdata,

  // just uniform until method, really should be in the jsonql-utils/node
  getDebug,
  getRainbowDebug,
  // this method just getting export to the top level to determine if there is a socket server
  checkSocketServerType,
  // bunch of properties
  wsServerDefaultOptions,
  wsServerConstProps,
  // this is just bunch of constants for use in other places
  jsonqlWsCoreConstants,
  // the actual methods that create the server
  jsonqlWsServerCore,
  jsonqlWsServerCoreAction,
  // @0.6.0
  getSocketHandler,
  // @0.8.5
  extractConfig,
  prepareConnectConfig,
  getCache,
  isCacheObj,
  getTokenFromUrl,
  processAuthTokenHeader,
  getTokenFromHeader,
  getWsAuthToken
}
