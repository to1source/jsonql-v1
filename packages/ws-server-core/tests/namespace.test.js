// need to find a better way to lock down the resolver to the ws
const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const contract = fsx.readJsonSync(join(__dirname, 'fixtures', 'contract.json'))
const { getRainbowDebug } = require('../src/share/helpers')
const debug = getRainbowDebug('test:namespace')

const {
  getNspInfoByConfig,
  groupByNamespace
} = require('jsonql-utils')

test(`It should able to lock down the nsp where the resolver belongs`, t => {

  const nspInfo = groupByNamespace(contract)
  // expect this to fail
  t.falsy(nspInfo.size)

  const nspInfo1 = getNspInfoByConfig({enableAuth: false, contract})
  debug('nspInfo', nspInfo1)
  t.truthy(nspInfo1.size)
})
