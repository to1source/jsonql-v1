// this is a standalone test to try the Object.defineProperty
const test = require('ava')

const { getResolverFromPayload } = require('jsonql-utils')

test.before( t => {
  const fn = require('./fixtures/fn')
  Object.defineProperty(fn, 'ctx', {
    value: function() {
      return 'I am ctx'
    }
  });
  t.context.fn = fn
})


test("I should able to defined an object property on a anonymous function", t => {
  let fn1 = t.context.fn
  const msg = fn1()

  // t.is('This is a fn', msg);
  t.is('I am ctx', msg)
})


test(`Different way to extract data from payload`, t => {
  const payload = { availableToEveryone: { args: [] } }

  const resolverName = Object.keys(payload)[0]
  const name = getResolverFromPayload(payload)
  
  t.is(resolverName, name)

})


test(`Testing the chain object style interface`, t => {
  function MyNumber(n) {
    function x () { }
    x.one = function() { n++; return this; };
    x.valueOf = function() { return n; };
    return x;
  } 

  const result = MyNumber(5).one().one().valueOf()

  t.is(7, result)
})