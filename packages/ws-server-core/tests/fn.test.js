const test = require('ava')
const { handleInterCom } = require('../src/handles/handle-intercom')
const { 
  extractConfig,
  prepareConnectConfig,
  getCache,
  isCacheObj
} = require('../index')
// const fsx = require('fs-extra')
const { join } = require('path')
const debug = require('debug')('jsonql-ws-core:test:fn')
const { 
  SOCKET_PING_EVENT_NAME, 
  CACHE_STORE_PROP_KEY
} = require('jsonql-constants')
const { resolveSocketMethod } = require('../src/resolver/resolve-socket-method')
const {
  wsServerCheckConfiguration 
} = require('../src/options')

test(`just testing that rexport methods existed`, t => {

  t.true(typeof extractConfig ===  'function')
  t.true(typeof prepareConnectConfig === 'function')
  t.true(typeof isCacheObj === 'function')
})


test.cb(`Just testing the handleInterCom function interface`, t => {
  t.plan(2)
  const config = {
    [CACHE_STORE_PROP_KEY]: getCache()
  } 
  const ws = {} 
  const deliverFn = function(...args) {
    debug('argument received from deliverFn', args)
    // t.is(args.length, 3)
    t.pass()
  }
  const req = {} 
  const connectedNamespace = 'jsonql' 
  const args = {args: [SOCKET_PING_EVENT_NAME,Date.now()]}
  
  
  handleInterCom(config, ws, deliverFn, req, connectedNamespace, args)

  // call the getToken again to observe if the node cache get re-init 
  setTimeout(() => {
    // const store = getNodeCache()
    // debug(`all keys`, store.keys())
    t.pass()
    t.end()
  }, 100)

})


test(`test the get-socket-resolver method mainly to look at the cache`, async t => {
  const opts = {
    contractDir: join(__dirname, 'fixtures'),
    resolverDir: join(__dirname, 'fixtures', 'resolvers')
  }
  const config = await wsServerCheckConfiguration(opts, {}, true)  
  const callbackFn = (str) => {
    console.info(str)
  }

  debug(`config`, config)

  const _params = [
    'jsonql',
    'chatroom',
    callbackFn,
    ['na na na', Date.now()], // this is the clean value from validateAsync
    config,
    {},
    false
  ]

  const result1 = await Reflect.apply(resolveSocketMethod, null, _params) 

  debug(result1)

  t.truthy(result1)

  // call it again 

  const result2 = await Reflect.apply(resolveSocketMethod, null, _params) 

  debug(result2)

  // debug(resolver1.toString())
  t.truthy(result2)

})
