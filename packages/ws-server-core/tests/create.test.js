// test the different between using a setter or getter to set property
const test = require('ava')
const { objDefineProps } = require('jsonql-utils')
const { SEND_MSG_FN_NAME } = require('jsonql-constants')
const { nil, getDebug } = require('../src/share/helpers')
const debug = getDebug('test:create')

test(`Create a setter as function to an object`, t => {

  let obj = {}

  obj = objDefineProps(obj, SEND_MSG_FN_NAME, nil, function sendHandler() {
    return function sendCallback(...args) {
      return args
        .filter(v => typeof v === 'string')
        .reduce((a, b) => {
          if (a==='') {
            return b
          }
          return [a, b].join(' ')
        }, '')
    }
  })

  const result = obj[SEND_MSG_FN_NAME]('I', 'am', 'a', 7, 'cat')

  debug(result)

  t.is(result,'I am a cat')
})
