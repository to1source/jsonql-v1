const debug = require('debug')('ws-client-core:fixtures:auth:logout')
/**
 * Intercept the login event 
 * @param {array} args 
 * @return {void} 
 */
module.exports = function logout(...args) {
  // @TODO 
  debug(`Intercept the logout event`, args)
}