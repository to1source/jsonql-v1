// if they didn't pass the contract then we need to grab it from the contractDir
const fsx = require('fs-extra')
const { join } = require('path')
const { isContract } = require('jsonql-utils')
const contractApi = require('jsonql-contract')
const { 
  DEFAULT_CONTRACT_FILE_NAME, 
  DEFAULT_RETRY_TIME,
  STANDALONE_PROP_KEY
} = require('jsonql-constants')
const { 
  CONTRACT_NOT_FOUND_ERR  
} = require('../options/constants')

/**
 * actually checking and reading the files
 * @param {string} contractDir contract directory
 * @return {boolean|object} false when failed
 */
function readContract(contractDir) {
  const file = join(contractDir, DEFAULT_CONTRACT_FILE_NAME)
  if (fsx.existsSync(file)) {
    let c = fsx.readJsonSync(file)
    if (isContract(c)) {
      return c
    }
  }
  return false
}

/**
 * Try to read the contract from file, if failed then retry once after 3 seconds
 * @param {object} config configuration
 * @return {promise} resolve the contract if success
 */
function getContractFromFile(config) {
  let c
  const { contractDir } = config
  return new Promise((resolver, rejecter) => {
    c = readContract(contractDir)
    if (!c) {
      if (config[STANDALONE_PROP_KEY] === true) {
        // call the contractApi to generate the contract 
        return contractApi(config)
                .then(resolver)
                .catch(rejecter)
      }
      if (config.initContract && config.initContract.then) {
        // this is a pending promise from the middleware (koa or express)
        return config
                .initContract
                .then(resolver)
                .catch(rejecter)
      }
      /* hacky way to try again - the final fallback */
      setTimeout(() => {
        c = readContract(contractDir)
        if (c) {
          return resolver(c)
        }
        rejecter(CONTRACT_NOT_FOUND_ERR)
      }, DEFAULT_RETRY_TIME)
    } else {
      resolver(c)
    }
  })
}

/**
 * get the contract with validation
 * When we init the server together with koa middleware
 * the contract might not be ready at this point, because it took 3 seconds to generate
 * therefore we change this interface to a promise and give it a chance to retry once
 * @param {object} config configuration
 * @return {promise} resolve the config with the contract as prop
 */
function getContract(config) {
  return new Promise((resolver, rejecter) => {
    if (config.contract && isContract(config.contract)) {
      return resolver(config)
    }
    getContractFromFile(config)
      .then(c => {
        config.contract = c
        resolver(config)
      })
      .catch(rejecter)
  })
}

module.exports = { getContract }
