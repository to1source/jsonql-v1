// Helpers methods
// jsonql libraries
const {
  SOCKET_CLIENT_ID_KEY,
  SOCKET_CLIENT_TS_KEY,
  INTERNAL_PREFIX,
  SOCKET_AUTH_NAME
} = require('jsonql-constants')
const {
  MODULE_NAME,
  SOCKET_STATE_KEY
} = require('../options/constants')
const {
  nil, // moved to the jsonql-utils
  isFunc,
  chainFns,
  objDefineProps,
  objHasProp,
  createWsReply,
  // port over to the jsonql-utils
  getResolverFromPayload,
  extractWsPayload,
  createEvt
} = require('jsonql-utils')

// create debug
const debug = require('debug')
const colors = require('colors/safe')

/**
 * wrapper method to create the event name for re-use 
 * @param {string} namespace 
 * @param {string} resolverName
 * @return {string}  
 */
function createInternalEvtName(namespace, resolverName) {
  return createEvt(INTERNAL_PREFIX, namespace, resolverName)
}

/**
 * Create the debug instance
 * @param {string} name
 * @return {function} the debug method
 */
const getDebug = name => debug(MODULE_NAME).extend(name)

/**
 * Create a rainbow effect debug on the first string
 * help to id where the hell things coming from
 * @param {string} name the extended name
 * @param {string} type just a switch to change color 
 * @return {function} the debug function
 */
const getRainbowDebug = (name, type = 'rainbow') => {
  const fn = getDebug(name)
  const method = type === 'rainbow' ? colors.rainbow : colors.black.bgBrightYellow
  return (str, ...args) => {
    Reflect.apply(fn, null, [method(str+'')].concat(args))
  }
}

/**
 * Should this be elsewhere because this is
 * get the userdata via the decoded jwt from the request object
 * @TODO we need to create a hook that let developer to plug in their own
 * implementation in the future,
 * therefore they could store this in a database or whatever
 * @param {object} req the request object
 * @return {object} userdata
 */
function getUserdata(req) {
  debug(`check what is missing`, req[SOCKET_STATE_KEY])
  return req && req[SOCKET_STATE_KEY] && req[SOCKET_STATE_KEY].userdata
    ? req[SOCKET_STATE_KEY].userdata
    : false
}

/**
 * Inject extra properties to the userdata object
 * @param {object} userdata the decoded userdata from the token string
 * @param {string} client_id the client_id issue by the socket server
 * @return {object} userdata injected with new properties
 */
function prepareUserdata(userdata, client_id) {
  const fn1 = () => objDefineProps(userdata, SOCKET_CLIENT_ID_KEY, client_id)
  const fn2 = () => objDefineProps(userdata, SOCKET_CLIENT_TS_KEY, Date.now())
  return chainFns([fn1, fn2])(userdata)
}

/**
 * check if an object is our userdata
 * @param {*} userdata
 * @return {boolean} true
 */
function isUserdata(userdata) {
  if (userdata !== false && typeof userdata === 'object') {
    return objHasProp(SOCKET_CLIENT_ID_KEY) && objHasProp(SOCKET_CLIENT_TS_KEY)
  }
  return false
}


/**
 * Make sure the resolver is belongs to this namespace
 * @param {string} resolverName calling from
 * @param {string} namespace currently connected namespace
 * @param {object} nspGroup namespace --> resolvers object map
 * @return {*} false when not found, or the params from the resolver
 */
function matchResolverByNamespace(resolverName, namespace, nspGroup) {
  const g = nspGroup[namespace]
  if (g && g[resolverName]) {
    return g[resolverName]
  }
  debug(`matchResolverByNamespace`, `${resolverName} is not in ${namespace}`)
  // we fail this silencely for now and see what happen
  return false
}

/**
 * This is for the intercom resolver to fetch the params from contract 
 * @param {object} contract 
 * @param {string} resolverName
 * @return {object} params 
 */
function getMethodParams(contract, resolverName) {
  const c = contract[SOCKET_AUTH_NAME]
  if (c) {
  
    return c[resolverName]
  }

  return false
}


// export
module.exports = {
  createInternalEvtName,

  getDebug,
  getRainbowDebug,

  createWsReply,

  extractWsPayload,

  getUserdata,
  prepareUserdata,
  isUserdata,

  nil,
  isFunc,

  getResolverFromPayload,

  matchResolverByNamespace,
  getMethodParams
}
