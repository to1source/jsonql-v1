/*
We now group all the ping, login, logout, disconnect, switch event
into one group as the intercom, and all using the same internal resolverName INTERCOME_RESOLVER_NAME
and use the type to tell the different, the main com type REPLY ACKNOWLEDGE will get handle by the ee
but the rest are outside of that scope, therefore it won't affect the currenct operation
*/
const { getDebug } = require('../share/helpers')
const debug = getDebug('handle-intercome')
const {
  INTERCOM_RESOLVER_NAME,
  SOCKET_PING_EVENT_NAME,
  SA_LOGIN_EVENT_NAME,
  LOGIN_EVENT_NAME ,
  LOGOUT_EVENT_NAME,
  DISCONNECT_EVENT_NAME,
  CACHE_STORE_PROP_KEY
} = require('jsonql-constants')

const { handleInitPing } = require('./handle-init-ping')
const { handleLogout } = require('./handle-logout')
const { handleStandaloneLogin } = require('./handle-standalone-login')
const { handleDisconnect } = require('./handle-disconnet')
const { handleUnknownPayload } = require('./handle-unknown-payload')
// we handle the get interceptor here in one place
const { getCacheSocketInterceptor } = require('jsonql-resolver')

/**
 * @TODO handle the intercom event disconnect / switch-user (future feature)
 * @param {object} config configuraton
 * @param {object} ws the socket instance
 * @param {function} deliverFn framework specific to send out message
 * @param {object} req the request object
 * @param {string} connectedNamespace the namesapce its connected to
 * @param {array} payload extract from the payload, the first is the event name
 * @param {*} userdata it could be empty most of the time
 * @return {void} nothing return just send back a reply
 */
function handleInterCom(config, ws, deliverFn, req, connectedNamespace, payload, userdata) {
  // the payload is part wrap iside the arg key
  const { args } = payload
  // the most important one to take a look at
  const eventName = args[0]

  args.splice(0, 1) // take the first one out, it could be empty

  debug('handleIntercom', eventName, args)

  let interceptorFn
  
  switch (eventName) {
    case SA_LOGIN_EVENT_NAME:
    case LOGOUT_EVENT_NAME:
    case DISCONNECT_EVENT_NAME:
    case LOGIN_EVENT_NAME:
      const key = [INTERCOM_RESOLVER_NAME, eventName].join('-')
      const store = config[CACHE_STORE_PROP_KEY]

      interceptorFn = getCacheSocketInterceptor(eventName, config, key, store)
      // interceptorFn = getSocketInterceptor(eventName, config)
    break
    default:
      // nothing
  }

  switch (eventName) {
    case SOCKET_PING_EVENT_NAME:
      return handleInitPing(config, ws, deliverFn, req, connectedNamespace, args)
    case SA_LOGIN_EVENT_NAME:
    case LOGIN_EVENT_NAME:
      return handleStandaloneLogin(config, ws, deliverFn, req, connectedNamespace, args, interceptorFn)
    case LOGOUT_EVENT_NAME:
      return handleLogout(config, ws, deliverFn, req, connectedNamespace, args, userdata, interceptorFn)
    case DISCONNECT_EVENT_NAME:
      return handleDisconnect(config, ws, deliverFn, req, connectedNamespace, args, userdata, interceptorFn)
    default:
      handleUnknownPayload(config, ws, deliverFn, req, connectedNamespace, args, userdata)
  }
}

module.exports = { handleInterCom }
