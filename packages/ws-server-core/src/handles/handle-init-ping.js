// we add this new event to deal with a confirmation of login problem
// when a client first connect to the server, it will issue a ping event 
// as soon as, then it will received a CSRF token, and all subsequence connection 
// will be using this token

// @TODO need to add hook that control the issue of CSRF token, and validation of the token 
// e.g. allow the user to store the token in a Redis, and subsequenely check it 

const { createCSRFToken } = require('@jsonql/security')
const { 
  CSRF_HEADER_KEY, 
  SOCKET_PING_EVENT_NAME,
  CACHE_STORE_PROP_KEY
} = require('jsonql-constants')
const { deliverIntercomMsg } = require('../resolver/resolver-methods')
const { getDebug } = require('../share/helpers')
const debug = getDebug('handles:init-ping')

/**
 * @TODO handle the intercom event disconnect / switch-user (future feature)
 * @param {object} config configuraton 
 * @param {object} ws the socket instance 
 * @param {function} deliverFn framework specific to send out message
 * @param {object} req the request object
 * @param {string} connectedNamespace the namesapce its connected to 
 * @param {array} args extract from the payload, the first is the event name
 * @return {void} nothing return just send back a reply
 */
function handleInitPing(config, ws, deliverFn, req, connectedNamespace, args) {
  debug('req.headers', req.headers)
  
  const store = config[CACHE_STORE_PROP_KEY]

  const token = createCSRFToken(store)
  // @TODO add hook to process this token 
  const payload = {[CSRF_HEADER_KEY]: token}

  deliverIntercomMsg(deliverFn, payload, SOCKET_PING_EVENT_NAME)
}

module.exports = { handleInitPing }