// All these methods move from the ws-server
// we need to make them all generic
const { getSocketHandler } = require('./get-socket-handler')

module.exports = { getSocketHandler }
