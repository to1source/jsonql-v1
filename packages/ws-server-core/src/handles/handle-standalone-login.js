// handle the login method when this is in standalone mode
const { getCacheSocketInterceptor } = require('jsonql-resolver')
const { JsonqlAuthorisationError } = require('jsonql-errors')
const { 
  INTERCOM_RESOLVER_NAME,
  SA_LOGIN_EVENT_NAME,
  LOGIN_EVENT_NAME,
  CACHE_STORE_PROP_KEY,
  TOKEN_PARAM_NAME
} = require('jsonql-constants')
const { jwtRsaToken } = require('@jsonql/security')

const { deliverIntercomMsg, handleError } = require('../resolver/resolver-methods')
const { getMethodParams } = require('../share/helpers')
const { getDebug } = require('../share/helpers')
const debug = getDebug('handle-intercome:login')

/**
 * When standalone:true and there is a login method in the folder
 * then we will use that resolver to deal with the login over socket
 * @param {object} config configuraton
 * @param {object} ws the socket instance
 * @param {function} deliverFn framework specific to send out message
 * @param {object} req the request object
 * @param {string} connectedNamespace the namesapce its connected to
 * @param {array} args extract from the payload, the first is the event name
 * @param {function} interceptorFn developer provide method
 * @return {void} nothing return just send back a reply
 */
function handleStandaloneLogin(config, ws, deliverFn, req, connectedNamespace, args, interceptorFn) {
  // @TODO 
  debug(`handleDisconnect called`, args)
  const { contract } = config 
  const resolverName = config[DISCONNECT_FN_NAME_PROP_KEY]
  const params = getMethodParams(contract, resolverName)

  const key = [INTERCOM_RESOLVER_NAME, SA_LOGIN_EVENT_NAME].join('-')
  const store = config[CACHE_STORE_PROP_KEY]

  const interceptorFn = getCacheSocketInterceptor(LOGIN_EVENT_NAME, config, key, store)

  if (isFunc(interceptorFn) && params) {
    // @TODO need to figure out how to extract the params here
    return validateInput(args, params)
      .then(_args => {
        return Reflect.apply(interceptorFn, null, _args)
      })
      .then(result => {
        // if there is no key then just throw error 
        return {
          key: config.privateKey,
          payload: result 
        }
      })
      // this is a standalone login therefore we will need to deliver the token back to the client 
      .then(({key, payload}) => {
        const token = jwtRsaToken(payload, key)
        deliverIntercomMsg(deliverFn, {[TOKEN_PARAM_NAME]: token}, SA_LOGIN_EVENT_NAME)
        return token // just return it
      })
      .catch(err => {
        debug('[handleStandaloneLogin] error', err)
        // and here if any error occured then we need to deliver back to the client as well
        handleError(deliverFn, INTERCOM_RESOLVER_NAME, err)

        return new JsonqlAuthorisationError('handleStandaloneLogin', err)
      })
      // what if the argument not pass the validation? 
  } else {
    debug(`[handleStandaloneLogin] resolver not found`, interceptorFn, `or params not found`, params)

    handleError(deliverFn, INTERCOM_RESOLVER_NAME, {
      message: '[handleStandaloneLogin] resolver not found or params not found'
    })
  }
}

module.exports = { handleStandaloneLogin }
