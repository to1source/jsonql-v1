// when user disconnect or can we issue a reverse disconnect from the same api? 
const { getCacheSocketInterceptor }  = require('jsonql-resolver')
const { 
  INTERCOM_RESOLVER_NAME,
  DISCONNECT_EVENT_NAME,
  CACHE_STORE_PROP_KEY,
  DISCONNECT_FN_NAME_PROP_KEY
} = require('jsonql-constants')
const { validateInput } = require('../resolver/resolver-methods')
const { getMethodParams, isFunc } = require('../share/helpers')
const { getDebug } = require('../share/helpers')
const debug = getDebug('handle-intercome:disconnect')

/**
 * This method suppose to try to take in the user defined disconnect interceptor 
 * also we could do some internal clean up? but this is framework specific operation
 * so this should be the last in the chain, or the first? 
 * @param {object} config configuraton 
 * @param {object} ws the socket instance 
 * @param {function} deliverFn framework specific to send out message
 * @param {object} req the request object
 * @param {string} connectedNamespace the namesapce its connected to 
 * @param {array} args extract from the payload, the first is the event name
 * @param {*} userdata it could be empty most of the time
 * @return {void} nothing return just send back a reply 
 */
function handleDisconnect(config, ws, deliverFn, req, connectedNamespace, args, userdata) {
  // @TODO 
  debug(`handleDisconnect called`, args)
  const { contract } = config 
  const resolverName = config[DISCONNECT_FN_NAME_PROP_KEY]
  const params = getMethodParams(contract, resolverName)

  const key = [INTERCOM_RESOLVER_NAME, DISCONNECT_EVENT_NAME].join('-')
  const store = config[CACHE_STORE_PROP_KEY]

  const interceptorFn = getCacheSocketInterceptor(DISCONNECT_EVENT_NAME, config, key, store)

  if (isFunc(interceptorFn) && params && params.params) {
    // @TODO need to figure out how to extract the params here
    return validateInput(args, params.params)
      .then(_args => {
        return Reflect.apply(interceptorFn, null, _args)
      })
      .catch(err => {
        debug('[handleDisconnect] error', err)
        debug(args, params.params)
      })
      // what if the argument not pass the validation? 
  } else {
    debug(`[handleDisconnect] resolver not found`, interceptorFn, `or params not found`, params)
  }
}

module.exports = { handleDisconnect }