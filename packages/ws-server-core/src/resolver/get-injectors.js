// add required properties to the resolver
const {
  SOCKET_NAME,
  INIT_CLIENT_PROP_KEY
} = require('jsonql-constants')
const { injectToFn } = require('jsonql-utils')
const { provideUserdata } = require('@jsonql/security')
const { injectNodeClient } = require('jsonql-resolver')

const { setupOnMethod } = require('./setup-on-method')
const { setupSendMethod } = require('./setup-send-method')
const { setupToMethod } = require('./setup-to-method')

const { getDebug } = require('../share/helpers')



const debug = getDebug(`share:addProperty`)

/*
  This method change from injecting the property via the promise chain 
  to organize injectors to fns for the resolver method to work with  
 */

/**
 * We just pass the whole lot, use some then passing them back with additonal prop to the resolver 
 * @param {*} fn 
 * @param {*} deliverFn 
 * @param {string} namespace
 * @param {*} resolverName 
 * @param {*} ws 
 * @param {*} userdata 
 * @param {*} opts 
 * @return {array} params for the next chain call
 */
const injectWsProp = (fn, deliverFn, namespace, resolverName, ws, userdata, opts) => [
  injectToFn(fn, SOCKET_NAME, ws),
  deliverFn, 
  namespace,
  resolverName, 
  ws, 
  userdata, 
  opts
]

const injectUserdata = (fn, deliverFn, namespace, resolverName, ws, userdata, opts) => [
  userdata ? provideUserdata(fn, userdata) : fn,
  deliverFn, 
  namespace,
  resolverName, 
  ws, 
  userdata, 
  opts
]

const injectSendMethod = (fn, deliverFn, namespace, resolverName, ws, userdata, opts) => [
  setupSendMethod(deliverFn, fn, resolverName, opts),
  deliverFn, 
  namespace,
  resolverName, 
  ws, 
  userdata, 
  opts
]

const injectOnMethod = (fn, deliverFn, namespace, resolverName, ws, userdata, opts) => [
  setupOnMethod(fn, namespace, resolverName, opts),
  deliverFn, 
  namespace,
  resolverName, 
  ws, 
  userdata, 
  opts
]

// the name injectoToFn already used 
const injectToMethod = (fn, deliverFn, namespace, resolverName, ws, userdata, opts) => [
  setupToMethod(fn, namespace, resolverName, userdata, opts),
  deliverFn,
  namespace,
  resolverName,
  ws,
  userdata,
  opts 
]

/**
 * This is the last in the chain, and we just return the resolver itself
 * @TODO should we integrate this into the to method
 * also this part should be part of the jsonql-resolver method
 * 
 * @param {function} resolver
 * @param {*} deliverFn 
 * @param {*} resolverName 
 * @param {*} ws 
 * @param {*} userdata 
 * @param {*} opts 
 * @return {function} resolver with all the props added 
 */
const injectClient = (resolver, deliverFn, resolverName, ws, userdata, opts) => {
  if (opts[INIT_CLIENT_PROP_KEY] && opts[INIT_CLIENT_PROP_KEY].then) {
    debug(`using INIT_CLIENT_PROP_KEY to add clients to the resolver`)

    return opts[INIT_CLIENT_PROP_KEY]
      .then(clients => injectNodeClient(resolver, clients))
  }
  debug('=========================== injectClient ==============================\n', typeof resolver )
  return resolver
}

/**
 * This is the end of the chain method and just return the resolver
 * @param {*} resolver 
 * @return {*} resolver
 */
const resolverInjectorFinalFn = resolver => resolver


/**
 * @return {array} list of injectors 
 */
const getInjectors = () => {
  return [
    injectWsProp,
    injectUserdata,
    injectSendMethod,
    // @TODO injectOnMethod,
    // @TODO injectToMethod,
    resolverInjectorFinalFn
  ]
}

module.exports = { getInjectors }
