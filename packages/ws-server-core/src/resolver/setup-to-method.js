// this is the new internal event 
// resolverName.to('resolverName').send(...args)
// then you can call another socket resolver or just a regular resolver
const { 
  TO_MSG_FN_NAME, 
  EMIT_MSG_FN_NAME, // use this instead of send less confusing  
  EMIT_REPLY_TYPE 
} = require('jsonql-constants')
const { objDefineProps } = require('jsonql-utils')
const { JsonqlError } = require('jsonql-errors')
const { nil, getRainbowDebug } = require('../share/helpers')
const debug = getRainbowDebug('create-send-method', 'x')

const { createInternalEvtName } = require('../share/helpers')

/**
 * It was just a simple resolverName.send setter
 * Now we make it full feature, also it should pair with a `receive` method?
 * @param {function} resolver the function itself
 * @param {string} resolverName the name of this resolver
 * @param {object} opts configuration 
 * @return {function} resolver with a `send` method property
 */
const setupToMethod = function(resolver, resolverName, opts) {

  const { nspInfo, eventEmitter, contract, log } = opts
  const { namespaces } = nspInfo
  const { socket } = contract

  return objDefineProps(
    resolver,
    TO_MSG_FN_NAME,
    nil,
    function sendHandler() {
      /**
       * The way how to call this --> resolverName.to(resolverName).send(...args)
       * or use the alias resolverName.to(resolverName).emit(...args)
       */
      return function sendCallback(otherResolverName) {
        if (otherResolverName !== resolverName) {
          // check if this resolverName existed 
          debug('sendCallback', args)
          // this method doesn't do anything but just emit the event 
          // the internal-event-handler will take care of the rest 
          const emitFn = (...args) => {
            log('emitFn', args)
          }
          return { [EMIT_MSG_FN_NAME]: emitFn }
        } 
        // if we throw here will break the chain 
        throw new JsonqlError(`You can not call yourself!`)
      }
    }
  )
}

module.exports = { setupToMethod }
