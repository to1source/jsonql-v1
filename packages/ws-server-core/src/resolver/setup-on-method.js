// this is brand new resolver.on method to listen to the message 
const { 
  SEND_ON_FN_NAME, 
  ON_RESULT_FN_NAME 
} = require('jsonql-constants')
const { objDefineProps } = require('jsonql-utils')

const { nil, getRainbowDebug } = require('../share/helpers')
const debug = getRainbowDebug('create-send-method', 'x')

/**
  There is a problem here, when another call another resolver 
  if they just listen to the event generate on that namespace, on that resolver 
  it could also trigger other listeners to trigger the callback 
  so how do we id this one off event 

  So what we need to do is 

  resolverA --> eventIssuer (with a random id) --> execute resolverB --> return result 
  --> eventIssuer (result + random id) --> resolverA 

 */


/**
 * @TODO setup a new resolver.on method that can listen to reply message also 
 * This will serve as the new inter communication method as well 
 * when call with just a cb resolver.on(cb) it listen to the client 
 * when call with resolver.on(resolverName, cb) then it will become the inter communication tool 
 * @param {*} resolver 
 * @param {string} namespace where this resolver 
 * @param {*} resolverName 
 * @param {object} opts configuration
 * @return {function} resolver
 */
function setupOnMethod(resolver, namespace, resolverName, opts) {
  return objDefineProps(
    resolver,
    SEND_ON_FN_NAME,
    nil,
    function onHandler() {
      /**
       * How to call this method: resolverName.on(function(resolverName, result) { })
       * the function will received two params, resolverName where its coming from 
       * and result, the result return from that resolver
       * This is the listener for internal events
       */
      return function sendOnback(fn) {
        
        
        debug('onCallback', args)
        
        
      }
    }
  )
}


module.exports = { setupOnMethod }