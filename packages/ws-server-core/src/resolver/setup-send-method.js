const { SEND_MSG_FN_NAME, EMIT_REPLY_TYPE } = require('jsonql-constants')
const { objDefineProps } = require('jsonql-utils')
const { deliverMsg } = require('./resolver-methods')
const { nil, getRainbowDebug } = require('../share/helpers')
const debug = getRainbowDebug('create-send-method', 'x')

/**
 * It was just a simple resolverName.send setter
 * Now we make it full feature, also it should pair with a `receive` method?
 * @param {function} deliveryFn framework specific to deliver message to client
 * @param {function} resolver the function itself
 * @param {string} resolverName the name of this resolver
 * @param {object} opts configuration 
 * @return {function} resolver with a `send` method property
 */
const setupSendMethod = function(deliverFn, resolver, resolverName, opts) {
  
  return objDefineProps(
    resolver,
    SEND_MSG_FN_NAME,
    nil,
    function sendHandler() {
      // @NOTE we don't need to validate here, if we need to in the future,
      // we should validate it against the return params
      return function sendCallback(...args) {
        
        debug('======= sendCallback ========', args)
        
        deliverMsg(deliverFn, resolverName, args, EMIT_REPLY_TYPE)
      }
    }
  )
}

module.exports = { setupSendMethod }
