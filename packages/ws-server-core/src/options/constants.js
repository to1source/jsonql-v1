const { join } = require('path')
const {
  JS_WS_SOCKET_IO_NAME,
  JS_WS_NAME,
  SOCKET_NAME,
  AUTH_TYPE,
} = require('jsonql-constants')
// use our own nameing instead of generic state
const SOCKET_STATE_KEY = 'jsonqlState'

const SOCKET_AUTH_DIR = join(SOCKET_NAME, AUTH_TYPE)
// short hand
const SOCKET_IO = JS_WS_SOCKET_IO_NAME
const WS = JS_WS_NAME
const AVAILABLE_SERVERS = [ SOCKET_IO, WS ]
// just message
const SOCKET_NOT_DEFINE_ERR = 'socket is not define in the contract file!'

const SERVER_NOT_SUPPORT_ERR = 'is not supported server name!'

const SECRET_MISSING_ERR = 'Secret is required!'

const MODULE_NAME = 'jsonql-ws-server-core'

const CONTRACT_NOT_FOUND_ERR = `No contract presented!`

const NO_TOKEN_ERR_MSG = `No token!`

const HEADER_CHECK_FAIL_MSG = 'Header check failed'
// prefix to those internal events
const INTERNAL_PREFIX = 'internal'

module.exports = {
  SOCKET_IO,
  WS,
  AVAILABLE_SERVERS,
  SOCKET_NOT_DEFINE_ERR,
  SERVER_NOT_SUPPORT_ERR,
  SECRET_MISSING_ERR,
  MODULE_NAME,
  CONTRACT_NOT_FOUND_ERR,
  SOCKET_AUTH_DIR,
  SOCKET_STATE_KEY,
  
  HEADER_CHECK_FAIL_MSG,
  NO_TOKEN_ERR_MSG,

  INTERNAL_PREFIX
}
