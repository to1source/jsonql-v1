// WS options
// methods
const { join } = require('path')
const fsx = require('fs-extra')

const { getCache, isCacheObj } = require('@jsonql/security')
const { JsonqlValidationError } = require('jsonql-errors')
const { getNspInfoByConfig } = require('jsonql-utils')
const {
  checkConfig,
  checkConfigAsync
} = require('jsonql-params-validator')

// properties
const {
  PEM_EXT,
  PUBLIC_KEY_NAME,
  PRIVATE_KEY_NAME,
  CACHE_STORE_PROP_KEY,
  EVENT_EMITTER_PROP_KEY,
  KEYS_DIR_PROP_KEY,
  ENABLE_AUTH_PROP_KEY,
  PUBLIC_NAMESPACE_PROP_KEY,
  NSP_INFO_PROP_KEY
} = require('jsonql-constants')
const { SECRET_MISSING_ERR } = require('./constants')
const {
  wsDefaultOptions,
  wsConstProps,
  socketAppProps
} = require('./defaults')
const { getContract } = require('../share/get-contract')
const { WsServerEventEmitter } = require('./event-emitter')
const { setupInternalEventListeners } = require('../handles/setup-internal-event-handler')

const debug = require('debug')('jsonql-ws-server-core:options')


/**
 * We need this to find the socket server type
 * @param {*} config
 * @return {string} the name of the socket server if any
 */
function checkSocketServerType(config) {
  return checkConfig(config, socketAppProps)
}

/**
 * Add pre-check method to see if it's already pass the check earlier
 * @param {object} config configuration
 * @param {object} checkMap as its
 * @return {object} checked opts
 */
function localCheckConfig(config, checkMap) {
  return checkConfigAsync(config, Object.assign(wsDefaultOptions, checkMap), wsConstProps)
}

/**
 * Wrapper method to check if there is already a cache object, if not init a new one
 * @param {object} opts configuration
 * @return {object} node cache instance 
 */
function getCacheStore(opts) {
  return opts[CACHE_STORE_PROP_KEY] && isCacheObj(opts[CACHE_STORE_PROP_KEY]) 
    ? opts[CACHE_STORE_PROP_KEY] 
    : getCache()
}

/**
 * We take the step two onward from the wsCheckConfig
 * @param {object} config configuration already checked
 * @return {promise} resolve to the options that is clean and ready
 */
function initWsServerOption(config) {
  return Promise.resolve(config)
            .then(getContract)
            // processing the key
            .then(opts => {
              const nspInfo = getNspInfoByConfig(opts)
              // debug('take a look at nspInfo', nspInfo)
              // add a check here and make sure the nspInfo is correct 
              if (!nspInfo[PUBLIC_NAMESPACE_PROP_KEY]) {
                throw new JsonqlValidationError(`initWsServerOption`, nspInfo)
              }
              // @0.7.0 add a new property to the config object
              opts[NSP_INFO_PROP_KEY] = nspInfo 
              
              // next the auth server options
              if (opts[ENABLE_AUTH_PROP_KEY] === true) {
                // @TODO should get rip of this useJwt as string and
                if (opts[KEYS_DIR_PROP_KEY]) {
                  opts[PUBLIC_KEY_NAME] = fsx.readFileSync(join(opts[KEYS_DIR_PROP_KEY], [PUBLIC_KEY_NAME, PEM_EXT].join('.')))
                  opts[PRIVATE_KEY_NAME] = fsx.readFileSync(join(opts[KEYS_DIR_PROP_KEY], [PRIVATE_KEY_NAME, PEM_EXT].join('.')))
                } else {
                  throw new JsonqlValidationError(`initWsServerOption`, SECRET_MISSING_ERR)
                }
              }
              const { log } = opts 
              // we init an cache object here now for re-use through out the app 
              opts[CACHE_STORE_PROP_KEY] = getCacheStore(opts)
            
              opts[EVENT_EMITTER_PROP_KEY] = new WsServerEventEmitter(log)
              
              // @TODO continue later
              setupInternalEventListeners(opts)

              return opts
            })
}

/**
 * This is a combine method that will check the options
 * @param {object} config user supply
 * @param {object} checkMap the additional property to add to the checkMap
 * @param {boolean} [init=false] this is mainly for internal test  
 * @return {object} promise resolve the opts
 */
function wsServerCheckConfiguration(config, checkMap = {}, init = false) {
  return localCheckConfig(config, checkMap)
    .then(config => init ? initWsServerOption(config) : config)
}

// breaking change export as name also the options for merge with the upstream server
module.exports = {
  checkSocketServerType,
  initWsServerOption,
  wsServerCheckConfiguration,
  wsDefaultOptions,
  wsConstProps
}
