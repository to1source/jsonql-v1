#!/usr/bin/env node
const { join } = require('path');
const fs = require('fs-extra');
const debug = require('debug')('jsonql-contract:test-doc-api');
const { inspect } = require('util');

/*
const watcher = require('./lib/watcher');

watcher({
  resolverDir: join(__dirname, 'tests', 'fixtures', 'resolvers'),
  interval: 1000,
  watch: 'true', // try the string option
  jsType: 'cjs'
});
*/
console.time('test')
let startBase, fullYear

const increase = (base, percent, year, target) => {
  if (!startBase && !fullYear) {
    startBase = base
    fullYear = year
  }
  base = (base * (100 + percent)) / 100;
  --year;
  if (target && base >= target) {
    console.info(`In year ${fullYear - year} and you will get ${Math.round(base)}`)
    return;
  }
  if (year > 0) {
    console.info(`(add ${startBase} each year) ${Math.round(base)} at ${percent}% interest for ${fullYear - year} comes to `, Math.round(base));
    increase(base + startBase, percent, year, target)
  } else {
    console.info(`Finally ${startBase} at ${percent}% interest for ${fullYear} years comes to `, Math.round(base));
  }
}

increase(40000, 8, 20, 1000000)

console.timeEnd('test')
