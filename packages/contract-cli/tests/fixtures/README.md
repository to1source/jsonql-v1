@TODO

Release in 1.1

```sh
$ jsonql-contract remote https://hostname/jsonql
```

This will fetch the contract from a remote server.

~~If the remote server require a `key=value` pair to access it.~~

This is breaking change, the contract.json no longer accept query parameter.
You need to pass the password via the parameter

```sh
$ jsonql-contract remote https://hostname/jsonql --pass = password
```

To specify the output location of output folder

```sh
$ jsonql-contract remote https://hostname/jsonql --out=/path/to/your/contract
```
