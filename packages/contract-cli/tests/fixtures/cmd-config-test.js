
const { join } = require('path');

const config = {
  inDir: join(__dirname, 'resolvers'),
  outDir: join(__dirname, 'tmp', 'cmd' ,'config-test')
};

module.exports = config;
