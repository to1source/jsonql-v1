// just to test if the search able to do the folder level as well

/**
 * @param {string} [message='nothing'] just something
 * @return {void} nothing to return
 */
module.exports = function anotherMethod(message='nothing') {
  console.log(`Do ${message} for the time being`);
}
