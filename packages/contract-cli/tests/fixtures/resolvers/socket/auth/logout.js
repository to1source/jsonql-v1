/**
 * logout interceptor
 * @param {object} userdata the user that just logout
 * @return {void}
 */
module.exports = function logout(userdata) {
  // when user logout from the nsp, they will send one last call with their userdata
  // to notify the server that this user has logout
  // if this file presented then this interceptor will get call
  console.log(userdata, 'just logout')
}
