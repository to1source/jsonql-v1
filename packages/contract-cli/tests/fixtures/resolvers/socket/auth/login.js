/**
 * When the socket in standalone mode this should get take into the contract as well
 * @param {string} username username
 * @param {string} password password
 * @return {object} userdata
 */
module.exports = function login(username, password) {
  return { username, ts: Date.now() }
}
