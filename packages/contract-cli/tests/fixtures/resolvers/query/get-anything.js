/**
 * A function without any parameters
 * @param {...string} args that doesn't existed yet
 * @return {string} a message
 */
function fn() {
  return 'This function has no params';
}

module.exports = fn;
