// we are using this to test a various way to write the resolver methods
// and capture the output style for jsdoc
const DEFAULT_VALUE = 10;

function sum(a, b) {
  return a + b;
}

/**
 * @param {number|string} param1 first
 * @param {number} [param2=10] second
 * @return {number} sum of both
 */
const getSomethingElse = function(param1, param2 = DEFAULT_VALUE) {
  return sum(param1, param2);
}

module.exports = getSomethingElse;
