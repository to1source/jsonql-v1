// This is a public available mutation method
// which is highly unlikely to have for mutation

/**
 * @param {*} payload the payload
 * @param {*} condition the condition
 * @return {object} put them together and send back
 */
module.exports = function(payload, condition) {
  return { payload, condition };
}
