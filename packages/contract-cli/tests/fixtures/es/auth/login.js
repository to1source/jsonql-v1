/**
 * @param {string} username username
 * @param {string} password password
 * @return {object} user object
 */
export default function login(username, password) {
  return { username, ts: Date.now() }
}
