// if the source is ES files then we will use the contract.json
// file to generate a import export files and copy the esm/import.js
// to the target directory
const { join } = require('path')
const fsx = require('fs-extra')
const { forEach } = require('lodash')
const { inArray } = require('jsonql-utils')
const {
  DEFAULT_RESOLVER_LIST_FILE_NAME,
  DEFAULT_RESOLVER_IMPORT_FILE_NAME,
  QUERY_NAME,
  MUTATION_NAME,
  SOCKET_NAME,
  AUTH_TYPE,
  MODULE_TYPE
} = require('jsonql-constants')
const { getDebug } = require('../utils')
const debug = getDebug('es-extra')

const IMPORT_JS_TEMPLATE = 'import.template.js'

/**
 * Get the path to the file from the `files` key
 * @param {string} type of resolver
 * @param {object} opts contract content
 * @return {string} relative path to the resolver
 */
const getFilePath = (type, opts) => {
  let { file } = opts;
  let parts = file.split('/')
  let output = []
  const ctn = parts.length;
  for (let i = ctn; i >= 0; --i) {
    let p = parts[i]
    if (p) {
      output.push(p)
      if (p === type) {
        output.reverse()
        return output.join('/')
      }
    }
  }
}

/**
 * Read the contract and generate the import export list files
 * @param {object} contract json
 * @return {void} nothing
 */
const generateExportList = contract => {
  const target = [QUERY_NAME, MUTATION_NAME, SOCKET_NAME, AUTH_TYPE] // @TODO replace with constants
  let src = ''
  let out = []
  forEach(contract, (resolvers, type) => {
    if (inArray(target, type)) {
      forEach(resolvers, (opts, resolverName) => {
        let filePath = getFilePath(type, opts)
        let name = type + resolverName;
        src += `import ${name} from './${filePath}' \r\n`
        out.push(name)
      })
    }
  })
  return Promise.resolve(
    src + '\r\n' + `export {
${'\t' + out.join(',\r\n')}
}`
  )
}

/**
 * Create the import and export file
 * @param {object} contract json
 * @param {string} resolverDir where to put the file
 */
const createResolverListFile = (contract, resolverDir) => {
  let dist = join(resolverDir, DEFAULT_RESOLVER_LIST_FILE_NAME)
  return generateExportList(contract)
    .then(content => fsx.outputFile(dist, content))
}

/**
 * Adding extra ES6 require files
 * @param {object} contract json object
 * @param {string} sourceType script or module
 * @param {string} resolverDir where to put the files
 * @return {void} nothing
 */
function esPostProcess(sourceType, resolverDir, contract) {
  debug('postProcess: sourceType:', sourceType, resolverDir)
  if (sourceType === MODULE_TYPE) {
    let src = join(__dirname , IMPORT_JS_TEMPLATE)
    let dist = join(resolverDir, DEFAULT_RESOLVER_IMPORT_FILE_NAME)
    // copy the export template
    return fsx.copy(src , dist)
      .then(() => createResolverListFile(contract, resolverDir))
      .catch(err => {
        console.error('Generate files for ES6 module style resolver FAILED!', err)
      })
  }
  return Promise.resolve(true)
}

module.exports = { esPostProcess }
