const test = require('ava')

const { join, resolve } = require('path')
const { spawn } = require('child_process')
const fsx = require('fs-extra')
const { DEFAULT_CONTRACT_FILE_NAME } = require('jsonql-constants')

const cliFile = resolve(join(__dirname, '..' , 'cli.js'))
const contractApi = require('../index')

const debug = require('debug')('jsonql-contract:test:watcher')

const srcDir = resolve(join(__dirname, 'fixtures', 'resolvers'))
const baseDir = resolve(join(__dirname, 'fixtures', 'tmp', 'watcher'))
const outDir = join(baseDir, 'contract')

const srcFile = join(srcDir, 'mutation', 'set-with-destruction.js')
const destFile = join(baseDir, 'resolvers', 'mutation', 'set-with-destruction.js')

// first we copy everything from the resolvers directory to the outDir
// because we need to change the files
test.before( async t => {
  const inDir = join(baseDir, 'resolvers')
  await fsx.copy(srcDir, inDir)
  t.context.inDir = inDir;
  t.context.outDir = outDir;
  // we remove one file from the folder then add it back later
  fsx.removeSync( destFile )

  // first generate the contract file
  t.context.result = await contractApi({
    resolverDir: inDir,
    contractDir: outDir,
    watch: true,
    returnAs: 'json',
    enableAuth: true
  })
})

test.after( async t => {
  fsx.removeSync( baseDir )
})

test.cb.skip("It should able to watch the folder of files change", t => {
  t.plan(2)

  t.falsy( t.context.result.mutation.setWithDestruction , 'It should not have a mutation.setDestruction method')

  setTimeout(() => {
    // put the file back
    fsx.copy(srcFile, destFile, (err) => {
      if (err) {
        console.error(err)
        t.end()
        return;
      }
      // wait a bit for the file to get generated
      setTimeout(() => {
        const json = fsx.readJsonSync( join(outDir, 'contract.json') )

        t.truthy( json.mutation.setWithDestruction, 'Now it should have the setWithDestruction method' )
        t.end()
      }, 1000)

    })
  }, 500)



})

test.todo('Using the cli should able to do the same thing')



// this is really just the same as above
// test.todo("It should able to generate a new contract file")

// this is not going to be in this release
// test.todo("It should able to call a socket end point to announce the change")
