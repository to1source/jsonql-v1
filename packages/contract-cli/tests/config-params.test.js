const test = require('ava')
const contractApi = require('../index')
// testing the configuration validation here
// important to check those optional with empty default value to see if they pass or not
const { join } = require('path')
const fsx = require('fs-extra')
const resolverDir = join(__dirname, 'fixtures', 'resolvers')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'private-test')
const { extend } = require('lodash')
// const contractFile = join(contractDir, 'contract.json')
// const publicFile = join(contractDir, 'public-contract.json')

const { JSONQL_PATH, PUBLIC_KEY, PRIVATE_KEY } = require('jsonql-constants')

const debug = require('debug')('jsonql-contract:test:config-params')

const config = {
  resolverDir,
  contractDir,
  returnAs: 'json',
  enableAuth: true
}

test.before(async t => {
  t.context.result = await contractApi(config)
  t.context.publicContract = await contractApi(extend(config, {public: true}))
})

test.after( t => {
  fsx.removeSync(contractDir)
})

test('Should able to have public flag', async t => {
  t.true(t.context.result.query.anyoneCanGetThis.public)
})

test('Should able to find custom private methods', async t => {

  t.truthy(t.context.result.query.privateFn)
  t.falsy(t.context.result.query.privateFn.public)

  t.truthy(t.context.result.query.privateFnInFolder)
  t.falsy(t.context.result.query.privateFnInFolder.public)

})

test('Should able to have namespace field in the contract.json', async t => {

  let { socket } = t.context.result;
  let publicNsp = `${JSONQL_PATH}/${PUBLIC_KEY}`;
  let privateNsp = `${JSONQL_PATH}/${PRIVATE_KEY}`;

  t.is(socket.alwaysAvailable.namespace, publicNsp)

  t.is(socket.chatroom.namespace, privateNsp)

  // test the public contract as well
  let publicSocket = t.context.publicContract.socket;

  t.is(publicSocket.alwaysAvailable.namespace, publicNsp)

  t.is(publicSocket.chatroom.namespace, privateNsp)

})
