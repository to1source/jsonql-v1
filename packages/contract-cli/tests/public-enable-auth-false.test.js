// test if it picks up the public folder even when enableAuth is false
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')

const baseDir = join(__dirname, 'fixtures')
const resolverDir = join(baseDir, 'resolvers')

const contractDir = join(baseDir, 'tmp', 'public')

const debug = require('debug')('jsonql-contract:test:debug-public')

const generator = require('../index')

test.before(async t => {
    t.context.result = await generator({
        resolverDir,
        contractDir,

        returnAs: 'json',
        enableAuth: false
    })
})


test.after(t => {
    // fsx.removeSync(contractDir)
})

test(`the contract should contain resolver inside the pulic folder`, t => {

    debug(t.context.result)

    t.truthy(t.context.result.query.anyoneCanGetThis)
})
