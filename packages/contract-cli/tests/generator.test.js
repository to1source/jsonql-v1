const test = require('ava')

const { join } = require('path')
const { inspect } = require('util')
const {
  MODULE_TYPE,
  PUBLIC_KEY,
  DEFAULT_CONTRACT_FILE_NAME,
  PUBLIC_CONTRACT_FILE_NAME,
  DEFAULT_RESOLVER_LIST_FILE_NAME,
  DEFAULT_RESOLVER_IMPORT_FILE_NAME
} = require('jsonql-constants');

const debug = require('debug')('jsonql-contract:test:generator')
const fsx = require('fs-extra')

const generator = require('../index')

const resolverDir = join(__dirname, 'fixtures', 'resolvers')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'with-auth')
const baseContractFile = join(contractDir, DEFAULT_CONTRACT_FILE_NAME)
const publicContractFile = join(contractDir, PUBLIC_CONTRACT_FILE_NAME)

const esContractDir = join(__dirname, 'tmp', 'es')
const esResolverDir = join(__dirname, 'fixtures', 'es')

const expired = Date.now() + 60*365*1000

test.after(async t => {
  fsx.removeSync(contractDir)
  fsx.removeSync(esContractDir)
  // remove the two generate files
  // @NOTE remove these two files in 1.7.21
  // fsx.removeSync( join(esResolverDir, DEFAULT_RESOLVER_LIST_FILE_NAME) )
  // fsx.removeSync( join(esResolverDir, DEFAULT_RESOLVER_IMPORT_FILE_NAME) )
})

test.serial('Should able to read list of files', async t => {
  const result = await generator({
    resolverDir,
    contractDir,
    returnAs: 'json'
  })

  t.truthy(result.query)
})

test.serial('There should be a contract.json output to the contractDir', async t => {
  const result = await generator({
    resolverDir,
    contractDir,
    enableAuth: true
  })

  debug(result)

  t.truthy( fsx.existsSync( baseContractFile ))
})
// serial.
test.only('Should able to create a public-contract.json', async t => {
  const result = await generator({
    resolverDir,
    contractDir,
    expired,
    enableAuth: true,
    public: true
  })

  debug('public-contract.json', result)

  t.true(fsx.existsSync(publicContractFile))
  const json = fsx.readJsonSync(publicContractFile)

  debug('output json', json)

  t.is(json.expired, expired, 'Expired field should be the same as what is given')

  t.false(!!json.auth.validator, 'should not have a validator field')
  // there is no auth in theres
  t.truthy(json.auth.login, 'should have a login')

  // now check if certain method is public
  t.true(json.query.anyoneCanGetThis.public, 'anyoneCanGetThis should be public')
  // now check if certain method in private folder is included
  // t.truthy(json.query.privateFn)
  // check for namespace
  t.true(json.socket.chatroom.namespace.indexOf('private') > -1)
  t.true(json.socket.alwaysAvailable.namespace.indexOf('public') > -1)
  // check if there is a description field in each

  t.truthy(json.socket.helloWorld, 'It should have a helloWorld method injected to it')

  t.falsy(json.query.anyoneCanGetThis.description, 'should not have a description in the public query')
  // @1.7.6 the description field is kept in the file
  // t.falsy(json.query.privateFn.description, 'should not have a description in the private query')
})

test.serial('It should able to parse ES6 style resolvers automatically', async t => {
  const result = await generator({
    resolverDir: esResolverDir,
    contractDir: esContractDir,
    returnAs: 'json'
  })
  t.true( fsx.existsSync( join(esContractDir, DEFAULT_CONTRACT_FILE_NAME) ) )

  // check if this is the module type
  t.is( result.sourceType,  MODULE_TYPE)

  // t.true( fsx.existsSync( join(esResolverDir, DEFAULT_RESOLVER_LIST_FILE_NAME) ))
  // t.true( fsx.existsSync( join(esResolverDir, DEFAULT_RESOLVER_IMPORT_FILE_NAME) ))
})

test.serial('The ES6 public contract file should not contain a sourceType field', async t => {

  const result = await generator({
    resolverDir: esResolverDir,
    contractDir: esContractDir,
    public: true,
    returnAs: 'json'
  })

  let json = fsx.readJsonSync(join(esContractDir, PUBLIC_CONTRACT_FILE_NAME))

  t.falsy( json.sourceType )

})
