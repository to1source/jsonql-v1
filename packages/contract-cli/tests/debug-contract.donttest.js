// keep on having problem with one of the resolvers setup with node-client
// also test the disable split-tasks here

const test = require('ava')
const { join } = require('path')
// const fsx = require('fs-extra')

const baseDir = join(__dirname, '..', '..', 'node-client', 'tests', 'fixtures')
const resolverDir = join(baseDir, 'resolvers')
const keysDir = join(baseDir, 'keys')
// we use this test to create a contract.json file for the node-client jwt test to use
const contractDir = join(baseDir, 'jwt')

const debug = require('debug')('jsonql-contract:test:debug-contract')

const generator = require('../index')

test.after(t => {
  fsx.removeSync(contractDir)
})

test(`It should able to create contract with this resolverDir when using custom methods and enableAuth`, async t => {

  const result = await generator({
    resolverDir,
    contractDir,
    keysDir,
    returnAs: 'json',
    enableAuth: true,
    loginHandlerName: 'customLogin',
    validatorHandlerName: 'customValidator'
  })

  debug(result)

  t.truthy(result.query.getUser)
  t.truthy(result.auth.customLogin)

})
