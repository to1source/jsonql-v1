const test = require('ava')
const { join } = require('path')
const { getPaths } = require('../src')
const dir = join(__dirname, 'fixtures', 'resolvers')
const debug = require('debug')('jsonql-contract:test:paths')

test('Should able to have in and out', async t => {
  const args = {
    resolverDir: dir,
    contractDir: join(dir, 'tmp')
  }

  const result = await getPaths(args)
  // debug('result', dir, result.outDir);
  t.is(true, (result.resolverDir !== undefined && result.contractDir !== undefined))
  t.is(dir , result.resolverDir)
})
