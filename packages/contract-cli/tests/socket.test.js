// develop the new socket auth feature for contract
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const colors = require('colors/safe')
const {
  HELLO_FN,
  SOCKET_NAME,
  RETURN_AS_JSON,
  SOCKET_AUTH_NAME,
  DEFAULT_CONTRACT_FILE_NAME,
  PUBLIC_CONTRACT_FILE_NAME,
  STANDALONE_PROP_KEY
} = require('jsonql-constants')

const generator = require('../index')
const {
  filterAuthFiles,
  getSocketAuthResolverFiles
} = require('../src/generator/get-socket-auth-resolver')
const { getSocketAuthResolver } = require('../extra')
const getConfig = require('./fixtures/socket/config')

const resolverDir = join(__dirname, 'fixtures', 'resolvers')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'socket-with-auth')
// const baseContractFile = join(contractDir, DEFAULT_CONTRACT_FILE_NAME)
// const publicContractFile = join(contractDir, PUBLIC_CONTRACT_FILE_NAME)

const debug = require('debug')('jsonql-contract:test:socket')
const colorDebug = (str, ...args) => Reflect.apply(debug, null, [colors.black.bgBrightCyan(str)].concat(args))

test.before(async t => {
  t.context.config = await getConfig({
    [STANDALONE_PROP_KEY]: true,  // this is very import for the socket public contract
    enableAuth: true,
    resolverDir
  })
})

test.after(t => {
  fsx.removeSync(contractDir)
})

test.cb(`It should able to return a list of socket auth files`, t => {
  t.plan(1)

  colorDebug('resolverdir', resolverDir)

  getSocketAuthResolverFiles(resolverDir)
    .then(files => {

      debug('files', files)

      t.truthy(files.length)

      const filteredFiles = filterAuthFiles(t.context.config, files)

      debug('filtered', filteredFiles)

      t.end()
    })
})

test.cb(`It should able to generate partial contract for socket auth`, t => {
  t.plan(1)

  getSocketAuthResolver(t.context.config)
    .then(contract => {
      colorDebug('partial contract', contract)

      t.truthy(contract)
      t.end()
    })
})

test.cb(`It should able to generate new entry when socket / auth has content`, t => {
  t.plan(1)
  generator({
    [STANDALONE_PROP_KEY]: true,
    resolverDir,
    contractDir,
    enableAuth: true
    // returnAs: RETURN_AS_JSON
  })
  .then(result => {
    t.truthy(result)
    // colorDebug('socket test output', result)
    t.end()
  })
})

test.cb(`Now test the public contract with socket`, t => {
  t.plan(4)

  generator({
    [STANDALONE_PROP_KEY]: true,
    resolverDir,
    contractDir,
    enableAuth: true,
    public: true,
    returnAs: RETURN_AS_JSON
  })
  .then(result => {

    colorDebug('public contract', result)

    t.truthy(result[SOCKET_AUTH_NAME], `It should have the ${SOCKET_AUTH_NAME} part`)

    t.truthy(result[SOCKET_AUTH_NAME].login, `It should have a login method defined in the socket auth`)

    t.truthy(result[SOCKET_NAME][HELLO_FN], `It should have a ${HELLO_FN} in ${SOCKET_NAME}`)

    t.falsy(result[SOCKET_AUTH_NAME].validator)

    t.end()
  })
})
