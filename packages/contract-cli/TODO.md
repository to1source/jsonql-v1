## 2020-02-21

- Need to add a clean up action when pass `development: true` then remove existing contract before start (instead of our current method in the post generate method)
- When `enableAuth: false` TBC if `development:true` should we include those resolvers inside the private folder as well
