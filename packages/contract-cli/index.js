// now when ever some one do contract = require('jsonql-contract') will include this one
const {
  applyDefaultOptions,
  generator,
  getPaths,
  watcher
} = require('./src')
// const { KEY_WORD } = require('jsonql-constants')
// const debug = require('debug')('jsonql-contract:api');
// main contract-cli @api public
module.exports = function(config) {
  return applyDefaultOptions(config)
    .then(
      opts => getPaths(opts)
        .then(generator)
        .then(result => {
          // let the watcher run
          watcher(opts)
          // always return the result for the next op
          return result
        })
    )
    .catch(err => {
      console.error(`An error has occured`, err)
    })
}
