const generator = require('./generator')
const getPaths = require('./get-paths')
const { checkFile, applyDefaultOptions } = require('./utils')
// export watcher
const watcher = require('./watcher')

// main
module.exports = {
  applyDefaultOptions,
  generator,
  getPaths,
  checkFile,
  watcher
}
