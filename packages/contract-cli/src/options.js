// default options @TODO add in the next upgrade
// const fs = require('fs')
const { resolve, join } = require('path')
const {
  checkConfigAsync,
  constructConfig,
  createConfig
} = require('jsonql-params-validator')
const {
  DEFAULT_RESOLVER_DIR,
  DEFAULT_CONTRACT_DIR,
  PUBLIC_KEY,
  PRIVATE_KEY,
  STRING_TYPE,
  BOOLEAN_TYPE,
  NUMBER_TYPE,
  OBJECT_TYPE,
  ACCEPTED_JS_TYPES,
  CJS_TYPE,
  ALIAS_KEY,
  OPTIONAL_KEY,
  ENUM_KEY,
  RETURN_AS_FILE,
  RETURN_AS_ENUM,
  LOGIN_FN_NAME,
  LOGOUT_FN_NAME,
  VALIDATOR_FN_NAME,
  DISCONNECT_FN_NAME,
  DEFAULT_CONTRACT_FILE_NAME,
  STANDALONE_PROP_KEY
} = require('jsonql-constants')

const BASE_DIR = process.cwd()

const constProps = {
  BASE_DIR,
  outputFilename: DEFAULT_CONTRACT_FILE_NAME
}

const checkMap = {
  // give the contract an expired time
  expired: createConfig(0, [NUMBER_TYPE]),
  // passing extra props to the contract.json
  extraContractProps: createConfig(false, [OBJECT_TYPE], {[OPTIONAL_KEY]: true}),
  // Auth related props @TODO replace with constants
  loginHandlerName: createConfig(LOGIN_FN_NAME, [STRING_TYPE]),
  logoutHandlerName: createConfig(LOGOUT_FN_NAME, [STRING_TYPE]),
  validatorHandlerName: createConfig(VALIDATOR_FN_NAME, [STRING_TYPE]),
  disconnectHandlerName: createConfig(DISCONNECT_FN_NAME, [STRING_TYPE]),
  [STANDALONE_PROP_KEY]: createConfig(false, [BOOLEAN_TYPE]),

  enableAuth: createConfig(false, BOOLEAN_TYPE, {[ALIAS_KEY]: 'auth'}),
  // file or json
  returnAs: createConfig(RETURN_AS_FILE, STRING_TYPE, {[ENUM_KEY]: RETURN_AS_ENUM}),
  // we need to force it to use useDoc = true for using jsdoc API now
  // useDoc: constructConfig(true, BOOLEAN_TYPE), // @TODO remove this later
  // there will be cjs, es, ts for different parser
  jsType: constructConfig(CJS_TYPE , STRING_TYPE, false, ACCEPTED_JS_TYPES),
  // matching the name across the project - the above two will become alias to this
  resolverDir: createConfig(resolve(join(BASE_DIR, DEFAULT_RESOLVER_DIR)) , STRING_TYPE, {[ALIAS_KEY]: 'inDir'}),
  contractDir: createConfig(resolve(join(BASE_DIR, DEFAULT_CONTRACT_DIR)), STRING_TYPE, {[ALIAS_KEY]: 'outDir'}),
  // show or hide the description field in the public contract
  // contractWithDesc: createConfig(false, [BOOLEAN_TYPE]), @1.7.6 move to Koa
  // remove the old one and always create a new one - useful during development
  alwaysNew: createConfig(false, [BOOLEAN_TYPE]),
  // where to put the public method
  publicMethodDir: constructConfig(PUBLIC_KEY, STRING_TYPE),
  // just try this with string type first
  privateMethodDir: constructConfig(PRIVATE_KEY, [STRING_TYPE], true),
  public: constructConfig(false, [BOOLEAN_TYPE]),
  banner: constructConfig(true, [BOOLEAN_TYPE]),
  // this are for the cmd mostly
  watch: createConfig(false, [BOOLEAN_TYPE, STRING_TYPE, NUMBER_TYPE], {[OPTIONAL_KEY]: true, [ALIAS_KEY]: 'w'}),
  interval: createConfig(10000, [NUMBER_TYPE, STRING_TYPE], {[OPTIONAL_KEY]: true, [ALIAS_KEY]: 'i'}),
  configFile: createConfig(false, [STRING_TYPE], {[OPTIONAL_KEY]: true, [ALIAS_KEY]: 'c'}),
  announceUrl: createConfig(false, [STRING_TYPE], {[OPTIONAL_KEY]: true, [ALIAS_KEY]: 'a'}),
  logDirectory: constructConfig(false, [STRING_TYPE], true),
  // @TODO do we still need this one?
  tmpDir: createConfig(join(BASE_DIR, 'tmp'), [STRING_TYPE]),
  // ported from jsonql-koa
  buildContractOnStart: constructConfig(false, [BOOLEAN_TYPE], true),
  // @1.8.x enable or disable the split tasks
  enableSplitTask: createConfig(false, [BOOLEAN_TYPE]),
  // add 1.8.7 to determine if we clean out the contract folder or not
  development: createConfig(false, [BOOLEAN_TYPE])
}

// export it as named module
module.exports = config => checkConfigAsync(config, checkMap, constProps)
