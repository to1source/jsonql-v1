// all the files related methods
const { join } = require('path')
const fsx = require('fs-extra')

const { isContract } = require('./helpers')
const { getDebug } = require('../utils')
const debug = getDebug('generator:files')

/**
 * Just output the final contract to json file
 * @param {string} dist where it should be written
 * @param {object} contract the json to write
 * @return {promise} resolve the contract
 */
function writeFileOut(dist, contract) {
  return new Promise((resolver, rejecter) => {
    fsx.outputJson(dist, contract, {spaces: 2}, err => {
      if (err) {
        return rejecter(err)
      }
      resolver(contract)
    })
  })
}

/**
 * @param {object} config pass by the init call
 * @param {string} dist where the contract file is
 * @return {promise} resolve or reject if remove files failed
 */
const keepOrCleanContract = function(config, dist) {
  return new Promise((resolver, rejecter) => {
    if (config.alwaysNew === true) {
      if (fsx.existsSync(dist)) {
        debug('[@TODO] Found existing contract [%s]', dist)
        fsx.remove(dist, err => {
          if (err) {
            return rejecter(err)
          }
          return resolver(true)
        })
      }
    }
    resolver(true)
  })
}

/**
 * check if there is already a contract.json file generated
 * @param {object} config options
 * @return {mixed} false on fail
 */
const isContractExisted = function(config) {
  const { contractDir, outputFilename } = config;
  const dist = join(contractDir, outputFilename)
  if (fsx.existsSync(dist)) {
    debug('[isContractExisted] found')
    const contract = fsx.readJsonSync(dist)
    return isContract(contract) ? contract : false
  }
  return false
}

// export
module.exports = {
  keepOrCleanContract,
  isContractExisted,
  writeFileOut
}
