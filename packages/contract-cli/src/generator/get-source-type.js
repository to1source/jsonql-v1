// breaking this out from get-resolver and re-use in two places 
const fsx = require('fs-extra')
const {
  MODULE_TYPE,
  SCRIPT_TYPE
} = require('jsonql-constants')

/**
 * There is a potential bug here if the first file is not a resolver than this will failed!
 * Use the first file to determine the source type NOT ALLOW MIX AND MATCH
 * @param {string} source the path to the file
 * @return {string} sourceType
 */
const sourceFileType = src => {
  const source = fsx.readFileSync(src, 'utf8').toString()
  if (source.indexOf('module.exports') > -1) {
    return SCRIPT_TYPE
  } else if (source.indexOf('export default') > -1) {
    return MODULE_TYPE
  }
  return false
}

/**
 * Try to find out the resourceType until we find it
 * @param {array} objs array of objs
 * @return {object} add the resourceType to the object
 */
function getSourceType(objs) {
  let sourceType
  let ctn = objs.length
  for (let i = 0; i < ctn; ++i) {
    if (!sourceType) {
      resourceType = sourceFileType(objs[i].file)
      if (resourceType) {
        return resourceType
      }
    }
  }
  throw new Error(`Can not determine the resourceType!`)
}

module.exports = { getSourceType }