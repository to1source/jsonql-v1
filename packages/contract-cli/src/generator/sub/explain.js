// this will take the last preprocess output then pass to the jsdoc acron to
// get the part for the contract
const { processResolverToContract } = require('../get-resolver')

/**
 * see processResolverToContract desc
 * @return {object} the process result
 */
module.exports = function processResolverFile({ type, name, file, public, namespace, sourceType }) {
  return processResolverToContract(type, name, file, public, namespace, sourceType)
}
