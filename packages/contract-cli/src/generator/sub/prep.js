// this will take the input files list then check if this is a resolver
const { getResolver } = require('../get-resolver')

/**
 * The process listener
 * @param {object} payload the complete payload
 * @param {function} payload.preprocessor the name of the processor
 * @param {string} payload.file the argument pass to the processor
 * @return {void} just use process.send back the idx, processor and result
 */
module.exports = function preprocessResolver({file, config, fileType}) {
  const fn = getResolver(config, fileType)
  return Reflect.apply(fn, null, [file])
}
