// this is the heart of the process that pass the resolver file into the AST to understand it
// const { join, basename } = require('path')
// const { EXT } = require('jsonql-constants')
const { merge } = require('lodash')
const {
  getResolver,
  getSourceType,
  getContractBase,
  processResolverToContract
} = require('./get-resolver')
const { splitTask } = require('./split-task')
const { NOT_ENOUGH_CPU } = require('nb-split-tasks/constants')
const { JsonqlError } = require('jsonql-errors')

// const { getDebug } = require('../utils')
// const debug = getDebug('process-file')

/**
 * @NOTE this should be replace with the split task
 * Take the map filter out to get the clean resolver objects
 * @param {array} _files files to process
 * @param {function} preprocessor for capture the resolver
 * @return {array} resolver objects
 */
function processFilesAction(files, fileType, config) {
  const preprocessor = getResolver(config, fileType)
  return Promise.resolve(
      files.map( preprocessor )
        .filter( obj => obj.ok )
      )
}

/**
 * This will decided if we need to use the split task or not
 * @param {array} files files to process
 * @param {function} preprocessor for capture the resolver
 * @return {array} resolver objects
 */
function processFilesTask(files, fileType, config) {
  if (config.enableSplitTask) {
    const payload = { fileType, files, config }
    return splitTask('pre', payload)
      .catch(err => {
        if (err === NOT_ENOUGH_CPU) {
          // fallback
          return processFilesAction(files, fileType, config)
        }
        throw new JsonqlError(err)
      })
  }
  // another fallback
  return processFilesAction(files, fileType, config)
}

/**
 * This is the step one to parallel process the resolver file
 * @param {array} files to process
 * @param {string} fileType extension to expect
 * @param {object} config options
 * @return {array} of preprocessed filtered resolver
 */
function preprocessResolverFile(files, fileType, config) {
  return processFilesTask(files, fileType, config)
    .then(objs => [
      getSourceType(objs),
      objs
    ])
}

/**
 * process the files
 * @param {string} fileType the ext
 * @param {array} files list of files
 * @param {object} config to control the inner working
 * @param {object} contractBase the base object for merging
 * @return {object} promise that resolve all the files key query / mutation
 */
function processResolverFileAction(sourceType, files, config, contractBase) {
  return Promise.resolve(
    files
      .map(({ type, name, file, public, namespace }) => (
        processResolverToContract(type, name, file, public, namespace, sourceType)
      ))
      .reduce(merge, contractBase)
  )
}

/**
 * process the files using split task or not
 * @param {string} fileType the ext
 * @param {array} files list of files
 * @param {object} config to control the inner working
 * @return {object} promise that resolve all the files key query / mutation
 */
function processResolverFile(sourceType, files, config) {
  const contractBase = getContractBase(sourceType)
  if (config.enableSplitTask) {
    const payload = { sourceType, files, config }
    return splitTask('process', payload, contractBase)
      .catch(err => {
        if (err === NOT_ENOUGH_CPU) {
          // fallback
          return processResolverFileAction(sourceType, files, config, contractBase)
        }
        throw new JsonqlError(err)
      })
  }
  // another fallback
  return processResolverFileAction(sourceType, files, config, contractBase)
}

/**
 * when enableAuth !== true then we should remove the auth related methods
 * @param {object} contract the generate contract json
 * @param {object} config options
 * @return {object} contract remove the auth if enableAuth !== true
 */
function postProcessResolverFile(contract, config) {
  if (config.enableAuth !== true) {
    contract.auth = {}
  }

  return contract
}

// export
module.exports = {
  preprocessResolverFile,
  processResolverFile,
  postProcessResolverFile
}
