// Here is the point where we need to split to load to different CPU
const { join } = require('path') // basename
// const os = require('os')
const glob = require('glob')
const colors = require('colors/safe')
const { EXT } = require('jsonql-constants')

const {
  preprocessResolverFile,
  processResolverFile
} = require('./process-file')
const { getDebug } = require('../utils')

const debug = getDebug('read-files-out-contract')

/**
 * just return the list of files for process
 * @param {string} resolverDir the path to the resolver directory
 * @param {string} [fileType=EXT] the file extension
 * @return {promise} resolve the files array on success or reject with error
 */
function getResolverFiles(resolverDir, fileType) {
  const pat = join(resolverDir, '**', ['*', fileType].join('.'))
  return new Promise((resolver, rejecter) => {
    glob(pat, (err, files) => {
      if (err) {
        debug(`File read error!`, err)
        return rejecter(err)
      }
      // debug('getResolverFiles', files)
      resolver(files)
    })
  })
}

/**
 * get list of files and put them in query / mutation
 * @param {object} config pass config to the underlying method
 * @return {object} query / mutation
 */
function readFilesOutputContract(config) {
  const { resolverDir } = config
  let fileType = config.ext || EXT
  let timestart = Date.now()

  return getResolverFiles(resolverDir, fileType)
    .then(files => preprocessResolverFile(files, fileType, config)
        .then(result => {
          const [sourceType, files] = result;
          return processResolverFile(sourceType, files, config)
                  .then(contract => [sourceType, contract])
        })
    )
    .then(result => {
      let timeend = Date.now() - timestart
      debug('Time it took:', colors.yellow(timeend))
      return result
    })
}

module.exports = {
  getResolverFiles,
  readFilesOutputContract
}
