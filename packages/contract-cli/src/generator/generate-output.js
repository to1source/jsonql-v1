// the final step to generate output
const { RETURN_AS_JSON } = require('jsonql-constants')
const { keepOrCleanContract, writeFileOut } = require('./files-op')
const { processSocketAuth } = require('./get-socket-auth-resolver')
const { mutateContract } = require('./helpers')
const { join } = require('path')
const debug = require('debug')('jsonql-contract:generate-output')


/**
 * Generate the final contract output to file
 * @param {object} config output directory
 * @param {object} contract processed result
 * @param {boolean} public id if its public or not
 * @return {boolean} true on success
 */
const generateOutput = function(config, contract, public = false) {

  debug(`generateOutput configuration ${public ? '[PUBLIC]' : ''}`, config)

  // this return a promise interface
  const { outputFilename, contractDir } = config // resolverDir
  const dist = join(contractDir, outputFilename)
    // keep or remove the existing contract file
  return keepOrCleanContract(config, dist)
      // @1.9.0 add the socket auth part
      .then(() => processSocketAuth(config, contract, public))
      .then(newContract => mutateContract(config, newContract))
      .then(finalContract => (
        writeFileOut(dist, finalContract)
          .then(() => (
            config.returnAs === RETURN_AS_JSON ? finalContract : dist
          ))
      ))
}

module.exports = { generateOutput }
