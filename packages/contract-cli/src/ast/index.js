// grouping all the AST related methods and re-export here
const { getJsdoc } = require('./jsdoc')
const {
  acornParser,
  extractReturns,
  extractParams,
  isExpression
} = require('./acorn')
const colors = require('colors/safe')
/**
 * wrapper for the AST parser (using acorn)
 * @param {string} source the stringify version of the function
 * @param {object} [options={}] configuraion options
 * @return {object} the result object
 */
const astParser = function(source, options = {}) {
  try {
    return acornParser(source, options)
  } catch(e) {
    console.error(colors.white.bgRed('AST parsing failed!'))
    console.log(options)
    console.error('source:', colors.green(source))
    console.error('error:', e)
    process.exit()
  }
}

module.exports = {
  getJsdoc,
  acornParser,
  extractReturns,
  extractParams,
  isExpression,
  astParser
}
