const fs = require('fs')
const { resolve } = require('path')

// const { getDebug } = require('./utils')
// const debug = getDebug('paths')

/**
 * The input from cmd and include are different and cause problem for client
 * @param {mixed} args array or object
 * @return {object} sorted params
 */
function getPaths(argv) {
  // debug(argv);
  return new Promise((resolver, rejecter) => {
    const baseDir = process.cwd()
    if (argv.resolverDir) {
      if (argv.contractDir) {
        argv.contractDir = resolve(argv.contractDir)
      } else {
        argv.contractDir = baseDir
      }
      argv.resolverDir = resolve(argv.resolverDir)
      return resolver(argv); // just return it
    } else if (argv._) {
      const args = argv._
      if (args[0]) {
        return resolver({
          resolverDir: resolve(args[0]),
          contractDir: args[1] ? resolve(args[1]) : baseDir,
          // raw: argv.raw, if they are using command line raw is not available as option
          public: argv.public
        })
      }
    }
    rejecter('You need to provide the input path!')
  })
}

/**
 * @param {object} paths in out
 * @return {object} promise
 */
function checkInputPath(paths) {
  return new Promise((resolver, rejecter) => {
    if (paths.resolverDir) {
      try {
        const stats = fs.lstatSync(paths.resolverDir)
        if (stats.isDirectory()) {
          resolver(paths)
        } else {
          rejecter(`The input directory path ${paths.resolverDir} does not existed`)
        }
      }
      catch (e) {
        rejecter(e)
      }
    }
  })
}

// main
module.exports = function(args) {
  return getPaths(args).then(checkInputPath)
}
