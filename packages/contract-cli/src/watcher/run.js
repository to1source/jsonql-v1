// this will get call to run the generator
const generator = require('../generator')
// forget the socket client for now
// const socketClient = require('./socket')
// const debug = require('debug')('jsonql-contract:watcher:run')

// let fn, config;

process.on('message', m => {
  if (m.change && m.config) {
    generator(config)
      .then(result => {
        process.send({ result })
      })
  }
  /*
  if (m.config) {
    config = m.config;
    fn = socketClient(m.config)
  } else {
    fn(m)
  }
  */
})
