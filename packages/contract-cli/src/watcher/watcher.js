// watching the file change and execute the generator to create new files
// also if the announceUrl is presented then we create socket.io / ws client
// to announce the change
const chokidar = require('chokidar')
const { join } = require('path')
const { fork } = require('child_process')
const kefir = require('kefir')
const colors = require('colors/safe')
const { EXT, TS_EXT, TS_TYPE } = require('jsonql-constants')
const debug = require('debug')('jsonql-contract:watcher')

let counter = 0;

/**
 * When passing option from cmd, it could turn into a string value
 * @param {object} config clean optios
 * @return {boolean} true OK
 */
const isEnable = config => {
  const { watch } = config;
  if (watch === true || watch === 'true' || watch === '1' || watch === 1) {
    const ext = config.jsType === TS_TYPE ? TS_EXT : EXT;
    return join( config.resolverDir, '**', ['*', ext].join('.'))
  }
  return false;
}

/**
 * main interface
 * @param {object} config clean options
 * @return {boolean | function} false if it's not enable
 */
module.exports = function(config) {
  let watchPath;
  if ((watchPath = isEnable(config)) !== false) {
    debug('watching this', watchPath)
    // create the watcher
    const watcher = chokidar.watch(watchPath, {})

    const closeFn = () => watcher.close()

    // create a fork process
    const ps = fork(join(__dirname, 'run.js'))
    // modify the config to make sure the contract file(s) get clean
    config.alwaysNew = true;
    // kick start the process
    // ps.send({ config })
    // now watch
    const stream = kefir.stream(emitter => {
      watcher.on('change', (evt, path, details) => {
        ++counter;
        debug(`(${counter}) got even here`, evt, path, details)
        emitter.emit({
          change: true,
          config
        })
      })
      // call exit
      return closeFn;
    })

    stream.throttle(config.interval).observe({
      value(value) {
        ps.send(value)
      }
    })

    // we can remotely shut it down - when using the socket option
    ps.on('message', msg => {
      if (msg.end) {
        console.info(colors.green('Watcher is shutting down'))
        watcher.close()
      } else if (msg.txt) {
        console.log(colors.yellow(msg.txt))
      }
    })
    // return the close method
    return closeFn;
  }
  return false; // nothing happen
}
