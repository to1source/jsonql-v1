#!/usr/bin/env node
// create a standalone watch.js for development purpose
const { watcher } = require('./lib')
const { join } = require('path')

// run cli api
require('yargs')
  .command('watch [inDir] [outDir]', 'Watch folder and recreate contract file', yargs => {
    yargs
      .positional('inDir', {
        describe: 'Resolver directory',
        default: join(__dirname, 'resolvers')
      })
      .positional('outDir', {
        describe: 'Contract output directory',
        default: join(__dirname, 'contract')
      })
  }, argv => {
    // we must check the inDir
    watcher(argv)
  })
