#!/usr/bin/env node

const { version } = require('./package.json')
/**
 * Using Jsdoc-api to generate our contract file
 * https://www.npmjs.com/package/jsdoc-api
 */
const {
  applyDefaultOptions,
  generator,
  getPaths,
  checkFile,
  watcher
} = require('./src')
// const { KEY_WORD } = require('jsonql-constants')
// const { join } = require('path')
// const debug = require('debug')('jsonql-contract:cli')

/**
 * wrapper for easily to call
 * @param {string} cmd which command is running
 * @param {object} argv options
 */
const run = (cmd, argv) => {
  console.info(`jsonql-contract cli: ${cmd} @ ${version}`)
  // execute when call
  applyDefaultOptions(argv, cmd)
    .then(config => (
      getPaths(config)
        .then(opts => (
          {
            result: generator(opts),
            config: opts
          }
        ))
        .then(
          ({result, config}) => (
            result.then(dist => checkFile(config)(dist))
          )
        )
        .then(watcher)
        .catch(err => {
          console.error('json:ql contract-cli error!', err)
        })
    ))
}

// call it like this
// $ jsonql-contract /path/to/files /path/to/output
// or
// create a public-contract.json for client
// $ jsonql-contract /path/to/files /path/to/output --raw=1

// create a proper interface
require('yargs')
  .command('create [inDir] [outDir]', 'Generate contract file', yargs => {
    yargs
      .positional('inDir', {
        describe: 'Resolver directory'
      })
      .positional('outDir', {
        describe: 'Contract output directory',
        default: './contract'
      })
  }, argv => {
    // we must check the inDir
    run('create', argv)
  })
  .command('config [configFile]', 'Pass a config file to execute the command', yargs => {
    yargs
      .positional('configFile', {
        describe: 'The path to your config json file'
      })
  }, argv => {
    run('config', argv)
  })
  .option('password', {
    alias: 'p',
    describe: 'Password for the contract.json server'
  })
  .option('public', {
    describe: 'If you want the public version of contract file',
    default: false
  })
  .option('raw', {
    describe: 'Return as json',
    alias: 'r',
    default: false
  })
  .argv
