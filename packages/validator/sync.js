// just copy the source to where it's needed for now.
// don't have time to deal with the tooling fucking problem
const { join } = require('path')
const fsx = require('fs-extra')
const debug = require('debug')('jsonql-params-validator:sync')
// sources
const src = [
  join(__dirname, 'src'),
  join(__dirname, 'index.js')
]
// destinations
const dest = [
  join(__dirname, '..', 'client', 'src', 'lib', 'validator'),
  join(__dirname, '..', 'rx-client', 'src', 'lib', 'validator')
]
// now copy
src.forEach( s => {
  const ds = s.split('/')
  const last = ds[ds.length - 1]
  debug('copy ', last)
  dest.forEach(d => {
    const result = fsx.copy(s, join(d, last))
    debug('copy result: ', result)
  })
})
