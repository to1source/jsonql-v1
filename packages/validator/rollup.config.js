/**
 * Rollup config
 */
import { join } from 'path'

import buble from '@rollup/plugin-buble'
import replace from '@rollup/plugin-replace'
import commonjs from '@rollup/plugin-commonjs'
import json from '@rollup/plugin-json'
import nodeResolve from '@rollup/plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
// support async functions
import async from 'rollup-plugin-async'
import { terser } from "rollup-plugin-terser"
// get the version info
import { version } from './package.json'

const env = process.env.NODE_ENV

let plugins = [
  json({
    preferConst: true
  }),
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    preferBuiltins: true,
    mainFields: ['module', 'browser']
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__PLACEHOLDER__': `version: ${version} module: ${env==='prod' ? 'cjs' : 'umd'}`
  }),
  terser(),
  size()
]

const file = env === 'prod' ? 'jsonql-params-validator.cjs.js' : 'jsonql-params-validator.umd.js';

console.log('outfile is %s', file)

let config = {
  input: join(__dirname, 'index.js'),
  output: {
    name: 'jsonqlParamsValidator',
    file: join(__dirname, 'dist', file),
    format: env === 'prod' ? 'cjs' : 'umd',
    sourcemap: true,
    globals: {
      'promise-polyfill': 'Promise',
      'debug': 'debug'
    }
  },
  external: [
    'promise-polyfill',
    'debug'
  ],
  plugins: plugins
}

export default config
