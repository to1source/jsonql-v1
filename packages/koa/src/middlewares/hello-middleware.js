// The helloWorld should always be available. Because this can serve as a ping even when its auth:true
import { getDebug, handleOutput, packResult } from '../utils'
import { HELLO, HELLO_FN, QUERY_NAME } from 'jsonql-constants'
const debug = getDebug('hello-middlware')
// export
export default function helloMiddleware(opts) {
  return async function(ctx, next) {
    const { isReq, resolverType, resolverName } = ctx.state.jsonql;
    // so here we check two things, the header and the url if they match or not
    if (isReq && resolverName === HELLO_FN && resolverType === QUERY_NAME) {
      debug('********* ITS CALLING THE HELLO WORLD *********')
      return handleOutput(opts)(ctx, packResult(HELLO))
    } else {
      // @NOTE For some reason if I don't wrap this in an else statment
      // I got an error said next get call multiple times
      await next()
    }
  }
}
