// main export interface
import authMiddleware from './auth-middleware'
import coreMiddleware from './core-middleware'
import contractMiddleware from './contract-middleware'
import helloMiddleware from './hello-middleware'

import consoleMiddleware from './console-middleware'
import publicMethodMiddleware from './public-method-middleware'
// import errorsHandlerMiddleware from './errors-handler-middleware'
import initMiddleware from './init-middleware'
// @1.4.0 file middleware
import fileMiddleware from './file-middleware'

// export
export {
  consoleMiddleware,
  authMiddleware,
  coreMiddleware,
  contractMiddleware,
  helloMiddleware,
  publicMethodMiddleware,
  initMiddleware,
  fileMiddleware
}
