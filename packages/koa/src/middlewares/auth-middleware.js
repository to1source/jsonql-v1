// auth middleware
import { NOT_FOUND_STATUS } from 'jsonql-constants'
import { JsonqlResolverNotFoundError } from 'jsonql-errors'
import {
  getDebug,
  forbiddenHandler,
  ctxErrorHandler,
  isNotEmpty,
  isObject
} from '../utils'
import {
  authHeaderParser,
  getToken,
  getValidator
} from '../utils/auth-middleware-helpers'
const debug = getDebug('auth-middleware')
/**
 * Auth middleware, we support
 * 2) JWT token
 * This is just front we don't do any real auth here,
 * instead we expect you to supply a function and pass you data and let you
 * handle the actual authentication
 * @TODO need to break this down further at the moment its very hard to debug what is going on here
 */
export default function authMiddleware(config) {
  // return middleware
  return async function(ctx, next) {
    // we should only care if there is api call involved
    const { isReq, contract } = ctx.state.jsonql
    if (isReq && config.enableAuth) {
      try {
        const token = authHeaderParser(ctx)
        if (token) {
          debug('got a token', token)
          // @BUG 1.4.9 this still throw the error when it shouldn't have
          let validatorFn = getValidator(config , contract)
          let userdata = await validatorFn(token)
          debug('validatorFn result', userdata)
          if (isNotEmpty(userdata) && isObject(userdata)) {
            // here we add the userData to the global
            // @TODO need more testing to see if this is going to work or not
            ctx.state.jsonql.userdata = userdata
            // debug('get user data result', userdata)
            await next()
          } else {
            debug('throw at wrong result', userdata)
            return forbiddenHandler(ctx, {message: 'userdata is empty?'})
          }
        } else {
          // @BUG this was throw a wrong error 
          debug('throw at headers not found', ctx.request.headers)
          return forbiddenHandler(ctx, {message: 'Auth header not found!'})
        }
      } catch(e) {
        if (e instanceof JsonqlResolverNotFoundError) {
          return ctxErrorHandler(ctx, NOT_FOUND_STATUS, e)
        } else {
          debug('throw at some where throw error', e)
          return forbiddenHandler(ctx, e)
        }
      }
    } else {
      await next()
    }
  }
}
