// this was move back from jsonql-utils
// because this is all koa specific there is no reason to put into a global module
// koa specific methods
import { FORBIDDEN_STATUS } from 'jsonql-constants'
// fix the default is not export by module error
import * as jsonqlErrors from 'jsonql-errors'
import { getDocLen } from 'jsonql-utils'
import { handleOutput, ctxErrorHandler } from 'jsonql-resolver'

/**
 * Just a wrapper to be clearer what error is it
 *
 * @param {object} ctx koa
 * @param {object} e error
 * @return {undefined} nothing
 */
const forbiddenHandler = (ctx, e) => (
  ctxErrorHandler(ctx, FORBIDDEN_STATUS, e, 'JsonqlForbiddenError')
)

export {
  handleOutput,
  ctxErrorHandler,
  forbiddenHandler
}
