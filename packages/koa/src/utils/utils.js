// All of the functions here we moved to jsonql-utils
import debug from 'debug';
const MODULE_NAME = 'jsonql-koa' // just keep the old name

const getDebug = (name) => debug(MODULE_NAME).extend(name)

export {
  getDebug
}
