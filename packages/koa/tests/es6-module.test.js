// testing the new ES6 modules import
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const superkoa = require('superkoa')
const {
  DEFAULT_RESOLVER_LIST_FILE_NAME,
  DEFAULT_RESOLVER_IMPORT_FILE_NAME,
  HELLO_FN
} = require('jsonql-constants')
const { createQuery } = require('jsonql-utils')
const debug = require('debug')('jsonql-koa:test:es6')

const { headers } = require('./fixtures/options')
const createServer = require('./helpers/server')

const esResolverDir = join(__dirname, 'fixtures', 'es')
const esContractDir = join(__dirname, 'fixtures', 'tmp', 'es')

const dir = 'es6';

test.before( t => {
  t.context.app = createServer({
    resolverDir: esResolverDir,
    contractDir: esContractDir
  }, dir)
})

test.after( t => {
  fsx.removeSync( esContractDir )
  fsx.removeSync( join(esResolverDir, DEFAULT_RESOLVER_LIST_FILE_NAME) )
  fsx.removeSync( join(esResolverDir, DEFAULT_RESOLVER_IMPORT_FILE_NAME) )
})

test('It should able to generate the contract with es6 modules', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(headers)
    .send(
      createQuery(HELLO_FN)
    )
  t.is(200, res.status)
  t.true( fsx.existsSync( join(esContractDir, 'contract.json') ) )
})

test('It should able to handle a ES6 function', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(headers)
    .send(
      createQuery('getSomething')
    )
  t.is(200, res.status)
  // @BUG the result is undefined

  t.true(Array.isArray(res.body.data))

})

test('It should able to serve up the correct public contract', async t => {

  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .query({_cb: Date.now()})
    .set(headers)

  t.is(200, res.status)

  t.truthy(res.body.data.timestamp)

})
