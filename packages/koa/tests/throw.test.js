// this will only test all kind of throw error scenarios
const test = require('ava')
const superkoa = require('superkoa')
const jsonqlErrors = require('jsonql-errors')
// @TODO some of the idea already cover in other test so this one just keep here for now
const {
  clientErrorsHandler,
  getErrorByStatus,
  JsonqlResolverNotFoundError
} = jsonqlErrors;

test('Should able understand the Error being throw from clientErrorsHandler', async t => {

  // JsonqlResolverNotFoundError some how this has a typeof function instead of object 

  const error = t.throws(() => {
    return clientErrorsHandler({
      error: {
        statusCode: 404,
        className: 'JsonqlResolverNotFoundError'
      }
    })
  }, null, 'Only give it a statusCode to find the error to throw')


})
