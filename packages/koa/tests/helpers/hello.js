// wrapper function to test the server is running or not
const superkoa = require('superkoa')
const { headers } = require('../fixtures/options')
// export
module.exports = function hello(app) {
  return superkoa(app)
    .post('/jsonql')
    .set(headers)
    .send({
      helloWorld: {
        args: []
      }
    })
}
