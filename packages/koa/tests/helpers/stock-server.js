// this will export the server for other test to use
// const test = require('ava');
// const http = require('http')
const Koa = require('koa')
// const { join } = require('path')
const bodyparser = require('koa-bodyparser')
const { jsonqlKoa } = require('../../main')

// add a dir to seperate the contract files
module.exports = function createServer() {
  // const srv =
  const opts = {name: 'stock-setup-server', appDir: 'tests/jsonql'}

  const app = new Koa()
  app.use(bodyparser())
  app.use(jsonqlKoa(opts))

  return app
}
