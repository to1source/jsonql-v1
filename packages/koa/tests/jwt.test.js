// this will use the enableAuth and useJwt options to test out all the new modules
const test = require('ava')

const superkoa = require('superkoa');
const { join } = require('path');
const clientJwtDecode = require('jwt-decode')
const { jwtDecode } = require('jsonql-jwt')
const { merge } = require('lodash')
const fsx = require('fs-extra')
const { RSA_ALGO } = require('jsonql-constants')

const debug = require('debug')('jsonql-koa:test:jwt');

const { type, headers, dirs } = require('./fixtures/options');
const createServer = require('./helpers/server')
const dir = 'jwt';
const keysDir = join(__dirname, 'fixtures', 'keys')

const { createTokenValidator, loginResultToJwt } = require('jsonql-jwt')

// start test
test.before( t => {
  t.context.app = createServer({
    enableAuth: true,
    useJwt: true,
    jwtTokenOption: { exp: 60*60 },
    loginHandlerName: 'customLogin',
    keysDir
  }, dir)

  const publicKey = fsx.readFileSync(join(keysDir, 'publicKey.pem'))

  t.context.validator = createTokenValidator({
    publicKey,
    useJwt: true
  })
})

test.after( t => {
  fsx.removeSync(join(dirs.contractDir, dir))
})

// we need this one run first to get the token
test('It should able to provide a token that can be decode with the client side module', async t => {
  let name = 'Joel';
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(headers)
    .send({
      customLogin: {
        args: [name, '123456']
      }
    })

  t.is(200, res.status)
  let token = res.body.data;
  debug('token', token)
  t.context.token = token;


  let payload = clientJwtDecode(token)
  t.is(payload.name, name)
  t.truthy(payload.exp)

  let payload2 = t.context.validator(token)

  t.is(payload2.name, name)
  t.truthy(payload2.exp)

})
