// we test all the fail scenario here
const test = require('ava')

const superkoa = require('superkoa')
const { join } = require('path')
const debug = require('debug')('jsonql-koa:test:fail')
const { merge } = require('lodash')
const { type, headers, dirs, returns, contractKeyName } = require('./fixtures/options')
const fsx = require('fs-extra')

const myKey = '4670994sdfkl'
const expired = Date.now() + 60*24*365*1000
const createServer = require('./helpers/server')
const dir = 'withauth'
const baseDir = join(__dirname, 'fixtures')
const contractDir1 = join(baseDir, 'contracts')

test.before((t) => {

  fsx.removeSync(contractDir1)

  t.context.app = createServer({
    expired,
    contractKey: myKey
  }, dir)
})

test.after( () => {
  fsx.removeSync(join(dirs.contractDir, dir))
})


test("It should Not return a contract", async (t) => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .query({_cb: Date.now()})
    .set(headers)

  t.is(401, res.body.error.statusCode)
})


test("It should able to get contract with a key", async (t) => {

  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .query({_cb: Date.now()})
    .set(merge({}, headers, {
      [contractKeyName]: myKey
    }))

  t.is(200, res.status)
  // t.is(res.body.data.expired, expired) // this keep fucking up not because of the code but the other test fuck it up
  t.deepEqual(returns, res.body.data.query.helloWorld.returns)
})

test("It should fail to get contract if we provide the wrong password", async t => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .query({_cb: Date.now()})
    .set(merge({}, headers, {
      [contractKeyName]: 'what-ever-that-key'
    }))

  t.is(401, res.body.error.statusCode)
})
