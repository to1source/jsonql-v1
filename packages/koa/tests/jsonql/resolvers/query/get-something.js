/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 * @param {number} min smallest
 * @param {number} max largest
 * @return {number} the number is or between
 */
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min
}

/**
 * Give me something
 * @return {string} random message
 */
module.exports = function getSomething() {
  const msg = [
    'I am cool',
    'You are cool',
    'That\' not cool',
    'ain\'t it cool'
  ]
  const max = msg.length - 1
  const idx = getRandomInt(0, max)
  return msg[idx] || 'That\s really NOT cool!!!'
}
