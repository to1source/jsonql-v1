// testing the ms feature
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const debug = require('debug')('jsonql-koa:test:node-client')
const nodeClient = require('jsonql-node-client')
// setup

const baseDir = join(__dirname, 'fixtures')
const createServer = require('./helpers/server-with-socket')

const clientContractDir = join(__dirname, 'fixtures', 'tmp', 'client6002')

const mainDir = 'log' // we an switch this later

const baseServerPort = 7001;
const msServerPort = 8001;
// @TODO @BUG if we try to generate contract on the fly the 8001 is not creating it
const baseContractDir = join(baseDir, mainDir, `server${baseServerPort}`)
const msContractDir = join(baseDir, mainDir, `server${msServerPort}`)

// base test setup
test.before(async t => {
  t.context.baseApp = await createServer(baseServerPort, {
    resolverDir: join(baseDir, 'resolvers'),
    contractDir: baseContractDir,
    enableAuth: true,
    name: `server${baseServerPort}`,
    socketServerType: 'ws',
    clientConfig: [{
      hostname: `http://localhost:${msServerPort}`,
      name: 'client0',
      serverType: 'ws'
    }]
  })

  t.context.msApp = await createServer(msServerPort, {
    resolverDir: join(baseDir, 'sub', 'resolver'),
    contractDir: msContractDir,
    socketServerType: 'ws'
  })

  // now init the clients connection
  t.context.isReady = new Promise((resolver) => {
    setTimeout(() => {
      t.context.baseAppClient = nodeClient({
        contractDir: join(baseDir, mainDir, `client-${baseServerPort}`),
        hostname: `http://localhost:${baseServerPort}`,
        serverType: 'ws'
      })

      t.context.msAppClient = nodeClient({
        contractDir: join(baseDir, mainDir, `client-${msServerPort}`),
        hostname: `http://localhost:${msServerPort}`,
        serverType: 'ws'
      })

      resolver(true)
    }, 300)
  })
})

test.after(t => {
  //setTimeout(() => {
    t.context.baseApp.stop()
    t.context.msApp.stop()

  //}, 3000)
  // fsx.removeSync(baseContractDir)
  // fsx.removeSync(msContractDir)
})

test(`First test calling the ${baseServerPort} directly with the mutation call`, async t => {
  const client = await nodeClient({
    hostname: `http://localhost:${msServerPort}`,
    contractDir: join(__dirname, 'fixtures', 'tmp', `client${msServerPort}`)
  })
  const result = await client.subMsService('testing')
  t.truthy(result.indexOf(`ms service`), `It also tested the namespaced option in the node-client`)
})

// @1.6.0 this problem still here but it could be a ws problem instead of our framework
test.cb.skip(`[still got socket hang up @ 1.5.5] First test server ${baseServerPort} and ${msServerPort} are running`, t => {

  t.plan(2)
  t.context.isReady.then(() => {
    t.context.baseAppClient.then(client1 => {
      client1.helloWorld()
        .then(res1 => {
          t.truthy(res1)
        })
    })

    t.context.msAppClient.then(client2 => {
      client2.helloWorld()
        .then(res2 => {
          t.truthy(res2)
          setTimeout(() => {
            t.end()
          }, 5000)
        })
    })
  })
})

test.skip(`It should able to call a resolver that access another ms`, async t => {
  // the problem now is calling the 6001 but received the 8001 public contract?
  const client = await nodeClient({
    hostname: `http://localhost:${baseServerPort}`,
    contractDir: clientContractDir
  })

  debug(client)

  const result = await client.subUpdateMsService('testing')
  t.truthy(result.indexOf(`ms service`))
})
