// reusable options
const { join } = require('path');

const { DEFAULT_HEADER, CONTRACT_KEY_NAME, CONTENT_TYPE } = require('jsonql-constants');

const type = CONTENT_TYPE;
const headers = DEFAULT_HEADER;
const contractKeyName = CONTRACT_KEY_NAME;

const dirs = {
  resolverDir: join(__dirname, 'resolvers'),
  contractDir: join(__dirname, 'tmp')
}

const secret = 'A little cat is watching you';

const bearer = 'sofud89723904lksd98234230823jlkjklsdfds';

const returns =  [
  {
    "type": "string",
    "description": "stock message"
  }
];

const dummy = 'yes I am dummy';

module.exports = { type, headers, dirs, secret, bearer, returns, contractKeyName, dummy };
