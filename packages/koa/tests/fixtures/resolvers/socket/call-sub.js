/**
 * Test interface to try to call the sub using socket client
 * @param {string} msg a message
 * @return {string} a reply from the sub
 */
module.exports = function callSub(msg) {

  let newMsg; /// fill this with the reply from the sub

  return `Got a ${newMsg || 'nothing'}`
}
