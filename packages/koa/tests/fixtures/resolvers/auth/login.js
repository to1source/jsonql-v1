const debug = require('debug')('jsonql:koa:issuer')
const { bearer } = require('../../options')

// this is the stock method for giving out author header

/**
 * Auth method
 * @param {string} username user name
 * @param {string} password password
 * @return {string|boolean} token on success, false on fail
 */
module.exports = function(username, password) {
  debug('received this', username, password)
  if (username === 'nobody' && password) {
    return bearer;
  }
  return false;
}
