const debug = require('debug')('jsonql:koa:validator')
const { bearer } = require('../../options')
// this is the core method for checking the Auth header
/**
 * @param {string} token jwt
 * @return {object|boolean} user data on success, false on fail
 */
module.exports = function validator(token) {
  debug('received this token', token, bearer)
  if (token === bearer) {
    return {userId: 1}
  }
  return false;
}
