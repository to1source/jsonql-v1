const debug = require('debug')('jsonql-koa:resolver:query:testList');
/**
 * @param {number} num a number
 * @return {object} @TODO need to figure out how to give keys to the returns
 */
module.exports = function testList(num) {
  debug('Call testList with this params', num);
  return {
    modified: Date.now(),
    text: testList.userdata && testList.userdata.dummy ? testList.userdata.dummy : 'nope',
    num: num ? --num : -1
  }
}
