// this will generate a event emitter and will be use everywhere
import EventEmitterClass from '@to1source/event'
// create a clone version so we know which one we actually is using
class JsonqlWsEvt extends EventEmitterClass {

  constructor(logger) {
    if (typeof logger !== 'function') {
      throw new Error(`Just die here the logger is not a function!`)
    }
    logger(`---> Create a new EventEmitter <---`)
    // this ee will always come with the logger
    // because we should take the ee from the configuration
    super({ logger })
  }

  get name() {
    return'jsonql-ws-client-core'
  }
}

/**
 * getting the event emitter
 * @param {object} opts configuration
 * @return {object} the event emitter instance
 */
const getEventEmitter = opts => {
  const { log, eventEmitter } = opts
  
  if (eventEmitter) {
    log(`eventEmitter is:`, eventEmitter.name)
    return eventEmitter
  }
  
  return new JsonqlWsEvt( opts.log )
}

export { 
  getEventEmitter, 
  EventEmitterClass // for other module to build from  
}
