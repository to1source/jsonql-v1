// move the get logger stuff here

// it does nothing
const dummyLogger = () => {}

/**
 * re-use the debugOn prop to control this log method
 * @param {object} opts configuration
 * @return {function} the log function
 */
const getLogger = (opts) => {
  const { debugOn } = opts  
  if (debugOn) {
    return (...args) => {
      Reflect.apply(console.info, console, ['[jsonql-ws-client-core]', ...args])
    }
  }
  return dummyLogger
}

/**
 * Make sure there is a log method
 * @param {object} opts configuration
 * @return {object} opts
 */
const getLogFn = opts => {
  const { log } = opts // 1.3.9 if we pass a log method here then we use this
  if (!log || typeof log !== 'function') {
    return getLogger(opts)
  }
  opts.log('---> getLogFn user supplied log function <---', opts)
  return log
}

export { getLogFn }