// take the sharable default options out and put here
// then we can export it and re-use on the downstream clients
import { createConfig } from 'jsonql-params-validator'
import { isContract } from '../utils'
import {
  STRING_TYPE,
  BOOLEAN_TYPE,
  OBJECT_TYPE,
  
  CHECKER_KEY,
  JSONQL_PATH,
  
  SOCKET_TYPE_CLIENT_ALIAS,
  
  ALIAS_KEY,
  LOGIN_FN_NAME,
  LOGOUT_FN_NAME,
  DISCONNECT_FN_NAME,
  SWITCH_USER_FN_NAME,

  SOCKET_TYPE_PROP_KEY,
  STANDALONE_PROP_KEY,
  DEBUG_ON_PROP_KEY,
  LOGIN_FN_NAME_PROP_KEY,
  LOGOUT_FN_NAME_PROP_KEY,
  DISCONNECT_FN_NAME_PROP_KEY,
  SWITCH_USER_FN_NAME_PROP_KEY,

  HOSTNAME_PROP_KEY,
  NAMESAPCE_PROP_KEY,
  WS_OPT_PROP_KEY,
  CONTRACT_PROP_KEY,
  ENABLE_AUTH_PROP_KEY,
  TOKEN_PROP_KEY,
  SUSPEND_EVENT_PROP_KEY,
  
  CSRF_PROP_KEY,
  CSRF_HEADER_KEY,
  USE_JWT_PROP_KEY,

  PUBLIC_KEY,
  PRIVATE_KEY,

} from 'jsonql-constants'



const configCheckMap = {
  [STANDALONE_PROP_KEY]: createConfig(false, [BOOLEAN_TYPE]), // to turn on or off some of the features
  [DEBUG_ON_PROP_KEY]: createConfig(false, [BOOLEAN_TYPE]),
  // useCallbackStyle: createConfig(false, [BOOLEAN_TYPE]), abandoned in 0.6.0
  // Please note the login method will be a public available always 
  // what if the developer wants to have a socket only setup to do a CORS SPA site
  [LOGIN_FN_NAME_PROP_KEY]: createConfig(LOGIN_FN_NAME, [STRING_TYPE]),
  [LOGOUT_FN_NAME_PROP_KEY]: createConfig(LOGOUT_FN_NAME, [STRING_TYPE]),
  // just matching the server side
  [DISCONNECT_FN_NAME_PROP_KEY]: createConfig(DISCONNECT_FN_NAME, [STRING_TYPE]),
  [SWITCH_USER_FN_NAME_PROP_KEY]: createConfig(SWITCH_USER_FN_NAME, [STRING_TYPE]),

  [HOSTNAME_PROP_KEY]: createConfig(false, [STRING_TYPE]),
  [NAMESAPCE_PROP_KEY]: createConfig(JSONQL_PATH, [STRING_TYPE]),
  [WS_OPT_PROP_KEY]: createConfig({}, [OBJECT_TYPE]),
  // make this null as default don't set this here, only set in the down stream
  // serverType: createConfig(null, [STRING_TYPE], {[ENUM_KEY]: AVAILABLE_SERVERS}),
  // we require the contract already generated and pass here
  [CONTRACT_PROP_KEY]: createConfig({}, [OBJECT_TYPE], {[CHECKER_KEY]: isContract}),
  [ENABLE_AUTH_PROP_KEY]: createConfig(false, [BOOLEAN_TYPE]),
  [TOKEN_PROP_KEY]: createConfig(false, [STRING_TYPE]),

  [CSRF_PROP_KEY]: createConfig(CSRF_HEADER_KEY, [STRING_TYPE]),
  // we will use this for determine the socket.io client type as well - @TODO remove or rename
  // [USE_JWT_PROP_KEY]: createConfig(true, [BOOLEAN_TYPE, STRING_TYPE]),
  // this is going to replace the use of useJwt === string next
  // @TODO remove this, use the interceptor to replace this options
  // authStrKey: createConfig(null, [STRING_TYPE]),
  // this is experimental to see the effects, might take downlater
  [SUSPEND_EVENT_PROP_KEY]: createConfig(false, [BOOLEAN_TYPE])
  // should this be moved to the framework instead

}

// socket client
const socketCheckMap = {
  // new prop for socket client
  [SOCKET_TYPE_PROP_KEY]: createConfig(null, [STRING_TYPE], {[ALIAS_KEY]: SOCKET_TYPE_CLIENT_ALIAS})
    // [ENUM_KEY]: [JS_WS_NAME, JS_WS_SOCKET_IO_NAME, JS_PRIMUS_NAME],
}

const wsCoreCheckMap = Object.assign(configCheckMap, socketCheckMap)

// constant props
const wsCoreConstProps = {
  [USE_JWT_PROP_KEY]: true, // should not allow to change anymore
  log: null,
  // contract: null, 
  eventEmitter: null,
  // we unify the two different client into one now
  // only expect different parameter
  nspClient: null,
  nspAuthClient: null,
  // contructed path
  wssPath: '',
  // for generate the namespaces
  publicNamespace: PUBLIC_KEY,
  privateNamespace: PRIVATE_KEY
}

export { 
  wsCoreCheckMap, 
  wsCoreConstProps, 
  socketCheckMap 
}
