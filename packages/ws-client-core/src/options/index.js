// create options
import {
  checkConfigAsync,
  checkConfig
} from 'jsonql-params-validator'
import {
  wsCoreCheckMap,
  wsCoreConstProps,
  socketCheckMap
} from './defaults'
import {
  fixWss,
  getHostName,
  getEventEmitter,
  getNspInfoByConfig,
  getLogFn
} from '../utils'

import {
  SUSPEND_EVENT_PROP_KEY,
  ON_RESULT_FN_NAME,
  ON_LOGIN_FN_NAME
} from 'jsonql-constants'

/**
 * We need this to find the socket server type
 * @param {*} config
 * @return {string} the name of the socket server if any
 */
function checkSocketClientType(config) {
  return checkConfig(config, socketCheckMap)
}

/**
 * Create a combine checkConfig for the creating the combine client
 * @param {*} configCheckMap
 * @param {*} constProps
 * @param {boolean} [withInject=false] if we need to run the postCheckInjectOpts 
 * @return {function} takes the user input config then resolve the configuration
 */
function createCombineConfigCheck(configCheckMap, constProps, withInject = false) {
  const combineCheckMap = Object.assign({}, wsCoreCheckMap, configCheckMap)
  const combineConstProps = Object.assign({}, wsCoreConstProps, constProps)

  return function runCheckConfigAsync(config) { 
    return checkConfigAsync(config, combineCheckMap, combineConstProps)
      .then(opts => withInject ? postCheckInjectOpts(opts) : opts)
  }
}


/**
 * wrapper method to check this already did the pre check
 * @param {object} config user supply config
 * @param {object} defaultOptions for checking
 * @param {object} constProps user supply const props
 * @return {promise} resolve to the checked opitons
 */
function checkConfiguration(config, defaultOptions, constProps) {
  const defaultCheckMap= Object.assign(wsCoreCheckMap, defaultOptions)
  const wsConstProps = Object.assign(wsCoreConstProps, constProps)

  return checkConfigAsync(config, defaultCheckMap, wsConstProps)
}

/**
 * Taking the `then` part from the method below
 * @param {object} opts
 * @return {promise} opts all done
 */
function postCheckInjectOpts(opts) {
  
  return Promise.resolve(opts)
    .then(opts => {
      if (!opts.hostname) {
        opts.hostname = getHostName()
      }
      // @TODO the contract now will supply the namespace information
      // and we need to use that to group the namespace call
      
      opts.wssPath = fixWss([opts.hostname, opts.namespace].join('/'), opts.serverType)
      // get the log function here
      opts.log = getLogFn(opts)

      opts.eventEmitter = getEventEmitter(opts)
    
      return opts
    })
}

/**
 * Don't want to make things confusing
 * Breaking up the opts process in one place
 * then generate the necessary parameter in another step
 * @2020-3-20 here we suspend operation by it's namespace first
 * Then in the framework part, after the connection establish we release
 * the queue
 * @param {object} opts checked --> merge --> injected
 * @return {object} {opts, nspMap, ee}
 */
function createRequiredParams(opts) {
  const nspMap = getNspInfoByConfig(opts)
  const ee = opts.eventEmitter
  // @TODO here we are going to add suspend event to the namespace related methods
  const { log } = opts 
  const { namespaces, publicNamespace } = nspMap
  log(`namespaces`, namespaces)
  // next we loop the namespace and suspend all the events prefix with namespace 
  if (opts[SUSPEND_EVENT_PROP_KEY] === true) {
    // ON_READY_FN_NAME
    // ON_LOGIN_FN_NAME

    // we create this as a function then we can call it again 
    // @TODO we need to add the onReady and the onLogin in the suspend list as well
    opts.$suspendNamepsace = () => {
      let eventNames = [ON_RESULT_FN_NAME, ...namespaces]
      if (opts.enableAuth) {
        eventNames.push(ON_LOGIN_FN_NAME)
      }
      // [ON_READY_FN_NAME, ON_LOGIN_FN_NAME, ...namespaces]
      Reflect.apply(ee.$suspendEvent, ee, eventNames)
    }
    // then we create a new method to releas the queue 
    // we prefix it with the $ to notify this is not a jsonql part methods
    opts.$releaseNamespace = () => {
      ee.$releaseEvent(ON_RESULT_FN_NAME, publicNamespace)
    }

    if (opts.enableAuth) {
      opts.$releasePrivateNamespace = () => {
        ee.$releaseEvent(ON_LOGIN_FN_NAME, namespaces[1])
      }
    }

    // now run it 
    opts.$suspendNamepsace()
  }
  
  return { opts, nspMap, ee }
}

export {
  // properties
  wsCoreCheckMap,
  wsCoreConstProps,
  // functions
  checkConfiguration,
  postCheckInjectOpts,
  createRequiredParams,
  // this will just get export for integration
  checkSocketClientType,
  createCombineConfigCheck
}
