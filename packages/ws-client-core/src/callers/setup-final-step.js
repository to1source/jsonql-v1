// The final step of the setup before it returns the client
import { 
  CONNECT_EVENT_NAME, 
  SUSPEND_EVENT_PROP_KEY
} from 'jsonql-constants'

import { setupInterCom } from './setup-intercom'
import { setupStatePropKeys } from '../utils'


/**
 * The final step to return the client
 * @param {object} obj the client
 * @param {object} opts configuration
 * @param {object} ee the event emitter
 * @return {object} client
 */
function setupFinalStep(obj, opts, ee) {
  
  let client = setupInterCom(obj, opts, ee)
  // opts.log(`---> The final step to return the ws-client <---`)
  // add some debug functions
  client.verifyEventEmitter = () => ee.is
  // we add back the two things into the client
  // then when we do integration, we run it in reverse,
  // create the ws client first then the host client
  client.eventEmitter = opts.eventEmitter
  client.log = opts.log

  // now at this point, we are going to call the connect event
  ee.$trigger(CONNECT_EVENT_NAME, [opts, ee]) // just passing back the entire opts object
  // also we can release the queue here 
  if (opts[SUSPEND_EVENT_PROP_KEY] === true) {
    opts.$releaseNamespace()
  }

  // finally inject some prop keys to the client 
  return setupStatePropKeys(obj)
}


export { setupFinalStep }
