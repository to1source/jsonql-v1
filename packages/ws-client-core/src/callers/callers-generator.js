// resolvers generator
// we change the interface to return promise from v1.0.3
// this way we make sure the obj return is correct and timely
import { NSP_GROUP } from 'jsonql-constants'
import { chainFns } from '../utils'

import { generateResolvers } from './generator-methods'
import {
  setupOnReadyListener,
  setupNamespaceErrorListener
} from '../listener/global-listeners'

import { setupAuthMethods } from '../auth'

import { setupFinalStep } from './setup-final-step'

/**
 * This is the starting point of binding callable method to the 
 * ws, and we have to determine which is actually active 
 * if enableAuth but we don't have a token, when the connection 
 * to ws establish we only release the public related event 
 * and the private one remain hold until the LOGIN_EVENT get trigger
 * @param {object} opts configuration
 * @param {object} nspMap resolvers index by their namespace
 * @param {object} ee EventEmitter
 * @return {object} of resolvers
 * @public
 */
export function callersGenerator(opts, nspMap, ee) {
  
  let fns = [
    generateResolvers,
    setupOnReadyListener,
    setupNamespaceErrorListener
  ]
  if (opts.enableAuth) {
    // there is a problem here, when this is a public namespace
    // it should not have a login logout event attach to it
    fns.push(setupAuthMethods)
  }
  // we will always get back the [ obj, opts, ee ]
  // then we only return the obj (wsClient)
  // This has move outside of here, into the main method
  // the reason is we could switch around the sequence much easier
  fns.push(setupFinalStep)
  // stupid reaon!!!
  const executer = Reflect.apply(chainFns, null, fns)
  // run it
  return executer(opts, ee, nspMap[NSP_GROUP])
}
