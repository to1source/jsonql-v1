// create a stock helloWorld method for tesing the connection
// DO we really need this at all? 

import { HELLO_FN, DATA_KEY, ERROR_KEY } from 'jsonql-constants' 
import { createQueryStr } from 'jsonql-utils/module'
import { JsonqlError } from 'jsonql-errors'
// import { injectToFn } from '../utils'
// import { setupResolver } from './setup-resolver'
import { extractSrvPayload } from './intercom-methods'

/**
 * This one is a special method (just like in http) as a built in method to test the connection
 * it just emit an event 
 * @param {function} deliverFn the framework specific method to send thing 
 * @return {void} it doesn't return anything because it only responsible send things out 
 */
export function helloWorld(deliverFn) {
  deliverFn(createQueryStr(HELLO_FN, []))
}


/**
 * This is the capture the helloWorld return method, after passing through the extractWsPayload
 * @param {object} data the extracted data from payload
 * @return {*} the data part 
 */
export function getHelloMsg(payload) {
  const data = payload.data ? payload.data : payload 
  const result = extractSrvPayload(data)

  if (result[ERROR_KEY]) {
    throw new JsonqlError('getHelloMsg', result[ERROR_KEY])
  }

  return result[DATA_KEY]
}

