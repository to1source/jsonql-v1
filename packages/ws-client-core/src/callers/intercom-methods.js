// this will be part of the init client sequence
// as soon as we create a ws client
// we listen to the on.connect event 
// then we send a init-ping event back to the server
// and server issue a csrf token back to use 
// we use this token to create a new client and destroy the old one
import {
  INTERCOM_RESOLVER_NAME, 
  SOCKET_PING_EVENT_NAME,
  HEADERS_KEY,
  DATA_KEY,
  CSRF_HEADER_KEY
} from 'jsonql-constants'
import { 
  createQueryStr, 
  extractWsPayload,
  timestamp,
  toJson 
} from 'jsonql-utils/module'
import {
  JsonqlError
} from 'jsonql-errors'
const CSRF_HEADER_NOT_EXIST_ERR = 'CSRF header is not in the received payload'

/**
 * Util method 
 * @param {string} payload return from server
 * @return {object} the useful bit 
 */
function extractSrvPayload(payload) {
  let json = toJson(payload)
  
  if (json && typeof json === 'object') {
    // note this method expect the json.data inside
    return extractWsPayload(json)
  }
  
  throw new JsonqlError('extractSrvPayload', json)
}

/**
 * call the server to get a csrf token 
 * @return {string} formatted payload to send to the server 
 */
function createInitPing() {
  const ts = timestamp()

  return createQueryStr(INTERCOM_RESOLVER_NAME, [SOCKET_PING_EVENT_NAME, ts])
}

/**
 * Take the raw on.message result back then decoded it 
 * @param {*} payload the raw result from server
 * @return {object} the csrf payload
 */
function extractPingResult(payload) {
  const result = extractSrvPayload(payload)
  
  if (result && result[DATA_KEY] && result[DATA_KEY][CSRF_HEADER_KEY]) {
    return {
      [HEADERS_KEY]: result[DATA_KEY]
    }
  }

  throw new JsonqlError('extractPingResult', CSRF_HEADER_NOT_EXIST_ERR)
}


/**
 * Create a generic intercom method
 * @param {string} type the event type 
 * @param {array} args if any 
 * @return {string} formatted payload to send
 */
function createIntercomPayload(type, ...args) {
  const ts = timestamp()
  let payload = [type].concat(args)
  payload.push(ts)
  return createQueryStr(INTERCOM_RESOLVER_NAME, payload)
}


export { 
  extractSrvPayload,
  createInitPing, 
  extractPingResult, 
  createIntercomPayload 
}