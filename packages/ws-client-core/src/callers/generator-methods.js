// put all the resolver related methods here to make it more clear

// this will be a mini client server architect
// The reason is when the enableAuth setup - the private route
// might not be validated, but we need the callable point is ready
// therefore this part will always take the contract and generate
// callable api for the developer to setup their front end
// the only thing is - when they call they might get an error or
// NOT_LOGIN_IN and they can react to this error accordingly

import { createResolver, setupResolver } from './setup-resolver'
import { injectToFn } from '../utils'

/**
 * step one get the clientmap with the namespace
 * @param {object} opts configuration
 * @param {object} ee EventEmitter
 * @param {object} nspGroup resolvers index by their namespace
 * @return {promise} resolve the clientmapped, and start the chain
 */
export function generateResolvers(opts, ee, nspGroup) {
  const { log } = opts
  let client= {}
  
  for (let namespace in nspGroup) {
    let list = nspGroup[namespace]
    for (let resolverName in list) {
      // resolverNames.push(resolverName)
      let params = list[resolverName]
      let fn = createResolver(ee, namespace, resolverName, params, log)
      // this should set as a getter therefore can not be overwrite by accident
      client = injectToFn(
        client,
        resolverName,
        setupResolver(namespace, resolverName, params, fn, ee, log)
      )
    }
  }
  // resolve the clientto start the chain
  // chain the result to allow the chain processing
  return [ client, opts, ee, nspGroup ]
}

