// this is a new method that will create several
// intercom method also reverse listen to the server
// such as disconnect (server issue disconnect)
import { injectToFn, chainFns } from '../utils'
import { 
  CONNECT_EVENT_NAME,
  CONNECTED_EVENT_NAME,
  DISCONNECT_EVENT_NAME,
  CONNECTED_PROP_KEY
} from 'jsonql-constants'

/**
 * Set up the CONNECTED_PROP_KEY to the client
 * @param {*} client 
 * @param {*} opts 
 * @param {*} ee 
 */
function setupConnectPropKey(client, opts, ee) {
  const { log } = opts 
  log('[1] setupConnectPropKey')
    // we just inject a helloWorld method here
  // set up the init state of the connect
  client = injectToFn(client, CONNECTED_PROP_KEY , false, true)
  return [ client, opts, ee ]
}


/**
 * setup listener to the connect event 
 * @param {*} client 
 * @param {*} opts 
 * @param {*} ee 
 */
function setupConnectEvtListener(client, opts, ee) {
  // @TODO do what at this point?
  const { log } = opts 

  log(`[2] setupConnectEvtListener`)

  ee.$on(CONNECT_EVENT_NAME, function(...args) {
    log(`setupConnectEvtListener pass and do nothing at the moment`, args)
  })
  
  return [client, opts, ee]
}

/**
 * setup listener to the connected event 
 * @param {*} client 
 * @param {*} opts 
 * @param {*} ee 
 */
function setupConnectedEvtListener(client, opts, ee) {
  const { log } = opts 

  log(`[3] setupConnectedEvtListener`)

  ee.$on(CONNECTED_EVENT_NAME, function() {
    client[CONNECTED_PROP_KEY] = true
    // new action to take release the holded event queue 
    const ctn = ee.$release()

    log(`CONNECTED_EVENT_NAME`, true, 'queue count', ctn)

  return {[CONNECTED_PROP_KEY]: true}
  })

  return [client, opts, ee]
}

/**
 * Listen to the disconnect event and set the property to the client 
 * @param {*} client 
 * @param {*} opts 
 * @param {*} ee 
 */
function setupDisconnectListener(client, opts, ee) {
  const { log } = opts 

  log(`[4] setupDisconnectListener`)

  ee.$on(DISCONNECT_EVENT_NAME, function() {
    client[CONNECTED_PROP_KEY] = false
    log(`CONNECTED_EVENT_NAME`, false)

  return {[CONNECTED_PROP_KEY]: false}
  })

  return [client, opts, ee]
}

/**
 * disconnect action
 * @param {*} client 
 * @param {*} opts 
 * @param {*} ee 
 * @return {object} this is the final step to return the client
 */
function setupDisconectAction(client, opts, ee) {
  const { disconnectHandlerName, log } = opts
  log(`[5] setupDisconectAction`)

  return injectToFn(
    client,
    disconnectHandlerName,
    function disconnectHandler(...args) {
      ee.$trigger(DISCONNECT_EVENT_NAME, args)
    }
  )
}

/**
 * this is the new method that setup the intercom handler
 * also this serve as the final call in the then chain to
 * output the client
 * @param {object} client the client
 * @param {object} opts configuration
 * @param {object} ee the event emitter
 * @return {object} client
 */
export function setupInterCom(client, opts, ee) {
  const fns = [
    setupConnectPropKey,
    setupConnectEvtListener,
    setupConnectedEvtListener,
    setupDisconnectListener,
    setupDisconectAction
  ]

  const executor = Reflect.apply(chainFns, null, fns)
  return executor(client, opts, ee)
}
