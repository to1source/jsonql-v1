// break up the original setup resolver method here
// import { JsonqlValidationError, finalCatch } from 'jsonql-errors'
import {
  ON_ERROR_FN_NAME,
  ON_MESSAGE_FN_NAME,
  ON_RESULT_FN_NAME
} from 'jsonql-constants'
import { finalCatch } from 'jsonql-errors'
import { validateAsync } from 'jsonql-params-validator'
// local
import { 
  chainFns, 
  objDefineProps, 
  injectToFn, 
  createEvt, 
  isFunc 
} from '../utils'
import { actionCall } from './action-call'
import { MY_NAMESPACE } from '../options/constants'
import { respondHandler } from './respond-handler'
import { setupSendMethod } from './setup-send-method'


/**
 * moved back from generator-methods  
 * create the actual function to send message to server
 * @param {object} ee EventEmitter instance
 * @param {string} namespace this resolver end point
 * @param {string} resolverName name of resolver as event name
 * @param {object} params from contract
 * @param {function} log pass the log function
 * @return {function} resolver
 */
function createResolver(ee, namespace, resolverName, params, log) {
  // note we pass the new withResult=true option
  return function resolver(...args) {
    return validateAsync(args, params.params, true)
      .then(_args => actionCall(ee, namespace, resolverName, _args, log))
      .catch(finalCatch)
  }
}

/**
 * The first one in the chain, just setup a namespace prop
 * the rest are passing through
 * @param {function} fn the resolver function
 * @param {object} ee the event emitter
 * @param {string} resolverName what it said
 * @param {object} params for resolver from contract
 * @param {function} log the logger function
 * @return {array}
 */
const setupNamespace = (fn, ee, namespace, resolverName, params, log) => [
  injectToFn(fn, MY_NAMESPACE, namespace),
  ee,
  namespace,
  resolverName,
  params,
  log
]

/**
 * onResult handler
 */
const setupOnResult = (fn, ee, namespace, resolverName, params, log) => [
  objDefineProps(fn, ON_RESULT_FN_NAME, function(resultCallback) {
    if (isFunc(resultCallback)) {
      ee.$on(
        createEvt(namespace, resolverName, ON_RESULT_FN_NAME),
        function resultHandler(result) {
          respondHandler(result, resultCallback, (error) => {
            log(`Catch error: "${resolverName}"`, error)
            ee.$trigger(
              createEvt(namespace, resolverName, ON_ERROR_FN_NAME),
              error
            )
          })
        }
      )
    }
  }),
  ee,
  namespace,
  resolverName,
  params,
  log
]

/**
 * we do need to add the send prop back because it's the only way to deal with
 * bi-directional data stream
 */
const setupOnMessage = (fn, ee, namespace, resolverName, params, log) => [
  objDefineProps(fn, ON_MESSAGE_FN_NAME, function(messageCallback) {
    // we expect this to be a function
    if (isFunc(messageCallback)) {
      // did that add to the callback
      let onMessageCallback = (args) => {
        log(`onMessageCallback`, args)
        respondHandler(
          args, 
          messageCallback, 
          (error) => {
            log(`Catch error: "${resolverName}"`, error)
            ee.$trigger(
              createEvt(namespace, resolverName, ON_ERROR_FN_NAME),
              error
            )
          })
        }
      // register the handler for this message event
      ee.$only(
        createEvt(namespace, resolverName, ON_MESSAGE_FN_NAME),
        onMessageCallback
      )
    }
  }),
  ee,
  namespace,
  resolverName,
  params,
  log
]

/**
 * ON_ERROR_FN_NAME handler
 */
const setupOnError = (fn, ee, namespace, resolverName, params, log) => [
  objDefineProps(fn, ON_ERROR_FN_NAME, function(resolverErrorHandler) {
    if (isFunc(resolverErrorHandler)) {
      // please note ON_ERROR_FN_NAME can add multiple listners
      ee.$only(
        createEvt(namespace, resolverName, ON_ERROR_FN_NAME),
        resolverErrorHandler
      )
    }
  }),
  ee,
  namespace,
  resolverName,
  params,
  log
]

/**
 * Add extra property / listeners to the resolver
 * @param {string} namespace where this belongs
 * @param {string} resolverName name as event name
 * @param {object} params from contract
 * @param {function} fn resolver function
 * @param {object} ee EventEmitter
 * @param {function} log function
 * @return {function} resolver
 */  
function setupResolver(namespace, resolverName, params, fn, ee, log) {
  let fns = [
    setupNamespace,
    setupOnResult,
    setupOnMessage,
    setupOnError,
    setupSendMethod
  ]
  const executor = Reflect.apply(chainFns, null, fns)
  // get the executor
  return executor(fn, ee, namespace, resolverName, params, log)
}

export { 
  createResolver, 
  setupResolver 
}