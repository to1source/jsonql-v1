// taking it out, this is kind of pointless at the moment and its potentially breaking things

/**
 * when useCallbackStyle=true use this instead of the above method
 * @param {object} obj the base object to attach to
 * @param {object} ee EventEmitter
 * @param {object} nspSet the map
 * @param {object} opts configuration
 * @return {object} obj
 */
export function createCallbackHandler(obj, ee, nspSet, opts) {
  return injectToFn(obj, CB_FN_NAME, function onHandler(evtName, callback) {
    if (isString(evtName) && isFunc(callback)) {
      switch (evtName) {
        case ERROR_PROP_NAME:
          for (let namespace in nspSet) {
            // this one is very tricky, we need to make sure the trigger is calling
            // with the namespace as well as the error
            ee.$on(createEvt(namespace, ERROR_PROP_NAME), callback)
          }
          break;
        case LOGIN_PROP_NAME: // @TODO should only available when enableAuth=true
          ee.$only(LOGIN_PROP_NAME, callback)
          break;
        case READY_PROP_NAME:
          ee.$on(READY_PROP_NAME, callback)
          break;
        default:
          ee.$trigger(ERROR_PROP_NAME, new JsonqlError(CB_FN_NAME, `Unknown event name ${evtName}!`))
      }
    }
    // @TODO need to issue another error here!
  })
}