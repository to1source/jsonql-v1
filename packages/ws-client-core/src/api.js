// the top level API
// The goal is to create a generic method that will able to handle
// any kind of clients
// import { injectToFn } from 'jsonql-utils'
import { callersGenerator } from './callers'
import {
  checkConfiguration,
  postCheckInjectOpts,
  createRequiredParams
} from './options'


/**
 * 0.5.0 we break up the wsClientCore in two parts one without the config check
 * @param {function} setupSocketClientListener just make sure what it said it does
 * @return {function} to actually generate the client
 */
export function wsClientCoreAction(setupSocketClientListener) {
  /**
   * This is a breaking change, to continue the onion skin design
   * @param {object} config the already checked config
   * @return {promise} resolve the client
   */
  return function createClientAction(config = {}) {

    return postCheckInjectOpts(config)
      .then(createRequiredParams)
      .then(
        ({opts, nspMap, ee}) => setupSocketClientListener(opts, nspMap, ee)
      )
      .then(
        ({opts, nspMap, ee}) => callersGenerator(opts, nspMap, ee)
      )
      .catch(err => {
        console.error(`[jsonql-ws-core-client init error]`, err)
      })
  }
}

/**
 * The main interface which will generate the socket clients and map all events
 * @param {object} socketClientListerner this is the one method export by various clients
 * @param {object} [configCheckMap={}] we should do all the checking in the core instead of the client
 * @param {object} [constProps={}] add this to supply the constProps from the downstream client
 * @return {function} accept a config then return the wsClient instance with all the available API
 */
export function wsClientCore(socketClientListener, configCheckMap = {}, constProps = {}) {
  // we need to inject property to this client later
  return (config = {}) => checkConfiguration(config, configCheckMap, constProps)
                            .then(
                              wsClientCoreAction(socketClientListener)
                            )
}
