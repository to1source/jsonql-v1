// move from generator-methods 
// they are global event listeners 
import {
  createEvt,
  objDefineProps,
  isFunc
} from '../utils'
import {
  ON_ERROR_FN_NAME,
  ON_READY_FN_NAME
} from 'jsonql-constants'

/**
 * This event will fire when the socket.io.on('connection') and ws.onopen
 * @param {object} client  client itself
 * @param {object} opts configuration
 * @param {object} ee Event Emitter
 * @return {array} [ obj, opts, ee ]
 */
export function setupOnReadyListener(client, opts, ee) {
  return [
    objDefineProps(
      client,
      ON_READY_FN_NAME,
      function onReadyCallbackHandler(onReadyCallback) {
        if (isFunc(onReadyCallback)) {
          // reduce it down to just one flat level
          // @2020-03-19 only allow ONE onReady callback otherwise
          // it will get fire multiple times which is not what we want
          ee.$only(ON_READY_FN_NAME, onReadyCallback)
        }
      }
    ),
    opts,
    ee
  ]
}

/**
 * The problem is the namespace can have more than one
 * and we only have on onError message
 * @param {object} clientthe client itself
 * @param {object} opts configuration
 * @param {object} ee Event Emitter
 * @param {object} nspGroup namespace keys
 * @return {array} [obj, opts, ee]
 */
export function setupNamespaceErrorListener(client, opts, ee, nspGroup) {
  return [
    objDefineProps(
      client,
      ON_ERROR_FN_NAME,
      function namespaceErrorCallbackHandler(namespaceErrorHandler) {
        if (isFunc(namespaceErrorHandler)) {
          // please note ON_ERROR_FN_NAME can add multiple listners
          for (let namespace in nspGroup) {
            // this one is very tricky, we need to make sure the trigger is calling
            // with the namespace as well as the error
            ee.$on(createEvt(namespace, ON_ERROR_FN_NAME), namespaceErrorHandler)
          }
        }
      }
    ),
    opts,
    ee
  ]
}

