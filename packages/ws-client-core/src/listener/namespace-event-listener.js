// This is share between different clients so we export it
// @TODO port what is in the ws-main-handler
// because all the client side call are via the ee
// and that makes it re-usable between different client setup

/*
Inside the map call but we take it out for now and until the WebSocket version is fully working
import { SOCKET_IO } from '../options/constants'
       // @TODO need to double check this
       if (opts.serverType === SOCKET_IO) {
        let { nspGroup } = nspMap
        args.push(nspGroup[namespace])
      }
*/
import { getPrivateNamespace } from 'jsonql-utils/src/namespace'
import { logoutEvtListener, notLoginListener } from './event-listeners'

/**
 * centralize all the comm in one place
 * @param {function} bindSocketEventHandler binding the ee to ws --> this is the core bit
 * @param {object} nsps namespaced nsp
 * @return {void} nothing
 */
export function namespaceEventListener(bindSocketEventListener, nsps) {
  /**
   * BREAKING CHANGE instead of one flat structure
   * we return a function to accept the two
   * @param {object} opts configuration
   * @param {object} nspMap this is not in the opts
   * @param {object} ee Event Emitter instance
   * @return {array} although we return something but this is the last step and nothing to do further
   */
  return (opts, nspMap, ee) => {
    // since all these params already in the opts
    const { log } = opts
    const { namespaces } = nspMap
    // @1.1.3 add isPrivate prop to id which namespace is the private nsp
    // then we can use this prop to determine if we need to fire the ON_LOGIN_PROP_NAME event
    const privateNamespace = getPrivateNamespace(namespaces)
    // The total number of namespaces (which is 2 at the moment) minus the private namespace number
    let ctn = namespaces.length - 1   
    // @NOTE we need to somehow id if this namespace already been connected 
    // and the event been released before 
    return namespaces.map(namespace => {
      let isPrivate = privateNamespace === namespace
      log(namespace, ` --> ${isPrivate ? 'private': 'public'} nsp --> `, nsps[namespace] !== false)
      if (nsps[namespace]) {
        log('[call bindWsHandler]', isPrivate, namespace)
        // we need to add one more property here to tell the bindSocketEventListener 
        // how many times it should call the onReady 
        let args = [namespace, nsps[namespace], ee, isPrivate, opts, --ctn]
        // Finally we binding everything together
        Reflect.apply(bindSocketEventListener, null, args)
        
      } else {
        log(`binding notLoginWsHandler to ${namespace}`)
        // a dummy placeholder
        // @TODO but it should be a not connect handler
        // when it's not login (or fail) this should be handle differently
        notLoginListener(namespace, ee, opts)
      }
      if (isPrivate) {
        log(`Has private and add logoutEvtHandler`)
        logoutEvtListener(nsps, namespaces, ee, opts)
      }
      // just return something its not going to get use anywhere
      return isPrivate
    })
  }
}
