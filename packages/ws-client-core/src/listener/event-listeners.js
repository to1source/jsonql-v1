// NOT IN USE AT THE MOMENT JUST KEEP IT HERE FOR THE TIME BEING 
import {
  LOGOUT_EVENT_NAME,
  NOT_LOGIN_ERR_MSG,
  ON_ERROR_FN_NAME,
  ON_RESULT_FN_NAME,
  DISCONNECT_EVENT_NAME
  // SUSPEND_EVENT_PROP_KEY
} from 'jsonql-constants'
import { EMIT_EVT, DISCONNECTED_ERROR_MSG } from '../options/constants'
import { createEvt, clearMainEmitEvt } from '../utils'
import { triggerNamespacesOnError } from './trigger-namespaces-on-error'

/**
 * A Event Listerner placeholder when it's not connect to any nsp
 * @param {string} namespace nsp
 * @param {object} ee EventEmitter
 * @param {object} opts configuration
 * @return {void}
 */
export const notConnectedListener = (namespace, ee, opts) => {
  const { log } = opts 

  ee.$only(
    createEvt(namespace, EMIT_EVT),
    function disconnectedEvtCallback(resolverName, args) {
      log(`[disconnectedListerner] hijack the ws call`, namespace, resolverName, args)
      // Now we suspend all the calls but note the existing one won't be affected 
      // we need to update the methods to move everything across 
      const error = {
        message: DISCONNECTED_ERROR_MSG
      }
      ee.$call(createEvt(namespace, resolverName, ON_ERROR_FN_NAME), [ error ])
      // also trigger the result Listerner, but wrap inside the error key
      ee.$call(createEvt(namespace, resolverName, ON_RESULT_FN_NAME), [{ error }])
    }
  )
}


/**
 * The disconnect event Listerner, now we log the client out from everything
 * @TODO now we are another problem they disconnect, how to reconnect
 * @param {object} nsps the available nsp(s)
 * @param {array} namespaces available namespace 
 * @param {object} ee eventEmitter 
 * @param {object} opts configuration
 * @return {void}
 */
export const disconnectListener = (nsps, namespaces, ee, opts) => {
  const { log } = opts 
  ee.$on(
    DISCONNECT_EVENT_NAME, 
    function disconnectEvtCallback() {
      triggerNamespacesOnError(ee, namespaces, DISCONNECT_EVENT_NAME)
      namespaces.forEach( namespace => {      
        log(`disconnect from ${namespace}`)

        clearMainEmitEvt(ee, namespace)
        nsps[namespace] = null 
        disconnectedListerner(namespace, ee, opts)
      })
    }
  )
}

/**
 * A Event Listerner placeholder when it's not connect to the private nsp
 * @param {string} namespace nsp
 * @param {object} ee EventEmitter
 * @param {object} opts configuration
 * @return {void}
 */
export const notLoginListener = (namespace, ee, opts) => {
  const { log } = opts 

  ee.$only(
    createEvt(namespace, EMIT_EVT),
    function notLoginListernerCallback(resolverName, args) {
      log('[notLoginListerner] hijack the ws call', namespace, resolverName, args)
      const error = { message: NOT_LOGIN_ERR_MSG }
      // It should just throw error here and should not call the result
      // because that's channel for handling normal event not the fake one
      ee.$call(createEvt(namespace, resolverName, ON_ERROR_FN_NAME), [ error ])
      // also trigger the result Listerner, but wrap inside the error key
      ee.$call(createEvt(namespace, resolverName, ON_RESULT_FN_NAME), [{ error }])
    }
  )
}

/**
 * Only when there is a private namespace then we bind to this event
 * @param {object} nsps the available nsp(s)
 * @param {array} namespaces available namespace 
 * @param {object} ee eventEmitter 
 * @param {object} opts configuration
 * @return {void}
 */
export const logoutEvtListener = (nsps, namespaces, ee, opts) => {
  const { log } = opts 
  // this will be available regardless enableAuth
  // because the server can log the client out
  ee.$on(
    LOGOUT_EVENT_NAME, 
    function logoutEvtCallback() {
      const privateNamespace = getPrivateNamespace(namespaces)
      log(`${LOGOUT_EVENT_NAME} event triggered`)
      // disconnect(nsps, opts.serverType)
      // we need to issue error to all the namespace onError Listerner
      triggerNamespacesOnError(ee, [privateNamespace], LOGOUT_EVENT_NAME)
      // rebind all of the Listerner to the fake one
      log(`logout from ${privateNamespace}`)

      clearMainEmitEvt(ee, privateNamespace)
      // we need to issue one more call to the server before we disconnect
      // now this is a catch 22, here we are not suppose to do anything platform specific
      // so that should fire before trigger this event
      // clear out the nsp
      nsps[privateNamespace] = null 
      // add a NOT LOGIN error if call
      notLoginWsListerner(privateNamespace, ee, opts)
    }
  )
}

