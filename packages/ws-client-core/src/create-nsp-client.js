/*
This two client is the final one that gets call 
all it does is to create the url that connect to 
and actually trigger the connection and return the socket 
therefore they are as generic as it can be
*/

/**
 * wrapper method to create a nsp without login
 * @param {string|boolean} namespace namespace url could be false
 * @param {object} opts configuration
 * @return {object} ws client instance
 */
function createNspClient(namespace, opts) {
  const { hostname, wssPath, nspClient, log } = opts
  const url = namespace ? [hostname, namespace].join('/') : wssPath
  log(`createNspClient with URL --> `, url)

  return nspClient(url, opts)
}

/**
 * wrapper method to create a nsp with token auth
 * @param {string} namespace namespace url
 * @param {object} opts configuration
 * @return {object} ws client instance
 */
function createNspAuthClient(namespace, opts) {
  const { hostname, wssPath, token, nspAuthClient, log } = opts
  const url = namespace ? [hostname, namespace].join('/') : wssPath
  
  log(`createNspAuthClient with URL -->`, url)

  if (token && typeof token !== 'string') {
    throw new Error(`Expect token to be string, but got ${token}`)
  }
  // now we need to get an extra options for framework specific method, which is not great
  // instead we just pass the entrie opts to the authClient 

  return nspAuthClient(url, opts, token)
}

export {
  createNspClient,
  createNspAuthClient
}
