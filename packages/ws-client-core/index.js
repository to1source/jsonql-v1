// This is the module entry point
import { 
  wsClientCore, 
  wsClientCoreAction 
} from './src/api'
import { 
  wsCoreCheckMap,
  wsCoreConstProps,

  checkSocketClientType,

  createCombineConfigCheck,
  postCheckInjectOpts,
  createRequiredParams
} from './src/options'
// these were in the share.js and now we combine into one
import * as jsonqlWsConstants from './src/options/constants'

import {
  namespaceEventListener,
  triggerNamespacesOnError, 
  handleNamespaceOnError
} from './src/listener'

import {
  fixWss,
  EventEmitterClass,
  getEventEmitter, 
  clearMainEmitEvt
} from './src/utils'
// new ping methods 
import {
  createInitPing, 
  extractPingResult,
  createIntercomPayload,
  extractSrvPayload,
  helloWorld,
  getHelloMsg
} from './src/callers'
// The onion skin cooker 
import {
  createCombineClient
} from './src/create-combine-client'
import {
  createNspClient,
  createNspAuthClient
} from './src/create-nsp-client'
import {
  prepareConnectConfig
} from '@jsonql/security'


// export 
export {
  // props 
  wsCoreCheckMap,
  wsCoreConstProps,
  jsonqlWsConstants,
  // generator methods
  wsClientCore,
  wsClientCoreAction,
  // helper methods
  
  checkSocketClientType,
  // this is for testing 
  createCombineConfigCheck, 
  postCheckInjectOpts,
  createRequiredParams,

  triggerNamespacesOnError,
  handleNamespaceOnError,

  fixWss,
  clearMainEmitEvt,
  getEventEmitter, 
  EventEmitterClass,

  namespaceEventListener,
  createCombineClient,

  createInitPing, 
  extractPingResult,
  createIntercomPayload,
  extractSrvPayload,
  
  helloWorld,
  getHelloMsg,

  createNspClient,
  createNspAuthClient,

  prepareConnectConfig
}
