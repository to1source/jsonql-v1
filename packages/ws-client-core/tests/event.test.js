// retest the nb-event-service here to try to figure out why some of the event not fired
const test = require('ava')
const NbEventService = require('@to1source/event')
const debug = require('debug')('jsonql-ws-client:test:event')
const { objDefineProps, injectToFn } = require('jsonql-utils')
const { getEventEmitter } = require('../src/utils/get-event-emitter')
// share methods between two test
const getter = function() {
  return function(...args) {
    return args.reduce((a, b) => a + b, 0)
  }
}

test.before(t => {
  t.context.evt = new NbEventService({logger: debug})
})

test(`Check why the ee not showing anything`, t => {
  const ee = getEventEmitter({ log: debug })
  ee.$on('something', value => {
    return value + 1
  })
  ee.$trigger('something', 1)
  t.is(ee.$done, 2)
})


test.cb(`It should able to trigger multiple callback`, t => {
  t.plan(3)
  let ctn = 0;
  const evt = t.context.evt
  evt.$on('something', function somethingOneHandler(value) {
    t.is(value, 1)
    debug(++ctn)
  })
  evt.$on('something', function somethingTwoHandler(value) {
    t.is(value, 1)
    debug(++ctn)
  })
  evt.$on('something', function somethingLastHandler(value) {
    debug(++ctn)
    t.is(value, 1)
    t.end()
  })
  evt.$trigger('something', 1)
})

test.cb(`Test the object getter to return a function to create a new on interface`, t => {
  t.plan(1)
    const setter = function(value) {
      console.log(value)
    }
    const getter = function() {
      return function(value, callback) {
        // console.log(value, callback)
        if (typeof callback === 'function') {
          callback(value)
        }
      }
    }
    let obj = {}
    obj = objDefineProps(obj, 'on', setter, getter)
    obj.on('yes', function cb(value) {
      t.is('yes', value, 'yes is the value')
      t.end()
    })
})
// 1
test(`Test another way to pass parameters to the getter then try to get a value back`, t => {
  const setter = function(value) {
    console.log(value)
  }
  let obj = {}
  obj = objDefineProps(obj, 'add', setter, getter)
  const result = obj.add(1,2)
  t.is(3, result, '1+2=3')
})

// 2
test(`Try to use the injectToFn to set the on as just a value getter`, t => {
  let obj = {}
  // The different is - we can directly add a function instead of func return func
  obj = injectToFn(obj, 'add', getter())

  const result = obj.add(1,1)

  t.is(2, result, '1+1=2')
})
