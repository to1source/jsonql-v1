// this will keep sending out message until received a terminate call
let timer
let ctn = 0
const debug = require('debug')('jsonql-ws-client:socket:continous')
/**
 * @param {string} msg a message
 * @return {string} a message with timestamp
 */
module.exports = function continuous(msg) {
  if (msg === 'terminate') {
    return clearInterval(timer)
  }
  // use the send setter instead
  timer = setInterval(() => {
    continuous.send = msg + ` [${++ctn}] ${Date.now()}`
  }, 1000)
  // return result
  return new Promise((resolver) => {
    resolver(`start at ${Date.now()}`)
  })
}
