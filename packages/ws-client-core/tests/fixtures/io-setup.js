// const Koa = require('koa');
// const bodyparser = require('koa-bodyparser')
const jsonqlWsServer = require('jsonql-ws-server')
const config = require('./contract-config')
const { join } = require('path')
const fsx = require('fs-extra')
const contract = fsx.readJsonSync(join(config.contractDir, 'contract.json'))
const debug = require('debug')('jsonql-ws-client:fixtures:io-setup')
const baseOptions = {
  serverType: 'socket.io',
  contract
}

module.exports = function(app, _config = {}) {
  const opts = Object.assign(baseOptions, config, _config)
  return new Promise(resolver => {
    jsonqlWsServer(opts, app)
      .then(io => {
        debug('setup completed')
        resolver({ app, io })
      })
  })
}
