// we create a fake ws client generator here 
// then run the whole thing and see what happen 
import debug from 'debug'
const log = debug('jsonql-ws-client:test:fake-client')
import { 
  ON_READY_FN_NAME,
  ON_LOGIN_FN_NAME
} from 'jsonql-constants'

function fakeWsClient(opts, nspMap, ee) {

  const { enableAuth } = opts 

  // log('\n/////////////////////fakeWsClient//////////////////\n')
  // log(obj)
  // log('\n///////////////////END fakeWsClient////////////////\n')
  // we fire the the onReady after 1/2 second 
  setTimeout(() => {
    if (enableAuth) {
      ee.$trigger(ON_LOGIN_FN_NAME, 'You are login')
    } else {
      ee.$trigger(ON_READY_FN_NAME, 'fake!')
    }
  }, 500)

  return { opts, nspMap, ee }
}

export { fakeWsClient, log }