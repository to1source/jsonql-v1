// testing the event, especially the switching to stimulate
// the pre-login / post-login situation
const test = require('ava')
const { join } = require('path')
const { JSONQL_PATH } = require('jsonql-constants')
const Event = require('@to1source/event')

const debug = require('debug')
const colors = require('colors/safe')

const logger = debug('jsonql-ws-client:test:evt:logger')
const localDebug = debug('jsonql-ws-client:test:evt')

const { chainPromises } = require('jsonql-utils')

const { basicClient, fullClient } = require('jsonql-ws-server/client') // require('../../ws-server/client')
const { helloWorld, getHelloMsg } = require('../src/callers/hello')
const { extractSrvPayload } = require('../index')

const wsServer = require('./fixtures/server-setup')
const createToken = require('./fixtures/token')

const port = 9012
const baseUrl = `ws://localhost:${port}/${JSONQL_PATH}`
const userdata = {name: 'Joel', id: 1}
const namespaces = ['public', 'private']

const asyncClient = (url, token) => Promise.resolve(basicClient(url, {}, token))

test.before(async t => {
  t.context.evt = new Event({ logger })

  const { app } = await wsServer({ 
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })

  t.context.server = app
  t.context.server.listen(port)

  t.context.token = createToken(userdata)

  t.context.clients = await chainPromises([
    asyncClient([baseUrl, namespaces[1]].join('/'), t.context.token),
    asyncClient([baseUrl, namespaces[0]].join('/'))
  ]).then(clients => clients
      .map((client, i) => {
        return {[namespaces[i]]: client}
      })
      .reduce((a, b) => Object.assign(a, b), {})
  )
})

test.after(t => {
  t.context.server.close()
})

test.serial.cb(`Test the chainPromises to create client`, t => {

  t.plan(2)

  const c1 = t.context.clients.public 
  const c2 = t.context.clients.private 

  c1.onopen = function() {
    t.pass()
  }

  c2.onopen = function() {
    t.pass()
    t.end()
  }

})

test.serial.cb(`Testing the fullClient with csrf`, t => {
  t.plan(4)
  const evt = t.context.evt
  let ctn = 0
  evt.$on('count', i => {
    ctn += i
    if (ctn > 3) {
      t.end()
    }
    return ctn 
  })

  const plus = () => evt.$trigger('count', 1)

  chainPromises(
    namespaces.map(namespace => (
      fullClient([baseUrl, namespace].join('/'), {}, t.context.token)
    ))
  ).then(clients => clients
      .map((client, i) => {
        return {[namespaces[i]]: client}
      })
      .reduce((a, b) => Object.assign(a, b), {})
    )
    .then(clients => {
      const c1 = clients.public 
      const c2 = clients.private 

      const d1 = p => c1.send(p)
      const d2 = p => c2.send(p)

      c1.onopen = function() {
        helloWorld(d1)
        plus()
        t.pass()
      }

      c2.onopen = function() {
        helloWorld(d2)
        plus()
        t.pass()
      }

      c1.onmessage = function(payload) {
        
        let msg = getHelloMsg(payload)
        localDebug('HELLO MESSAGE ----> ',  msg)

        t.pass()
        plus()
        
      }

      c2.onmessage = function(payload) {
        
        let msg = getHelloMsg(payload)
        localDebug('HELLO MESSAGE ----> ',  msg)

        t.pass()
        
        plus()
        
      }
    })

})

// test.todo(`Testing the switch over callback binding`)
