// mocking the client generator
// and only test the internal
// espcially debug the event system
const test = require('ava')
const { mockClient, log } = require('./fixtures/lib/mock-client')
const { createEvt } = require('../src/utils')
const {
  ON_ERROR_FN_NAME,
  ON_READY_FN_NAME,
  ON_LOGIN_FN_NAME,
  ON_RESULT_FN_NAME
} = require('jsonql-constants')
// const { createWsReply } = require('jsonql-utils')

test.before(async t => {
  t.context.client = await mockClient()
  t.context.client1 = await mockClient(true)
})

test.serial.cb(`We should able to get a list of event register via the eventEmitter`, t => {

  const client = t.context.client
  t.plan(2)

  //log('show client', client)
  log(client.verifyEventEmitter())

  t.truthy(client)

  // note it's one name onError that will listen to all the nsp errors
  client[ON_ERROR_FN_NAME] = function(err) {
    log(`OnError callback added`, err)
  }

  // there is only one onReady call now
  client[ON_READY_FN_NAME] = function(msg) {
    log(`onError callback added`, msg)
    t.pass()
    t.end()
    return msg.replace('!',' news!')
  }
})

test.serial.cb(`Should able to listen to the onLogin callback`, t => {
  
  t.plan(1)

  t.context.client1[ON_LOGIN_FN_NAME] = function(msg) {
    log(`onLogin callback`, msg)
    t.truthy(msg)
    t.end()
  }
})


test.serial.cb(`Should able to call the socket function`, t => {

  const resolverName = 'sendExtraMsg'

  const namespace = 'jsonql'

  const evt = createEvt(namespace, resolverName, ON_RESULT_FN_NAME)

  t.plan(1)

  const ee = t.context.client.eventEmitter

  // log('EventEmitter', ee)

  t.context.client[resolverName](1)
    .then(result => {
      t.truthy(result)
      t.end()
    })
    .catch(err => {
      log(err)
      t.end()
    })

  setTimeout(() => {

    ee.$trigger(evt, [{data: 'na na na'}])
  }, 50)

})