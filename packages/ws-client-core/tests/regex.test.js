// just testing some regex ideas
const test = require('ava')

const { fixWss } = require('../main')

test(`Should able to replace a http:// or HTTP:// with ws:// or wss://`, t => {

  const base = 'some-stupid-people-pass-this.com'

  const url = `http://${base}`
  const url1 = `https://${base}`
  const url2 = `HTTPS://${base}`

  const url3 = 'ws://this-is-a-good-url.com'

  const result = fixWss(url)

  t.is(result, `ws://${base}`)

  const result1 = fixWss(url1)

  t.is(result1, `wss://${base}`)

  const result2 = fixWss(url2)

  t.is(result2, `wss://${base}`)

  const result3 = fixWss(url3)

  t.is(result3, url3)
  
})