// testing the throw error problem
const test = require('ava')
const debug = require('debug')('jsonql-resolver:test:throw')
const {
  JsonqlResolverNotFoundError,
  JsonqlAuthorisationError,
  JsonqlValidationError,
  JsonqlResolverAppError,
  getErrorNameByInstance
  // UNKNOWN_ERROR
} = require('jsonql-errors')

const { join } = require('path')
const { executeResolver, getLocalValidator } = require('../')
const { createQuery } = require('jsonql-utils')
const readJson = require('./fixtures/read-json')

const contractPath = join(__dirname, 'fixtures', 'contract', 'contract.json')

test.before(async t => {
  t.context.contract = await readJson(contractPath)
  t.context.opts = {
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    contractDir: join(__dirname, 'fixtures', 'contract')
  }
})

test.cb(`When the resolver throw certain type of error, it should forward the same`, t => {
  t.plan(1)
  const resolverName = 'fakeLogin'
  const payload = createQuery(resolverName, ['jack'])
  executeResolver(
    t.context.opts,
    'query',
    resolverName,
    payload[resolverName],
    t.context.contract
  )
  .catch(e => {
    debug('catch the same', e)
    let errorName = getErrorNameByInstance([
      JsonqlResolverNotFoundError,
      JsonqlAuthorisationError,
      JsonqlValidationError,
      JsonqlResolverAppError
    ], e)
    debug('errorName', errorName)
    t.is(errorName, 'JsonqlAuthorisationError')
    t.end()
  })
})

test.cb(`When resolver throw error, it should able to throw back a JsonqlResolverAppError`, t => {
  t.plan(1)

  const resolverName = 'toFail'
  const payload = createQuery(resolverName, [])

  executeResolver(
    t.context.opts,
    'query',
    resolverName,
    payload[resolverName],
    t.context.contract
  )
  .catch(e => {
    debug(e)

    let errorName = getErrorNameByInstance([
      JsonqlResolverNotFoundError,
      JsonqlAuthorisationError,
      JsonqlValidationError,
      JsonqlResolverAppError
    ], e)

    t.is(errorName, 'JsonqlResolverAppError')
    t.end()

  })
})

test(`The getLocalValidator should throw a JsonqlResolverNotFoundError`, t => {

  const error = t.throws(() => {
    return getLocalValidator({
      validatorHandlerName: 'validator'
    }, t.context.contract)
  }, /* JsonqlResolverNotFoundError*/ null, 'There is no validator and should throw error')
  
  // debug('', error)
  t.is(error.className , 'JsonqlResolverNotFoundError')
})
