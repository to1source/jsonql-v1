// test the ES module import
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const debug = require('debug')('jsonql-resolver:test:es')
const contractApi = require('jsonql-contract')
// method to test
const { executeResolver } = require('../')
const { createQuery } = require('jsonql-utils')
// variables
const resolverDir = join(__dirname, 'fixtures', 'es')
const contractDir = join(__dirname, 'fixtures', 'contract', 'es')
// before test
test.before(async t => {
  t.context.opts = {
    resolverDir,
    contractDir,
    returnAs: 'json'
  }
  // generate contract file for the es
  t.context.contract = await contractApi(t.context.opts)
})
// after test
test.after(t => {
  fsx.removeSync(contractDir)
})

// start testing
test(`It should able to generate a contract for the es module`, t => {
  const contract = t.context.contract

  debug(contract)

  t.truthy(contract.query)
})

test.only(`It should able to use the requireEsModule to import ES module to commonjs code base`, async t => {
  const resolverName = 'getSomething';
  const payload = createQuery(resolverName, [])

  const result = await executeResolver(
    t.context.opts,
    'query',
    resolverName,
    payload[resolverName],
    t.context.contract
  )

  t.is('Hello there', result[0])

})
