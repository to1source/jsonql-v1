// this is for testing the inject client
const test = require('ava')
const { join } = require('path')
// const nodeClient = require('jsonql-node-client')
// const debug = require('debug')('jsonql-resolver:test:clients')
const anotherServer = require('./fixtures/another-server')
const { executeResolver } = require('../')
const readJson = require('./fixtures/read-json')
const { createQuery } = require('jsonql-utils')
const baseDir = join(__dirname, 'fixtures')
const { rmdirs } = require('./fixtures/rm')
const contractPath = join(baseDir, 'contract', 'contract.json')

test.before(async t => {
  const { stop, app } = anotherServer()
  t.context.stop = stop
  t.context.app = app
  t.context.clientConfig = {
    hostname: 'http://localhost:8001'
  }
  t.context.contract = await readJson(contractPath)
  t.context.opts = {
    resolverDir: join(baseDir, 'resolvers'),
    contractDir: join(baseDir, 'contract')
  }
})

test.after(t => {
  t.context.stop()
  // remove the client contract , because that's part of the test
  rmdirs(join(baseDir, 'contract', 'nodeClient0'))
})


test(`The callMsService resolver should able to call another jsonql service`, async t => {

  const resolverName = 'callMsService'
  const payload = createQuery(resolverName, ['Joel'])

  let opts = t.context.opts
  opts.clientConfig = [t.context.clientConfig]

  const result = await executeResolver(
    opts,
    'query',
    resolverName,
    payload[resolverName],
    t.context.contract
  )

  t.truthy(result.indexOf('another service'))

})
