// test a new combine resolver and associate methods 
const test = require('ava')
const {
  getCompleteSocketResolver,
  getCacheResolver,
  getCacheSocketInterceptor
} = require('../index')
const { isCacheObj, getCache } = require('@jsonql/security')
const debug = require('debug')('jsonql-resolver:test:combine')
const fsx = require('fs-extra')
const { join } = require('path')
const { injectToFn, objHasProp } = require('jsonql-utils')
const {
  LOGIN_EVENT_NAME,
  LOGIN_FN_NAME_PROP_KEY
} = require('jsonql-constants')


test.before(t => {
  t.context.cacheInstance = getCache()

  t.context.contract = fsx.readJsonSync(join(__dirname, 'fixtures', 'contract', 'contract.json'))
  t.context.socketContract = fsx.readJsonSync(join(__dirname, 'fixtures', 'contract', 'socket', 'contract.json'))
})

test(`Testing the basic cache function first`, t => {
  
  const cache = t.context.cacheInstance
  const result = cache.set('some-key', 'some-value')

  t.true(result)
  const check = isCacheObj(cache)

  t.true(check)
})

test(`Test the getCompleteSocketResolver method`, t => {
  const resolverName = 'sendSomething'
  const cacheStore = t.context.cacheInstance
  const contract = t.context.contract
  const args = [
    resolverName,
    { contract },
    resolverName, /// this is the key
    cacheStore
  ]

  const runInjectorsFn = Reflect.apply(getCompleteSocketResolver, null, args)

  const ts = Date.now()
  // part 2 add injector
  const injectors = [
    (fn, key, value) => {
      debug('main injector')
      // debug('pass fn with', fn, key, value)
      return injectToFn(fn, key, value, true)
    },
    (fn) => {
      debug('second injector')
      // debug('next pass with ', fn)
      return injectToFn(fn, 'timestamp', ts)
    },
    (fn) => {
      debug('final injector')

      return injectToFn(fn, 'whatever', 'some property')
    }
  ]

  const somekey = 'some-key'
  const someval = 'some-value'

  const resolver = runInjectorsFn(injectors, [somekey, someval])

  debug(somekey, objHasProp(resolver, somekey))

  debug(resolver('na na na'))

  t.truthy(objHasProp(resolver, somekey))
  t.is(resolver.timestamp, ts)

  t.true(typeof resolver === 'function', 'resovler should be a function')

  const resolver0 = runInjectorsFn(injectors, [somekey, someval]) // this should return the cache version
  const resolver1 = getCacheResolver(resolverName, t.context.cacheInstance)

  const msg0 = resolver0('la la la')

  debug('resolver0 result', msg0)

  const msg1 = resolver1('la la la')

  debug('resolver1 result', msg1)

  t.is(resolver1.timestamp, ts)

  t.true(typeof resolver1 === 'function', 'retrieve cache function')
  t.true(typeof resolver0 === 'function')
})


test.only(`testing the getSocketAuthInterceptor methods`, async t => {
  const store = getCache()
  const opts = { 
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    contract: t.context.socketContract, 
    [LOGIN_FN_NAME_PROP_KEY]: 'login'
  }

  const login1 = getCacheSocketInterceptor(LOGIN_EVENT_NAME, opts, LOGIN_EVENT_NAME, store)

  t.true(typeof login1 === 'function')

  // t.is(await login1('1','2'), '12')

  const login2 = getCacheSocketInterceptor(LOGIN_EVENT_NAME, opts, LOGIN_EVENT_NAME, store)

  t.true(typeof login2 === 'function')

})