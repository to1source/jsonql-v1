const test = require('ava')

const { join } = require('path')
const { CACHE_STORE_PROP_KEY } = require('jsonql-constants')
const { createQuery } = require('jsonql-utils')
const { getCache } = require('@jsonql/security')

const { executeResolver } = require('../')
const readJson = require('./fixtures/read-json')

const contractPath = join(__dirname, 'fixtures', 'contract', 'contract.json')

test.before(async t => {
  t.context.contract = await readJson(contractPath)
  t.context.opts = {
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    contractDir: join(__dirname, 'fixtures', 'contract'),
    [CACHE_STORE_PROP_KEY]: getCache() 
  }
})


test(`It should able to call a resolver`, async t => {
  const resolverName = 'getSomething'

  const payload = createQuery(resolverName, ['Joel'])
  
  const result = await executeResolver(
    t.context.opts,
    'query',
    resolverName,
    payload[resolverName],
    t.context.contract
  )

  t.is(result, `Hello Joel!`)

  // now call it again and observe the internal debug message 
  const payload1 = createQuery(resolverName, ['Davide'])
  const result1 = await executeResolver(
    t.context.opts,
    'query',
    resolverName,
    payload1[resolverName],
    t.context.contract
  )

  t.is(result1, 'Hello Davide!')

})
