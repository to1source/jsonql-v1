// this is the ms run on 8001
const serverIoCore = require('server-io-core')
const { jsonqlKoa } = require('jsonql-koa')
const { join } = require('path')
// export the return for use later
module.exports = () => serverIoCore({
  webroot: ['./dummy'],
  port: 8001,
  debugger: false,
  open: false,
  socket: false,
  middlewares: [
    jsonqlKoa({
      resolverDir: join(__dirname, 'resolvers'),
      contractDir: join(__dirname, 'contract', 'another')
    })
  ]
})
