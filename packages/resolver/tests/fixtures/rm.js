// from https://stackoverflow.com/questions/18052762/remove-directory-which-is-not-empty
const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const readdir = promisify(fs.readdir)
const rmdir = promisify(fs.rmdir)
const unlink = promisify(fs.unlink)

/*
exports.rmdirs = async function rmdirs(dir) {
  let entries = await readdir(dir, { withFileTypes: true });
  await Promise.all(entries.map(entry => {
    let fullPath = path.join(dir, entry.name);
    return entry.isDirectory() ? rmdirs(fullPath) : unlink(fullPath);
  }));
  await rmdir(dir);
};
*/

exports.rmdirs = async function rmdirs(dir) {
  let entries = await readdir(dir, { withFileTypes: true });
  let results = await Promise.all(entries.map(entry => {
    let fullPath = path.join(dir, entry.name)
    let task = entry.isDirectory() ? rmdirs(fullPath) : unlink(fullPath)
    return task.catch(error => ({ error }))
  }));
  results.forEach(result => {
    // Ignore missing files/directories; bail on other errors
    if (result && result.error.code !== 'ENOENT') throw result.error
  })
  await rmdir(dir)
}
