const debug = require('debug')('jsonql-resolver:call-ms-service')
/**
 * pass a txt value then process else where
 * @param {string} txt
 * @return {string} transformted txt
 */
module.exports = async function callMsService(txt) {
  // call another jsonql service
  const client0 = await callMsService.client()

  debug(client0)

  let txt0 = ['callMsService', txt].join(' ')
  return await client0.anotherService(txt0)
}
