const { JsonqlAuthorisationError } = require('jsonql-errors')

/**
 * Fake the login and throw a JsonqlAuthorisationError error to see if it catches it
 * @param {string} name username
 * @return {object} userdata
 */
module.exports = function fakeLogin(name) {
  if (name === 'john') {
    return {name: 'John Doe'}
  }
  throw new JsonqlAuthorisationError('fakeLogin', 'name not found!')
}
