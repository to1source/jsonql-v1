// this will get call from a different setup

/**
 * @param {string} msg input
 * @return {string} msg output
 */
module.exports = function anotherService(msg) {
  return msg + ' pass another service';
}
