// Try to cache the resolver using the node-cache
/* 
experiement using node-cache to store the result resolver 

several problems to note, 
1. this is only the first part of the getting the resolver, in each framework inject extra properties to it 
therefore simply cache this part is not enough 
2. We probably have to create a new method that takes in a list of processor (injectors) take the result fn 
 and additional params, run through the injector, then we can cache this resolver 

*/
const { isCacheObj } = require('@jsonql/security')
const { JsonqlError } = require('jsonql-errors')

/**
 * get it 
 * @param {*} key 
 * @param {*} store 
 * @return {*} false when not found
 */
function getCacheResolver(key, store) {
  if (key && store && isCacheObj(store)) {

    return store.has(key) ? store.get(key) : false 
  }

  return false 
}


/**
 * We don't init the store here, we take the store then re-use to fetch the cache resolver
 * @param {*} store store object
 * @return {function} method to find the resolver
 */
function cacheResolver(store) {
  if (!isCacheObj(store)) {
    throw new JsonqlError(`We expect the cache store to be JsonqlCache but instead we got ${typeof cache}`)
  }
  /**
   * Although we call it resolverName in the param but this will not just be the resolver name 
   * to avoid name collison
   * @param {string} key key to id this item 
   * @param {function} resolver function 
   * @return {function} resolver itself it found
   */
  return function cacheResolverAction(key, resolver) {  
    // the set return true on success, then we return the resolver for use next
    return store.set(key, resolver) ? resolver : false 
  }
}


module.exports = { 
  getCacheResolver, 
  cacheResolver
}