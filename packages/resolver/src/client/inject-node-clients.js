// injecting the clients into the resolver
const { injectToFn } = require('jsonql-utils')
const { CLIENT_PROP_NAME } = require('jsonql-constants')

const { getDebug } = require('../utils')
const debug = getDebug(`inject-node-clients`)
/**
 * kind of curry the function back to inject into the resolver
 * @param {array} clients the node clients
 * @return {function} to the end resolver to use
 */
function resolveClients(clients) {
  /**
   * When pass as number then check this index, if its name then
   * find with the name property, all failed then return false
   * @param {number|string} name to id the client could be undefined when there is only one
   * @return {boolean|object} false when failed
   */
  return function(name) {
    debug(`resolveClients ${name}`, clients)

    if ((name === undefined || name === null) && clients.length === 1) {
    
      return clients[0]
    } else if (typeof name === 'string') {
    
      return clients
        .filter(client => client.name === name)
        .reduce((base, result) => {
    
          return result || base
        }, false)
    } else if (clients[name]) {
    
      return clients[name]
    }
    return false  
  }
}

/**
 * inject the clients to the resolver
 * @param {function} resolver the resolver to get injection
 * @param {array} clients the jsonql node clients
 * @return {function} the injected resolver
 */
function injectNodeClient(resolver, clients) {
  return injectToFn(resolver, CLIENT_PROP_NAME, resolveClients(clients))
}

module.exports = { injectNodeClient }
