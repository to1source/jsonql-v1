// @TODO I don't really want to add more dependencies to this package
// if they are not using it then is it possible to include this as
// peer dependencies?
const jsonqlNodeClient = require('jsonql-node-client')
const { injectToFn } = require('jsonql-utils')
const NAME_PROP = 'name'
/**
 * create the node client using the already passed configurations
 * @param {object} clientConfigs the clients configuration
 * @return {promise} resolve to the node client
 */
function clientsGenerator(clientConfigs) {
  return Promise.all(
    clientConfigs.map(clientConfig => {
      let name = clientConfig.name
      // @TODO we need to cache it somewhere 
      return jsonqlNodeClient(clientConfig)
        .then(client => injectToFn(client, NAME_PROP, name))
    })
  )
}

module.exports = { clientsGenerator }
