// this will be a separate extened methods for the socket to get the interceptors
// login, logout, disconnect, validator etc
////////////////////////////////////////////
//            SOCKET AUTH                 //
////////////////////////////////////////////

// const fsx = require('fs-extra')
const {
  SOCKET_AUTH_NAME,
  LOGIN_EVENT_NAME,
  SA_LOGIN_EVENT_NAME,
  LOGOUT_EVENT_NAME,
  DISCONNECT_EVENT_NAME,
  VALIDATOR_FN_NAME,
  LOGIN_FN_NAME_PROP_KEY,
  LOGOUT_FN_NAME_PROP_KEY,
  DISCONNECT_FN_NAME_PROP_KEY
  // RESOLVER_DIR_PROP_KEY
} = require('jsonql-constants')
const { JsonqlResolverAppError } = require('jsonql-errors')
const { getPathToFn } = require('jsonql-utils')
const { validateAsync } = require('jsonql-params-validator')

const { getCacheResolver, cacheResolver } = require('./cache')
const { getFnBySourceType } = require('./search-resolvers')
const { isFunc } = require('./utils')

const debug = require('debug')('jsonql-resolver:get-socket-interceptor')

/**
 * Another wrapper method to make less duplication
 * to find the file part from the contract
 * @param {object} searchPart
 * @param {object} opts
 * @return {string} the file part
 */
function getInterceptorFileFn(opts) {
  const { contract } = opts 
  const socketAuthContract = contract[SOCKET_AUTH_NAME] || {}
  /**
   * @param {string} fnName
   * @return {string} the file part
   */
  return function(name) {
    const resolverName = opts[name]
    const params = socketAuthContract[resolverName]
    const file = getPathToFn(resolverName, SOCKET_AUTH_NAME, opts)

    debug('[getInterceptorFileFn]', name, resolverName, file, params)

    return { 
      params: params && params.params ? params.params : null, 
      file 
    }
  }
}

/**
 * Construct the search path to find interceptors
 * This is actually a fall back option if the contract didn't provide it
 * @param {string} evtName type of event
 * @param {object} opts configuration
 * @return {object} the interceptor info from contract
 */
function getInterceptor(evtName, opts) {
  const getFileFn = getInterceptorFileFn(opts)

  switch(evtName) {
    case LOGIN_EVENT_NAME:
    case SA_LOGIN_EVENT_NAME:
      return getFileFn(LOGIN_FN_NAME_PROP_KEY)
    case LOGOUT_EVENT_NAME:
      return getFileFn(LOGOUT_FN_NAME_PROP_KEY)
    /* // not implement at the moment @1.2.0
    case CONNECTED_EVENT_NAME:
      return getFileFn(INIT_CONNECTION_FN_NAME_PROP_KEY)
    case SWITCH_USER_EVENT_NAME:
      return getFileFn(SWITCH_USER_FN_NAME_PROP_KEY)
    */
    // @TBC this should be an internal event handler
    case DISCONNECT_EVENT_NAME:
      return getFileFn(DISCONNECT_FN_NAME_PROP_KEY)

    // this one is a special case because it's an internal method
    case VALIDATOR_FN_NAME:
      return getFileFn(VALIDATOR_FN_NAME)
    default:
      // @TODO this feature is not going to implement until a few versions later
      throw new Error(`"${evtName}" is not implement!`)
  }
}


/**
 * For socket to add auth interceptors
 * @param {string} evtName of the interceptor
 * @param {object} opts configuration
 * @return {function} the interceptor method to handle the callback
 */
function getSocketAuthInterceptorFn(evtName, opts) {
  let { params, file } = getInterceptor(evtName, opts)
  if (params && file) {
    const { sourceType } = opts.contract
    const fn = getFnBySourceType(file, sourceType)

    return [ fn, params ]
  }
  
  return false
}

/**
 * Putting all the socket interceptor method together in one
 * @param {string} evtName event name
 * @param {object} opts configuration
 * @return {function} to execute and get validate when pass arguments
 */
function getSocketInterceptor(evtName, opts) {
  const result = getSocketAuthInterceptorFn(evtName, opts)
  if (result) {
    const [ fn, params ] = result
    /**
     * @param {array} args
     */
    return (...args) => {

      return validateAsync(args, params)
        .then(_args => {
          try {
            return Reflect.apply(fn, null, _args)
          } catch(e) {
            throw new JsonqlResolverAppError(evtName, e)
          }
        })
    }
  }
  // if it's not found then we just carry on, its not important
  debug('[getSocketInterceptor]', `${evtName} interceptor not found!`)

  return false 
}

/**
 * Get the interceptor, cache it then call it again to return the cache version
 * @param {string} evtName event name for the interceptor  
 * @param {object} opts configuration 
 * @param {string} key for storing the interceptor 
 * @param {object} store the cache store instance
 * @return {function} the actual interceptor function
 */
function getCacheSocketInterceptor(evtName, opts, key, store) {
  let resolverFn = getCacheResolver(key, store)
  if (resolverFn && isFunc(resolverFn)) {
    debug(`[getCacheSocketInterceptor] return from cache with key: ${key}`)

    return resolverFn
  }
  resolverFn = getSocketInterceptor(evtName, opts)
  if (resolverFn !== false) {
  
    return cacheResolver(store)(key, resolverFn)
  }

  return false
}

module.exports = {
  getSocketInterceptor,
  getSocketAuthInterceptorFn,
  getCacheSocketInterceptor
}
