// combine-resolver with cache and injectors
const { SOCKET_NAME } = require('jsonql-constants')
const { chainFns } = require('jsonql-utils')

const { getCacheResolver, cacheResolver } = require('./cache')
const { getResolverAction } = require('./resolve-methods')

const debug = require('debug')('jsonql-resolver:combine-resolver')
const colors = require('colors/safe')

/** 
 * this is a new feature that allow
 * the framework to pass a list of injectors, and extra props 
 * then run through this to get the final injectors
 * 
 * First we create one for the socket callback methods, because they don't hook into the rendering system 
 * 
 * 1. pass the original params that the retrieve resolver will get 
 * 2. pass the array of extra injectors, with their require parameters but leave out the resolver 
 * 3. then run its to get a complete resolver
 * 4. With a additonal key property to id this resolver, and try to get from the cache first
 * @param {string} resolverName name of the resolver
 * @param {object} opts configuration
 * @param {*} key to id the resolver
 * @param {*} store instance of the nodeCache  
 * @return {function} the injectors chain for adding extra props to the fn 
 */
function getCompleteSocketResolver(resolverName, opts, key = null, store = null) {
  // the contract is within the opts 
  const { contract } = opts 
  
  /**
   * @param {array<function>} injectors to run through the injection
   * @param {array<*>} params then we add the resolver at the end to pass through the injectors
   * @return {function} the complete resolver
   */
  return function prepareResolver(injectors, params) {
    // it should be here otherwise, we call the prepare method will just keep injecting
    debug(`start getCompleteSocketResolver`, colors.white.bgRed(key), typeof store)

    let resolver = getCacheResolver(key, store)  
    if (resolver !== false) {
      debug('return the cache version with key', colors.bgMagenta(key), resolver)

      return resolver
    }
    
    resolver = getResolverAction(resolverName, SOCKET_NAME, contract, opts)
    
    debug(`return resolver using getResolverAction`, resolverName, typeof resolver)

    const executor = Reflect.apply(chainFns, null, injectors)
    // please note the final injector must return just the resolver 
    const resolverFn = Reflect.apply(executor, null, [resolver, ...params])

    if (key && store) {
      const cacherFn = cacheResolver(store)
      debug(`caching the resolver using`, key)

      return cacherFn(key, resolver)
    }
    
    return resolverFn 
  }
}


module.exports = { getCompleteSocketResolver }