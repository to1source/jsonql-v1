// validation wrapper
const {
  JsonqlValidationError,
  JsonqlResolverAppError,
  JsonqlAuthorisationError
} = require('jsonql-errors')
const { AUTH_TYPE, HSA_ALGO, RSA_ALGO } = require('jsonql-constants')
const { validateSync, isString } = require('jsonql-params-validator')
const { extractParamsFromContract } = require('jsonql-utils')
const { loginResultToJwt } = require('@jsonql/security')

const { getDebug } = require('./utils')
const debug = getDebug('validate-and-call')
// for caching @TODO should this be more a protective way to cache
var resultMethod

/**
 * get the encode method also cache it
 * @param {object} opts configuration
 * @return {function} encode method
 */
const getEncodeJwtMethod = opts => {
  if (resultMethod && typeof resultMethod === 'function') {
    return resultMethod
  }
  let key = isString(opts.useJwt) ? opts.useJwt : opts.privateKey
  let alg = isString(opts.useJwt) ? HSA_ALGO : RSA_ALGO
  // add jwtTokenOption for the extra configuration for generate token
  return loginResultToJwt(key, opts.jwtTokenOption, alg)
}

/**
 * This will hijack some of the function for the auth type
 * @param {string} type of resolver we only after the auth type
 * @param {string} name of the resolver function
 * @param {object} opts configuration
 * @return {*} encoded jwt 
 */
const applyJwtMethod = (type, name, opts) => {
  return result => {
    if (type === AUTH_TYPE && name === opts.loginHandlerName && opts.enableAuth) {
      return getEncodeJwtMethod(opts)(result)
    }
    return result
  }
}

/**
 * Break out from the method below, to seperate the process of running the resolver
 * @param {string} name the resolver name for report error
 * @param {function} fn the resolver function
 * @param {array} args the argument
 * @return {promise} resolve when success
 */
function runResolver(name, fn, args) {
  // @NOTE here is the problem
  return new Promise((resolver, rejecter) => {
    try {
      resolver(Reflect.apply(fn, null, args))
    } catch(e) {
      debug('runResolver throw error', e)
      switch (true) {
        case e instanceof JsonqlAuthorisationError:
          rejecter(new JsonqlAuthorisationError(name, e))
        break;
        default:
          rejecter(new JsonqlResolverAppError(name, e))
      }
    }
  })
}

/**
 * @TODO we should cut down the parameters here, the opts and contract should merge
 * also the type, name and args should merge together as one input for the fn
 * Main method to replace the fn.apply call inside the core method
 * @param {function} fn the resolver to get execute
 * @param {array} args the argument list
 * @param {object} contract the full contract.json
 * @param {string} type query | mutation
 * @param {string} name of the function
 * @param {object} opts configuration option to use in the future
 * @return {object} now return a promise that resolve whatever the resolver is going to return and packed
 */
function validateAndCall(fn, args, contract, type, name, opts) {
  const { params } = extractParamsFromContract(contract, type, name)
  let errors = validateSync(args, params)
  if (errors.length) {
    debug(`validation failed on client side`, args, params)
    throw new JsonqlValidationError(name, errors)
  }

  return runResolver(name, fn, args)
          .then(applyJwtMethod(type, name, opts, contract))
}

module.exports = { validateAndCall }
