// The basic test copy from main.test.js

QUnit.test('jsonqlClient should able to connect to server', function(assert) {
  var done1 = assert.async()
  var done2 = assert.async()

  jsonqlClient({
      hostname: 'http://localhost:8081',
      showContractDesc: true,
      keepContract: false,
      debugOn: false
    })
    .then(function(client) {

    client.query.helloWorld().then(function(result) {
      assert.equal('Hello world!', result, "Hello world test done")

      done1()
    })

    client.query.getSomething(1).catch(err => {
      assert.equal(err.className, 'JsonqlValidationError', 'Expect validation error')
      done2()
    })

  })

})
