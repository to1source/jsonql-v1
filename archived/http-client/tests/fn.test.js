const test = require('ava')

const { isObjectHasKey } = require('jsonql-params-validator')


test('It should have isObjectHasKey exported', t => {
  const obj = {query: false, mutation: null, auth: true}

  t.true(isObjectHasKey(obj, 'query'))
  t.true(isObjectHasKey(obj, 'mutation'))

  t.false(isObjectHasKey(obj, 'socket'))

})
