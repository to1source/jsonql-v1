// this is testing some of the theory

const obj = {};

const fn = () => {
  console.info('Should call somebody else');
}


Object.defineProperty(obj, 'key', {
  get() {
    return obj.key;
  }
  set(newValue) {
    obj.key = newValue;
  },
  enumerable: true,
  configurable: true
});
