const contractApi = require('../../../contract-cli');
const { join } = require('path');

contractApi({
  inDir: join(__dirname, 'resolvers'),
  outDir: join(__dirname, 'contracts', 'tmp'),
  useDoc: true,
  logDirectory: join(__dirname, '..','..' , '..', 'contract-cli','tests','fixtures' ,'tmp', 'contracts', 'logs')
});
