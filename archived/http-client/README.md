# @jsonql/http-client

> This is jsonql http client for javascript, previously release as jsonql-client

## How to use it

First this version **DOES NOT** distribute with the [Flyio](https://github.com/wendux/fly) module. You have to explicitly import it yourself.
The reason is - we intend to support as many platform as possible, and Fly allow you to do just that, check their
[documentation](https://github.com/wendux/fly) for more information.

```js
import Fly from 'flyio'
import jsonqlClient from '@jsonql/http-client'

const client = jsonqlClient(Fly, config)

client.then(c => {
  c.query.helloWorld()
    .then(msg => {
      // should be a Hello World message
    })
})

```

### Pass contract via config gives you the client directly

If you pass the `contract` via the config, then you don't need to call the end

```js
import contract from 'some/where/contract.json'
import Fly from 'flyio'
import jsonqlClient from '@jsonql/http-client'

let config = { contract }
const client = jsonqlClient(Fly, config)

client.query.helloWorld()
  .then(msg => {
    // Hello world
  })
```

### Using a static client

There is a different style client that you can use:

```js


```



Please consult [jsonql.org](https:jsonql.js.org) for more information.

---

NB + T1S
