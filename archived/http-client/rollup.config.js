/**
 * Rollup config for building the slim version
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'
import { terser } from "rollup-plugin-terser"
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'
import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
import async from 'rollup-plugin-async'

import pkg from './package.json'

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    preferBuiltins: true,
    mainFields: ['module', 'browser']
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__VERSION__': pkg.version
  })
]



let globals = {
  'debug': 'debug',
  'promise-polyfill': 'Promise',
  'flyio': 'Fly'
}
let external =  [
  'flyio',
  'debug',
  'fetch',
  'Promise',
  'promise-polyfill',
  'superagent',
  'handlebars',
  'tty'
]
let moduleName = 'jsonqlClient'
let sourceFile = 'index.js'
let distFile = 'core.js'
switch (env) {
  case 'BROWSER':
    sourceFile = 'full.js'
    distFile = join('dist', 'jsonql-client.umd.js')
  break;
  case 'STATIC':
    moduleName = 'jsonqlClientStatic'
    sourceFile = join('src', 'static.js')
    distFile = 'static.js'
  break;
  case 'FULL':
    moduleName = 'jsonqlClientStatic'
    sourceFile = join('src', 'static-full.js')
    distFile = join('dist', 'jsonql-client.static.js')
  break;
  default:
    sourceFile = 'index.js'

}
plugins.push(terser())
plugins.push(size())

let config = {
  input: join(__dirname, sourceFile),
  output: {
    name: moduleName,
    file: join(__dirname, distFile),
    format: 'umd',
    sourcemap: true,
    globals
  },
  plugins,
  external
}

export default config
