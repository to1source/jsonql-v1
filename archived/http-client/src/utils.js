// take only the module part which is what we use here
// and export it again to use through out the client
// this way we avoid those that we don't want node.js module got build into the code
import {
    createEvt,

    createQuery,
    createMutation,
    getNameFromPayload,
    cacheBurst,
    urlParams,
    resultHandler,

    isContract,
    timestamp,
    inArray
} from './jsonql-utils' // this should point to the module.js
/**
 * @param {object} jsonqlInstance the init instance of jsonql client
 * @param {object} contract the static contract
 * @return {object} contract may be from server
 */
const getContractFromConfig = function(jsonqlInstance, contract = {}) {
  if (isContract(contract)) {
    return Promise.resolve(contract)
  }
  return jsonqlInstance.getContract()
}

// export some constants as well
// since it's only use here there is no point of adding it to the constants module
// or may be we add it back later
const ENDPOINT_TABLE = 'endpoint';
const USERDATA_TABLE = 'userdata';

// export
export {
  getContractFromConfig,
  ENDPOINT_TABLE,
  USERDATA_TABLE,
  createEvt,

  createQuery,
  createMutation,
  getNameFromPayload,
  cacheBurst,
  urlParams,
  resultHandler,

  isContract,
  timestamp,
  inArray
}
