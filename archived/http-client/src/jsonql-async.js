// this is new for the flyio and normalize the name from now on
import JsonqlBaseClient from './base'
import generator from './core/jsonql-api-generator'
import { checkOptionsAsync } from './options'
import { getContractFromConfig } from './utils'

/**
 * Main interface for jsonql fetch api
 * @param {object} config
 * @param {object} Fly this is really pain in the backside ... long story
 * @return {object} jsonql client
 */
export default function(ee, config = {}, Fly = null) {
  return checkOptionsAsync(config)
    .then(opts => (
      {
        baseClient: new JsonqlBaseClient(opts, Fly),
        opts: opts
      }
    ))
    .then( ({baseClient, opts}) => (
      getContractFromConfig(baseClient, opts.contract)
        .then(contract => generator(baseClient, opts, contract, ee)
        )
      )
    )
}
