// we must ensure the user passing the correct options
// therefore we need to validate against the properties as well

import { appProps, constProps } from './base-options'
import { checkConfigAsync } from 'jsonql-params-validator'

export default function checkOptionsAsync(config) {
  let { contract } = config;
  return checkConfigAsync(config, appProps, constProps)
    .then(opts => {
      opts.contract = contract;
      return opts;
    })
}
