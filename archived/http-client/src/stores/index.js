// export store interface
import localStoreEngine from './local-store'
import sessionStoreEngine from './session-store'

// export back the raw version for development purposes
export const localStore = localStoreEngine
export const sessionStore = sessionStoreEngine
