// this one will use the esm module

// main export interface

// @2019-05-09 new interface export with Fetch
import { jsonqlAsync, jsonqlSync, ee as getEventEmitter } from './src'
import { isContract } from './src/utils'

/**
 * When pass a static contract then it return a static interface
 * otherwise it will become the async interface
 * @param {object} Fly the http engine
 * @param {object} config configuration
 * @return {object} jsonqlClient
 */
export default function jsonqlClient(Fly, config) {
  const ee = getEventEmitter(config.debugOn)
  if (config.contract && isContract(config.contract)) {
    return jsonqlSync(ee, config, Fly)
  }
  return jsonqlAsync(ee, config, Fly)
}
