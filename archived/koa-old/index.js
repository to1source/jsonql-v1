/**
 * This is the main interface to export the middleware(s)
 * we will take the config here and export array of middleware using koa-compose
 */
import fs  from 'fs'
import { merge }  from 'lodash'
import compose  from 'koa-compose'
import {
  coreMiddleware,
  authMiddleware,
  contractMiddleware,
  helloMiddleware,
  consoleMiddleware,
  publicMethodMiddleware,
  initMiddleware
}  from './src/middlewares'
import { configCheck } from './src/options'
import { getDebug }  from './src/utils'
const debug = getDebug('main')
// main
function jsonqlKoa(config = {}) {
  // first check the config
  const opts = configCheck(config)
  debug('[jsonql-koa] init opts', opts)
  // export
  let middlewares = [
    initMiddleware(opts),
    helloMiddleware(opts),
    contractMiddleware(opts)
  ]
  // only available when enable it
  if (config.enableWebConsole) {
    middlewares.push(consoleMiddleware(opts))
  }

  if (opts.enableAuth) {
    middlewares.push(
      publicMethodMiddleware(opts),
      authMiddleware(opts)
    )
  }
  middlewares.push(
    coreMiddleware(opts)
  )
  // finally
  return compose(middlewares)
}

export {
  jsonqlKoa
}
