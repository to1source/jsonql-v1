// testing the config
const test = require('ava')
const fsx = require('fs-extra')
const { join, resolve } = require('path')

const { configCheck } = require('../src/options')
const processJwtKeysDefault = require('../src/options/process-jwt-keys')
const processJwtKeys = processJwtKeysDefault.default

const { jwtProcessKey } = require('../src/options/options')

const debug = require('debug')('jsonql-koa:test:config')
const resolverDir = join(__dirname, 'fixtures', 'resolvers')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'config-test')
const keysDir = join(__dirname, 'fixtures', 'tmp', 'keys')

// mocking a ctx object
let ctx = {
  state: {
    jsonql: {
      setter: () => {},
      getter: () => {}
    }
  }
}

test.after( t => {
  // fsx.removeSync(keysDir)
})

test('It should able to check the in dir', t => {
  const opts = configCheck({
    resolverDir,
    contractDir
  })

  const dir = resolve(resolverDir)
  t.is( dir, opts.resolverDir )
});

test('It should have privateKey and publicKey when set useJwt = true', async t => {
  let opts = configCheck({
    enableAuth: true,
    useJwt: true,
    resolverDir,
    contractDir,
    keysDir
  })

  opts = await processJwtKeys(ctx, opts)

  t.truthy(opts.privateKey && opts.publicKey)

})
