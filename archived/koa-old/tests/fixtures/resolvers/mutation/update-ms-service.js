const debug = require('debug')('jsonql-koa:mutation:update-ms-service')
/**
 * this will be calling a microserivce setup using the nodeClient
 * @param {string} payload incoming
 * @return {string} msg return from nodeClient
 */
module.exports = async function updateMsService(payload) {
  const client = await updateMsService.client()

  debug(client)

  return client.query.subMsService(payload)
}
