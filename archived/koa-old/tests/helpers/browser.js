// browser test the web console

const serverIoCore = require('server-io-core')
const { join } = require('path')
const { jsonqlKoa } = require('../../main')

const baseDir = join(__dirname, '..', 'fixtures')

serverIoCore({
  webroot: [
    join(baseDir, 'tmp', 'browser')
  ],
  middlewares: [
    jsonqlKoa({
      resolverDir: join(baseDir, 'resolvers'),
      contractDir: join(baseDir, 'tmp', 'browser'),
      enableWebConsole: true
    })
  ]
})
