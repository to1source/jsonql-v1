// lib main export
const {

  chainFns,

  inArray,
  getDebug,

  headerParser,
  getDocLen,
  packResult,
  printError,
  forbiddenHandler,
  ctxErrorHandler,

  isJsonqlPath,
  isJsonqlRequest,
  isJsonqlConsoleUrl,

  getCallMethod,
  isHeaderPresent,

  isObject,
  isNotEmpty,
  isContractJson,
  handleOutput,
  extractArgsFromPayload,
  getNameFromPayload,

  extractParamsFromContract
} = require('./utils')

const getContract = require('./contract-generator')
const processJwtKeys = require('./config-check/process-jwt-keys')

const { createTokenValidator } = require('jsonql-jwt')
// update on v1.3.7
const { resolveMethod } = require('jsonql-resolver')
// export
module.exports = {
  chainFns,
  inArray,
  getDebug,

  isObject,
  headerParser,
  getDocLen,
  packResult,
  printError,
  forbiddenHandler,
  ctxErrorHandler,
  // v1.3.8
  resolveMethod,

  isJsonqlPath,
  isJsonqlRequest,
  isJsonqlConsoleUrl,
  getCallMethod,
  isHeaderPresent,
  isNotEmpty,
  isContractJson,
  handleOutput,
  extractArgsFromPayload,

  getContract,

  getNameFromPayload,
  extractParamsFromContract,

  processJwtKeys,

  createTokenValidator
}
