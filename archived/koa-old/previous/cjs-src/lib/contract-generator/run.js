// /lib/contract-generator/run.js
const { contractGenerator, readContract } = require('./contract-generator')
// listening
process.on('message', m => {
  const { config, pub } = m;
  if (config) {
    let contract = readContract(config.contractDir, pub)
    if (contract !== false) {
      return process.send({ contract })
    }
    contractGenerator(config, pub)
      .then(contract => {
        process.send({ contract })
      })
      .catch(error => {
        process.send({ error })
      })
  }
})
