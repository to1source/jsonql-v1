// main export interface
const configCheck = require('./lib/config-check')
const authMiddleware = require('./auth-middleware')
const coreMiddleware = require('./core-middleware')
const contractMiddleware = require('./contract-middleware')
const helloMiddleware = require('./hello-middleware')


const consoleMiddleware = require('./console-middleware')
const publicMethodMiddleware = require('./public-method-middleware')
const errorsHandlerMiddleware = require('./errors-handler-middleware')
const initMiddleware = require('./init-middleware')

// export
module.exports = {
  configCheck,

  consoleMiddleware,
  authMiddleware,
  coreMiddleware,
  contractMiddleware,
  helloMiddleware,
  
  publicMethodMiddleware,
  errorsHandlerMiddleware,
  initMiddleware
}
