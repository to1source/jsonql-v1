const test = require('ava')

const { SHOW_CONTRACT_DESC_PARAM } = require('jsonql-constants')
const superkoa = require('superkoa')
const { join } = require('path')
const debug = require('debug')('jsonql-koa:test:koa')
const { type, headers, dirs, returns } = require('./fixtures/options')
const fsx = require('fs-extra')

const { resolverDir, contractDir } = dirs;

const createServer = require('./helpers/server')
const dir = 'standard';

const makeServer = function() {
  return new Promise(resolver => {
    const app = createServer({}, dir)
    setTimeout(() => {
      resolver(app)
    }, 5000)
  })
}

test.before( async (t) => {
  t.context.app = await makeServer()
})

test.after( () => {
  // remove the files after
  fsx.removeSync(join(contractDir, dir))
})

// start test
// @1.3.4 test if there is a public / private contract generated
test('It should create a contract and a public contract file at the same time', t => {

  const cd = join(contractDir, dir)

  t.truthy( fsx.existsSync(join(cd, 'contract.json')) )
  t.truthy( fsx.existsSync(join(cd, 'public-contract.json')) )

})

test("Hello world test", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      helloWorld: {
        args: []
      }
    });
  // debug(res.body)
  t.is(200, res.status)
  t.is('Hello world!', res.body.data)
})

test("It should return a public contract with helloWorld", async (t) => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .set(headers)
  let contract = res.body.data;

  t.is(200, res.status)

  t.deepEqual(returns, contract.query.helloWorld.returns)
  // it should not have a description field
  t.falsy(contract.query.helloWorld.description)
})


// start test
test('It should return json object',async (t) => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      testList: {
        args: [2]
      }
    });
  t.is(200, res.status)
  t.is(1, res.body.data.num)
})

test('It should change the json object', async(t) => {
  let res = await superkoa(t.context.app)
    .put('/jsonql')
    .set(headers)
    .send({
      updateList: {
        payload: {
          user: 1
        },
        condition: {
          where: 'nothing'
        }
      }
    });
  t.is(200, res.status)
  t.is(2, res.body.data.user)
})

// test the web console
test("It should able see a dummy web console page", async t => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
  t.is(200, res.status)
})

// for some reason if I run this as standalone then it works but run with other with
// the new test with both contract created failed
test.only("It should return a contract file without the description field", async t => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .query(SHOW_CONTRACT_DESC_PARAM)
    .set(headers)
  let contract = res.body.data;

  debug(SHOW_CONTRACT_DESC_PARAM, contract.query)

  t.truthy(contract.query.helloWorld.description)

})
