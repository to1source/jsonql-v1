// we test all the fail scenario here
const test = require('ava')
const superkoa = require('superkoa')
const { join } = require('path')
const debug = require('debug')('jsonql-koa:test:fail')
const jsonqlMiddleware = require(join(__dirname, '..', 'index'))
const { type, headers, dirs } = require('./fixtures/options')
const fsx = require('fs-extra')

const createServer = require('./helpers/server')

test.before((t) => {
  t.context.app = createServer({}, 'failed');
})

test.after( () => {
  // remove the files after
  fsx.removeSync(join(dirs.contractDir, 'failed'))
})

test("Should fail this Hello world test", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql/somethingelse?_cb=9082390483204830')
    .set(headers)
    .send({
      helloWorld: {
        args: {}
      }
    })
  t.is(404, res.status)

})
