// separate test for the contract interface
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const generator = require('../src/lib/contract-generator')
const { isContractJson } = require('../src/lib')
const debug = require('debug')('jsonql-koa:test:gen')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'generator')
const resolverDir = join(__dirname, 'fixtures', 'resolvers')

test.before(t => {
  t.context.initContract = generator({
    contractDir,
    resolverDir,
    returnAs: 'json'
  })
})

test.after( t => {
  fsx.removeSync(contractDir)
})

test('It should able to generate a contract file', async t => {
  // this way can test if this setup works for the middleware as well
  const contract = await t.context.initContract;
  debug(contract)
  t.truthy(isContractJson(contract))
})
