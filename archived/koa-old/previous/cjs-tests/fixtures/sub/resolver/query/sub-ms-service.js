const debug = require('debug')('jsonql-koa:query:ms-service')
/**
 * @param {string} msg incoming message
 * @return {string} out going message
 */
module.exports = function subMsService(msg) {

  debug('msg', msg)

  return msg + ' ms service';
}
