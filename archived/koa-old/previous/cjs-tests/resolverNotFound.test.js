// this one will test the resolver not found and the application error

const test = require('ava')
const createServer = require('./helpers/server')
const superkoa = require('superkoa')
const { headers } = require('./fixtures/options')
const debug = require('debug')('jsonql-koa:test:ResolverNotFoundError')
const fsx = require('fs-extra')
const { join } = require('path')

test.before( t => {
  t.context.app = createServer({}, 'notfound')
})

test.after( t => {
  fsx.removeSync(join(__dirname, 'fixtures', 'tmp', 'notfound'))
})

test('it should throw a ResolverNotFoundError', async (t) => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      helloWorld2: {
        args: []
      }
    })

  debug(res.body)

  t.is(res.body.error.statusCode, 404)

})

test("It should cause an Application error but nothing throw", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      causeError: {
        args: [1]
      }
    })

  debug(res.body)

  t.is(true, res.status ===  200)

})
