// hope this will solve the missing contract of what not problem once and for all
import nodeCache from 'node-cache'
import { getDebug } from './utils'

const cache = new nodeCache()
const debug = getDebug('node-cache')

// @TODO perhaps we should have an extra key if it's presented

/**
 * @param {string} key to id
 * @param {*} value value to store
 * @return {*} value on success
 */
const setter = (key, value) => cache.set(key, value) ? value : false;

/**
 * throw error if the data is missing
 * @param {string} key to id
 * @return {*} value or throw error if missing
 */
const getter = key => {
  debug(key, ' read from cache')
  return cache.get(key)
}

export {
  setter,
  getter
}
